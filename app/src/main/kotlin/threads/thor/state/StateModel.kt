package threads.thor.state

import android.Manifest
import android.app.Application
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.net.Uri
import android.webkit.CookieManager
import android.webkit.PermissionRequest
import android.webkit.WebBackForwardList
import android.webkit.WebHistoryItem
import android.webkit.WebResourceResponse
import android.webkit.WebView
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateMapOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.compose.runtime.snapshots.SnapshotStateMap
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import androidx.work.WorkManager
import com.google.zxing.BarcodeFormat
import com.google.zxing.common.BitMatrix
import com.google.zxing.qrcode.QRCodeWriter
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import tech.lp2p.Lite
import tech.lp2p.core.BlockStore
import tech.lp2p.core.Cid
import tech.lp2p.core.Dir
import tech.lp2p.core.FileStore
import tech.lp2p.core.Header
import tech.lp2p.core.Link
import tech.lp2p.core.MimeType
import tech.lp2p.core.Node
import tech.lp2p.core.PeerId
import tech.lp2p.core.Peeraddr
import tech.lp2p.core.Scheme
import tech.lp2p.core.Status
import threads.magnet.magnet.MagnetUriParser
import threads.thor.R
import threads.thor.data.Bookmark
import threads.thor.data.Bookmarks
import threads.thor.data.Engine
import threads.thor.data.Peers
import threads.thor.data.Preferences
import threads.thor.data.Provider
import threads.thor.data.Tab
import threads.thor.data.Tabs
import threads.thor.data.Task
import threads.thor.data.Tasks
import threads.thor.model.DOWNLOADS_TAG
import threads.thor.model.MDNS
import threads.thor.model.MDNS.Companion.mdns
import threads.thor.model.META
import threads.thor.model.STYLE
import threads.thor.model.createErrorMessage
import threads.thor.model.error
import threads.thor.model.getFileSize
import threads.thor.model.getMimeType
import threads.thor.model.getSearchEngine
import threads.thor.model.getSvgDownload
import threads.thor.model.getSvgResource
import threads.thor.model.getUriTitle
import threads.thor.work.DownloadContentWorker
import threads.thor.work.DownloadFileWorker
import threads.thor.work.DownloadMagnetWorker
import threads.thor.work.DownloadTorrentWorker
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.net.URI
import java.nio.charset.StandardCharsets
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle
import java.util.concurrent.ConcurrentHashMap

class StateModel(application: Application) :
    AndroidViewModel(application) {


    private val tabs = Tabs.getInstance(getApplication())
    private val provider = Provider.getInstance(getApplication())
    private val tasks = Tasks.getInstance(getApplication())
    private val bookmarks = Bookmarks.getInstance(getApplication())

    val peers = Peers.getInstance(application)
    val blockStore: BlockStore = FileStore(application.filesDir)
    var pages: SnapshotStateMap<Long, String> = mutableStateMapOf()
    var online: Boolean by mutableStateOf(false)
    var showTabs: Boolean by mutableStateOf(false)
    var offlineMode: Boolean by mutableStateOf(false)

    val activeTasks: Flow<Boolean> = tasks.tasksDao().active()

    private val locals = ConcurrentHashMap<PeerId, Peeraddr>()
    private var mdns: MDNS = mdns(getApplication())


    init {
        mdns.startDiscovery { peeraddr: Peeraddr -> local(peeraddr) }
        checkOnline()
    }


    override fun onCleared() {
        super.onCleared()

        error("StateModel", "onStop")
        mdns.stop()
    }

    fun tasks(pid: Long): Flow<List<Task>> {
        return tasks.tasksDao().tasks(pid)
    }

    fun bookmarkIcon(url: String): Int {
        val uri = Uri.parse(url)
        return when (uri.scheme) {
            Scheme.file.name -> {
                R.drawable.bookmark_mht
            }

            Scheme.pns.name -> {
                R.drawable.bookmark_pns
            }

            else -> {
                R.drawable.bookmark
            }
        }
    }


    fun doSearch(tabId: Long, origin: String) {
        if (origin.isNotBlank()) {
            val uri = Uri.parse(origin)
            val scheme = uri.scheme
            if (scheme != Scheme.pns.name &&
                scheme != Scheme.http.name &&
                scheme != Scheme.https.name
            ) {
                viewModelScope.launch {
                    val engine = getEngine().first()
                    showPage(tabId, engine.query + origin)

                }

            } else {
                viewModelScope.launch {
                    showPage(tabId, uri.toString())
                }
            }
        }
    }

    fun tabIcon(url: String?): Int {
        if (url.isNullOrBlank()) {
            return R.drawable.https
        }
        val uri = Uri.parse(url)
        return when (uri.scheme) {
            Scheme.file.name -> {
                R.drawable.mht
            }

            Scheme.pns.name -> {
                R.drawable.pns
            }

            else -> {
                R.drawable.https
            }
        }
    }

    fun bookmarks(): Flow<List<Bookmark>> {
        return bookmarks.bookmarkDao().bookmarks()
    }

    fun tabs(): Flow<List<Tab>> {
        return tabs.tabDao().tabs()
    }


    fun title(id: Long): Flow<String?> {
        return tabs.tabDao().title(id)
    }

    fun ids(): Flow<List<Long>> {
        return tabs.tabDao().ids()
    }

    fun cancelTask(task: Task) {
        if (task.workUUID() != null) {
            WorkManager.getInstance(getApplication()).cancelWorkById(task.workUUID()!!)
        }
        viewModelScope.launch {
            tasks.tasksDao().cancel(task.id)
        }
    }

    fun startTask(task: Task) {
        viewModelScope.launch {
            val uri = Uri.parse(task.uri)

            val scheme = uri.scheme
            if (scheme == Scheme.magnet.name) {
                tasks.tasksDao().active(task.id)
                val uuid = DownloadMagnetWorker.download(getApplication(), task.id, uri)
                tasks.tasksDao().updateWork(task.id, uuid.toString())
            } else if (scheme == Scheme.pns.name) {
                tasks.tasksDao().active(task.id)
                val uuid = DownloadContentWorker.download(
                    getApplication(),
                    task.id, uri
                )
                tasks.tasksDao().updateWork(task.id, uuid.toString())
            } else if (scheme == Scheme.http.name) {
                val mimeType = task.mimeType
                if (mimeType == MimeType.TORRENT_MIME_TYPE.name) {
                    tasks.tasksDao().active(task.id)
                    val uuid = DownloadTorrentWorker.download(
                        getApplication(),
                        task.id, uri
                    )
                    tasks.tasksDao().updateWork(task.id, uuid.toString())
                } else {
                    tasks.tasksDao().active(task.id)
                    val uuid = DownloadFileWorker.download(
                        getApplication(),
                        task.id, uri
                    )
                    tasks.tasksDao().updateWork(task.id, uuid.toString())
                }
            } else if (scheme == Scheme.https.name) {
                val mimeType = task.mimeType
                if (mimeType == MimeType.TORRENT_MIME_TYPE.name) {
                    tasks.tasksDao().active(task.id)
                    val uuid = DownloadTorrentWorker.download(
                        getApplication(), task.id,
                        uri
                    )
                    tasks.tasksDao().updateWork(task.id, uuid.toString())
                } else {
                    tasks.tasksDao().active(task.id)
                    val uuid = DownloadFileWorker.download(
                        getApplication(), task.id,
                        uri
                    )
                    tasks.tasksDao().updateWork(task.id, uuid.toString())
                }
            }
        }
    }


    fun hasBookmark(url: String?): Flow<Boolean> {
        return bookmarks.bookmarkDao().hasBookmark(url)
    }

    fun alterBookmark(
        url: String, title: String?, bitmap: Bitmap?,
        onWarningRequest: (String) -> Unit = {}
    ) {
        viewModelScope.launch {
            var bookmark = bookmarks.bookmarkDao().bookmark(url)
            val bookmarkTitle = webViewTitle(url, title)
            if (bookmark != null) {
                bookmarks.bookmarkDao().delete(bookmark)
                onWarningRequest.invoke(
                    getApplication<Application>().getString(
                        R.string.bookmark_removed, bookmarkTitle
                    )
                )

            } else {
                bookmark = Bookmark(0, url, bookmarkTitle, bytes(bitmap))
                bookmarks.bookmarkDao().insert(bookmark)
                onWarningRequest.invoke(
                    getApplication<Application>().getString(
                        R.string.bookmark_added, bookmarkTitle
                    )
                )

            }
        }
    }

    private fun bytes(bitmap: Bitmap?): ByteArray? {
        if (bitmap == null) {
            return null
        }
        val config = checkNotNull(bitmap.config)
        val copy = bitmap.copy(config, true)
        val stream = ByteArrayOutputStream()
        copy.compress(Bitmap.CompressFormat.PNG, 100, stream)
        val byteArray = stream.toByteArray()
        copy.recycle()
        return byteArray
    }

    fun cleanUrl(url: String?): String {
        if (url.isNullOrEmpty()) {
            return ""
        }
        val uri = Uri.parse(url)
        if (uri.scheme == Scheme.pns.name) {
            return Uri.Builder().scheme(uri.scheme).authority(uri.authority).build().toString()
        }
        return uri.toString()
    }

    fun intentUri(intent: Intent): Uri? {
        val action = intent.action
        if (Intent.ACTION_VIEW == action) {
            val uri = intent.data
            if (uri != null) {
                return uri
            }
        }
        if (Intent.ACTION_SEND == action) {
            if (intent.type == MimeType.PLAIN_MIME_TYPE.name) {
                val query = intent.getStringExtra(Intent.EXTRA_TEXT)

                if (!query.isNullOrEmpty()) {
                    return Uri.parse(query)
                }
            }
        }
        return null
    }


    fun removeBookmark(bookmark: Bookmark) {
        viewModelScope.launch {
            bookmarks.bookmarkDao().delete(bookmark)
        }
    }


    fun clearTabs() {
        Thread {
            tabs.clearAllTables()
            pages.clear()
            ensureHasTab()
        }.start()
    }

    private suspend fun hasNoTabs(): Boolean {
        return !tabs.tabDao().hasTabs()
    }

    fun showBookmarkPage(tabId: Long, bookmark: Bookmark) {
        viewModelScope.launch {
            tabs.updateTab(tabId, bookmark.url, bookmark.title, bytes(bookmark.bitmap()))
            pages[tabId] = bookmark.url
        }
    }

    fun ensureHasTab() {
        viewModelScope.launch {
            checkEnsureHasTab()
        }
    }

    private suspend fun checkEnsureHasTab() {
        if (hasNoTabs()) {
            createPrimaryTab()
        }
    }

    fun removeTab(tab: Tab) {
        viewModelScope.launch {
            tabs.tabDao().delete(tab)
            pages.remove(tab.id)
            checkEnsureHasTab()
        }
    }

    fun titleTab(tabId: Long, uri: String, title: String) {
        viewModelScope.launch {
            tabs.tabDao().title(tabId, uri, title)
        }
    }


    fun showPage(url: String) {
        viewModelScope.launch {
            val title = getUriTitle(url)
            val tab = Tab(0, title, url, null)
            val id = tabs.tabDao().insert(tab)
            pages[id] = url
        }
    }

    private suspend fun showPage(tabId: Long, url: String, title: String) {
        tabs.updateTab(tabId, url, title, null)
        pages[tabId] = url
    }

    private suspend fun showPage(tabId: Long, url: String) {
        val title = getUriTitle(url)
        showPage(tabId, url, title)
    }

    fun imageTab(tabId: Long, url: String, bitmap: Bitmap?) {
        viewModelScope.launch {
            tabs.tabDao().image(tabId, url, bytes(bitmap))
        }
    }

    private fun local(peeraddr: Peeraddr) {
        locals[peeraddr.peerId] = peeraddr
    }

    private fun local(peerId: PeerId?): Peeraddr {
        return checkNotNull(locals[peerId])
    }

    private fun hasLocal(peerId: PeerId?): Boolean {
        return locals.containsKey(peerId)
    }

    // todo tabId
    fun response(lite: Lite, tabId: Long, uri: Uri): WebResourceResponse {
        try {
            var pnsUri = URI.create(uri.toString())

            // when offline
            // or no public ipv6 check if it is possible to load previous site

            val peerId = PeerId.extractPeerId(URI.create(uri.toString()))
            if (peerId.isPresent) {
                // check if local is available
                if (hasLocal(peerId.get())) {
                    val localRequest = local(peerId.get()).toUri()
                    lite.ping(localRequest)
                }
            }


            val optionalRoot = lite.fetchRoot(pnsUri)
            if (optionalRoot.isPresent) {
                if (!Peeraddr.extractPeeraddr(pnsUri).isPresent) {
                    val root = optionalRoot.get()
                    val peeraddr = root.peeraddr
                    pnsUri = Lite.createFetchUri(peeraddr, root.cid)
                }
                return response(lite, tabId, pnsUri!!)
            } else {
                if (isNetworkConnected) {
                    if (!Lite.reservationFeaturePossible()) {
                        return createErrorMessage(
                            getApplication<Application>().getString(R.string.pns_service_limitation_ip)
                        )
                    }
                }

                return createErrorMessage(
                    getApplication<Application>().getString(R.string.page_load_error)
                )
            }
        } catch (throwable: Throwable) {
            return createErrorMessage(throwable)
        }
    }


    private val isNetworkConnected: Boolean
        get() {
            val connectivityManager =
                getApplication<Application>().getSystemService(
                    Context.CONNECTIVITY_SERVICE
                ) as ConnectivityManager
            val nw = connectivityManager.activeNetwork ?: return false
            val actNw = connectivityManager.getNetworkCapabilities(nw)
            return actNw != null && (actNw.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)
                    || actNw.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)
                    || actNw.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET))
        }

    fun reset() {

        // cancel all jobs
        WorkManager.getInstance(getApplication()).cancelAllWorkByTag(DOWNLOADS_TAG)

        Tasks.getInstance(getApplication()).clearAllTables()

        // Clear all the cookies
        CookieManager.getInstance().removeAllCookies(null)
        CookieManager.getInstance().flush()

        // Clear cache and data
        Provider.cleanCache(getApplication())
        Provider.cleanData(getApplication())

        WorkManager.getInstance(getApplication()).pruneWork()

    }

    fun createPrimaryPage() {
        viewModelScope.launch {
            createPrimaryTab()
        }
    }


    private suspend fun createPrimaryTab() {
        var page = Preferences.primaryPage(getApplication(), -1)
        val tab = Tab(0, page.title, page.url, null)
        val id = tabs.tabDao().insert(tab)
        page = page.copy(id = id)
        pages[id] = page.url
    }


    fun checkOnline() {
        this.online = isNetworkConnected
    }

    fun isJavascriptEnabled(): Flow<Boolean> = Preferences.isJavascriptEnabled(getApplication())

    private fun getEngine(): Flow<Engine> {
        return searchEngine().map { engine ->
            getSearchEngine(engine)
        }
    }

    fun searchEngine(): Flow<String> = Preferences.searchEngine(getApplication())

    fun setJavascriptEnabled(checked: Boolean) {
        viewModelScope.launch {
            Preferences.setJavascriptEnabled(getApplication(), checked)
        }
    }

    fun setSearchEngine(name: String) {
        viewModelScope.launch {
            Preferences.setSearchEngine(getApplication(), name)
        }
    }


    fun transform(list: WebBackForwardList): List<WebHistoryItem> {
        val items: MutableList<WebHistoryItem> = ArrayList()
        for (i in 0 until list.size) {
            items.add(list.getItemAtIndex(i))
        }
        return items
    }


    fun download(webView: WebView) {

        val url = webView.url
        if (!url.isNullOrEmpty()) {
            val uri = Uri.parse(url)
            if (uri.scheme == Scheme.pns.name) {
                val name = getUriTitle(url)
                contentDownloader(uri, name)
            } else if (uri.scheme == Scheme.http.name || uri.scheme == Scheme.https.name) {
                var name = webView.title
                if (name.isNullOrEmpty()) {
                    name = "unknown"
                }
                name = name.replace("\n", "")
                    .replace("\r", "")
                    .replace(".", "")
                    .replace(",", "")
                val display = "$name.mht"

                val file = provider.createDataFile(display)
                webView.saveWebArchive(file.absolutePath)
                val dataUri = file.toURI()

                viewModelScope.launch {
                    tasks.tasksDao().insert(
                        Task(
                            0, 0, display, MimeType.MHT_MIME_TYPE.name,
                            dataUri.toString(), file.length(), null,
                            active = false,
                            finished = true,
                            progress = 0f
                        )
                    )
                }

            }
        }
    }

    fun contentDownloader(uri: Uri, name: String) {

        viewModelScope.launch {
            val mimeType = getMimeType(name)

            val taskId = tasks.tasksDao().insert(
                Task(
                    0, 0, name, mimeType,
                    uri.toString(), 0, null,
                    active = true,
                    finished = false,
                    progress = 0f
                )
            )

            val uuid = DownloadContentWorker.download(getApplication(), taskId, uri)
            tasks.tasksDao().updateWork(taskId, uuid.toString())
        }


    }

    fun magnetDownloader(uri: Uri) {
        viewModelScope.launch {

            val magnetUri = MagnetUriParser.parse(uri.toString())

            var name = uri.toString()
            if (magnetUri.displayName.isPresent) {
                name = magnetUri.displayName.get()
            }

            val taskId = tasks.tasksDao().insert(
                Task(
                    0, 0, name, MimeType.DIR_MIME_TYPE.name,
                    uri.toString(), -1, null,
                    active = true,
                    finished = false,
                    progress = 0f
                )
            )

            val uuid = DownloadMagnetWorker.download(getApplication(), taskId, uri)
            tasks.tasksDao().updateWork(taskId, uuid.toString())
        }
    }

    fun fileDownloader(uri: Uri, filename: String, mimeType: String, size: Long) {

        viewModelScope.launch {
            val taskId = tasks.tasksDao().insert(
                Task(
                    0, 0, filename, mimeType, uri.toString(), size,
                    null,
                    active = true,
                    finished = false,
                    progress = 0f
                )
            )

            if (mimeType == MimeType.TORRENT_MIME_TYPE.name) {
                val uuid = DownloadTorrentWorker.download(getApplication(), taskId, uri)
                tasks.tasksDao().updateWork(taskId, uuid.toString())
            } else {
                val uuid = DownloadFileWorker.download(getApplication(), taskId, uri)
                tasks.tasksDao().updateWork(taskId, uuid.toString())
            }
        }

    }


    fun resetHomepage() {
        viewModelScope.launch {
            Preferences.homepageUri(getApplication(), null)
            Preferences.homepageTitle(getApplication(), null)
        }
    }

    fun setHomepage(url: String, title: String) {
        viewModelScope.launch {
            Preferences.homepageUri(getApplication(), url)
            Preferences.homepageTitle(getApplication(), title)
        }
    }

    fun homepageUri(): Flow<String> {
        return Preferences.homepageUri(getApplication())
    }

    fun pruneWork() {
        WorkManager.getInstance(getApplication()).pruneWork()
        viewModelScope.launch {
            tasks.tasksDao().purge()
        }
    }

    fun tabsIconResource(size: Int): Int {
        return when (size) {
            0 -> R.drawable.numeric_0_box_multiple_outline
            1 -> R.drawable.numeric_1_box_multiple_outline
            2 -> R.drawable.numeric_2_box_multiple_outline
            3 -> R.drawable.numeric_3_box_multiple_outline
            4 -> R.drawable.numeric_4_box_multiple_outline
            5 -> R.drawable.numeric_5_box_multiple_outline
            6 -> R.drawable.numeric_6_box_multiple_outline
            7 -> R.drawable.numeric_7_box_multiple_outline
            8 -> R.drawable.numeric_8_box_multiple_outline
            9 -> R.drawable.numeric_9_box_multiple_outline
            else -> R.drawable.numeric_9_plus_box_multiple_outline
        }
    }

    fun infoIcon(url: String): Int {
        val uri = Uri.parse(url)
        var res: Int = R.drawable.view
        if (uri.scheme == Scheme.file.name) {
            res = R.drawable.bookmark_mht
        } else if (uri.scheme == Scheme.pns.name) {
            res = R.drawable.bookmark_pns
        }
        return res
    }

    private fun webViewTitle(url: String, webViewTitle: String?): String {
        var title = webViewTitle
        if (title == null) {
            title = Uri.parse(url).authority ?: ""
        }
        return title
    }

    fun canBookmark(url: String?): Boolean {
        return if (!url.isNullOrBlank()) {
            !isFileUri(url) && !isPnsUriWithFragment(url)
        } else {
            false
        }
    }

    fun downloadActive(url: String?): Boolean {
        if (!url.isNullOrEmpty()) {
            val uri = Uri.parse(url)
            return uri.scheme == Scheme.pns.name ||
                    uri.scheme == Scheme.http.name ||
                    uri.scheme == Scheme.https.name
        }
        return false
    }

    fun isFileUri(url: String?): Boolean {
        if (!url.isNullOrBlank()) {
            val uri = Uri.parse(url)
            return uri.scheme == Scheme.file.name
        }
        return false
    }

    fun showPrimaryPage(tabId: Long) {
        viewModelScope.launch {
            val page = Preferences.primaryPage(getApplication(), tabId)
            tabs.updateTab(tabId, page.url, page.title, null)
            pages[tabId] = page.url
        }

    }

    fun infoTitle(title: String?, url: String): String {
        return if (title.isNullOrEmpty() || isPnsUriWithFragment(url)) {
            getUriTitle(url)
        } else {
            title
        }
    }

    private fun isPnsUriWithFragment(uri: Uri): Boolean {
        if (uri.scheme == Scheme.pns.name) {
            val fragment = uri.fragment
            return !fragment.isNullOrBlank()
        }
        return false
    }

    private fun isPnsUriWithFragment(url: String?): Boolean {
        if (!url.isNullOrBlank()) {
            val uri = Uri.parse(url)
            if (uri != null) {
                return isPnsUriWithFragment(uri)
            }
        }
        return false
    }


    fun getBitmap(url: String): Bitmap {
        val bitMatrix = QRCodeWriter().encode(
            url, BarcodeFormat.QR_CODE, 250, 250
        )
        return createBitmap(bitMatrix)
    }

    private fun createBitmap(matrix: BitMatrix): Bitmap {
        val width = matrix.width
        val height = matrix.height
        val pixels = IntArray(width * height)
        for (y in 0 until height) {
            val offset = y * width
            for (x in 0 until width) {
                pixels[offset + x] = if (matrix[x, y]) -0x1000000 else -0x1
            }
        }

        val bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
        bitmap.setPixels(pixels, 0, width, 0, 0, width, height)
        return bitmap
    }


    fun showPage(tabId: Long, task: Task) {
        viewModelScope.launch {
            showPage(tabId, task.uri, task.name)
        }
    }

    fun invokeTask(task: Task, onWarningRequest: (String) -> Unit = {}) {
        try {
            val viewIntent = Intent(Intent.ACTION_VIEW, Uri.parse(task.uri))
            viewIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
            viewIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            getApplication<Application>().startActivity(viewIntent)
        } catch (throwable: Throwable) {
            onWarningRequest.invoke(
                getApplication<Application>().getString(R.string.no_activity_found_to_handle_uri)
            )
        }
    }


    fun taskActionVisible(task: Task): Boolean {
        if (!task.finished) {
            return task.mimeType != MimeType.MHT_MIME_TYPE.name
        }
        return false
    }

    private fun generateDirectoryHtml(uri: URI, dir: Dir, links: List<Link>?): String {
        val title = dir.name

        val answer = StringBuilder(
            "<html>" + "<head>" + META +
                    "<title>" + title + "</title>"
        )

        answer.append("</head>")
        answer.append(STYLE)
        answer.append("<body>")

        if (links != null) {
            if (links.isNotEmpty()) {
                answer.append("<form><table  width=\"100%\" style=\"border-spacing: 8px;\">")


                for (info in links) {
                    val mimeType = getMimeType(info)
                    val link: Uri = Uri.parse(uri.toString()).buildUpon()
                        .appendQueryParameter(Header.CONTENT_TITLE, info.name())
                        .appendQueryParameter(Header.CONTENT_TYPE, mimeType)
                        .fragment(Cid.encode(info.cid())).build()

                    answer.append("<tr>")

                    answer.append("<td>")
                    answer.append(getSvgResource(mimeType))
                    answer.append("</td>")

                    answer.append("<td width=\"100%\" style=\"word-break:break-word\">")
                    answer.append("<a href=\"")
                    answer.append(link)
                    answer.append("\">")
                    answer.append(info.name())
                    answer.append("</a>")
                    answer.append("</td>")

                    answer.append("<td>")
                    answer.append(getFileSize(info.size()))
                    answer.append("</td>")

                    answer.append("<td align=\"center\">")

                    val name = info.name()

                    val text = "<button style=\"float:none!important;display:inline;\" " +
                            "name=\"" + Header.CONTENT_DOWNLOAD + "\" " +
                            "value=\"" + name + "\" " +
                            "formenctype=\"text/plain\" " +
                            "formmethod=\"get\" " +
                            "type=\"submit\" " +
                            "formaction=\"" +
                            link + "\">" + getSvgDownload() + "</button>"

                    answer.append(text)
                    answer.append("</td>")
                    answer.append("</tr>")
                }
                answer.append("</table></form>")
            }
        }

        val zoned = ZonedDateTime.now()
        val pattern = DateTimeFormatter.ofLocalizedDate(FormatStyle.FULL)

        answer.append("</body><div class=\"footer\">")
            .append("<p>")
            .append(zoned.format(pattern))
            .append("</p>")
            .append("</div></html>")


        return answer.toString()
    }


    @Throws(IOException::class)
    private fun getContentResponse(
        lite: Lite,
        uri: URI,
        node: Node
    ): WebResourceResponse {
        val mimeType = getMimeType(node)
        lite.stream(uri).use { inputStream ->
            val responseHeaders: MutableMap<String, String> = HashMap()
            responseHeaders[Header.CONTENT_LENGTH] = node.size().toString()
            responseHeaders[Header.CONTENT_TYPE] = mimeType
            return WebResourceResponse(
                mimeType, StandardCharsets.UTF_8.name(), Status.OK.toInt(),
                "OK", responseHeaders, inputStream
            )
        }
    }


    @Throws(Exception::class)
    fun response(lite: Lite, tabId: Long, uri: URI): WebResourceResponse {
        val node = lite.info(uri) // is resolved
        if (node is Dir) {
            // todo tabId
            titleTab(tabId, uri.toString(), node.name)

            val answer = generateDirectoryHtml(uri, node, node.links())
            return WebResourceResponse(
                MimeType.HTML_MIME_TYPE.name,
                StandardCharsets.UTF_8.name(), ByteArrayInputStream(answer.toByteArray())
            )
        } else {
            return getContentResponse(lite, uri, node)
        }
    }


    fun permission(request: PermissionRequest): String {
        val resources = request.resources
        if (resources == null || resources.isEmpty()) {
            return ""
        }


        val resource = resources[0]
        return when (resource) {
            "android.webkit.resource.AUDIO_CAPTURE" -> Manifest.permission.RECORD_AUDIO
            "android.webkit.resource.VIDEO_CAPTURE" -> Manifest.permission.CAMERA
            else -> ""
        }
    }

    fun showHistoryPage(tabId: Long, history: WebHistoryItem) {
        viewModelScope.launch {
            tabs.updateTab(tabId, history.url, history.title, null)
            pages[tabId] = history.url
        }
    }

    fun removePage(tabId: Long) {
        viewModelScope.launch {
            pages.remove(tabId)
        }
    }

    fun isAd(uri: Uri): Boolean {
        return threads.thor.model.isAd(getApplication(), uri)
    }

    fun removeTask(task: Task) {
        if (task.workUUID() != null) {
            WorkManager.getInstance(getApplication()).cancelWorkById(task.workUUID()!!)
        }
        viewModelScope.launch {
            tasks.tasksDao().delete(task)
            ensureHasTab()
        }
    }

    fun showDocumentation(tabId: Long) {
        viewModelScope.launch {
            showPage(tabId, "https://gitlab.com/lp2p/thor")
        }
    }
}
