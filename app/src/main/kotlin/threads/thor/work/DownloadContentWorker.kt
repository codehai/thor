package threads.thor.work

import android.content.Context
import android.net.Uri
import android.os.Environment
import androidx.work.CoroutineWorker
import androidx.work.Data
import androidx.work.ExistingWorkPolicy
import androidx.work.OneTimeWorkRequest
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.OutOfQuotaPolicy
import androidx.work.WorkManager
import androidx.work.WorkerParameters
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runInterruptible
import kotlinx.coroutines.withContext
import tech.lp2p.Lite
import tech.lp2p.core.BlockStore
import tech.lp2p.core.Dir
import tech.lp2p.core.FileStore
import tech.lp2p.core.MimeType
import tech.lp2p.core.Node
import threads.thor.data.Peers
import threads.thor.data.Tasks
import threads.thor.model.DOWNLOADS_TAG
import threads.thor.model.bootstrap
import threads.thor.model.downloadsUri
import threads.thor.model.error
import threads.thor.model.getMimeType
import java.io.File
import java.net.URI
import java.nio.channels.Channels
import java.util.Stack
import java.util.UUID
import java.util.concurrent.atomic.AtomicLong
import java.util.function.Consumer


class DownloadContentWorker(context: Context, params: WorkerParameters) :
    CoroutineWorker(context, params) {


    override suspend fun doWork(): Result {

        val taskId = inputData.getLong(TID, 0)

        val tasks = Tasks.getInstance(applicationContext)
        val task = tasks.tasksDao().task(taskId)

        tasks.tasksDao().active(taskId)
        withContext(Dispatchers.IO) {
            try {
                val peers = Peers.getInstance(applicationContext)

                val blockStore: BlockStore = FileStore(applicationContext.filesDir)
                Lite.newBuilder()
                    .bootstrap(bootstrap())
                    .blockStore(blockStore)
                    .peerStore(peers)
                    .disableServer()
                    .build().use { lite ->
                        val request = URI.create(task.uri)
                        val node: Node = lite.info(request)

                        val paths = Stack<String>()
                        paths.push(node.name())

                        val size = node.size()
                        if (node is Dir) {
                            download(lite, tasks, taskId, request, paths, node, 0L, size)
                        } else {
                            download(lite, tasks, taskId, request, paths, 0L, size)
                        }

                    }
            } catch (throwable: Throwable) {
                error(TAG, throwable)
            }
        }

        if (isStopped) {
            return Result.retry()
        }
        return Result.success()
    }


    @Throws(Exception::class)
    private suspend fun download(
        lite: Lite,
        tasks: Tasks,
        taskId: Long,
        resolvedUri: URI,
        paths: Stack<String>,
        init: Long, size: Long
    ): Long {
        val name = paths.pop()
        checkNotNull(name)

        val mimeType = getMimeType(name)
        val path = Environment.DIRECTORY_DOWNLOADS + File.separator +
                java.lang.String.join((File.separator), paths)

        val targetUri = downloadsUri(applicationContext, mimeType, name, path)
        checkNotNull(targetUri)
        val contentResolver = applicationContext.contentResolver

        val read = AtomicLong(init)

        runInterruptible {
            try {
                contentResolver.openOutputStream(targetUri).use { os ->
                    checkNotNull(os)
                    val fosChannel = Channels.newChannel(os)
                    lite.channel(resolvedUri).transferTo(fosChannel, object : Consumer<Int> {
                        var remember: Int = 0

                        override fun accept(n: Int) {
                            val value = read.addAndGet(n.toLong())
                            if (size > 0) {
                                val percent = ((value * 100.0f) / size).toInt()
                                if (percent > remember) {
                                    remember = percent

                                    tasks.tasksDao().progress(taskId, percent / 100.0f)
                                }
                            }
                        }
                    })
                }
            } catch (throwable: Throwable) {
                error(TAG, "Delete $targetUri")
                contentResolver.delete(targetUri, null, null)
                throw throwable
            }
        }
        tasks.tasksDao().finished(taskId, targetUri.toString())
        return read.get()
    }


    @Throws(Exception::class)
    private suspend fun download(
        lite: Lite,
        tasks: Tasks,
        taskId: Long,
        request: URI,
        paths: Stack<String>,
        dir: Dir,
        read: Long,
        size: Long
    ): Long {
        var readLocal = read
        for (child in dir.links) {
            if (!isStopped) {
                val cid = child.cid() // not yet resolved
                paths.push(child.name())
                val pnsUri = Lite.createFetchUri(request, cid)
                val node = lite.info(pnsUri)

                readLocal = if (node is Dir) { // now it is resolved

                    val subTaskId = tasks.createOrGet(
                        taskId, child.name(),
                        MimeType.DIR_MIME_TYPE.name, pnsUri.toString(), child.size(), id
                    )
                    tasks.tasksDao().active(subTaskId)
                    download(lite, tasks, subTaskId, pnsUri, paths, node, readLocal, size)

                } else {

                    val subTaskId = tasks.createOrGet(
                        taskId, child.name(),
                        getMimeType(child.name()), pnsUri.toString(), child.size(), id
                    )
                    tasks.tasksDao().active(subTaskId)
                    download(lite, tasks, subTaskId, pnsUri, paths, readLocal, size)

                }
            }
        }

        val path = Environment.DIRECTORY_DOWNLOADS + File.separator +
                java.lang.String.join((File.separator), paths)

        val targetUri = downloadsUri(
            applicationContext, MimeType.DIR_MIME_TYPE.name,
            dir.name, path
        )
        checkNotNull(targetUri)
        tasks.tasksDao().finished(taskId, targetUri.toString())

        return readLocal
    }

    companion object {
        private val TAG: String = DownloadContentWorker::class.java.simpleName

        private const val TID = "tid"
        private fun getWork(taskId: Long): OneTimeWorkRequest {
            val data = Data.Builder().putLong(TID, taskId)

            return OneTimeWorkRequestBuilder<DownloadContentWorker>()
                .addTag(DOWNLOADS_TAG)
                .setInputData(data.build())
                .setExpedited(OutOfQuotaPolicy.RUN_AS_NON_EXPEDITED_WORK_REQUEST)
                .build()
        }

        fun download(context: Context, taskId: Long, uri: Uri): UUID {
            val work = getWork(taskId)
            WorkManager.getInstance(context).enqueueUniqueWork(
                uri.toString(), ExistingWorkPolicy.KEEP, work
            )
            return work.id
        }
    }
}
