package threads.thor.work

import android.content.Context
import android.net.Uri
import androidx.work.Constraints
import androidx.work.CoroutineWorker
import androidx.work.Data
import androidx.work.ExistingWorkPolicy
import androidx.work.NetworkType
import androidx.work.OneTimeWorkRequest
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.OutOfQuotaPolicy
import androidx.work.WorkManager
import androidx.work.WorkerParameters
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import tech.lp2p.core.Utils
import threads.magnet.Client
import threads.magnet.magnet.MagnetUriParser
import threads.magnet.torrent.TorrentSessionState
import threads.thor.data.Provider
import threads.thor.data.Tasks
import threads.thor.model.DOWNLOADS_TAG
import threads.thor.model.error
import threads.thor.model.uploadDirectory
import threads.thor.model.uploadFile
import java.io.File
import java.util.Stack
import java.util.UUID

class DownloadMagnetWorker(context: Context, params: WorkerParameters) :
    CoroutineWorker(context, params) {
    override suspend fun doWork(): Result {
        val taskId = inputData.getLong(TID, 0)
        val provider = Provider.getInstance(applicationContext)
        val tasks = Tasks.getInstance(applicationContext)

        tasks.tasksDao().active(taskId)
        withContext(Dispatchers.IO) {

            val task = tasks.tasksDao().task(taskId)
            val magnetUri = MagnetUriParser.parse(task.uri)

            val dataDir = provider.getDataDir()

            val root = File(dataDir, task.name)
            if (!root.exists()) {
                val success = root.mkdir()
                Utils.checkTrue(success, "Could not create directory")
            }

            val client = Client.create(root, magnetUri)


            client.start({ torrentSessionState: TorrentSessionState ->
                val completePieces = torrentSessionState.piecesComplete.toLong()
                val totalPieces = torrentSessionState.piecesTotal.toLong()

                // -1 because of final copy action
                val progress = (completePieces * 1.0f) / (totalPieces * 1.0f)

                error(TAG, " pieces : $completePieces/$totalPieces")

                tasks.tasksDao().progress(taskId, progress)

                if (isStopped) {
                    client.stop()
                }
            }, 1000)

            if (!isStopped) {
                val dirs = Stack<String>()
                if (root.isDirectory) {
                    uploadDirectory(applicationContext, tasks, taskId, root, dirs, id)
                } else {
                    uploadFile(applicationContext, tasks, taskId, root, dirs)
                }
                Provider.deleteFile(root, true) // todo

                tasks.tasksDao().progress(taskId, 1.0f)
            }
        }

        if (isStopped) {
            return Result.retry()
        }
        return Result.success()

    }


    companion object {
        private val TAG: String = DownloadMagnetWorker::class.java.simpleName
        private const val TID = "tid"
        private fun getWork(taskId: Long): OneTimeWorkRequest {
            val builder: Constraints.Builder = Constraints.Builder()
                .setRequiredNetworkType(NetworkType.CONNECTED)

            val data: Data.Builder = Data.Builder()
            data.putLong(TID, taskId)

            return OneTimeWorkRequestBuilder<DownloadMagnetWorker>()
                .addTag(DOWNLOADS_TAG)
                .setInputData(data.build())
                .setConstraints(builder.build())
                .setExpedited(OutOfQuotaPolicy.RUN_AS_NON_EXPEDITED_WORK_REQUEST)
                .build()
        }

        fun download(context: Context, taskId: Long, uri: Uri): UUID {
            val work = getWork(taskId)
            WorkManager.getInstance(context).enqueueUniqueWork(
                uri.toString(),
                ExistingWorkPolicy.KEEP, work
            )
            return work.id
        }
    }
}
