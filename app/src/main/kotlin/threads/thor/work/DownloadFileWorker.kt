package threads.thor.work

import android.content.Context
import android.net.Uri
import android.os.Environment
import androidx.work.Constraints
import androidx.work.CoroutineWorker
import androidx.work.Data
import androidx.work.ExistingWorkPolicy
import androidx.work.NetworkType
import androidx.work.OneTimeWorkRequest
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.OutOfQuotaPolicy
import androidx.work.WorkManager
import androidx.work.WorkerParameters
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import tech.lp2p.core.Utils
import threads.thor.data.Tasks
import threads.thor.model.DOWNLOADS_TAG
import threads.thor.model.downloadsUri
import java.io.IOException
import java.io.OutputStream
import java.net.HttpURLConnection
import java.net.URL
import java.util.UUID

class DownloadFileWorker(context: Context, params: WorkerParameters) :
    CoroutineWorker(context, params) {


    @Throws(IOException::class)
    private fun downloadUrl(tasks: Tasks, taskId: Long, urlCon: URL, os: OutputStream, size: Long) {
        HttpURLConnection.setFollowRedirects(false)

        val huc = urlCon.openConnection() as HttpURLConnection

        huc.readTimeout = 30000 // 30 sec
        huc.connect()

        huc.inputStream.use { `is` ->
            Utils.copy(`is`, os, 0L, size) { progress: Int ->
                check(!isStopped)
                tasks.tasksDao().progress(taskId, progress / 100.0f)
            }
        }
    }

    override suspend fun doWork(): Result {
        val taskId = inputData.getLong(TID, 0)
        val tasks = Tasks.getInstance(applicationContext)

        tasks.tasksDao().active(taskId)
        withContext(Dispatchers.IO) {

            val task = tasks.tasksDao().task(taskId)
            val uri = Uri.parse(task.uri)

            val urlCon = URL(uri.toString())

            val downloads = downloadsUri(
                applicationContext, task.mimeType, task.name,
                Environment.DIRECTORY_DOWNLOADS
            )
            checkNotNull(downloads)
            val contentResolver = applicationContext.contentResolver

            try {
                contentResolver.openOutputStream(downloads).use { os ->
                    checkNotNull(os)
                    downloadUrl(tasks, taskId, urlCon, os, task.size)
                }
                tasks.tasksDao().finished(taskId, downloads.toString())
            } catch (throwable: Throwable) {
                contentResolver.delete(downloads, null, null)
                throw throwable
            }
        }

        if (isStopped) {
            return Result.retry()
        }
        return Result.success()
    }

    companion object {
        private const val TID = "tid"

        private fun getWork(taskId: Long): OneTimeWorkRequest {
            val builder: Constraints.Builder = Constraints.Builder()
                .setRequiredNetworkType(NetworkType.CONNECTED)

            val data: Data.Builder = Data.Builder()
            data.putLong(TID, taskId)

            return OneTimeWorkRequestBuilder<DownloadFileWorker>()
                .addTag(DOWNLOADS_TAG)
                .setInputData(data.build())
                .setConstraints(builder.build())
                .setExpedited(OutOfQuotaPolicy.RUN_AS_NON_EXPEDITED_WORK_REQUEST)
                .build()
        }

        fun download(context: Context, taskId: Long, uri: Uri): UUID {
            val work = getWork(taskId)
            WorkManager.getInstance(context).enqueueUniqueWork(
                uri.toString(),
                ExistingWorkPolicy.KEEP, work
            )
            return work.id
        }
    }
}
