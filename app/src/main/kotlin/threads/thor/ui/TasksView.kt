package threads.thor.ui

import androidx.compose.animation.animateContentSize
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.WindowInsets
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.ArrowBack
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material.icons.filled.MoveUp
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SwipeToDismissBox
import androidx.compose.material3.SwipeToDismissBoxValue
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.minimumInteractiveComponentSize
import androidx.compose.material3.rememberSwipeToDismissBoxState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import tech.lp2p.core.MimeType
import threads.thor.R
import threads.thor.data.Task
import threads.thor.model.getMediaResource
import threads.thor.state.StateModel


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun TasksView(
    stateModel: StateModel,
    tabId: Long,
    onDismissRequest: () -> Unit = {},
    onWarningRequest: (String) -> Unit = {}
) {
    var pid: Long by remember { mutableStateOf(0L) }
    val tasks by stateModel.tasks(pid).collectAsState(emptyList())
    val stack = remember { mutableStateListOf<Long>() }



    Scaffold(
        topBar = {
            TopAppBar(
                navigationIcon = {
                    IconButton(onClick = {
                        onDismissRequest.invoke()
                    }) {
                        Icon(
                            Icons.AutoMirrored.Filled.ArrowBack,
                            contentDescription = stringResource(id = android.R.string.cancel)
                        )
                    }
                },
                windowInsets = WindowInsets(0.dp, 0.dp, 0.dp, 0.dp),
                title = {
                    Text(stringResource(id = R.string.downloads))
                },
                actions = {
                    if (!stack.isEmpty()) {
                        IconButton(onClick = {
                            pid = stack.removeAt(stack.size - 1)
                        }) {
                            Icon(
                                imageVector = Icons.Filled.MoveUp,
                                contentDescription = stringResource(android.R.string.untitled)
                            )
                        }
                    }

                    IconButton(onClick = {
                        stateModel.pruneWork()
                    }) {
                        Icon(
                            painter = painterResource(id = R.drawable.broom),
                            contentDescription = stringResource(R.string.cleanup_downloads)
                        )
                    }
                },
            )
        }, content = { padding ->
            LazyColumn(modifier = Modifier.padding(padding)) {
                items(items = tasks,
                    key = { task ->
                        task.id
                    }) { task ->


                    val dismissState = rememberSwipeToDismissBoxState(
                        confirmValueChange = { value ->
                            if (value == SwipeToDismissBoxValue.EndToStart) {
                                stateModel.removeTask(task)
                                true
                            } else {
                                false
                            }
                        }
                    )

                    SwipeToDismissBox(
                        modifier = Modifier.animateContentSize(),
                        state = dismissState,
                        backgroundContent = {

                            Box(
                                modifier = Modifier
                                    .fillMaxSize()
                                    .background(MaterialTheme.colorScheme.errorContainer),
                                contentAlignment = Alignment.CenterEnd

                            ) {
                                Icon(
                                    Icons.Filled.Delete,
                                    stringResource(id = android.R.string.untitled)
                                )
                            }
                        },
                        enableDismissFromEndToStart = !task.active,
                        enableDismissFromStartToEnd = false,
                    ) {
                        TaskItem(stateModel, tabId, task, onDismissRequest, onWarningRequest,
                            onDirectoryRequest = {
                                stack.add(pid)
                                pid = task.id
                            })
                    }
                }
            }
        }
    )
}

@Composable
fun TaskItem(
    stateModel: StateModel,
    tabId: Long,
    task: Task,
    onDismissRequest: () -> Unit = {},
    onWarningRequest: (String) -> Unit = {},
    onDirectoryRequest: (Task) -> Unit = {}
) {

    Column(
        modifier = Modifier
            .fillMaxWidth()
            .clickable(enabled = task.finished) {
                when (task.mimeType) {
                    MimeType.MHT_MIME_TYPE.name -> {
                        stateModel.showPage(tabId, task)
                        onDismissRequest.invoke()
                    }

                    MimeType.DIR_MIME_TYPE.name -> {
                        onDirectoryRequest.invoke(task)
                    }

                    else -> {
                        stateModel.invokeTask(task, onWarningRequest)
                    }
                }
            }
            .background(MaterialTheme.colorScheme.surface)
    ) {
        Row(
            modifier = Modifier
                .padding(0.dp, 16.dp)
                .fillMaxWidth()
                .minimumInteractiveComponentSize()
        ) {
            Box(
                modifier = Modifier
                    .minimumInteractiveComponentSize()
                    .align(Alignment.CenterVertically)
            ) {
                Icon(
                    painterResource(id = getMediaResource(task.mimeType)),
                    contentDescription = stringResource(id = android.R.string.untitled),
                    modifier = Modifier.align(Alignment.Center)
                )

                if (task.active) {
                    if (task.progress > 0) {
                        CircularProgressIndicator(
                            progress = {
                                task.progress
                            },
                            strokeWidth = 2.dp,
                            modifier = Modifier.align(Alignment.Center)
                        )
                    } else {
                        CircularProgressIndicator(
                            strokeWidth = 2.dp,
                            modifier = Modifier.align(Alignment.Center)
                        )
                    }
                }
            }

            Text(
                modifier = Modifier
                    .align(Alignment.CenterVertically)
                    .weight(1.0f, true),
                text = task.name,
                maxLines = 2,
                style = MaterialTheme.typography.labelLarge,
                softWrap = true
            )

            if (stateModel.taskActionVisible(task)) {
                IconButton(
                    modifier = Modifier.align(Alignment.CenterVertically),
                    onClick = {
                        if (!task.finished) {
                            if (task.active) {
                                stateModel.cancelTask(task)
                            } else {
                                stateModel.startTask(task)
                            }
                        }
                    }) {
                    Icon(
                        painterResource(
                            id = if (task.finished) {
                                R.drawable.dots_vertical
                            } else if (task.active) {
                                R.drawable.pause
                            } else {
                                R.drawable.restart
                            }
                        ),
                        contentDescription = stringResource(id = android.R.string.untitled)
                    )
                }
            } else {
                Spacer(modifier = Modifier.minimumInteractiveComponentSize())
            }
        }
    }
}