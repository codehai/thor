package threads.thor.ui

import androidx.compose.animation.animateContentSize
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.focusable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.WindowInsets
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.pager.PagerState
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.ArrowBack
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material.icons.outlined.MoreVert
import androidx.compose.material3.DropdownMenu
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SwipeToDismissBox
import androidx.compose.material3.SwipeToDismissBoxValue
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.minimumInteractiveComponentSize
import androidx.compose.material3.rememberSwipeToDismissBoxState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import kotlinx.coroutines.launch
import threads.thor.R
import threads.thor.data.Tab
import threads.thor.state.StateModel


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun TabsView(
    stateModel: StateModel,
    pagerState: PagerState,
    onDismissRequest: () -> Unit = {},
) {

    val tabs by stateModel.tabs().collectAsState(initial = emptyList())
    var showMenu by remember { mutableStateOf(false) }
    val listState = rememberLazyListState()

    Scaffold(
        topBar = {
            TopAppBar(
                navigationIcon = {
                    IconButton(onClick = {
                        onDismissRequest.invoke()
                    }) {
                        Icon(
                            Icons.AutoMirrored.Filled.ArrowBack,
                            contentDescription = stringResource(id = android.R.string.cancel)
                        )
                    }
                },
                windowInsets = WindowInsets(0.dp, 0.dp, 0.dp, 0.dp),
                title = {
                    Text(stringResource(id = R.string.tabs))
                },
                actions = {
                    IconButton(onClick = {
                        stateModel.createPrimaryPage()
                        // todo onDismissRequest.invoke()
                    }) {
                        Icon(
                            imageVector = Icons.Default.Add,
                            contentDescription = stringResource(id = R.string.add_tab)
                        )
                    }
                    IconButton(onClick = {
                        showMenu = !showMenu
                    }) {
                        Icon(
                            imageVector = Icons.Outlined.MoreVert,
                            contentDescription = stringResource(id = R.string.more_options)
                        )
                    }
                    DropdownMenu(expanded = showMenu, onDismissRequest = { showMenu = false })
                    {
                        DropdownMenuItem(
                            text = { Text(text = stringResource(id = R.string.close_tabs)) },

                            onClick = {
                                stateModel.clearTabs()
                                showMenu = false
                                // todo onDismissRequest.invoke()
                            })
                    }
                },
            )
        }, content = { padding ->
            LazyColumn(modifier = Modifier.padding(padding)) {
                itemsIndexed(items = tabs, key = { _, tab ->
                    tab.id
                }) { index, tab ->

                    val dismissState = rememberSwipeToDismissBoxState(
                        confirmValueChange = { value ->
                            if (value == SwipeToDismissBoxValue.EndToStart) {
                                stateModel.removeTab(tab)
                                true
                            } else {
                                false
                            }
                        }
                    )


                    SwipeToDismissBox(
                        modifier = Modifier.animateContentSize(),
                        state = dismissState,
                        backgroundContent = {

                            Box(
                                modifier = Modifier
                                    .fillMaxSize()
                                    .background(MaterialTheme.colorScheme.errorContainer),
                                contentAlignment = Alignment.CenterEnd

                            ) {
                                Icon(
                                    Icons.Filled.Delete,
                                    stringResource(id = android.R.string.untitled),
                                    modifier = Modifier.minimumInteractiveComponentSize()
                                )
                            }
                        },
                        enableDismissFromEndToStart = true,
                        enableDismissFromStartToEnd = false,
                    ) {
                        TabItem(stateModel, pagerState, tab, index, onDismissRequest)
                    }

                }
            }
        }
    )

    LaunchedEffect(pagerState.currentPage) {
        listState.animateScrollToItem(pagerState.currentPage)
    }
}

@Composable
fun TabItem(
    stateModel: StateModel,
    pagerState: PagerState,
    tab: Tab,
    index: Int,
    onDismissRequest: () -> Unit = {},
) {

    val scope = rememberCoroutineScope()
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .focusable(false)
            .clickable {
                scope.launch {
                    pagerState.animateScrollToPage(index)
                    onDismissRequest.invoke() // todo remove when sheet dialog
                }
            }
            .background(
                if (pagerState.currentPage == index) {
                    MaterialTheme.colorScheme.secondaryContainer
                } else {
                    MaterialTheme.colorScheme.surface
                }
            )
    ) {
        Row(
            modifier = Modifier
                .padding(16.dp)
                .fillMaxWidth()
        ) {
            val image = tab.bitmap()

            if (image != null) {
                Image(
                    image.asImageBitmap(),
                    contentDescription = stringResource(id = R.string.tabs),
                    modifier = Modifier
                        .size(24.dp)
                        .align(
                            Alignment.CenterVertically
                        )
                )
            } else {
                Image(
                    painterResource(id = stateModel.bookmarkIcon(tab.url)),
                    contentDescription = stringResource(id = R.string.tabs),
                    modifier = Modifier
                        .size(24.dp)
                        .align(
                            Alignment.CenterVertically
                        )
                )
            }
            Column(
                modifier = Modifier
                    .padding(16.dp, 0.dp, 16.dp, 0.dp)
                    .weight(1.0f, true)
            ) {
                Text(
                    text = tab.title, maxLines = 1, modifier = Modifier.fillMaxWidth(),
                    style = MaterialTheme.typography.bodyMedium,
                    softWrap = false
                )

                Text(
                    text = stateModel.cleanUrl(tab.url),
                    maxLines = 2,
                    modifier = Modifier.fillMaxWidth(),
                    style = MaterialTheme.typography.labelSmall,
                    softWrap = false
                )
            }
        }
    }
}