package threads.thor.ui

import android.webkit.WebView
import android.webkit.WebViewDatabase
import androidx.compose.foundation.Image
import androidx.compose.foundation.focusable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.WindowInsets
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.ArrowBack
import androidx.compose.material.icons.filled.Close
import androidx.compose.material3.BasicAlertDialog
import androidx.compose.material3.Button
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Switch
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.minimumInteractiveComponentSize
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import threads.thor.R
import threads.thor.data.AuthRequest
import threads.thor.state.StateModel


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun HttpAuthDialog(
    stateModel: StateModel,
    authRequest: AuthRequest,
    onDismissRequest: () -> Unit
) {

    val database = WebViewDatabase.getInstance(LocalContext.current)
    val data = database.getHttpAuthUsernamePassword(authRequest.host, authRequest.realm)


    var storedName: String by remember {
        mutableStateOf(
            if (data != null) {
                data[0]
            } else {
                ""
            }
        )
    }
    var storedPass: String by remember {
        mutableStateOf(
            if (data != null) {
                data[1]
            } else {
                ""
            }
        )
    }

    BasicAlertDialog(onDismissRequest) {

        Card(
            elevation = CardDefaults.cardElevation(
                defaultElevation = 6.dp
            )
        ) {
            val focusRequester = remember { FocusRequester() }
            LaunchedEffect(Unit) {
                focusRequester.requestFocus()
            }

            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(16.dp)
            ) {
                TextField(

                    modifier = Modifier
                        .focusRequester(focusRequester)
                        .fillMaxWidth(),
                    label = {
                        Text(text = stringResource(R.string.user_name))
                    },
                    singleLine = true,
                    keyboardOptions = KeyboardOptions(
                        autoCorrectEnabled = true,
                        keyboardType = KeyboardType.Text,
                    ),
                    value = storedName, maxLines = 1, onValueChange = { value ->
                        storedName = value
                    })

                TextField(
                    modifier = Modifier.fillMaxWidth(),
                    label = {
                        Text(text = stringResource(R.string.password))
                    },
                    singleLine = true,
                    keyboardOptions = KeyboardOptions(
                        autoCorrectEnabled = true,
                        keyboardType = KeyboardType.Password
                    ),
                    value = storedPass, maxLines = 1, onValueChange = { value ->
                        storedPass = value
                    })
                Row(
                    modifier = Modifier
                        .padding(8.dp)
                        .fillMaxWidth()
                ) {
                    Button(onClick = {
                        authRequest.handler.cancel()
                        onDismissRequest.invoke()
                    }) {
                        Text(
                            text = stringResource(id = android.R.string.cancel),
                        )
                    }
                    Box(modifier = Modifier.weight(1.0f, true))
                    Button(onClick = {
                        database.setHttpAuthUsernamePassword(
                            authRequest.host, authRequest.realm, storedName, storedPass
                        )

                        authRequest.handler.proceed(storedName, storedPass)
                        onDismissRequest.invoke()
                    }
                    ) {
                        Text(
                            text = stringResource(id = android.R.string.ok),
                        )
                    }
                }
            }
        }
    }
}


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun InfoView(
    stateModel: StateModel,
    webViewState: WebViewState,
    onDismissRequest: () -> Unit = {},
) {

    val url = remember { stateModel.cleanUrl(webViewState.lastLoadedUrl) }
    val title = remember { stateModel.infoTitle(webViewState.pageTitle, url) }
    val qrCode = remember { stateModel.getBitmap(url) }
    val linkIconRes = remember { stateModel.infoIcon(url) }
    val isEnabled = remember { !stateModel.isFileUri(url) }
    var isChecked by remember { mutableStateOf(false) }
    val homepage by stateModel.homepageUri().collectAsState("")

    homepage.let { page ->
        isChecked = if (page.isBlank()) {
            false
        } else {
            page == url
        }
    }


    Scaffold(
        topBar = {
            TopAppBar(
                navigationIcon = {
                    IconButton(onClick = {
                        onDismissRequest.invoke()
                    }) {
                        Icon(
                            Icons.AutoMirrored.Filled.ArrowBack,
                            contentDescription = stringResource(id = android.R.string.cancel)
                        )
                    }
                },
                windowInsets = WindowInsets(0.dp, 0.dp, 0.dp, 0.dp),
                title = {
                    Text(stringResource(id = R.string.information))
                },
            )
        }, content = { padding ->
            Column(modifier = Modifier.padding(padding)) {

                Text(
                    text = title,
                    style = MaterialTheme.typography.labelLarge,
                    textAlign = TextAlign.Center,
                    modifier = Modifier
                        .padding(16.dp)
                        .fillMaxWidth()
                        .align(Alignment.CenterHorizontally)
                )

                Image(
                    bitmap = qrCode.asImageBitmap(),
                    contentDescription = stringResource(id = R.string.url_access),
                    modifier = Modifier
                        .size(240.dp)
                        .fillMaxWidth()
                        .align(Alignment.CenterHorizontally)
                )

                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(16.dp)
                ) {
                    val bitmap = webViewState.pageIcon
                    if (bitmap != null) {
                        Image(
                            bitmap = bitmap.asImageBitmap(),
                            contentDescription = stringResource(id = android.R.string.untitled),
                            modifier = Modifier
                                .size(24.dp)
                                .align(Alignment.CenterVertically)
                        )
                    } else {
                        Image(
                            painterResource(id = linkIconRes),
                            contentDescription = stringResource(id = android.R.string.untitled),
                            modifier = Modifier
                                .size(24.dp)
                                .align(Alignment.CenterVertically)
                        )
                    }
                    Text(
                        text = url,
                        style = MaterialTheme.typography.labelMedium,
                        textAlign = TextAlign.Start,
                        modifier = Modifier
                            .weight(1.0f, true)
                            .focusable(false)
                            .padding(8.dp, 0.dp, 0.dp, 0.dp)
                            .align(Alignment.CenterVertically)
                    )
                }

                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(16.dp)
                ) {
                    Switch(
                        enabled = isEnabled,
                        checked = isChecked,
                        onCheckedChange = { isChecked ->

                            if (isChecked) {
                                stateModel.setHomepage(url, title)
                            } else {
                                stateModel.resetHomepage()
                            }
                        })
                    Text(
                        text = stringResource(R.string.home_page_set),
                        modifier = Modifier
                            .align(Alignment.CenterVertically)
                            .padding(16.dp, 0.dp, 16.dp, 0.dp)
                    )
                }

                Spacer(modifier = Modifier.minimumInteractiveComponentSize())
            }
        }
    )
}


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun SearchView(
    stateModel: StateModel,
    webView: WebView,
    onDismissRequest: () -> Unit = {}
) {

    var numbers by remember { mutableStateOf("0/0") }
    var text by remember { mutableStateOf("") }

    val focusRequester = remember { FocusRequester() }
    LaunchedEffect(Unit) {
        focusRequester.requestFocus()
    }


    TopAppBar(

        actions = {
            IconButton(modifier = Modifier.align(Alignment.CenterVertically),
                onClick = {
                    webView.findNext(false)
                }) {
                Icon(
                    painterResource(id = R.drawable.chevron_up),
                    contentDescription = stringResource(id = R.string.previous_search)
                )
            }

            IconButton(modifier = Modifier.align(Alignment.CenterVertically),
                onClick = {
                    webView.findNext(true)
                }) {
                Icon(
                    painterResource(id = R.drawable.chevron_down),
                    contentDescription = stringResource(id = R.string.next_search)
                )
            }

            IconButton(modifier = Modifier.align(Alignment.CenterVertically), onClick = {
                webView.clearMatches()
                webView.setFindListener(null)
                onDismissRequest.invoke()
            }) {
                Icon(
                    Icons.Filled.Close,
                    contentDescription = stringResource(id = android.R.string.cancel)
                )
            }
        },

        title = {
            Row(modifier = Modifier.fillMaxWidth()) {
                TextField(
                    shape = MaterialTheme.shapes.extraLarge,
                    colors = TextFieldDefaults.colors(
                        focusedIndicatorColor = Color.Transparent,
                        unfocusedIndicatorColor = Color.Transparent,
                        disabledIndicatorColor = Color.Transparent,
                    ),
                    textStyle = MaterialTheme.typography.titleSmall,
                    leadingIcon = {
                        Icon(
                            painterResource(id = R.drawable.pencil),
                            contentDescription = stringResource(id = android.R.string.cancel)
                        )
                    },
                    modifier = Modifier
                        .focusRequester(focusRequester)
                        .weight(1.0f, true),

                    placeholder = {
                        Text(
                            text = stringResource(R.string.enter_search),
                            style = MaterialTheme.typography.titleSmall,
                            color = MaterialTheme.colorScheme.primary
                        )
                    },
                    singleLine = true,
                    keyboardOptions = KeyboardOptions(
                        autoCorrectEnabled = true,
                        keyboardType = KeyboardType.Text,
                        imeAction = ImeAction.Search
                    ),
                    keyboardActions = KeyboardActions(onSearch = {
                        webView.clearMatches()
                        webView.setFindListener(null)
                        onDismissRequest.invoke()
                    }),
                    value = text,
                    maxLines = 1,
                    onValueChange = { value ->
                        text = value
                        webView.findAllAsync(value)
                    })


                Text(
                    modifier = Modifier
                        .padding(8.dp)
                        .align(Alignment.CenterVertically),
                    style = MaterialTheme.typography.titleSmall,
                    text = numbers
                )


            }
        })

    webView.setFindListener { activeMatchOrdinal: Int, numberOfMatches: Int, _: Boolean ->
        numbers = "$activeMatchOrdinal/$numberOfMatches"
    }
}


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun SearchWeb(
    stateModel: StateModel,
    tabId: Long,
    onDismissRequest: () -> Unit = {}
) {
    val focusRequester = remember { FocusRequester() }
    var query by remember { mutableStateOf("") }

    LaunchedEffect(Unit) {
        focusRequester.requestFocus()
    }


    TopAppBar(

        actions = {
            IconButton(onClick = {
                onDismissRequest.invoke()
            }) {
                Icon(
                    Icons.Filled.Close,
                    contentDescription = stringResource(id = android.R.string.cancel)
                )
            }
        },

        title = {
            TextField(
                shape = MaterialTheme.shapes.extraLarge,
                textStyle = MaterialTheme.typography.titleSmall,
                leadingIcon = {
                    Icon(
                        painterResource(id = R.drawable.pencil),
                        contentDescription = stringResource(id = android.R.string.untitled)
                    )
                },
                colors = TextFieldDefaults.colors(
                    focusedIndicatorColor = Color.Transparent,
                    unfocusedIndicatorColor = Color.Transparent,
                    disabledIndicatorColor = Color.Transparent,
                ),
                modifier = Modifier
                    .focusRequester(focusRequester)
                    .fillMaxWidth(),
                placeholder = {
                    Text(
                        text = stringResource(R.string.search_uri),
                        style = MaterialTheme.typography.titleSmall,
                        color = MaterialTheme.colorScheme.primary
                    )
                },
                singleLine = true,
                keyboardOptions = KeyboardOptions(
                    autoCorrectEnabled = true,
                    keyboardType = KeyboardType.Text,
                    imeAction = ImeAction.Search
                ),
                keyboardActions = KeyboardActions(onSearch = {
                    stateModel.doSearch(tabId, query)
                    onDismissRequest.invoke()
                }),
                value = query,
                maxLines = 1,
                onValueChange = { value ->
                    query = value
                })


        })
}
