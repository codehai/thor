package threads.thor.ui

import android.webkit.WebHistoryItem
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.WindowInsets
import androidx.compose.foundation.layout.absolutePadding
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.ArrowBack
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.minimumInteractiveComponentSize
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import threads.thor.R
import threads.thor.model.getUriTitle
import threads.thor.state.StateModel


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun HistoriesView(
    stateModel: StateModel,
    tabId: Long,
    list: List<WebHistoryItem>,
    onDismissRequest: () -> Unit = {},
) {

    Scaffold(
        topBar = {
            TopAppBar(
                navigationIcon = {
                    IconButton(onClick = {
                        onDismissRequest.invoke()
                    }) {
                        Icon(
                            Icons.AutoMirrored.Filled.ArrowBack,
                            contentDescription = stringResource(id = android.R.string.cancel)
                        )
                    }
                },
                windowInsets = WindowInsets(0.dp, 0.dp, 0.dp, 0.dp),
                title = {
                    Text(stringResource(id = R.string.history))
                },
            )
        }, content = { padding ->
            LazyColumn(modifier = Modifier.padding(padding)) {
                items(items = list.reversed()) { item ->
                    WebHistoryItem(stateModel, tabId, item, onDismissRequest)
                }
            }
        }
    )
}

@Composable
fun WebHistoryItem(
    stateModel: StateModel,
    tabId: Long,
    history: WebHistoryItem,
    onDismissRequest: () -> Unit = {},
) {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .background(MaterialTheme.colorScheme.surface)
            .clickable {
                stateModel.showHistoryPage(tabId, history)
                onDismissRequest.invoke()
            },
    ) {
        Row(
            modifier = Modifier
                .padding(16.dp)
                .fillMaxWidth()
                .minimumInteractiveComponentSize()
        ) {

            if (history.favicon != null) {
                Image(
                    history.favicon!!.asImageBitmap(),
                    contentDescription = stringResource(id = R.string.history),
                    modifier = Modifier
                        .size(24.dp)
                        .align(
                            Alignment.CenterVertically
                        )
                )
            } else {
                Image(
                    painterResource(id = stateModel.bookmarkIcon(history.url)),
                    contentDescription = stringResource(id = R.string.history),
                    modifier = Modifier
                        .size(24.dp)
                        .align(
                            Alignment.CenterVertically
                        )
                )
            }
            Column(modifier = Modifier.absolutePadding(16.dp)) {
                Text(
                    text = if (history.title.isNullOrBlank()) {
                        getUriTitle(history.url)
                    } else {
                        history.title
                    },
                    maxLines = 1,
                    modifier = Modifier.fillMaxWidth(),
                    style = MaterialTheme.typography.bodyMedium, softWrap = false
                )
                Text(
                    text = stateModel.cleanUrl(history.url), maxLines = 2,
                    modifier = Modifier.fillMaxWidth(),
                    style = MaterialTheme.typography.labelSmall, softWrap = false
                )
            }
        }
    }
}
