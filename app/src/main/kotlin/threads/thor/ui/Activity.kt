package threads.thor.ui

import android.Manifest
import android.app.Activity
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.ConnectivityManager
import android.net.ConnectivityManager.NetworkCallback
import android.net.Network
import android.net.NetworkCapabilities
import android.net.NetworkRequest
import android.net.Uri
import android.net.http.SslError
import android.os.Build
import android.os.Bundle
import android.os.Message
import android.print.PrintAttributes
import android.print.PrintManager
import android.provider.DocumentsContract
import android.view.View
import android.view.ViewGroup
import android.webkit.ConsoleMessage
import android.webkit.CookieManager
import android.webkit.HttpAuthHandler
import android.webkit.JsPromptResult
import android.webkit.PermissionRequest
import android.webkit.SslErrorHandler
import android.webkit.ValueCallback
import android.webkit.WebResourceError
import android.webkit.WebResourceRequest
import android.webkit.WebResourceResponse
import android.webkit.WebSettings
import android.webkit.WebView
import android.widget.FrameLayout
import androidx.activity.ComponentActivity
import androidx.activity.SystemBarStyle
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.compose.setContent
import androidx.activity.enableEdgeToEdge
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.focusable
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.pager.HorizontalPager
import androidx.compose.foundation.pager.rememberPagerState
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.ArrowBack
import androidx.compose.material.icons.automirrored.filled.ArrowForward
import androidx.compose.material.icons.filled.Refresh
import androidx.compose.material.icons.filled.Share
import androidx.compose.material.icons.outlined.MoreVert
import androidx.compose.material3.Card
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.DropdownMenu
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.ModalBottomSheet
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SnackbarDuration
import androidx.compose.material3.SnackbarHost
import androidx.compose.material3.SnackbarHostState
import androidx.compose.material3.SnackbarResult
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.material3.VerticalDivider
import androidx.compose.material3.dynamicDarkColorScheme
import androidx.compose.material3.dynamicLightColorScheme
import androidx.compose.material3.lightColorScheme
import androidx.compose.material3.minimumInteractiveComponentSize
import androidx.compose.material3.pulltorefresh.PullToRefreshBox
import androidx.compose.material3.pulltorefresh.rememberPullToRefreshState
import androidx.compose.material3.rememberModalBottomSheetState
import androidx.compose.material3.windowsizeclass.ExperimentalMaterial3WindowSizeClassApi
import androidx.compose.material3.windowsizeclass.WindowSizeClass
import androidx.compose.material3.windowsizeclass.WindowWidthSizeClass
import androidx.compose.material3.windowsizeclass.calculateWindowSizeClass
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.toArgb
import androidx.compose.ui.input.nestedscroll.nestedScroll
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalView
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.core.view.WindowCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.WindowInsetsControllerCompat
import androidx.lifecycle.viewmodel.compose.viewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import tech.lp2p.Lite
import tech.lp2p.core.Header
import tech.lp2p.core.MimeType
import tech.lp2p.core.Scheme
import threads.thor.R
import threads.thor.data.AuthRequest
import threads.thor.model.bootstrap
import threads.thor.model.createEmptyResource
import threads.thor.model.createErrorMessage
import threads.thor.model.error
import threads.thor.model.getFileName
import threads.thor.model.parseContentDisposition
import threads.thor.state.StateModel


class Activity : ComponentActivity() {

    private val TAG: String = Activity::class.java.simpleName

    @Composable
    fun SystemBarTheme(useDarkTheme: Boolean = isSystemInDarkTheme()) {
        val barColor = Color.Transparent.toArgb()
        LaunchedEffect(key1 = useDarkTheme) {
            if (useDarkTheme) {
                enableEdgeToEdge(statusBarStyle = SystemBarStyle.dark(barColor))
            } else {
                enableEdgeToEdge(statusBarStyle = SystemBarStyle.light(barColor, barColor))
            }
        }
    }

    @Composable
    fun KeepScreenOn() {
        val currentView = LocalView.current
        DisposableEffect(Unit) {
            currentView.keepScreenOn = true
            onDispose {
                currentView.keepScreenOn = false
            }
        }
    }


    @Composable
    fun AppTheme(useDarkTheme: Boolean = isSystemInDarkTheme(), content: @Composable () -> Unit) {
        val context = LocalContext.current

        val lightingColorScheme =
            lightColorScheme(primary = Color(0xFF2E5D49)) // todo primary color

        val colorScheme =
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
                if (useDarkTheme)
                    dynamicDarkColorScheme(context) else dynamicLightColorScheme(context)
            } else {
                lightingColorScheme
            }

        MaterialTheme(
            colorScheme = colorScheme,
            content = content
        )
    }

    private fun clearWebView(webView: WebView) {
        webView.clearHistory()
        webView.clearCache(true)
        webView.clearFormData()
    }

    fun network(stateModel: StateModel, connectivityManager: ConnectivityManager) {
        val networkCallback: NetworkCallback =
            object : NetworkCallback() {
                override fun onAvailable(network: Network) {
                    super.onAvailable(network)
                    stateModel.online = true
                }

                override fun onLost(network: Network) {
                    super.onLost(network)
                    stateModel.online = false
                }
            }

        val networkRequest = NetworkRequest.Builder()
            .addCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)
            .addTransportType(NetworkCapabilities.TRANSPORT_WIFI)
            .addTransportType(NetworkCapabilities.TRANSPORT_CELLULAR)
            .addTransportType(NetworkCapabilities.TRANSPORT_ETHERNET)
            .build()

        connectivityManager.registerNetworkCallback(networkRequest, networkCallback)

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        val connectivityManager = getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager
        enableEdgeToEdge()
        setContent {
            val stateModel: StateModel = viewModel()
            network(stateModel, connectivityManager)
            KeepScreenOn()
            SystemBarTheme()

            AppTheme {
                MainView(stateModel, savedInstanceState)
            }
        }
    }


    @OptIn(ExperimentalMaterial3WindowSizeClassApi::class, ExperimentalMaterial3Api::class)
    @Composable
    fun MainView(stateModel: StateModel, savedInstanceState: Bundle?) {

        val uri by remember { mutableStateOf(stateModel.intentUri(intent)) }

        val ids by stateModel.ids().collectAsState(initial = emptyList())

        val pagerState =
            rememberPagerState(pageCount = { ids.size }) // TODO this does not look good
        val windowSize = calculateWindowSizeClass(this)


        // todo understand when LaunchedEffect is used
        if (uri != null) {
            error(TAG, "create tab $uri")
            stateModel.showPage(uri!!.toString())
        } else {
            error(TAG, "create empty tab maybe")
            stateModel.ensureHasTab()
        }


        HorizontalPager(
            state = pagerState,
            userScrollEnabled = false,
            beyondViewportPageCount = 20,
            key = { index ->
                ids[index]
            }
        ) { page ->

            val id = ids[page]

            error(TAG, "HorizontalPager recompose $page to $id current " + pagerState.currentPage)
            MainPage(stateModel, savedInstanceState, windowSize, id, page)
        }

        if (stateModel.showTabs) {

            val sheetState = rememberModalBottomSheetState(
                skipPartiallyExpanded = false
            )
            // todo should be a SideSheet
            ModalBottomSheet(
                modifier = Modifier.fillMaxHeight(),
                sheetState = sheetState,
                onDismissRequest = { stateModel.showTabs = false }) {

                // todo
                TabsView(stateModel, pagerState,
                    onDismissRequest = { stateModel.showTabs = false })
            }
        }
    }

    private fun share(url: String) {
        try {
            val names = arrayOf(ComponentName(this, Activity::class.java))

            val intent = Intent(Intent.ACTION_SEND)
            intent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.share_link))
            intent.putExtra(Intent.EXTRA_TEXT, url)
            intent.setType(MimeType.PLAIN_MIME_TYPE.name)
            intent.putExtra(DocumentsContract.EXTRA_EXCLUDE_SELF, true)
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)


            val chooser = Intent.createChooser(intent, getText(R.string.share))
            chooser.putExtra(Intent.EXTRA_EXCLUDE_COMPONENTS, names)
            startActivity(chooser)
        } catch (throwable: Throwable) {
            error(TAG, throwable)
        }
    }


    private fun createPrintJob(webView: WebView) {
        try {
            val printManager = getSystemService(Context.PRINT_SERVICE) as PrintManager
            val jobName = getString(R.string.app_name)
            val printDocumentAdapter = webView.createPrintDocumentAdapter(jobName)
            printManager.print(
                jobName, printDocumentAdapter,
                PrintAttributes.Builder().build()
            )
        } catch (throwable: Throwable) {
            error(TAG, throwable)
        }
    }

    // todo
    fun onPause1() {
        // todo super.onPause()
        // webView!!.onPause()
    }

    // todo
    fun onResume1() {
        // todo super.onResume()
        //  webView!!.onResume()
    }


    private inner class CustomWebViewClient(
        private val stateModel: StateModel,
        private val tabId: Long,
        private val onDownloadRequest: () -> Unit = {},
        private val onAuthRequest: (AuthRequest) -> Unit = {}
    ) : AccompanistWebViewClient() {

        // todo maybe closed
        private val lite by lazy {
            Lite.newBuilder()
                .bootstrap(bootstrap())
                .blockStore(stateModel.blockStore)
                .peerStore(stateModel.peers)
                .disableServer().build()
        }
        private val adsUrls: MutableMap<Uri, Boolean> = HashMap()


        override fun onReceivedHttpError(
            view: WebView, request: WebResourceRequest,
            errorResponse: WebResourceResponse
        ) {
            super.onReceivedHttpError(view, request, errorResponse)
            error(TAG, "onReceivedHttpError " + errorResponse.reasonPhrase)
        }

        override fun onReceivedSslError(view: WebView, handler: SslErrorHandler, error: SslError) {
            super.onReceivedSslError(view, handler, error)
            error(TAG, "onReceivedSslError $error")
        }


        override fun onPageCommitVisible(view: WebView, url: String) {
            super.onPageCommitVisible(view, url)
            error(TAG, "onPageCommitVisible $url")
        }

        override fun onReceivedHttpAuthRequest(
            view: WebView,
            handler: HttpAuthHandler,
            host: String,
            realm: String
        ) {
            super.onReceivedHttpAuthRequest(view, handler, host, realm)
            onAuthRequest.invoke(AuthRequest(handler, host, realm))
        }


        override fun onLoadResource(view: WebView, url: String) {
            super.onLoadResource(view, url)
            error(TAG, "onLoadResource : $url")
        }

        override fun doUpdateVisitedHistory(view: WebView, url: String, isReload: Boolean) {
            super.doUpdateVisitedHistory(view, url, isReload)
            error(TAG, "doUpdateVisitedHistory : $url $isReload")
        }

        override fun onPageStarted(view: WebView, url: String, favicon: Bitmap?) {
            super.onPageStarted(view, url, favicon)
            error(TAG, "onPageStarted : $url")

            stateModel.imageTab(tabId, url, favicon)
        }


        override fun onPageFinished(view: WebView, url: String) {
            super.onPageFinished(view, url)
            error(TAG, "onPageFinished : $url")

            stateModel.checkOnline()

        }

        override fun onReceivedError(
            view: WebView,
            request: WebResourceRequest,
            error: WebResourceError
        ) {
            super.onReceivedError(view, request, error)
            error(
                TAG, "onReceivedError " + view.url + " " +
                        error.description + " " + error.errorCode
            )
        }


        override fun shouldOverrideUrlLoading(view: WebView, request: WebResourceRequest): Boolean {
            try {
                val uri = request.url

                error(TAG, "shouldOverrideUrlLoading : $uri")

                when (uri.scheme) {
                    Scheme.about.name -> {
                        error(TAG, "about scheme $uri")
                        return true
                    }

                    Scheme.magnet.name -> {
                        stateModel.magnetDownloader(uri)
                        onDownloadRequest.invoke()
                        return true
                    }

                    Scheme.pns.name -> {
                        val name = uri.getQueryParameter(Header.CONTENT_DOWNLOAD)
                        if (name != null) {
                            stateModel.contentDownloader(uri, name)
                            onDownloadRequest.invoke()
                            return true
                        }
                        return false
                    }

                    Scheme.http.name, Scheme.https.name -> {
                        return false
                    }

                    else -> {
                        // all other stuff
                        try {
                            val intent = Intent(Intent.ACTION_VIEW, uri)
                            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                            startActivity(intent)
                        } catch (ignore: Throwable) {
                            error(TAG, "Not  handled uri $uri")
                        }
                        return true
                    }
                }
            } catch (throwable: Throwable) {
                error(TAG, throwable)
            }

            return false
        }


        override fun shouldInterceptRequest(
            view: WebView,
            request: WebResourceRequest
        ): WebResourceResponse? {
            val uri = request.url

            error(TAG, "shouldInterceptRequest : $uri")

            if (uri.scheme == Scheme.http.name || uri.scheme == Scheme.https.name) {
                var advertisement = false
                if (!adsUrls.containsKey(uri)) {
                    advertisement = stateModel.isAd(uri)
                    adsUrls[uri] = advertisement
                } else {
                    val value = adsUrls[uri]
                    if (value != null) {
                        advertisement = value
                    }
                }

                return if (advertisement) {
                    createEmptyResource()
                } else {
                    null
                }
            } else if (uri.scheme == Scheme.pns.name) {
                return try {
                    stateModel.response(lite, tabId, uri)
                } catch (throwable: Throwable) {
                    createErrorMessage(throwable)
                }
            }
            return null
        }
    }

    private inner class CustomWebChromeClient(
        private val stateModel: StateModel,
        private val tabId: Long,
        private val onPermissionRequest: (PermissionRequest) -> Unit = {},
        private val onFilePickerRequest: (ValueCallback<Array<Uri>>) -> Unit = {}
    ) : AccompanistWebChromeClient() {
        private var mCustomView: View? = null
        private var mCustomViewCallback: CustomViewCallback? = null


        override fun onHideCustomView() {
            super.onHideCustomView()

            error(TAG, "onHideCustomView begin")

            (this@Activity.window.decorView as FrameLayout).removeView(this.mCustomView)
            this.mCustomView = null

            mCustomViewCallback!!.onCustomViewHidden()
            this.mCustomViewCallback = null

            error(TAG, "onHideCustomView done")
        }

        override fun onCreateWindow(
            view: WebView,
            dialog: Boolean,
            userGesture: Boolean,
            resultMsg: Message
        ): Boolean {
            val result = view.hitTestResult
            val data = result.extra
            val context = view.context
            val browserIntent =
                Intent(Intent.ACTION_VIEW, Uri.parse(data), context, Activity::class.java)
            context.startActivity(browserIntent)
            return false
        }

        override fun onRequestFocus(view: WebView) {
            error(TAG, "onRequestFocus " + view.url)
        }

        override fun onCloseWindow(window: WebView) {
            error(TAG, "onCloseWindow ")
        }

        override fun onJsPrompt(
            view: WebView, url: String, message: String,
            defaultValue: String, result: JsPromptResult
        ): Boolean {
            error(TAG, "onJsPrompt ")
            return false
        }

        override fun onShowCustomView(
            paramView: View,
            paramCustomViewCallback: CustomViewCallback
        ) {
            super.onShowCustomView(paramView, paramCustomViewCallback)
            error(TAG, "onShowCustomView begin")

            this.mCustomView = paramView
            this.mCustomViewCallback = paramCustomViewCallback
            (this@Activity.window.decorView as FrameLayout)
                .addView(
                    this.mCustomView, FrameLayout.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT
                    )
                )
            error(TAG, "onShowCustomView done")
        }

        private fun hideSystemUI() {
            WindowCompat.setDecorFitsSystemWindows(this@Activity.window, false)
            val controller = WindowInsetsControllerCompat(
                this@Activity.window, mCustomView!!
            )
            controller.hide(WindowInsetsCompat.Type.systemBars())
            controller.systemBarsBehavior =
                WindowInsetsControllerCompat.BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE
        }

        private fun showSystemUI() {
            WindowCompat.setDecorFitsSystemWindows(this@Activity.window, true)
            WindowInsetsControllerCompat(this@Activity.window, this@Activity.window.decorView)
                .show(WindowInsetsCompat.Type.systemBars())
        }

        override fun getDefaultVideoPoster(): Bitmap {
            return Bitmap.createBitmap(10, 10, Bitmap.Config.ARGB_8888)
        }

        override fun onReceivedTitle(view: WebView, title: String) {
            super.onReceivedTitle(view, title)
            error(TAG, "onReceivedTitle " + title + " " + view.url)

            view.url?.let { stateModel.titleTab(tabId, it, title) }
        }


        override fun onReceivedTouchIconUrl(
            view: WebView, url: String,
            precomposed: Boolean
        ) {
            error(TAG, "onReceivedTouchIconUrl")
        }

        override fun onReceivedIcon(view: WebView, icon: Bitmap) {
            super.onReceivedIcon(view, icon)
            error(TAG, "onReceivedIcon " + view.url)
            view.url?.let { stateModel.imageTab(tabId, it, icon) }
        }

        override fun onShowFileChooser(
            webView: WebView,
            filePathCallback: ValueCallback<Array<Uri>>,
            fileChooserParams: FileChooserParams
        ): Boolean {
            onFilePickerRequest.invoke(filePathCallback)
            return true
        }

        override fun onPermissionRequest(request: PermissionRequest) {
            val permission = stateModel.permission(request)
            if (permission.isEmpty()) {
                request.deny()
                return
            }

            if (Manifest.permission.CAMERA == permission) {
                if (!this@Activity.packageManager.hasSystemFeature(
                        PackageManager.FEATURE_CAMERA_ANY
                    )
                ) {
                    request.deny()
                    return
                }
            }
            onPermissionRequest.invoke(request)
        }

        override fun onPermissionRequestCanceled(request: PermissionRequest) {
            super.onPermissionRequestCanceled(request)
            error(TAG, "onPermissionRequestCanceled $request")
        }

        override fun onConsoleMessage(consoleMessage: ConsoleMessage): Boolean {
            error(TAG, "onConsoleMessage " + consoleMessage.message())
            return super.onConsoleMessage(consoleMessage)
        }

        override fun getVideoLoadingProgressView(): View? {
            error(TAG, "getVideoLoadingProgressView ")
            return null
        }

        override fun getVisitedHistory(callback: ValueCallback<Array<String>>) {
            error(TAG, "getVisitedHistory ")
        }

    }

    @OptIn(ExperimentalMaterial3Api::class)
    @Composable
    fun MainPage(
        stateModel: StateModel,
        savedInstanceState: Bundle?,
        windowSize: WindowSizeClass,
        tabId: Long,
        pageIndex: Int,
    ) {

        error(TAG, "MainPage TabIdx $tabId")

        val webViewState = rememberSaveableWebViewState()
        webViewState.viewState = savedInstanceState // todo evaluate
        val webViewNavigator = rememberWebViewNavigator()
        var filePickRequest: ValueCallback<Array<Uri>>? by remember { mutableStateOf(null) }

        val filePickerLauncher = rememberLauncherForActivityResult(
            ActivityResultContracts.GetContent()
        ) { uri ->
            if (filePickRequest != null) {
                if (uri != null) {
                    filePickRequest!!.onReceiveValue(arrayOf(uri))
                } else {
                    filePickRequest!!.onReceiveValue(null)
                }
            }
            filePickRequest = null
        }
        if (filePickRequest != null) {
            filePickerLauncher.launch(MimeType.ALL.name)
        }

        var permissionRequest: PermissionRequest? by remember { mutableStateOf(null) }

        val permissionLauncher = rememberLauncherForActivityResult(
            ActivityResultContracts.RequestPermission()
        ) { isGranted: Boolean ->
            try {
                if (isGranted) {
                    permissionRequest!!.grant(
                        permissionRequest!!.resources
                    )
                } else {
                    permissionRequest!!.deny()
                }
            } finally {
                permissionRequest = null
            }
        }
        var authRequest: AuthRequest? by remember { mutableStateOf(null) }
        if (authRequest != null) {
            HttpAuthDialog(stateModel, authRequest!!,
                onDismissRequest = { authRequest = null })
        }


        if (permissionRequest != null) {
            permissionLauncher.launch(
                stateModel.permission(permissionRequest!!)
            )
        }


        // todo may possible without webView storage
        var webView: WebView? by remember { mutableStateOf(null) }


        val page = stateModel.pages[tabId]
        if (!page.isNullOrBlank()) {
            error(TAG, "!!! Load TAB $tabId $page") // todo remove
            webViewNavigator.stopLoading()
            webViewNavigator.loadUrl(page)
            stateModel.removePage(tabId)
        }

        val tabTitle by stateModel.title(tabId).collectAsState("")
        val hasActiveTasks by stateModel.activeTasks.collectAsState(false)


        val pullToRefreshState = rememberPullToRefreshState()

        val hasBookmark by stateModel
            .hasBookmark(webViewState.lastLoadedUrl)
            .collectAsState(false)


        webView?.let {
            webView!!.setNetworkAvailable(stateModel.online)
        }

        if (stateModel.online) {
            webView?.let {
                stateModel.offlineMode = stateModel.isFileUri(webViewState.lastLoadedUrl)
                webView!!.settings.cacheMode = WebSettings.LOAD_DEFAULT
            }
        } else {
            stateModel.offlineMode = true
            webView?.let {
                webView!!.settings.cacheMode = WebSettings.LOAD_CACHE_ONLY
            }
        }


        val snackbarHostState = remember { SnackbarHostState() }
        var warning: String by remember { mutableStateOf("") }

        if (warning.isNotBlank()) {
            LaunchedEffect(warning) {
                val result = snackbarHostState.showSnackbar(
                    message = warning,
                    duration = SnackbarDuration.Short
                )
                warning = when (result) {
                    SnackbarResult.Dismissed -> {
                        ""
                    }

                    SnackbarResult.ActionPerformed -> {
                        ""
                    }
                }
            }
        }

        var showMenu by remember { mutableStateOf(false) }


        val enableJavascript by stateModel.isJavascriptEnabled().collectAsState(true)
        webView?.let { view ->
            view.settings.javaScriptEnabled = enableJavascript
        }

        var isRefreshing by remember { mutableStateOf(false) }

        val scope = rememberCoroutineScope()
        val onRefresh: () -> Unit = {
            isRefreshing = true
            scope.launch {
                webViewNavigator.reload()
                delay(1500)
                isRefreshing = false
            }
        }
        var showInfo: Boolean by remember { mutableStateOf(false) }
        var showHistory: Boolean by remember { mutableStateOf(false) }
        var showBookmarks: Boolean by remember { mutableStateOf(false) }
        var showSettings: Boolean by remember { mutableStateOf(false) }
        var showTasks: Boolean by remember { mutableStateOf(false) }

        var searchView: Boolean by remember { mutableStateOf(false) }
        var searchWeb: Boolean by remember { mutableStateOf(false) }


        val scrollBehavior = TopAppBarDefaults.enterAlwaysScrollBehavior(
            canScroll = {
                !searchView && !searchWeb
            }
        )


        Scaffold(

            snackbarHost = {
                SnackbarHost(hostState = snackbarHostState)
            },
            modifier = Modifier.nestedScroll(scrollBehavior.nestedScrollConnection),
            topBar = {

                if (searchView) {
                    SearchView(stateModel, webView!!,
                        onDismissRequest = { searchView = false })
                } else if (searchWeb) {
                    SearchWeb(stateModel, tabId,
                        onDismissRequest = { searchWeb = false })
                } else {
                    TopAppBar(
                        navigationIcon = {
                            IconButton(onClick = {
                                stateModel.showPrimaryPage(tabId)
                            }) {
                                Icon(
                                    painterResource(id = R.drawable.home_outline),
                                    contentDescription = stringResource(id = R.string.home_page)
                                )
                            }
                        },
                        title = {
                            Card(
                                modifier = Modifier
                                    .fillMaxWidth()
                                    .minimumInteractiveComponentSize(),
                                shape = MaterialTheme.shapes.extraLarge
                            ) {
                                Row {
                                    Box {
                                        if (webViewState.isLoading) {
                                            CircularProgressIndicator(
                                                strokeWidth = 2.dp,
                                                modifier = Modifier.align(Alignment.Center)
                                            )
                                        }
                                        IconButton(
                                            enabled = !webViewState.lastLoadedUrl.isNullOrBlank(),
                                            onClick = {
                                                showInfo = true
                                            }) {
                                            Icon(
                                                painterResource(
                                                    id =
                                                    stateModel.tabIcon(webViewState.lastLoadedUrl)
                                                ),
                                                contentDescription = stringResource(id = R.string.information)
                                            )
                                        }
                                    }

                                    Text(
                                        text =
                                        if (tabTitle.isNullOrBlank()) {
                                            ""
                                        } else {
                                            tabTitle!!
                                        },
                                        maxLines = 1,
                                        softWrap = false,
                                        style = MaterialTheme.typography.titleSmall,
                                        modifier = Modifier
                                            .focusable(false)
                                            .weight(1.0f, true)
                                            .clickable {
                                                searchWeb = true
                                            }
                                            .align(Alignment.CenterVertically)
                                    )

                                }
                            }
                        },
                        actions = {

                            if (windowSize.widthSizeClass == WindowWidthSizeClass.Medium) {
                                // todo fix webView when not available
                                IconButton(enabled = webViewNavigator.canGoBack,
                                    onClick = { webViewNavigator.navigateBack() }
                                ) {
                                    Icon(
                                        Icons.AutoMirrored.Filled.ArrowBack,
                                        contentDescription = stringResource(id = R.string.previous_page)
                                    )
                                }
                            }

                            if (windowSize.widthSizeClass == WindowWidthSizeClass.Medium) {
                                // todo fix webView when not available
                                IconButton(onClick = {
                                    webViewNavigator.reload()
                                }) {
                                    Icon(
                                        Icons.Default.Refresh,
                                        contentDescription = stringResource(id = R.string.reload_page)
                                    )
                                }
                            }

                            if (windowSize.widthSizeClass == WindowWidthSizeClass.Medium) {
                                // todo fix webView when not available
                                IconButton(
                                    enabled = webViewNavigator.canGoForward,
                                    onClick = { webViewNavigator.navigateForward() }
                                ) {
                                    Icon(
                                        Icons.AutoMirrored.Filled.ArrowForward,
                                        contentDescription = stringResource(id = R.string.next_page)
                                    )
                                }
                            }

                            if (windowSize.widthSizeClass == WindowWidthSizeClass.Medium) {
                                VerticalDivider()
                            }

                            IconButton(onClick = {
                                showBookmarks = true
                            }) {
                                Icon(
                                    painterResource(id = R.drawable.bookmark_multiple_outline),
                                    contentDescription = stringResource(id = R.string.bookmarks)
                                )
                            }

                            IconButton(onClick = {
                                stateModel.showTabs = true
                            }) {
                                Icon(
                                    painterResource(
                                        id = stateModel.tabsIconResource(pageIndex + 1)
                                    ),
                                    contentDescription = stringResource(id = R.string.tabs)
                                )
                            }


                            IconButton(
                                enabled = !webViewState.lastLoadedUrl.isNullOrBlank(),
                                onClick = {
                                    showMenu = true
                                }) {
                                Icon(
                                    imageVector = Icons.Outlined.MoreVert,
                                    contentDescription = stringResource(id = R.string.more_options)
                                )
                            }

                            DropdownMenu(
                                expanded = showMenu,
                                onDismissRequest = { showMenu = false })
                            {
                                Row {
                                    if (windowSize.widthSizeClass != WindowWidthSizeClass.Medium) {
                                        IconButton(
                                            enabled = webViewNavigator.canGoForward,
                                            onClick = {
                                                webViewNavigator.navigateForward()
                                                showMenu = false
                                            }) {
                                            Icon(
                                                painterResource(id = R.drawable.arrow_right),
                                                contentDescription = stringResource(id = R.string.next_page)
                                            )
                                        }
                                    }

                                    IconButton(enabled =
                                    webViewState.loadingState == LoadingState.Finished
                                            && stateModel.canBookmark(webViewState.lastLoadedUrl),
                                        onClick = {
                                            stateModel.alterBookmark( // todo clean uri and valid title etc
                                                webViewState.lastLoadedUrl!!,
                                                webViewState.pageTitle,
                                                webViewState.pageIcon,
                                                onWarningRequest = {
                                                    warning = it
                                                }
                                            )

                                            showMenu = false
                                        }) {
                                        Icon(

                                            painterResource(
                                                id = if (hasBookmark) {
                                                    R.drawable.star
                                                } else {
                                                    R.drawable.star_outline
                                                }
                                            ),
                                            contentDescription = stringResource(id = R.string.bookmark)
                                        )
                                    }
                                    IconButton(onClick = {
                                        searchView = true
                                        showMenu = false
                                    }) {
                                        Icon(
                                            painterResource(id = R.drawable.search),
                                            contentDescription = stringResource(id = R.string.find_page)
                                        )
                                    }
                                    IconButton(enabled = stateModel.downloadActive(
                                        webViewState.lastLoadedUrl
                                    ),
                                        onClick = {
                                            stateModel.download(webView!!)
                                            showTasks = true
                                            showMenu = false
                                        }) {
                                        Icon(
                                            painterResource(id = R.drawable.download),
                                            contentDescription = stringResource(id = R.string.download)
                                        )
                                    }
                                    IconButton(enabled =
                                    webViewState.loadingState == LoadingState.Finished &&
                                            !stateModel.isFileUri(webViewState.lastLoadedUrl), // todo
                                        onClick = {
                                            share(
                                                stateModel.cleanUrl(
                                                    webViewState.lastLoadedUrl
                                                )
                                            )
                                            showMenu = false
                                        }) {
                                        Icon(
                                            Icons.Filled.Share,
                                            contentDescription = stringResource(id = R.string.share)
                                        )
                                    }
                                }
                                HorizontalDivider()



                                DropdownMenuItem(
                                    text = { Text(text = stringResource(id = R.string.downloads)) },
                                    trailingIcon = {
                                        Icon(
                                            painterResource(id = R.drawable.progress_download),
                                            contentDescription = stringResource(id = R.string.downloads)
                                        )
                                    },
                                    onClick = {
                                        showTasks = true
                                        showMenu = false
                                    })

                                DropdownMenuItem(
                                    text = { Text(text = stringResource(id = R.string.history)) },
                                    onClick = {
                                        showHistory = true
                                        showMenu = false
                                    })

                                DropdownMenuItem(
                                    text = { Text(text = stringResource(id = R.string.print)) },
                                    trailingIcon = {
                                        Icon(
                                            painterResource(id = R.drawable.printer_outline),
                                            contentDescription = stringResource(id = R.string.print)
                                        )
                                    },
                                    onClick = {
                                        createPrintJob(webView!!)
                                        showMenu = false
                                    })

                                HorizontalDivider()


                                DropdownMenuItem(
                                    text = { Text(text = stringResource(id = R.string.settings)) },
                                    trailingIcon = {
                                        Icon(
                                            painterResource(id = R.drawable.tune),
                                            contentDescription = stringResource(id = R.string.settings)
                                        )
                                    },
                                    onClick = {
                                        showSettings = true
                                        showMenu = false
                                    })

                                DropdownMenuItem(
                                    text = { Text(text = stringResource(id = R.string.documentation)) },
                                    onClick = {
                                        stateModel.showDocumentation(tabId)
                                        showMenu = false
                                    })

                                DropdownMenuItem(
                                    text = { Text(text = stringResource(id = R.string.action_cleanup)) },
                                    trailingIcon = {
                                        Icon(
                                            painterResource(id = R.drawable.broom),
                                            contentDescription = stringResource(id = R.string.action_cleanup)
                                        )
                                    },
                                    onClick = {
                                        try {
                                            // clear web view data
                                            clearWebView(webView!!)
                                            // todo virtual thread
                                            Thread {
                                                stateModel.reset()
                                            }.start()
                                        } finally {
                                            warning =
                                                getString(R.string.browser_cleanup_message)

                                        }
                                        showMenu = false
                                    })
                            }
                        },

                        scrollBehavior = scrollBehavior
                    )
                }


            }) { padding ->


            PullToRefreshBox(
                modifier = Modifier
                    .fillMaxSize()
                    .padding(padding),
                isRefreshing = isRefreshing,
                state = pullToRefreshState,
                onRefresh = onRefresh
            ) {

                //  modifier = Modifier.verticalScroll(state = rememberScrollState())
                Column {

                    if (stateModel.offlineMode) {
                        Text(
                            text = stringResource(R.string.offline_mode),
                            color = MaterialTheme.colorScheme.onPrimary,
                            style = MaterialTheme.typography.labelMedium,
                            textAlign = TextAlign.Center,
                            modifier = Modifier
                                .fillMaxWidth()
                                .background(MaterialTheme.colorScheme.primary)
                                .align(Alignment.CenterHorizontally)
                        )
                    }
                    if (hasActiveTasks) {
                        Text(
                            text = stringResource(R.string.active_downloads),
                            color = MaterialTheme.colorScheme.onPrimary,
                            style = MaterialTheme.typography.labelMedium,
                            textAlign = TextAlign.Center,
                            modifier = Modifier
                                .fillMaxWidth()
                                .background(MaterialTheme.colorScheme.primary)
                                .align(Alignment.CenterHorizontally)
                        )
                    }

                    WebView(
                        state = webViewState,
                        navigator = webViewNavigator,
                        chromeClient = remember {
                            CustomWebChromeClient(stateModel, tabId,
                                onPermissionRequest = {
                                    permissionRequest = it
                                }, onFilePickerRequest = {
                                    filePickRequest = it
                                })
                        },
                        client = remember {
                            CustomWebViewClient(stateModel, tabId,
                                onDownloadRequest = { showTasks = true },
                                onAuthRequest = { authRequest = it })
                        },
                        onCreated = { view ->
                            webView = view

                            error(TAG, "create WebClient")

                            CookieManager.getInstance().setAcceptThirdPartyCookies(view, false)


                            view.setDownloadListener { downloadUrl: String?,
                                                       userAgent: String?,
                                                       contentDisposition: String?,
                                                       mimeType: String,
                                                       contentLength: Long ->
                                try {
                                    val uri = Uri.parse(downloadUrl)

                                    var filename = parseContentDisposition(contentDisposition)

                                    if (filename.isNullOrEmpty()) {
                                        filename = getFileName(uri)
                                    }

                                    if (uri.scheme == Scheme.pns.name) {
                                        val name = uri.getQueryParameter(Header.CONTENT_DOWNLOAD)
                                        if (name == null) {
                                            warning = getString(
                                                R.string.browser_handle_file, filename
                                            )

                                        } else {
                                            stateModel.contentDownloader(uri, name)
                                            showTasks = true
                                        }
                                    } else {
                                        stateModel.fileDownloader(
                                            uri, filename, mimeType, contentLength
                                        )
                                        showTasks = true
                                    }
                                } catch (throwable: Throwable) {
                                    error(TAG, throwable)
                                }
                            }
                        })
                }
            }
        }


        if (showSettings) {
            val sheetState = rememberModalBottomSheetState(
                skipPartiallyExpanded = false
            )
            ModalBottomSheet(
                modifier = Modifier.fillMaxHeight(),
                sheetState = sheetState,
                onDismissRequest = { showSettings = false }) {

                SettingsView(
                    stateModel = stateModel,
                    onDismissRequest = { showSettings = false })
            }
        }

        if (showTasks) {
            val sheetState = rememberModalBottomSheetState(
                skipPartiallyExpanded = false
            )
            ModalBottomSheet(
                modifier = Modifier.fillMaxHeight(),
                sheetState = sheetState,
                onDismissRequest = { showTasks = false }) {

                TasksView(stateModel, tabId,
                    onDismissRequest = { showTasks = false },
                    onWarningRequest = { it ->
                        warning = it
                    })
            }
        }



        if (showBookmarks) {
            val sheetState = rememberModalBottomSheetState(
                skipPartiallyExpanded = false
            )
            ModalBottomSheet(
                modifier = Modifier.fillMaxHeight(),
                sheetState = sheetState,
                onDismissRequest = { showBookmarks = false }) {

                BookmarksView(stateModel, tabId, onDismissRequest = { showBookmarks = false })
            }
        }



        if (showHistory) {
            val sheetState = rememberModalBottomSheetState(
                skipPartiallyExpanded = false
            )
            ModalBottomSheet(
                modifier = Modifier.fillMaxHeight(),
                sheetState = sheetState,
                onDismissRequest = { showHistory = false }) {
                val list = stateModel.transform(webView!!.copyBackForwardList()) // todo optimize
                HistoriesView(stateModel, tabId, list, onDismissRequest = { showHistory = false })
            }
        }


        if (showInfo) {
            val sheetState = rememberModalBottomSheetState(
                skipPartiallyExpanded = true
            )
            ModalBottomSheet(
                modifier = Modifier.fillMaxHeight(),
                sheetState = sheetState,
                onDismissRequest = { showInfo = false }) {
                InfoView(stateModel, webViewState, onDismissRequest = { showInfo = false })
            }
        }
    }
}
