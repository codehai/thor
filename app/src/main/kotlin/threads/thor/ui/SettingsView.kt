package threads.thor.ui

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.WindowInsets
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.ArrowBack
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.ExposedDropdownMenuBox
import androidx.compose.material3.ExposedDropdownMenuDefaults
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.MenuAnchorType
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Switch
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.minimumInteractiveComponentSize
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import threads.thor.R
import threads.thor.model.searchEngines
import threads.thor.state.StateModel


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun SettingsView(
    stateModel: StateModel,
    onDismissRequest: () -> Unit = {}
) {

    val jsScriptEnabled by stateModel.isJavascriptEnabled().collectAsState(true)
    var menuExpanded by remember { mutableStateOf(false) }
    val searchEngines by remember { mutableStateOf(searchEngines()) }
    val searchEngine by stateModel.searchEngine().collectAsState("DuckDuckGo")

    Scaffold(
        topBar = {
            TopAppBar(
                navigationIcon = {
                    IconButton(onClick = {
                        onDismissRequest.invoke()
                    }) {
                        Icon(
                            Icons.AutoMirrored.Filled.ArrowBack,
                            contentDescription = stringResource(id = android.R.string.cancel)
                        )
                    }
                },
                windowInsets = WindowInsets(0.dp, 0.dp, 0.dp, 0.dp),
                title = {
                    Text(stringResource(id = R.string.settings))
                },
            )
        }, content = { padding ->
            Column(modifier = Modifier.padding(padding)) {
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(16.dp)
                ) {
                    Switch(
                        checked = jsScriptEnabled,
                        onCheckedChange = { isChecked ->
                            stateModel.setJavascriptEnabled(isChecked)
                        })
                    Text(
                        text = stringResource(R.string.enable_javascript),
                        modifier = Modifier
                            .align(Alignment.CenterVertically)
                            .padding(16.dp, 0.dp),
                    )
                }

                Text(
                    text = stringResource(id = R.string.javascript_text),
                    style = MaterialTheme.typography.bodySmall,
                    modifier = Modifier
                        .padding(16.dp)
                        .fillMaxWidth()
                )

                HorizontalDivider(modifier = Modifier.padding(16.dp))

                ExposedDropdownMenuBox(
                    expanded = menuExpanded,
                    modifier = Modifier.padding(16.dp),
                    onExpandedChange = { value ->
                        menuExpanded = value
                    }) {
                    TextField(
                        readOnly = true,
                        value = searchEngine,
                        modifier = Modifier.menuAnchor(MenuAnchorType.PrimaryEditable, true),
                        onValueChange = {

                        },
                        label = {
                            Text(text = stringResource(R.string.search_engine))
                        },
                        trailingIcon = {
                            ExposedDropdownMenuDefaults.TrailingIcon(expanded = menuExpanded)
                        },
                        colors = ExposedDropdownMenuDefaults.textFieldColors()
                    )

                    ExposedDropdownMenu(expanded = menuExpanded,

                        onDismissRequest = {
                            menuExpanded = false
                        }) {

                        searchEngines.forEach { item ->
                            val name = remember { item.value.name }
                            DropdownMenuItem(text = {
                                Text(text = name)
                            },
                                onClick = {
                                    stateModel.setSearchEngine(name)
                                    menuExpanded = false
                                })
                        }
                    }
                }

                Spacer(
                    modifier = Modifier
                        .padding(16.dp)
                        .minimumInteractiveComponentSize()
                )
            }
        }
    )
}