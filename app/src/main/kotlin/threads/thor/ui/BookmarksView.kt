package threads.thor.ui

import androidx.compose.animation.animateContentSize
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.WindowInsets
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.ArrowBack
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SwipeToDismissBox
import androidx.compose.material3.SwipeToDismissBoxValue
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.minimumInteractiveComponentSize
import androidx.compose.material3.rememberSwipeToDismissBoxState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import threads.thor.R
import threads.thor.data.Bookmark
import threads.thor.state.StateModel


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun BookmarksView(
    stateModel: StateModel,
    tabId: Long,
    onDismissRequest: () -> Unit = {},
) {

    val bookmarks by stateModel.bookmarks().collectAsState(initial = emptyList())

    Scaffold(
        topBar = {
            TopAppBar(
                navigationIcon = {
                    IconButton(onClick = {
                        onDismissRequest.invoke()
                    }) {
                        Icon(
                            Icons.AutoMirrored.Filled.ArrowBack,
                            contentDescription = stringResource(id = android.R.string.cancel)
                        )
                    }
                },
                windowInsets = WindowInsets(0.dp, 0.dp, 0.dp, 0.dp),
                title = {
                    Text(stringResource(id = R.string.bookmarks))
                },
            )
        }, content = { padding ->
            LazyColumn(
                modifier = Modifier.padding(padding)
            ) {
                items(items = bookmarks,
                    key = { bookmark ->
                        bookmark.id
                    }) { bookmark ->

                    val dismissState = rememberSwipeToDismissBoxState(
                        confirmValueChange = { value ->
                            if (value == SwipeToDismissBoxValue.EndToStart) {
                                stateModel.removeBookmark(bookmark)
                                true
                            } else {
                                false
                            }
                        }
                    )


                    SwipeToDismissBox(
                        modifier = Modifier.animateContentSize(),
                        state = dismissState,
                        backgroundContent = {

                            Box(
                                modifier = Modifier
                                    .fillMaxSize()
                                    .background(MaterialTheme.colorScheme.errorContainer),
                                contentAlignment = Alignment.CenterEnd

                            ) {
                                Icon(
                                    imageVector = Icons.Filled.Delete,
                                    contentDescription = stringResource(android.R.string.untitled)
                                )
                            }
                        },
                        enableDismissFromEndToStart = true,
                        enableDismissFromStartToEnd = false,
                    ) {
                        BookmarkItem(stateModel, tabId, bookmark, onDismissRequest)
                    }
                }
            }
        }
    )
}


@Composable
fun BookmarkItem(
    stateModel: StateModel,
    tabId: Long,
    bookmark: Bookmark,
    onDismissRequest: () -> Unit = {},
) {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .clickable {
                stateModel.showBookmarkPage(tabId, bookmark)
                onDismissRequest.invoke()
            }
            .background(MaterialTheme.colorScheme.surface)
    ) {
        Row(
            modifier = Modifier
                .padding(16.dp)
                .fillMaxWidth()
                .minimumInteractiveComponentSize()
        ) {
            val image = bookmark.bitmap()
            if (image != null) {
                Image(
                    image.asImageBitmap(),
                    contentDescription = stringResource(id = R.string.bookmark),
                    modifier = Modifier
                        .size(24.dp)
                        .align(
                            Alignment.CenterVertically
                        )
                )
            } else {
                Image(
                    painterResource(id = stateModel.bookmarkIcon(bookmark.url)),
                    contentDescription = stringResource(R.string.bookmark),
                    modifier = Modifier
                        .size(24.dp)
                        .align(
                            Alignment.CenterVertically
                        )
                )
            }
            Column(
                modifier = Modifier
                    .padding(16.dp, 0.dp, 16.dp, 0.dp)
                    .weight(1.0f, true)
            ) {
                Text(
                    text = bookmark.title, maxLines = 1, modifier = Modifier.fillMaxWidth(),
                    style = MaterialTheme.typography.bodyMedium, softWrap = false
                )
                Text(
                    text = bookmark.url, maxLines = 2, modifier = Modifier.fillMaxWidth(),
                    style = MaterialTheme.typography.labelSmall, softWrap = false
                )
            }
        }
    }
}
