package threads.thor.model

import android.content.ContentValues
import android.content.Context
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.provider.MediaStore
import android.util.ArrayMap
import android.webkit.MimeTypeMap
import android.webkit.WebResourceResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import tech.lp2p.Lite
import tech.lp2p.core.Header
import tech.lp2p.core.Link
import tech.lp2p.core.MimeType
import tech.lp2p.core.Peeraddrs
import tech.lp2p.core.Scheme
import tech.lp2p.core.Utils
import threads.magnet.magnet.MagnetUriParser
import threads.thor.R
import threads.thor.data.Engine
import threads.thor.data.Tasks
import java.io.ByteArrayInputStream
import java.io.File
import java.io.FileInputStream
import java.io.IOException
import java.nio.charset.StandardCharsets
import java.util.Optional
import java.util.Stack
import java.util.UUID
import java.util.regex.Pattern

const val META: String =
    "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=2, user-scalable=yes\">" +
            "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">"
val STYLE: String = """
            <style>
                  body {
                    background-color: white;
                    color: black;
                    font-size: 18px;
                  }
                  h4 {
                    text-align: center;
                  }
                 .footer {
                    position: fixed;
                    left: 0;
                    bottom: 0;
                    width: 100%;
                    background-color: white;
                    color: black;
                    font-size: 14px;
                    text-align: center;
                 }
             
             @media (prefers-color-scheme: dark) {
                  body {
                    background-color: #222222;
                    color: white;
                    font-size: 18px;
                  }
                 .footer {
                    position: fixed;
                    left: 0;
                    bottom: 0;
                    width: 100%;
                    background-color: #222222;
                    color: white;
                    font-size: 14px;
                    text-align: center;
                 }
                 /* unvisited link */
                 a:link {
                    color: #0F82AF;
                 }
                 
                 /* visited link */
                 a:visited {
                    color: green;
                 }
                 
                 /* mouse over link */
                 a:hover {
                    color: inherit;
                 }
                 
                 /* selected link */
                 a:active {
                    color: inherit;
                 } }

            @media (prefers-color-scheme: light) {
                  body {
                    background-color: white;
                    color: black;
                    font-size: 18px;
                  }
                 .footer {
                    position: fixed;
                    left: 0;
                    bottom: 0;
                    width: 100%;
                    background-color: white;
                    color: black;
                    font-size: 14px;
                    text-align: center;
                 }
            }</style>
            """.trimIndent()

private const val SVG_FOLDER =
    "<svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\"  version=\"1.1\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\"><path fill=\"currentColor\" d=\"M10,4H4C2.89,4 2,4.89 2,6V18C2,19.1 2.9,20 4,20H20C21.1,20 22,19.1 22,18V8C22,6.89 21.1,6 20,6H12L10,4Z\" /></svg>"

private const val SVG_DOWNLOAD: String =
    "<svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"1.1\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\"><path fill=\"currentColor\" d=\"M5,20H19V18H5M19,9H15V3H9V9H5L12,16L19,9Z\" /></svg>"
private const val SVG_OCTET =
    "<svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\"  version=\"1.1\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\"><path fill=\"currentColor\" d=\"M14,2H6C4.89,2 4,2.9 4,4V20C4,21.1 4.9,22 6,22H18C19.1,22 20,21.1 20,20V8L14,2M14.5,18.9L12,17.5L9.5,19L10.2,16.2L8,14.3L10.9,14.1L12,11.4L13.1,14L16,14.2L13.8,16.1L14.5,18.9M13,9V3.5L18.5,9H13Z\" /></svg>"
private const val SVG_TEXT =
    "<svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"1.1\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\"><path fill=\"currentColor\" d=\"M6,2C4.9,2 4,2.9 4,4V20C4,21.1 4.9,22 6,22H18C19.1,22 20,21.1 20,20V8L14,2H6M6,4H13V9H18V20H6V4M8,12V14H16V12H8M8,16V18H13V16H8Z\" /></svg>"
private const val SVG_APPLICATION =
    "<svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"1.1\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\"><path fill=\"currentColor\" d=\"M19,4C20.11,4 21,4.9 21,6V18C21,19.1 20.1,20 19,20H5C3.89,20 3,19.1 3,18V6C3,4.9 3.9,4 5,4H19M19,18V8H5V18H19Z\" /></svg>"
private const val SVG_PDF =
    "<svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"1.1\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\"><path fill=\"currentColor\" d=\"M12,10.5H13V13.5H12V10.5M7,11.5H8V10.5H7V11.5M20,6V18C20,19.1 19.1,20 18,20H6C4.9,20 4,19.1 4,18V6C4,4.9 4.9,4 6,4H18C19.1,4 20,4.9 20,6M9.5,10.5C9.5,9.67 8.83,9 8,9H5.5V15H7V13H8C8.83,13 9.5,12.33 9.5,11.5V10.5M14.5,10.5C14.5,9.67 13.83,9 13,9H10.5V15H13C13.83,15 14.5,14.33 14.5,13.5V10.5M18.5,9H15.5V15H17V13H18.5V11.5H17V10.5H18.5V9Z\" /></svg>"
private const val SVG_MOVIE =
    "<svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"1.1\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\"><path fill=\"currentColor\" d=\"M5.76,10H20V18H4V6.47M22,4H18L20,8H17L15,4H13L15,8H12L10,4H8L10,8H7L5,4H4C2.9,4 2,4.9 2,6V18C2,19.1 2.9,20 4,20H20C21.1,20 22,19.1 22,18V4Z\" /></svg>"
private const val SVG_IMAGE =
    "<svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"1.1\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\"><path fill=\"currentColor\"  d=\"M12,10L11.06,12.06L9,13L11.06,13.94L12,16L12.94,13.94L15,13L12.94,12.06L12,10M20,5H16.83L15,3H9L7.17,5H4A2,2 0 0,0 2,7V19A2,2 0 0,0 4,21H20A2,2 0 0,0 22,19V7A2,2 0 0,0 20,5M20,19H4V7H8.05L8.64,6.35L9.88,5H14.12L15.36,6.35L15.95,7H20V19M12,8A5,5 0 0,0 7,13A5,5 0 0,0 12,18A5,5 0 0,0 17,13A5,5 0 0,0 12,8M12,16A3,3 0 0,1 9,13A3,3 0 0,1 12,10A3,3 0 0,1 15,13A3,3 0 0,1 12,16Z\" /></svg>"
private const val SVG_AUDIO =
    "<svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"1.1\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\"><path fill=\"currentColor\"  d=\"M14,3.23V5.29C16.89,6.15 19,8.83 19,12C19,15.17 16.89,17.84 14,18.7V20.77C18,19.86 21,16.28 21,12C21,7.72 18,4.14 14,3.23M16.5,12C16.5,10.23 15.5,8.71 14,7.97V16C15.5,15.29 16.5,13.76 16.5,12M3,9V15H7L12,20V4L7,9H3Z\" /></svg>"


fun getMimeType(info: Link): String {
    if (info.isDir) {
        return MimeType.DIR_MIME_TYPE.name
    }
    val name = info.name()
    if (name.isEmpty()) {
        return MimeType.OCTET_MIME_TYPE.name
    }
    return getMimeType(name)
}


fun getSvgResource(mimeType: String): String {
    if (mimeType.isNotEmpty()) {
        if (mimeType == MimeType.DIR_MIME_TYPE.name) {
            return SVG_FOLDER
        }
        if (mimeType == MimeType.OCTET_MIME_TYPE.name) {
            return SVG_OCTET
        }
        if (mimeType == MimeType.PDF_MIME_TYPE.name) {
            return SVG_PDF
        }
        if (mimeType.startsWith(MimeType.TEXT.name)) {
            return SVG_TEXT
        }
        if (mimeType.startsWith(MimeType.VIDEO.name)) {
            return SVG_MOVIE
        }
        if (mimeType.startsWith(MimeType.IMAGE.name)) {
            return SVG_IMAGE
        }
        if (mimeType.startsWith(MimeType.AUDIO.name)) {
            return SVG_AUDIO
        }
        if (mimeType.startsWith(MimeType.APPLICATION.name)) {
            return SVG_APPLICATION
        }
    }
    return SVG_OCTET
}

fun getMediaResource(uri: Uri): Int {
    if (uri.scheme == Scheme.magnet.name) {
        return R.drawable.folder
    }
    if (uri.scheme == Scheme.pns.name) {
        val mimeType = uri.getQueryParameter(Header.CONTENT_TYPE)
        if (!mimeType.isNullOrBlank()) {
            return getMediaResource(mimeType)
        }
        return R.drawable.folder
    }

    val paths = uri.pathSegments
    if (paths.isNotEmpty()) {
        val name = paths[paths.size - 1]
        val mimeType = getMimeType(name)
        return getMediaResource(mimeType)
    }
    return R.drawable.file_star
}

fun getMediaResource(mimeType: String): Int {
    if (mimeType.isNotEmpty()) {
        if (mimeType == MimeType.TORRENT_MIME_TYPE.name) {
            return R.drawable.arrow_up_down_bold
        }
        if (mimeType == MimeType.OCTET_MIME_TYPE.name) {
            return R.drawable.file_star
        }
        if (mimeType == MimeType.JSON_MIME_TYPE.name) {
            return R.drawable.json
        }
        if (mimeType == MimeType.PLAIN_MIME_TYPE.name) {
            return R.drawable.file
        }
        if (mimeType == MimeType.PDF_MIME_TYPE.name) {
            return R.drawable.pdf
        }
        if (mimeType == MimeType.MHT_MIME_TYPE.name) {
            return R.drawable.mht
        }
        if (mimeType == MimeType.OPEN_EXCEL_MIME_TYPE.name) {
            return R.drawable.microsoft_excel
        }
        if (mimeType == MimeType.CSV_MIME_TYPE.name) {
            return R.drawable.microsoft_excel
        }
        if (mimeType == MimeType.EXCEL_MIME_TYPE.name) {
            return R.drawable.microsoft_excel
        }
        if (mimeType == MimeType.WORD_MIME_TYPE.name) {
            return R.drawable.microsoft_word
        }
        if (mimeType.startsWith(MimeType.TEXT.name)) {
            return R.drawable.file
        }
        if (mimeType == MimeType.DIR_MIME_TYPE.name) {
            return R.drawable.folder
        }
        if (mimeType.startsWith(MimeType.VIDEO.name)) {
            return R.drawable.movie_outline
        }
        if (mimeType.startsWith(MimeType.IMAGE.name)) {
            return R.drawable.camera
        }
        if (mimeType.startsWith(MimeType.AUDIO.name)) {
            return R.drawable.audio
        }
        if (mimeType.startsWith(MimeType.PGP_KEYS_MIME_TYPE.name)) {
            return R.drawable.fingerprint
        }
        if (mimeType.startsWith(MimeType.APPLICATION.name)) {
            return R.drawable.application
        }

        return R.drawable.settings
    }

    return R.drawable.help
}


@Throws(IOException::class)
suspend fun uploadDirectory(
    context: Context,
    tasks: Tasks,
    taskId: Long,
    dir: File,
    dirs: Stack<String>,
    uuid: UUID
) {
    dirs.add(dir.name)
    val children = dir.list()

    if (children != null) {
        for (child in children) {
            val fileChild = File(dir, child)
            if (fileChild.isDirectory) {
                val subTaskId = tasks.createOrGet(
                    taskId, fileChild.name,
                    MimeType.DIR_MIME_TYPE.name, fileChild.toURI().toString(),
                    fileChild.length(), uuid
                )
                tasks.tasksDao().active(subTaskId)
                uploadDirectory(context, tasks, subTaskId, fileChild, dirs, uuid)
            } else {
                val subTaskId = tasks.createOrGet(
                    taskId, fileChild.name,
                    getMimeType(fileChild.name), fileChild.toURI().toString(),
                    fileChild.length(), uuid
                )
                tasks.tasksDao().active(subTaskId)
                uploadFile(context, tasks, subTaskId, fileChild, dirs)
            }
        }
    }
    dirs.pop()

    val mimeType = getMimeType(dir.name)
    val path = Environment.DIRECTORY_DOWNLOADS + File.separator + java.lang.String.join(
        (File.separator), dirs
    )
    val targetUri = downloadsUri(context, mimeType, dir.name, path)
    checkNotNull(targetUri)
    tasks.tasksDao().finished(taskId, targetUri.toString())
}

@Throws(IOException::class)
suspend fun uploadFile(
    context: Context, tasks: Tasks, taskId: Long,
    file: File, dirs: Stack<String>
) {
    val name = file.name
    checkNotNull(name)

    val mimeType = getMimeType(name)
    val path = Environment.DIRECTORY_DOWNLOADS + File.separator + java.lang.String.join(
        (File.separator), dirs
    )

    val targetUri = downloadsUri(context, mimeType, name, path)
    checkNotNull(targetUri)
    val contentResolver = context.contentResolver

    try {
        withContext(Dispatchers.IO) {
            FileInputStream(file).use { `is` ->
                contentResolver.openOutputStream(targetUri).use { os ->
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
                        `is`.transferTo(os)
                    } else {
                        Utils.copy(`is`, os)
                    }
                }
            }
        }
        tasks.tasksDao().finished(taskId, targetUri.toString())
    } catch (throwable: Throwable) {
        contentResolver.delete(targetUri, null, null)
        throw throwable
    }
}

fun getMimeType(name: String): String {
    val mimeType = evaluateMimeType(name)
    if (mimeType != null) {
        return mimeType
    }
    return MimeType.OCTET_MIME_TYPE.name
}

private fun getExtension(filename: String?): Optional<String> {
    return Optional.ofNullable(filename)
        .filter { f: String -> f.contains(".") }
        .map { f: String ->
            f.substring(
                filename!!.lastIndexOf('.') + 1
            )
        }
}


private fun evaluateMimeType(filename: String): String? {
    try {
        val extension = getExtension(filename)
        if (extension.isPresent) {
            val mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension.get())
            if (mimeType != null) {
                return mimeType
            }
        } else {
            // no extension directory assumed
            return MimeType.DIR_MIME_TYPE.name
        }
    } catch (throwable: Throwable) {
        error(TAG, throwable)
    }
    return null
}

fun getSvgDownload(): String {
    return SVG_DOWNLOAD
}


const val DOWNLOADS_TAG: String = "downloads"
private const val NO_NAME = "download-file.bin"

private val DUCKDUCKGO = Engine(
    "DuckDuckGo",
    "https://start.duckduckgo.com/", "https://duckduckgo.com/?q="
)
private val GOOGLE = Engine(
    "Google",
    "https://www.google.com/", "https://www.google.com/search?q="
)
private val ECOSIA = Engine(
    "Ecosia",
    "https://www.ecosia.org/", "https://www.ecosia.org/search?q="
)
private val BING = Engine(
    "Bing",
    "https://www.bing.com/", "https://www.bing.com/search?q="
)

private val SEARCH_ENGINES: Map<String, Engine> = java.util.Map.of(
    DUCKDUCKGO.name, DUCKDUCKGO,
    GOOGLE.name, GOOGLE,
    ECOSIA.name, ECOSIA,
    BING.name, BING
)

private const val TAG: String = "API"


private val CONTENT_DISPOSITION_PATTERN: Pattern = Pattern.compile(
    "attachment;\\s*filename\\s*=\\s*(\"?)([^\"]*)\\1(\"?);",
    Pattern.CASE_INSENSITIVE
)


fun searchEngines(): ArrayMap<String, Engine> {
    val map = ArrayMap<String, Engine>()
    map.putAll(SEARCH_ENGINES)
    return map
}


fun getSearchEngine(searchEngine: String): Engine {
    return SEARCH_ENGINES[searchEngine] ?: return DUCKDUCKGO
}


fun getFileName(uri: Uri): String {
    val paths = uri.pathSegments
    return if (paths.isNotEmpty()) {
        paths[paths.size - 1]
    } else {
        NO_NAME
    }
}


fun getUriTitle(url: String?): String {
    if (url.isNullOrBlank()) {
        return ""
    }
    val uri = Uri.parse(url)
    val scheme = uri.scheme
    if (scheme == Scheme.magnet.name) {
        val magnetUri = MagnetUriParser.parse(uri.toString())

        var name = uri.toString()
        if (magnetUri.displayName.isPresent) {
            name = magnetUri.displayName.get()
        }
        return name
    } else if (scheme == Scheme.pns.name) {
        val title = uri.getQueryParameter(Header.CONTENT_TITLE)
        if (!title.isNullOrBlank()) {
            return title
        }
        val name = uri.getQueryParameter(Header.CONTENT_DOWNLOAD)
        if (!name.isNullOrBlank()) {
            return name
        }
        val host = checkNotNull(uri.host)
        return host
    } else {
        val host = uri.host
        if (!host.isNullOrBlank()) {
            return host
        } else {
            val paths = uri.pathSegments
            return if (paths.isNotEmpty()) {
                paths[paths.size - 1]
            } else {
                ""
            }
        }
    }
}


fun createEmptyResource(): WebResourceResponse {
    return WebResourceResponse(
        MimeType.PLAIN_MIME_TYPE.name,
        StandardCharsets.UTF_8.name(), ByteArrayInputStream("".toByteArray())
    )
}


fun createErrorMessage(throwable: Throwable): WebResourceResponse {
    error(TAG, throwable)
    var message = throwable.message
    if (message.isNullOrEmpty()) {
        message = throwable.javaClass.simpleName
    }
    val report = generateErrorHtml(message!!)
    return WebResourceResponse(
        MimeType.HTML_MIME_TYPE.name,
        StandardCharsets.UTF_8.name(), ByteArrayInputStream(report.toByteArray())
    )
}


fun createErrorMessage(message: String): WebResourceResponse {
    val report = generateErrorHtml(message)
    return WebResourceResponse(
        MimeType.HTML_MIME_TYPE.name,
        StandardCharsets.UTF_8.name(), ByteArrayInputStream(report.toByteArray())
    )
}


private fun generateErrorHtml(message: String): String {
    return """
                <html><head>$META<title>Error</title></head>
                $STYLE<body><div <div>$message</div></body></html>
                """.trimIndent()
}


fun getFileSize(size: Long): String {
    val fileSize: String

    if (size < 1000) {
        fileSize = size.toString()
        return "$fileSize B"
    } else if (size < 1000 * 1000) {
        fileSize = (size / 1000).toDouble().toString()
        return "$fileSize KB"
    } else {
        fileSize = (size / (1000 * 1000)).toDouble().toString()
        return "$fileSize MB"
    }
}


fun bootstrap(): Peeraddrs {
    // "/ip4/104.131.131.82/udp/4001/quic/p2p/QmaCpDMGvV2BGHeYERUEnRQAwe3N8SzbUtfsmvsqQLuvuJ"
    val peeraddrs = Peeraddrs()
    try {
        peeraddrs.add(
            Lite.createPeeraddr(
                "QmaCpDMGvV2BGHeYERUEnRQAwe3N8SzbUtfsmvsqQLuvuJ",
                "104.131.131.82", 4001
            )
        )
    } catch (throwable: Throwable) {
        error(TAG, throwable)
    }
    return peeraddrs
}


fun downloadsUri(context: Context, mimeType: String, name: String, path: String): Uri? {
    val contentValues = ContentValues()
    error(TAG, "$mimeType $name $path")
    contentValues.put(MediaStore.Downloads.DISPLAY_NAME, name)
    contentValues.put(MediaStore.Downloads.MIME_TYPE, mimeType)
    contentValues.put(MediaStore.Downloads.RELATIVE_PATH, path)

    val contentResolver = context.contentResolver
    return contentResolver.insert(MediaStore.Downloads.EXTERNAL_CONTENT_URI, contentValues)
}


fun parseContentDisposition(contentDisposition: String?): String? {
    try {
        val m = CONTENT_DISPOSITION_PATTERN.matcher(contentDisposition.toString())
        if (m.find()) {
            return m.group(2)
        }
    } catch (ex: IllegalStateException) {
        // This function is defined as returning null when it can't parse the header
    }
    return null
}


