package threads.thor.model

import android.util.Log

@Suppress("SameReturnValue")
private val isDebug: Boolean get() = true


fun error(tag: String, message: String) {
    if (isDebug) {
        Log.e(tag, message)
    }
}


fun error(tag: String, throwable: Throwable) {
    if (isDebug) {
        Log.e(tag, throwable.localizedMessage, throwable)
    }
}

