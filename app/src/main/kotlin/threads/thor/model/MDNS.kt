package threads.thor.model

import android.content.Context
import android.net.nsd.NsdManager
import android.net.nsd.NsdManager.DiscoveryListener
import android.net.nsd.NsdManager.ServiceInfoCallback
import android.net.nsd.NsdServiceInfo
import android.os.Build
import androidx.annotation.RequiresApi
import tech.lp2p.Lite
import tech.lp2p.core.Peeraddr
import tech.lp2p.core.Peeraddrs
import java.net.InetSocketAddress
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import java.util.function.Consumer
import kotlin.concurrent.Volatile

class MDNS private constructor(private val nsdManager: NsdManager) {
    @Volatile
    private var discoveryService: DiscoveryService? = null

    fun startDiscovery(consumer: Consumer<Peeraddr>) {
        try {
            discoveryService = DiscoveryService(nsdManager, consumer)

            nsdManager.discoverServices(
                MDNS_SERVICE, NsdManager.PROTOCOL_DNS_SD,
                discoveryService
            )
        } catch (throwable: Throwable) {
            error(TAG, throwable)
        }
    }

    fun stop() {
        try {
            if (discoveryService != null) {
                nsdManager.stopServiceDiscovery(discoveryService)
            }
        } catch (throwable: Throwable) {
            error(TAG, throwable) // not ok when fails
        }
    }


    @JvmRecord
    private data class DiscoveryService(
        val nsdManager: NsdManager,
        val consumer: Consumer<Peeraddr>
    ) : DiscoveryListener {
        override fun onStartDiscoveryFailed(serviceType: String, errorCode: Int) {
            error(TAG, "onStartDiscoveryFailed $serviceType")
        }

        override fun onStopDiscoveryFailed(serviceType: String, errorCode: Int) {
            error(TAG, "onStopDiscoveryFailed $serviceType")
        }

        override fun onDiscoveryStarted(serviceType: String) {
            error(TAG, "onDiscoveryStarted $serviceType")
        }

        override fun onDiscoveryStopped(serviceType: String) {
            error(TAG, "onDiscoveryStopped $serviceType")
        }

        override fun onServiceFound(serviceInfo: NsdServiceInfo) {
            error(TAG, "onServiceFound ")

            if (Build.VERSION.SDK_INT >= 34) {
                nsdManager.registerServiceInfoCallback(serviceInfo, EXECUTOR,
                    object : ServiceInfoCallback {
                        override fun onServiceInfoCallbackRegistrationFailed(i: Int) {
                            error(TAG, "onServiceInfoCallbackRegistrationFailed $i")
                        }

                        override fun onServiceUpdated(nsdServiceInfo: NsdServiceInfo) {
                            evaluateHosts(nsdServiceInfo)
                        }

                        override fun onServiceLost() {
                            error(TAG, "onServiceLost")
                        }

                        override fun onServiceInfoCallbackUnregistered() {
                            error(TAG, "onServiceInfoCallbackUnregistered")
                        }
                    })
            } else {
                nsdManager.resolveService(serviceInfo, object : NsdManager.ResolveListener {
                    override fun onResolveFailed(nsdServiceInfo: NsdServiceInfo, i: Int) {
                        error(TAG, "onResolveFailed $nsdServiceInfo")
                    }

                    override fun onServiceResolved(nsdServiceInfo: NsdServiceInfo) {
                        evaluateHost(nsdServiceInfo)
                    }
                })
            }
        }

        override fun onServiceLost(serviceInfo: NsdServiceInfo) {
            error(TAG, "onServiceLost $serviceInfo")
        }

        private fun evaluateHost(serviceInfo: NsdServiceInfo) {
            try {
                val peerId = Lite.decodePeerId(serviceInfo.serviceName)
                val inetAddress = serviceInfo.host

                val peeraddr = Peeraddr.create(
                    peerId,
                    InetSocketAddress(inetAddress, serviceInfo.port)
                )
                consumer.accept(peeraddr)
            } catch (throwable: Throwable) {
                error(TAG, throwable) // is ok, fails for kubo
            }
        }

        @RequiresApi(Build.VERSION_CODES.UPSIDE_DOWN_CAKE)
        private fun evaluateHosts(serviceInfo: NsdServiceInfo) {
            try {
                val peerId = Lite.decodePeerId(serviceInfo.serviceName)

                val peeraddrs = Peeraddrs()
                for (inetAddress in serviceInfo.hostAddresses) {
                    val peeraddr = Peeraddr.create(
                        peerId,
                        InetSocketAddress(inetAddress, serviceInfo.port)
                    )
                    peeraddrs.add(peeraddr)
                }

                if (!peeraddrs.isEmpty()) {
                    val peeraddr = Peeraddrs.best(peeraddrs)
                    peeraddr.ifPresent(consumer)
                }
            } catch (throwable: Throwable) {
                error(TAG, throwable) // is ok, fails for kubo
            }
        }

        companion object {
            private val TAG: String = DiscoveryService::class.java.simpleName
        }
    }

    companion object {
        const val MDNS_SERVICE: String = "_p2p._udp." // default libp2p [or kubo] service name

        @RequiresApi(Build.VERSION_CODES.UPSIDE_DOWN_CAKE)
        private val EXECUTOR: ExecutorService = Executors.newSingleThreadExecutor()

        private val TAG: String = MDNS::class.java.name
        fun mdns(context: Context): MDNS {
            val nsdManager = context.getSystemService(Context.NSD_SERVICE) as NsdManager
            return MDNS(nsdManager)
        }
    }
}
