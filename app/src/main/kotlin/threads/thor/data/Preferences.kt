package threads.thor.data

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.booleanPreferencesKey
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.stringPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map
import threads.thor.model.getSearchEngine


class Preferences {

    companion object {
        private val Context.dataStore: DataStore<Preferences> by preferencesDataStore("settings")
        private val javascript = booleanPreferencesKey("javascript")
        private val searchEngine = stringPreferencesKey("searchEngine")


        suspend fun setJavascriptEnabled(context: Context, checked: Boolean) {
            context.dataStore.edit { settings ->
                settings[javascript] = checked
            }
        }

        suspend fun setSearchEngine(context: Context, name: String) {
            context.dataStore.edit { settings ->
                settings[searchEngine] = name
            }
        }

        fun searchEngine(context: Context): Flow<String> =
            context.dataStore.data.map { settings ->
                settings[searchEngine] ?: "DuckDuckGo"
            }


        private val homepageUri = stringPreferencesKey("homepageUri")
        private val homepageTitle = stringPreferencesKey("homepageTitle")


        suspend fun homepageUri(context: Context, uri: String?) {
            context.dataStore.edit { settings ->
                if (uri == null) {
                    settings.remove(homepageUri)
                } else {
                    settings[homepageUri] = uri
                }
            }
        }

        suspend fun homepageTitle(context: Context, title: String?) {
            context.dataStore.edit { settings ->
                if (title == null) {
                    settings.remove(homepageTitle)
                } else {
                    settings[homepageTitle] = title
                }
            }
        }

        fun isJavascriptEnabled(context: Context): Flow<Boolean> =
            context.dataStore.data.map { settings ->
                settings[javascript] ?: true
            }

        fun homepageUri(context: Context): Flow<String> {
            return context.dataStore.data.map { settings ->
                settings[homepageUri] ?: ""
            }
        }

        suspend fun primaryPage(context: Context, tabId: Long): Page {
            return context.dataStore.data.map { settings ->
                val title = settings[homepageTitle]
                val uri = settings[homepageUri]

                if (uri.isNullOrBlank() || title.isNullOrBlank()) {
                    val searchEngine = settings[searchEngine] ?: "DuckDuckGo"
                    val eng = getSearchEngine(searchEngine)
                    Page(tabId, eng.name, eng.uri)
                } else {
                    Page(tabId, title, uri)
                }
            }.first()
        }
    }
}