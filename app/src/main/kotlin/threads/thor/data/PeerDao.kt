package threads.thor.data

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import tech.lp2p.core.PeerId

@Dao
interface PeerDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(peer: Peer)

    @Query("SELECT * FROM Peer ORDER BY RANDOM() LIMIT :limit")
    fun randomPeers(limit: Int): List<Peer>

    @Query("DELETE FROM Peer WHERE peerId = :peerId")
    fun delete(peerId: PeerId)
}
