package threads.thor.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import tech.lp2p.core.PeerId
import tech.lp2p.core.Peeraddr

@Entity
data class Peer(
    @field:PrimaryKey val peerId: PeerId,
    @field:ColumnInfo(typeAffinity = ColumnInfo.BLOB) val raw: ByteArray
) {
    fun peeraddr(): Peeraddr {
        return checkNotNull(Peeraddr.create(peerId, raw))
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Peer

        return peerId == other.peerId
    }

    override fun hashCode(): Int {
        return peerId.hashCode()
    }

}
