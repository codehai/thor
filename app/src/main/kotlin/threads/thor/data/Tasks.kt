package threads.thor.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room.databaseBuilder
import androidx.room.RoomDatabase
import java.util.UUID


@Database(entities = [Task::class], version = 1, exportSchema = false)
abstract class Tasks : RoomDatabase() {
    abstract fun tasksDao(): TasksDao

    suspend fun createOrGet(
        pid: Long, name: String, mimeType: String, uri: String,
        size: Long, uuid: UUID
    ): Long {
        val id = tasksDao().parent(pid, name)
        if (id != null) {
            return id
        }
        return tasksDao().insert(
            Task(
                0, pid, name, mimeType, uri,
                size, uuid.toString(),
                active = false,
                finished = false,
                progress = 0f
            )
        )
    }

    companion object {
        @Volatile
        private var INSTANCE: Tasks? = null

        fun getInstance(context: Context): Tasks {
            if (INSTANCE == null) {
                synchronized(Tasks::class.java) {
                    if (INSTANCE == null) {
                        INSTANCE = databaseBuilder(
                            context,
                            Tasks::class.java, Tasks::class.java.simpleName
                        ).fallbackToDestructiveMigration().build()
                    }
                }
            }
            return INSTANCE!!
        }
    }
}
