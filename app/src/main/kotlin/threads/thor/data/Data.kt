package threads.thor.data

import android.webkit.HttpAuthHandler


data class AuthRequest(val handler: HttpAuthHandler, val host: String, val realm: String)

data class Engine(val name: String, val uri: String, val query: String)

data class Page(val id: Long, val title: String, val url: String)