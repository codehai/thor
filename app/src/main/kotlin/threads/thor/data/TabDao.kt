package threads.thor.data

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import kotlinx.coroutines.flow.Flow

@Dao
interface TabDao {

    @Query("SELECT * FROM Tab")
    fun tabs(): Flow<List<Tab>>

    @Query("SELECT id FROM Tab")
    fun ids(): Flow<List<Long>>

    @Query("SELECT title FROM Tab WHERE id = :id")
    fun title(id: Long): Flow<String?>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(tab: Tab): Long

    @Delete
    suspend fun delete(tab: Tab)

    @Query("UPDATE Tab SET title = :title, url = :url, image = :image WHERE id = :id")
    suspend fun update(id: Long, url: String, title: String, image: ByteArray?)

    @Query("UPDATE Tab SET url = :url, title = :title WHERE id = :id")
    suspend fun title(id: Long, url: String, title: String)

    @Query("UPDATE Tab SET  url = :url, image = :image WHERE id = :id")
    suspend fun image(id: Long, url: String, image: ByteArray?)

    @Query("SELECT EXISTS(SELECT * FROM Tab)")
    suspend fun hasTabs(): Boolean
}
