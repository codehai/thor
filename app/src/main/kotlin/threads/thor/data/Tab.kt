package threads.thor.data

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Tab(
    @field:PrimaryKey(autoGenerate = true) val id: Long, val title: String, val url: String,
    @field:ColumnInfo(name = "image", typeAffinity = ColumnInfo.BLOB) val image: ByteArray?
) {

    fun bitmap(): Bitmap? {
        if (image != null) {
            return BitmapFactory.decodeByteArray(image, 0, image.size)
        }
        return null
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Tab

        if (id != other.id) return false
        if (title != other.title) return false
        if (url != other.url) return false
        if (image != null) {
            if (other.image == null) return false
            if (!image.contentEquals(other.image)) return false
        } else if (other.image != null) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + title.hashCode()
        result = 31 * result + url.hashCode()
        result = 31 * result + (image?.contentHashCode() ?: 0)
        return result
    }
}
