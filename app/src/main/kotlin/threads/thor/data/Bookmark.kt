package threads.thor.data

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity
data class Bookmark(
    @field:PrimaryKey(autoGenerate = true) val id: Long, val url: String,
    val title: String,
    @field:ColumnInfo(typeAffinity = ColumnInfo.BLOB) val icon: ByteArray?
) {

    fun bitmap(): Bitmap? {
        if (icon != null) {
            return BitmapFactory.decodeByteArray(icon, 0, icon.size)
        }
        return null
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Bookmark

        if (url != other.url) return false
        if (title != other.title) return false
        if (icon != null) {
            if (other.icon == null) return false
            if (!icon.contentEquals(other.icon)) return false
        } else if (other.icon != null) return false

        return true
    }

    override fun hashCode(): Int {
        var result = url.hashCode()
        result = 31 * result + title.hashCode()
        result = 31 * result + (icon?.contentHashCode() ?: 0)
        return result
    }
}
