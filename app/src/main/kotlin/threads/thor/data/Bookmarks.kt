package threads.thor.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room.databaseBuilder
import androidx.room.RoomDatabase

@Database(entities = [Bookmark::class], version = 2, exportSchema = false)
abstract class Bookmarks : RoomDatabase() {
    abstract fun bookmarkDao(): BookmarkDao

    companion object {
        @Volatile
        private var INSTANCE: Bookmarks? = null

        fun getInstance(context: Context): Bookmarks {
            if (INSTANCE == null) {
                synchronized(Bookmarks::class.java) {
                    if (INSTANCE == null) {
                        INSTANCE = databaseBuilder(
                            context,
                            Bookmarks::class.java, Bookmarks::class.java.simpleName
                        ).fallbackToDestructiveMigration().build()
                    }
                }
            }
            return INSTANCE!!
        }
    }
}
