package threads.thor.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room.databaseBuilder
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import tech.lp2p.core.PeerStore
import tech.lp2p.core.Peeraddr

@Database(entities = [Peer::class], version = 2, exportSchema = false)
@TypeConverters(Converters::class)
abstract class Peers : RoomDatabase(), PeerStore {
    abstract fun bootstrapDao(): PeerDao

    override fun peeraddrs(limit: Int): List<Peeraddr> {
        val peeraddrs: MutableList<Peeraddr> = ArrayList()
        val list = bootstrapDao().randomPeers(limit)
        for (peer in list) {
            peeraddrs.add(peer.peeraddr())
        }
        return peeraddrs
    }

    override fun storePeeraddr(peeraddr: Peeraddr) {
        bootstrapDao().insert(Peer(peeraddr.peerId, peeraddr.encoded()))
    }


    override fun removePeeraddr(peeraddr: Peeraddr) {
        bootstrapDao().delete(peeraddr.peerId)
    }

    companion object {
        @Volatile
        private var INSTANCE: Peers? = null
        fun getInstance(context: Context): Peers? {
            if (INSTANCE == null) {
                synchronized(Peers::class.java) {
                    if (INSTANCE == null) {
                        INSTANCE = databaseBuilder(
                            context,
                            Peers::class.java,
                            Peers::class.java.simpleName
                        ).fallbackToDestructiveMigration().build()
                    }
                }
            }
            return INSTANCE!!
        }
    }
}
