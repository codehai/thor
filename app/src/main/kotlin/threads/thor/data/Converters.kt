package threads.thor.data

import androidx.room.TypeConverter
import tech.lp2p.core.PeerId

object Converters {

    @TypeConverter
    fun toPeerId(data: ByteArray): PeerId {
        return PeerId(data)
    }

    @TypeConverter
    fun toArray(peerId: PeerId): ByteArray {
        return peerId.hash
    }
}