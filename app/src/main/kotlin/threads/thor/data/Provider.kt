package threads.thor.data

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Binder
import android.provider.DocumentsContract
import android.provider.OpenableColumns
import androidx.core.content.FileProvider
import tech.lp2p.core.MimeType
import threads.thor.model.error
import java.io.File
import java.io.IOException
import java.util.Objects
import kotlin.concurrent.Volatile

class Provider private constructor(context: Context) {
    private val dataDir: File = File(context.filesDir, DATA)
    private val cacheDir: File = File(context.cacheDir, CACHE)


    private fun getDataFile(filename: String): File {
        return File(getDataDir(), filename)
    }

    @Throws(IOException::class)
    fun createDataFile(name: String): File {
        val file = getDataFile(name) // todo make stuff new
        if (file.exists()) {
            val result = file.delete()
            if (!result) {
                error(TAG, "Deleting failed")
            }
        }
        val success = file.createNewFile()
        if (!success) {
            error(TAG, "Failed create a new file")
        }
        return file
    }


    private fun getCacheDir(): File {
        if (!cacheDir.isDirectory && !cacheDir.exists()) {
            val result = cacheDir.mkdir()
            if (!result) {
                throw RuntimeException("image directory does not exists")
            }
        }
        return cacheDir
    }

    fun getDataDir(): File {
        if (!dataDir.isDirectory && !dataDir.exists()) {
            val result = dataDir.mkdir()
            if (!result) {
                throw RuntimeException("image directory does not exists")
            }
        }
        return dataDir
    }

    companion object {
        private val TAG: String = Provider::class.java.simpleName
        private const val AUTHORITY: String = "threads.thor.provider"
        private const val DATA = "data"
        private const val CACHE = "cache"

        @Volatile
        private var INSTANCE: Provider? = null
        fun getInstance(context: Context): Provider {
            if (INSTANCE == null) {
                synchronized(Provider::class.java) {
                    if (INSTANCE == null) {
                        INSTANCE = Provider(context)
                    }
                }
            }
            return INSTANCE!!
        }


        fun cleanData(context: Context) {
            deleteFile(context.filesDir, false)
        }


        fun cleanCache(context: Context) {
            deleteFile(context.cacheDir, false)
        }


        fun deleteFile(file: File, deleteWhenDir: Boolean) {
            try {
                if (file.isDirectory) {
                    val children = file.listFiles()
                    if (children != null) {
                        for (child in children) {
                            deleteFile(child, true)
                        }
                    }
                    if (deleteWhenDir) {
                        error(TAG, "File delete " + file.name)
                        val result = file.delete()
                        if (!result) {
                            error(TAG, "File " + file.name + " not deleted")
                        }
                    }
                } else {
                    error(TAG, "File delete " + file.name)
                    val result = file.delete()
                    if (!result) {
                        error(TAG, "File " + file.name + " not deleted")
                    }
                }
            } catch (throwable: Throwable) {
                error(TAG, throwable)
            }
        }


        fun getMimeType(context: Context, uri: Uri): String {
            var mimeType = context.contentResolver.getType(uri)
            if (mimeType == null) {
                mimeType = MimeType.OCTET_MIME_TYPE.name
            }
            return mimeType!!
        }

        fun hasReadPermission(context: Context, uri: Uri): Boolean {
            val perm = context.checkUriPermission(
                uri, Binder.getCallingPid(), Binder.getCallingUid(),
                Intent.FLAG_GRANT_READ_URI_PERMISSION
            )
            return perm != PackageManager.PERMISSION_DENIED
        }

        fun hasWritePermission(context: Context, uri: Uri): Boolean {
            val perm = context.checkUriPermission(
                uri, Binder.getCallingPid(), Binder.getCallingUid(),
                Intent.FLAG_GRANT_WRITE_URI_PERMISSION
            )
            return perm != PackageManager.PERMISSION_DENIED
        }

        fun getFileName(context: Context, uri: Uri): String {
            var filename: String? = null

            val contentResolver = context.contentResolver
            try {
                contentResolver.query(
                    uri,
                    null, null, null, null
                ).use { cursor ->
                    Objects.requireNonNull(cursor)
                    cursor!!.moveToFirst()
                    val nameIndex = cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME)
                    filename = cursor.getString(nameIndex)
                }
            } catch (throwable: Throwable) {
                error(TAG, throwable)
            }

            if (filename == null) {
                filename = uri.lastPathSegment
            }

            if (filename == null) {
                filename = "file_name_not_detected"
            }

            return filename!!
        }

        fun getFileSize(context: Context, uri: Uri): Long {
            val contentResolver = context.contentResolver

            try {
                contentResolver.query(
                    uri,
                    null, null, null, null
                ).use { cursor ->
                    Objects.requireNonNull(cursor)
                    cursor!!.moveToFirst()
                    val nameIndex = cursor.getColumnIndex(OpenableColumns.SIZE)
                    return cursor.getLong(nameIndex)
                }
            } catch (throwable: Throwable) {
                error(TAG, throwable)
            }


            try {
                contentResolver.openFileDescriptor(uri, "r").use { fd ->
                    Objects.requireNonNull(fd)
                    return fd!!.statSize
                }
            } catch (throwable: Throwable) {
                error(TAG, throwable)
            }
            return -1
        }

        fun isPartial(context: Context, uri: Uri): Boolean {
            val contentResolver = context.contentResolver
            try {
                contentResolver.query(
                    uri,
                    arrayOf(DocumentsContract.Document.COLUMN_FLAGS),
                    null,
                    null,
                    null
                ).use { cursor ->
                    Objects.requireNonNull(cursor)
                    cursor!!.moveToFirst()

                    val docFlags = cursor.getInt(0)
                    if ((docFlags and DocumentsContract.Document.FLAG_PARTIAL) != 0) {
                        return true
                    }
                }
            } catch (throwable: Throwable) {
                error(TAG, throwable)
            }
            return false
        }

        fun getDataUri(context: Context, file: File): Uri {
            return FileProvider.getUriForFile(
                context, AUTHORITY, file
            )
        }


    }
}
