package threads.thor.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room.inMemoryDatabaseBuilder
import androidx.room.RoomDatabase


@Database(entities = [Tab::class], version = 1, exportSchema = false)
abstract class Tabs : RoomDatabase() {
    abstract fun tabDao(): TabDao

    suspend fun updateTab(tabId: Long, url: String, title: String, bytes: ByteArray?) {
        tabDao().update(tabId, url, title, bytes)
    }


    companion object {
        @Volatile
        private var INSTANCE: Tabs? = null

        fun getInstance(context: Context): Tabs {
            if (INSTANCE == null) {
                synchronized(Tabs::class.java) {
                    if (INSTANCE == null) {
                        INSTANCE = inMemoryDatabaseBuilder(context, Tabs::class.java).build()
                    }
                }
            }
            return INSTANCE!!
        }
    }
}
