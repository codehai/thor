package threads.thor.data

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.UUID

@Entity
data class Task(
    @field:PrimaryKey(autoGenerate = true) val id: Long,
    val pid: Long,
    val name: String,
    val mimeType: String,
    val uri: String,
    val size: Long,
    val work: String?,
    val active: Boolean,
    val finished: Boolean,
    val progress: Float
) {


    fun workUUID(): UUID? {
        if (work != null) {
            return UUID.fromString(work)
        }
        return null
    }

}
