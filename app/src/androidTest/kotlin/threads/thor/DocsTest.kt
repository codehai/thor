package threads.thor

import android.content.Context
import androidx.test.core.app.ApplicationProvider
import org.junit.Assert
import org.junit.BeforeClass
import org.junit.Test
import threads.thor.model.parseContentDisposition

class DocsTest {
    @Test
    @Throws(Exception::class)
    fun parseContentDisposition() {
        val filename =
            parseContentDisposition("attachment; filename=\"README.md\"; filename=\"UTF-8 README.md\"")
        Assert.assertEquals(filename, "README.md")
    }


    companion object {
        private var context: Context? = null

        @JvmStatic
        @BeforeClass
        fun setup() {
            context = ApplicationProvider.getApplicationContext()
        }
    }
}
