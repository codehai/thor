package threads.thor

import android.app.Application
import android.content.Context
import androidx.test.core.app.ApplicationProvider
import junit.framework.TestCase
import org.junit.BeforeClass
import org.junit.Test
import tech.lp2p.Lite
import tech.lp2p.core.BlockStore
import tech.lp2p.core.FileStore
import tech.lp2p.ident.IdentifyService
import tech.lp2p.quic.Connection
import tech.lp2p.quic.ConnectionBuilder
import threads.thor.data.Peers
import threads.thor.model.bootstrap
import threads.thor.state.StateModel


class BootstrapTest {
    @Test
    @Throws(Exception::class)
    fun test_bootstrap() {

        val peers = Peers.getInstance(context!!)
        val blockStore: BlockStore = FileStore(context!!.filesDir)
        val lite = Lite.newBuilder()
            .bootstrap(bootstrap())
            .blockStore(blockStore)
            .peerStore(peers)
            .disableServer().build()

        for (peeraddr in lite.bootstrap()) {
            threads.thor.model.error(TAG, "Routing Peer $peeraddr")

            var connection: Connection
            try {
                connection = ConnectionBuilder.connect(lite, peeraddr)
            } catch (throwable: Throwable) {
                threads.thor.model.error(
                    TAG, "Connection failed " +
                            throwable.javaClass.simpleName
                )
                continue
            }
            try {
                TestCase.assertNotNull(connection)
                val info = IdentifyService.identify(connection)
                TestCase.assertNotNull(info)
                threads.thor.model.error(TAG, info.toString())
            } catch (throwable: Throwable) {
                threads.thor.model.error(
                    TAG, "PeerInfo failed " +
                            throwable.javaClass.simpleName
                )
            }
            TestCase.assertNotNull(connection)
            connection.close()
        }
    }

    @Test
    fun test_bootstrap_peers() {
        val bootstrap = bootstrap()
        TestCase.assertNotNull(bootstrap)


        for (address in bootstrap) {
            threads.thor.model.error(TAG, "Bootstrap $address")
        }
        TestCase.assertTrue(bootstrap.size >= 4)
    }

    companion object {
        private val TAG: String = BootstrapTest::class.java.simpleName
        private var context: Context? = null

        @JvmStatic
        @BeforeClass
        fun setup() {
            context = ApplicationProvider.getApplicationContext()
        }
    }
}
