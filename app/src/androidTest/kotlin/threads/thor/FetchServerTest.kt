package threads.thor

import junit.framework.TestCase
import org.junit.Assert
import org.junit.Test
import tech.lp2p.Lite
import tech.lp2p.core.BlockStore
import tech.lp2p.core.Fid
import tech.lp2p.core.Utils
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.nio.file.Files
import java.util.Random

class FetchServerTest {
    @Test
    @Throws(Exception::class)
    fun fetchDataTest() {
        Lite.newLite().use { server ->
            val blockStore = server.blockStore()
            val uri = server.loopbackAddress().toUri()

            var timestamp = System.currentTimeMillis()

            val fid = createContent(blockStore, 100) // 1000 * Short.MAX_VALUE
            TestCase.assertNotNull(fid)

            System.err.println(
                "Time Create Content " + (System.currentTimeMillis() - timestamp)
                        + " [ms] Size " + (fid.size / 1000000) + " MB"
            )

            blockStore.root(fid.cid)

            timestamp = System.currentTimeMillis()
            Lite.newLite().use { client ->
                val request = Lite.createFetchUri(uri, fid)
                val out = createCacheFile()
                FileOutputStream(out).use { outputStream ->
                    client.stream(request).use { inputStream ->
                        Utils.copy(inputStream, outputStream)
                    }
                }
                System.err.println(
                    "Time Client Read Content " +
                            (System.currentTimeMillis() - timestamp) + " [ms] "
                )

                Assert.assertEquals(out.length(), fid.size)
                Assert.assertTrue(out.delete())
            }
        }
    }

    companion object {
        private val random = Random()

        @Throws(Exception::class)
        fun createContent(blockStore: BlockStore, iteration: Int): Fid {
            val cacheFile = createCacheFile()
            FileOutputStream(cacheFile).use { outputStream ->
                for (i in 0 until iteration) {
                    val randomBytes = getRandomBytes(
                        Short.MAX_VALUE.toInt()
                    )
                    outputStream.write(randomBytes)
                }
            }
            val fid = blockStore.storeFile(cacheFile)
            val check = cacheFile.delete()
            if (!check) {
                threads.thor.model.error(FetchServerTest::class.java.simpleName, "file not deleted")
            }
            return fid
        }

        private fun getRandomBytes(number: Int): ByteArray {
            val bytes = ByteArray(number)
            random.nextBytes(bytes)
            return bytes
        }

        @Throws(IOException::class)
        fun createCacheFile(): File {
            return Files.createTempFile("temp", ".cid").toFile()
        }
    }
}
