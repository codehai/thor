package threads.magnet.net;

import java.net.InetAddress;

import threads.magnet.peer.PeerOptions;

public interface Peer {

    /**
     * @return Peer Internet address.
     * @since 1.0
     */
    InetAddress getInetAddress();

    /**
     * @return true, if the peer's listening port is not known yet
     * @see #getPort()
     * @since 1.9
     */
    boolean isPortUnknown();

    int getPort();

    /**
     * @return Peer options and preferences
     * @since 1.2
     */
    PeerOptions getOptions();
}
