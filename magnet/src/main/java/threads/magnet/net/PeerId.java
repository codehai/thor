package threads.magnet.net;

import java.util.Arrays;
import java.util.Random;

public record PeerId(byte[] bytes) {

    private static final int PEER_ID_LENGTH = 20;


    public static PeerId createPeerId(byte[] data) {
        if (data.length != PEER_ID_LENGTH) {
            throw new IllegalArgumentException("Invalid peer id length");
        }
        return new PeerId(data);
    }

    public static PeerId createPeerId() {
        return new PeerId(createID());
    }

    public static int length() {
        return PEER_ID_LENGTH;
    }

    private static byte[] buildVersionPrefix() {
        int major = 0; // Version mayor
        int minor = 5; // Version minor
        return new byte[]{'-', 'B', 't', (byte) major, (byte) minor, 0, (byte) 0, '-'};
    }

    private static byte[] buildPeerId(byte[] versionPrefix) {

        if (versionPrefix.length >= PeerId.length()) {
            throw new IllegalArgumentException("Prefix is too long: " + versionPrefix.length);
        }

        byte[] tail = new byte[PeerId.length() - versionPrefix.length];
        Random random = new Random(System.currentTimeMillis());
        random.nextBytes(tail);

        byte[] peerId = new byte[PeerId.length()];
        System.arraycopy(versionPrefix, 0, peerId, 0, versionPrefix.length);
        System.arraycopy(tail, 0, peerId, versionPrefix.length, tail.length);
        return peerId;
    }

    private static byte[] createID() {
        return buildPeerId(buildVersionPrefix());
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(bytes);
    }

    @Override
    public boolean equals(Object obj) {

        if (obj == null || !PeerId.class.isAssignableFrom(obj.getClass())) {
            return false;
        }
        return (obj == this) || Arrays.equals(bytes, ((PeerId) obj).bytes());
    }

}
