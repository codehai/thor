package threads.magnet.net;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import threads.magnet.LogUtils;
import threads.magnet.Settings;
import threads.magnet.metainfo.TorrentId;

public record ConnectionSource(PeerConnectionFactory connectionFactory,
                               PeerConnectionPool connectionPool,
                               ExecutorService connectionExecutor,
                               Map<ConnectionKey, CompletableFuture<ConnectionResult>> pendingConnections,
                               ConcurrentMap<Peer, Long> unreachablePeers,
                               IncomingConnectionListener incomingListener) {

    private static final String TAG = ConnectionSource.class.getSimpleName();

    public static ConnectionSource create(Set<SocketChannelConnectionAcceptor> connectionAcceptors,
                                          PeerConnectionFactory connectionFactory,
                                          PeerConnectionPool connectionPool) {
        ExecutorService connectionExecutor = Executors.newFixedThreadPool(
                Settings.maxPendingConnectionRequests);

        IncomingConnectionListener incomingListener = new IncomingConnectionListener(
                connectionAcceptors, connectionExecutor, connectionPool);

        return new ConnectionSource(connectionFactory, connectionPool,
                connectionExecutor, new ConcurrentHashMap<>(), new ConcurrentHashMap<>(),
                incomingListener);
    }

    public void startup() {
        incomingListener.startup();
    }

    public void shutdown() {
        connectionExecutor.shutdownNow();
        incomingListener.shutdown();
    }


    public void getConnectionAsync(Peer peer, TorrentId torrentId) {
        ConnectionKey key = new ConnectionKey(peer, peer.getPort(), torrentId);

        CompletableFuture<ConnectionResult> connection = getExistingOrPendingConnection(key);
        if (connection != null) {
            return;
        }

        Long bannedAt = unreachablePeers.get(peer);
        if (bannedAt != null) {
            if (System.currentTimeMillis() - bannedAt >= Settings.unreachablePeerBanDuration.toMillis()) {
                LogUtils.debug(TAG, "Removing temporary ban for unreachable peer");
                unreachablePeers.remove(peer);
            } else {

                CompletableFuture.completedFuture(ConnectionResult.failure());
                return;
            }
        }

        if (connectionPool.size() >= Settings.maxPeerConnections) {

            CompletableFuture.completedFuture(ConnectionResult.failure());
            return;
        }

        synchronized (pendingConnections) {
            connection = getExistingOrPendingConnection(key);
            if (connection != null) {
                return;
            }

            connection = CompletableFuture.supplyAsync(() -> {
                try {
                    ConnectionResult connectionResult =
                            connectionFactory.createOutgoingConnection(peer, torrentId);
                    if (connectionResult.isSuccess()) {
                        PeerConnection established = connectionResult.getConnection();
                        PeerConnection added = connectionPool.addConnectionIfAbsent(established);
                        if (added != established) {
                            established.closeQuietly();
                        }
                        return ConnectionResult.success(added);
                    } else {
                        return connectionResult;
                    }
                } finally {
                    synchronized (pendingConnections) {
                        pendingConnections.remove(key);
                    }
                }
            }, connectionExecutor).whenComplete((acquiredConnection, throwable) -> {
                if (acquiredConnection == null || throwable != null) {
                    unreachablePeers.putIfAbsent(peer, System.currentTimeMillis());
                }
                if (throwable != null) {
                    LogUtils.error(TAG,
                            "Failed to establish outgoing connection to peer: ", throwable);
                }
            });

            pendingConnections.put(key, connection);
        }
    }

    private CompletableFuture<ConnectionResult> getExistingOrPendingConnection(ConnectionKey key) {
        PeerConnection existingConnection = connectionPool.getConnection(key);
        if (existingConnection != null) {
            return CompletableFuture.completedFuture(ConnectionResult.success(existingConnection));
        }

        return pendingConnections.get(key);
    }
}
