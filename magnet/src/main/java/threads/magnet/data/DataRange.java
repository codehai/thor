package threads.magnet.data;


public interface DataRange extends Range {

    void visitUnits(DataRangeVisitor visitor);

    DataRange getSubrange(long offset, long length);

    DataRange getSubrange(long offset);
}
