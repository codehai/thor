package threads.magnet.data;

import java.nio.ByteBuffer;
import java.util.function.Function;

import threads.magnet.net.buffer.ByteBufferView;

public record SynchronizedDataRange(SynchronizedRange delegate,
                                    Function<Range, DataRange> converter) implements DataRange {


    @Override
    public void visitUnits(DataRangeVisitor visitor) {
        delegate.lock().writeLock().lock();
        try {
            converter.apply(delegate.delegate()).visitUnits(visitor);
        } finally {
            delegate.lock().writeLock().unlock();
        }
    }

    @Override
    public long length() {
        return delegate.length();
    }

    @Override
    public DataRange getSubrange(long offset, long length) {
        return new SynchronizedDataRange(delegate.getSubrange(offset, length), converter);
    }

    @Override
    public DataRange getSubrange(long offset) {
        return new SynchronizedDataRange(delegate.getSubrange(offset), converter);
    }

    @Override
    public byte[] getBytes() {
        return delegate.getBytes();
    }

    @Override
    public boolean getBytes(ByteBuffer buffer) {
        return delegate.getBytes(buffer);
    }

    @Override
    public void putBytes(byte[] block) {
        delegate.putBytes(block);
    }

    @Override
    public void putBytes(ByteBufferView buffer) {
        delegate.putBytes(buffer);
    }


}
