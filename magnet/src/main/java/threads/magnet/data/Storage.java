package threads.magnet.data;

import threads.magnet.metainfo.TorrentFile;

public interface Storage {
    StorageUnit getUnit(TorrentFile torrentFile);
}
