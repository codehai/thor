package threads.magnet.data;

public record ChunkDescriptor(SynchronizedDataRange data, BlockSet blockSet, byte[] checksum) {

    public int blockCount() {
        return blockSet.blockCount();
    }

    public long blockSize() {
        return blockSet.blockSize();
    }

    public boolean isPresent(int blockIndex) {
        return blockSet.isPresent(blockIndex);
    }

    public boolean isComplete() {
        return blockSet.isComplete();
    }

    public void clear() {
        blockSet.clear();
    }
}
