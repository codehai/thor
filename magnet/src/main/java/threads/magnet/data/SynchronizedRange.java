package threads.magnet.data;

import java.nio.ByteBuffer;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import threads.magnet.net.buffer.ByteBufferView;

public record SynchronizedRange(BlockRange delegate, ReadWriteLock lock) {

    public static SynchronizedRange createSynchronizedRange(BlockRange delegate) {
        return new SynchronizedRange(delegate, new ReentrantReadWriteLock());
    }

    public long length() {
        return delegate.length();
    }

    public SynchronizedRange getSubrange(long offset, long length) {
        return new SynchronizedRange(delegate.getSubrange(offset, length), lock);
    }

    public SynchronizedRange getSubrange(long offset) {
        return new SynchronizedRange(delegate.getSubrange(offset), lock);
    }

    public byte[] getBytes() {
        lock.readLock().lock();
        try {
            return delegate.getBytes();
        } finally {
            lock.readLock().unlock();
        }
    }


    public boolean getBytes(ByteBuffer buffer) {
        lock.readLock().lock();
        try {
            return delegate.getBytes(buffer);
        } finally {
            lock.readLock().unlock();
        }
    }

    public void putBytes(byte[] block) {
        lock.writeLock().lock();
        try {
            delegate.putBytes(block);
        } finally {
            lock.writeLock().unlock();
        }
    }


    public void putBytes(ByteBufferView buffer) {
        lock.writeLock().lock();
        try {
            delegate.putBytes(buffer);
        } finally {
            lock.writeLock().unlock();
        }
    }
}
