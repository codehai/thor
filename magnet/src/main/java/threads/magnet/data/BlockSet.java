package threads.magnet.data;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public record BlockSet(MutableBlockSet delegate, Lock lock) {

    public static BlockSet synchronizedBlockSet(MutableBlockSet blockSet) {
        return new BlockSet(blockSet, new ReentrantLock());
    }

    public int blockCount() {
        return delegate.blockCount();
    }

    public long blockSize() {
        return delegate.blockSize();
    }

    public long lastBlockSize() {
        return delegate.lastBlockSize();
    }

    public boolean isPresent(int blockIndex) {
        lock.lock();
        try {
            return delegate.isPresent(blockIndex);
        } finally {
            lock.unlock();
        }
    }

    public boolean isComplete() {
        lock.lock();
        try {
            return delegate.isComplete();
        } finally {
            lock.unlock();
        }
    }

    public void clear() {
        lock.lock();
        try {
            delegate.clear();
        } finally {
            lock.unlock();
        }
    }
}
