package threads.magnet.data;

import java.io.Closeable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import threads.magnet.LogUtils;
import threads.magnet.Settings;
import threads.magnet.event.EventBus;
import threads.magnet.metainfo.Torrent;
import threads.magnet.metainfo.TorrentFile;


public class DataDescriptor implements Closeable {

    private static final String TAG = DataDescriptor.class.getSimpleName();
    private final Storage storage;

    private final Torrent torrent;

    private final EventBus eventBus;
    private List<ChunkDescriptor> chunkDescriptors;
    private Bitfield bitfield;
    private Map<Integer, List<TorrentFile>> filesForPieces;
    private Set<StorageUnit> storageUnits;

    public DataDescriptor(Storage storage, Torrent torrent, EventBus eventBus) {
        this.storage = storage;
        this.torrent = torrent;
        this.eventBus = eventBus;

        init();

    }

    private static ChunkDescriptor buildChunkDescriptor(ReadWriteDataRange data,
                                                        long blockSize, byte[] checksum) {
        BlockRange blockRange = BlockRange.createBlockRange(data, blockSize);
        SynchronizedRange synchronizedRange = SynchronizedRange.createSynchronizedRange(blockRange);
        SynchronizedDataRange synchronizedData =
                new SynchronizedDataRange(synchronizedRange,
                        range -> (DataRange) ((BlockRange) range).delegate());
        BlockSet synchronizedBlockSet = new BlockSet(
                blockRange.mutableBlockSet(), synchronizedRange.lock().readLock());

        return new ChunkDescriptor(synchronizedData, synchronizedBlockSet, checksum);
    }

    private void init() {
        long transferBlockSize = Settings.transferBlockSize;
        List<TorrentFile> files = torrent.getFiles();

        long totalSize = torrent.size();
        long chunkSize = torrent.chunkSize();

        if (transferBlockSize > chunkSize) {
            transferBlockSize = chunkSize;
        }

        @SuppressWarnings("IntegerDivisionInFloatingPointContext")
        int chunksTotal = (int) Math.ceil(totalSize / chunkSize);
        Map<Integer, List<TorrentFile>> filesForPieces = new HashMap<>((int) (chunksTotal / 0.75d) + 1);
        List<ChunkDescriptor> chunks = new ArrayList<>(chunksTotal + 1);

        Iterator<byte[]> chunkHashes = torrent.getIterableChunkHashes().iterator();

        Map<StorageUnit, TorrentFile> storageUnitsToFilesMap = new LinkedHashMap<>((int) (files.size() / 0.75d) + 1);
        files.forEach(f -> storageUnitsToFilesMap.put(storage.getUnit(f), f));

        // filter out empty files (and create them at once)
        List<StorageUnit> nonEmptyStorageUnits = new ArrayList<>();
        for (StorageUnit unit : storageUnitsToFilesMap.keySet()) {
            if (unit.capacity() > 0) {
                nonEmptyStorageUnits.add(unit);
            }
        }

        if (!nonEmptyStorageUnits.isEmpty()) {
            long limitInLastUnit = nonEmptyStorageUnits.get(nonEmptyStorageUnits.size() - 1).capacity();
            ReadWriteDataRange data = ReadWriteDataRange.createReadWriteDataRange(
                    nonEmptyStorageUnits, 0, limitInLastUnit);

            long off, lim;
            long remaining = totalSize;
            while (remaining > 0) {
                off = chunks.size() * chunkSize;
                lim = Math.min(chunkSize, remaining);

                ReadWriteDataRange subrange = data.getSubrange(off, lim);

                if (!chunkHashes.hasNext()) {
                    throw new RuntimeException("Wrong number of chunk hashes in the torrent: too few");
                }

                List<TorrentFile> chunkFiles = new ArrayList<>();
                subrange.visitUnits((unit, off1, lim1) -> chunkFiles.add(storageUnitsToFilesMap.get(unit)));
                filesForPieces.put(chunks.size(), chunkFiles);

                chunks.add(buildChunkDescriptor(subrange, transferBlockSize, chunkHashes.next()));

                remaining -= chunkSize;
            }
        }

        if (chunkHashes.hasNext()) {
            throw new RuntimeException("Wrong number of chunk hashes in the threads.torrent: too many");
        }

        this.bitfield = buildBitfield(chunks);
        this.chunkDescriptors = chunks;
        this.storageUnits = storageUnitsToFilesMap.keySet();
        this.filesForPieces = filesForPieces;

    }

    private Bitfield buildBitfield(List<ChunkDescriptor> chunks) {
        Bitfield bitfield = new Bitfield(chunks.size());
        ChunkVerifier.verify(eventBus, torrent.torrentId(), chunks, bitfield);
        return bitfield;
    }


    public List<ChunkDescriptor> getChunkDescriptors() {
        return chunkDescriptors;
    }


    public Bitfield getBitfield() {
        return bitfield;
    }


    public List<TorrentFile> getFilesForPiece(int pieceIndex) {
        if (pieceIndex < 0 || pieceIndex >= bitfield.getPiecesTotal()) {
            throw new IllegalArgumentException("Invalid piece index: " + pieceIndex +
                    ", expected 0.." + bitfield.getPiecesTotal());
        }
        return filesForPieces.get(pieceIndex);
    }

    @Override
    public void close() {
        storageUnits.forEach(unit -> {
            try {
                unit.close();
            } catch (Exception e) {
                LogUtils.error(TAG, "Failed to close storage unit: " + unit);
            }
        });
    }

}
