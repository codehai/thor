package threads.magnet.data;

import java.nio.ByteBuffer;

import threads.magnet.net.buffer.ByteBufferView;


public record BlockRange(Range delegate, long offset,
                         MutableBlockSet mutableBlockSet) implements Range {

    public static BlockRange createBlockRange(Range delegate, long blockSize) {
        return new BlockRange(delegate, 0,
                MutableBlockSet.createMutableBlockSet(delegate.length(), blockSize));
    }

    @Override
    public long length() {
        return delegate.length();
    }

    @Override
    public BlockRange getSubrange(long offset, long length) {
        return new BlockRange(delegate.getSubrange(offset, length), offset, mutableBlockSet);
    }

    @Override
    public BlockRange getSubrange(long offset) {
        return new BlockRange(delegate.getSubrange(offset), offset, mutableBlockSet);
    }

    @Override
    public byte[] getBytes() {
        return delegate.getBytes();
    }

    @Override
    public boolean getBytes(ByteBuffer buffer) {
        return delegate.getBytes(buffer);
    }

    @Override
    public void putBytes(byte[] block) {
        delegate.putBytes(block);
        mutableBlockSet.markAvailable(offset, block.length);
    }

    @Override
    public void putBytes(ByteBufferView buffer) {
        int length = buffer.remaining();
        delegate.putBytes(buffer);
        mutableBlockSet.markAvailable(offset, length);
    }

}
