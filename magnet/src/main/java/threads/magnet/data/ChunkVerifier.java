package threads.magnet.data;


import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;

import threads.magnet.LogUtils;
import threads.magnet.Settings;
import threads.magnet.event.EventBus;
import threads.magnet.metainfo.TorrentId;

public interface ChunkVerifier {

    String TAG = ChunkVerifier.class.getSimpleName();
    int STEP = 2 << 22;  // 8 MB

    static void verify(EventBus eventBus, TorrentId torrentId, List<ChunkDescriptor> chunks, Bitfield bitfield) {
        if (chunks.size() != bitfield.getPiecesTotal()) {
            throw new IllegalArgumentException("Bitfield has different size than the list of chunks. Bitfield size: " +
                    bitfield.getPiecesTotal() + ", number of chunks: " + chunks.size());
        }

        collectParallel(eventBus, torrentId, chunks, bitfield);

    }

    static byte[] digest(SynchronizedDataRange data) {
        MessageDigest digest = createDigest();

        data.visitUnits((unit, off, lim) -> {
            long remaining = lim - off;
            if (remaining > Integer.MAX_VALUE) {
                throw new RuntimeException("Too much data -- can't read to buffer");
            }
            byte[] bytes = new byte[STEP];
            do {
                if (remaining < STEP) {
                    bytes = new byte[(int) remaining];
                }
                int read = unit.readBlock(bytes, off);
                if (read == -1) {
                    // end of data, terminate
                    return;
                } else if (read < bytes.length) {
                    digest.update(Arrays.copyOfRange(bytes, 0, read));
                    remaining -= read;
                    off += read;
                } else {
                    digest.update(bytes);
                    remaining -= STEP;
                    off += STEP;
                }
            } while (remaining > 0);

        });

        return digest.digest();
    }


    static byte[] digestForced(SynchronizedDataRange data) {
        MessageDigest digest = createDigest();

        data.visitUnits((unit, off, lim) -> {
            long remaining = lim - off;
            if (remaining > Integer.MAX_VALUE) {
                throw new RuntimeException("Too much data -- can't read to buffer");
            }
            byte[] bytes = new byte[STEP];
            ByteBuffer buffer = ByteBuffer.wrap(bytes);
            do {
                if (remaining < STEP) {
                    bytes = new byte[(int) remaining];
                    buffer = ByteBuffer.wrap(bytes);
                }
                buffer.clear();
                unit.readBlockFully(buffer, off);
                if (buffer.hasRemaining()) {
                    throw new IllegalStateException("Failed to read data fully: " + buffer.remaining() + " bytes remaining");
                }
                digest.update(bytes);
                remaining -= STEP;
                off += STEP;
            } while (remaining > 0);

        });

        return digest.digest();
    }

    static MessageDigest createDigest() {
        try {
            return MessageDigest.getInstance("SHA-1");
        } catch (NoSuchAlgorithmException e) {
            // not going to happen
            throw new RuntimeException("Unexpected error", e);
        }
    }

    static boolean verify(ChunkDescriptor chunk) {
        byte[] expected = chunk.checksum();
        byte[] actual = digestForced(chunk.data());
        return Arrays.equals(expected, actual);
    }


    private static boolean verifyIfPresent(ChunkDescriptor chunk) {
        byte[] expected = chunk.checksum();
        byte[] actual = digest(chunk.data());
        return Arrays.equals(expected, actual);
    }

    private static void collectParallel(EventBus eventBus, TorrentId torrentId,
                                        List<ChunkDescriptor> chunks, Bitfield bitfield) {
        int n = Settings.numOfHashingThreads;
        ExecutorService executor = Executors.newFixedThreadPool(n);

        List<Task> tasks = new ArrayList<>();

        int batchSize = chunks.size() / n;
        int i, limit = 0;
        while ((i = limit) < chunks.size()) {
            if (tasks.size() == n - 1) {
                // assign the remaining bits to the last worker
                limit = chunks.size();
            } else {
                limit = i + batchSize;
            }
            tasks.add(new Task(i, limit));
        }
        final AtomicBoolean exception = new AtomicBoolean(false);

        CountDownLatch latch = new CountDownLatch(tasks.size());
        for (Task task : tasks) {
            executor.execute(() -> {
                try {
                    executeVerify(eventBus, torrentId, chunks, task.task(), task.limit(), bitfield);
                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                    exception.set(true);
                } finally {
                    latch.countDown();
                }
            });
        }
        executor.shutdown();

        try {
            latch.await();
        } catch (InterruptedException e) {
            throw new RuntimeException("Unexpectedly interrupted");
        }

        if (exception.get()) {
            throw new RuntimeException("Failed to verify torrent data ");
        }

    }

    private static void executeVerify(EventBus eventBus, TorrentId torrentId,
                                      List<ChunkDescriptor> chunks,
                                      int from, int to, Bitfield bitfield) {

        int i = from;
        while (i < to) {
            // optimization to speedup the initial verification of threads.torrent's data
            int[] emptyUnits = new int[]{0};
            chunks.get(i).data().visitUnits((u, off, lim) -> {
                // limit of 0 means an empty file,
                // and we don't want to account for those
                if (u.size() == 0 && lim != 0) {
                    emptyUnits[0]++;
                }
            });

            // if any of this chunk's storage units is empty,
            // then the chunk is neither complete nor verified
            if (emptyUnits[0] == 0) {
                boolean verified = verifyIfPresent(chunks.get(i));
                if (verified) {
                    bitfield.markVerified(i);
                    eventBus.firePieceVerified(torrentId, i);
                }
            }
            i++;
        }

    }

    record Task(int task, int limit) {
    }

}
