package threads.magnet.dht;


import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.util.Objects;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Stream;

import threads.magnet.LogUtils;
import threads.magnet.Settings;
import threads.magnet.data.DataDescriptor;
import threads.magnet.event.EventSource;
import threads.magnet.kad.DHT;
import threads.magnet.kad.DHT.DHTtype;
import threads.magnet.kad.Key;
import threads.magnet.kad.PeerAddressDBItem;
import threads.magnet.kad.tasks.PeerLookupTask;
import threads.magnet.metainfo.TorrentId;
import threads.magnet.net.InetPeer;
import threads.magnet.net.InetPeerAddress;
import threads.magnet.net.Peer;
import threads.magnet.net.PeerId;
import threads.magnet.torrent.TorrentDescriptor;
import threads.magnet.torrent.TorrentRegistry;

public class DHTService {
    private static final String TAG = DHTService.class.getSimpleName();

    private final DHT dht;
    private final int port;
    private final int acceptorPort;
    private final PeerId peerId;

    private final TorrentRegistry torrentRegistry;

    public DHTService(PeerId peerId, TorrentRegistry torrentRegistry,
                      EventSource eventSource, int acceptorPort) {

        this.dht = new DHT(DHT.hasIpv6() ? DHTtype.IPV6_DHT : DHTtype.IPV4_DHT);
        this.acceptorPort = acceptorPort;

        this.torrentRegistry = torrentRegistry;

        eventSource.onTorrentStarted(e -> onTorrentStarted(e.getTorrentId()));
        this.peerId = peerId;
        this.port = nextFreePort();

    }

    private static int nextFreePort() {
        int port = ThreadLocalRandom.current().nextInt(10001, 65535);
        while (true) {
            if (isLocalPortFree(port)) {
                return port;
            } else {
                port = ThreadLocalRandom.current().nextInt(10001, 65535);
            }
        }
    }

    private static boolean isLocalPortFree(int port) {
        try {
            new ServerSocket(port).close();
            return true;
        } catch (IOException ignore) {
            return false;
        }
    }

    public int getPort() {
        return port;
    }

    public void startup() {
        try {
            dht.start(peerId, port);
            Settings.BOOTSTRAP_NODES.forEach(this::addNode);
        } catch (Throwable e) {
            throw new RuntimeException("Failed to start DHT", e);
        }
    }


    private void onTorrentStarted(TorrentId torrentId) {
        InetAddress localAddress = DHT.getInetAddressFromNetworkInterfaces();
        TorrentDescriptor td = torrentRegistry.getDescriptor(torrentId);
        if (td != null) {
            DataDescriptor dd = td.getDataDescriptor();
            boolean seed = (dd != null) && (dd.getBitfield().getPiecesIncomplete() == 0);
            dht.getDatabase().store(new Key(torrentId.bytes()),
                    PeerAddressDBItem.createFromAddress(localAddress, acceptorPort, seed));
        }
    }

    public void shutdown() {
        dht.stop();
    }


    Stream<Peer> getPeers(TorrentId torrentId) {
        try {
            dht.getServerManager().awaitActiveServer().get();
            final PeerLookupTask lookup = dht.createPeerLookup(torrentId.bytes());
            final StreamAdapter<Peer> streamAdapter = new StreamAdapter<>();

            Objects.requireNonNull(lookup);
            lookup.setResultHandler((k, p) -> {
                Peer peer = InetPeer.build(p.getInetAddress(), p.getPort());
                streamAdapter.addItem(peer);
            });
            lookup.addListener(t -> {
                streamAdapter.finishStream();
                if (torrentRegistry.isSupportedAndActive(torrentId)) {
                    TorrentDescriptor td = torrentRegistry.getDescriptor(torrentId);
                    if (td != null) {
                        DataDescriptor dd = td.getDataDescriptor();
                        boolean seed = (dd != null) && (dd.getBitfield().getPiecesIncomplete() == 0);
                        dht.announce(lookup, seed, acceptorPort);
                    }
                }
            });
            dht.getTaskManager().addTask(lookup);
            return streamAdapter.stream();
        } catch (Throwable e) {
            LogUtils.error(TAG, String.format("Unexpected error in peer lookup: %s. " +
                            "See DHT log file for diagnostic information.",
                    e.getMessage()), e);
            throw new RuntimeException(e);
        }
    }

    private void addNode(InetPeerAddress address) {
        addNode(address.getHostname(), address.getPort());
    }

    private void addNode(String hostname, int port) {
        dht.addDHTNode(hostname, port);
    }

}
