package threads.magnet.torrent;


import java.security.MessageDigest;

import threads.magnet.LogUtils;
import threads.magnet.data.BlockRange;
import threads.magnet.data.BlockSet;
import threads.magnet.data.ByteRange;
import threads.magnet.data.SynchronizedRange;

/**
 * BEP-9 threads.torrent metadata, thread-safe
 */
class ExchangedMetadata {
    private static final String TAG = ExchangedMetadata.class.getSimpleName();
    private final Object digestLock;
    private final SynchronizedRange metadata;
    private final BlockSet metadataBlocks;
    private volatile byte[] digest;

    ExchangedMetadata(byte[] data, int blockSize) {
        BlockRange range = BlockRange.createBlockRange(ByteRange.createByteRange(data), blockSize);
        this.metadata = SynchronizedRange.createSynchronizedRange(range);
        this.metadataBlocks = BlockSet.synchronizedBlockSet(range.mutableBlockSet());

        this.digestLock = new Object();
    }

    ExchangedMetadata(int totalSize, int blockSize) {
        this(new byte[totalSize], blockSize);
    }

    public boolean isBlockPresent(int blockIndex) {
        return metadataBlocks.isPresent(blockIndex);
    }

    public void setBlock(int blockIndex, byte[] block) {
        validateBlockIndex(blockIndex);
        metadata.getSubrange(blockIndex * metadataBlocks.blockSize()).putBytes(block);
    }

    public int getBlockCount() {
        return metadataBlocks.blockCount();
    }

    public boolean isComplete() {
        return metadataBlocks.isComplete();
    }

    /**
     * @throws IllegalStateException if metadata is not complete
     * @see #isComplete()
     */
    public byte[] getSha1Digest() {
        if (!metadataBlocks.isComplete()) {
            throw new IllegalStateException("Metadata is not complete");
        }

        if (digest == null) {
            synchronized (digestLock) {
                if (digest == null) {
                    try {
                        MessageDigest instance = MessageDigest.getInstance("SHA-1");
                        instance.update(metadata.getBytes());
                        digest = instance.digest();
                    } catch (Throwable throwable){
                        LogUtils.error(TAG, throwable);
                    }
                }
            }
        }
        return digest;
    }

    /**
     * @throws IllegalStateException if metadata is not complete
     * @see #isComplete()
     */
    public byte[] getBytes() {
        if (!metadataBlocks.isComplete()) {
            throw new IllegalStateException("Metadata is not complete");
        }

        return metadata.getBytes();
    }

    public byte[] getBlock(int blockIndex) {
        validateBlockIndex(blockIndex);

        int blockLength;
        // last piece may be smaller than the rest
        if (blockIndex == metadataBlocks.blockCount() - 1) {
            blockLength = (int) metadataBlocks.lastBlockSize();
        } else {
            blockLength = (int) metadataBlocks.blockSize();
        }

        return metadata.getSubrange(blockIndex * metadataBlocks.blockSize(), blockLength).getBytes();
    }

    public int length() {
        return (int) metadata.length();
    }

    private void validateBlockIndex(int blockIndex) {
        int blockCount = metadataBlocks.blockCount();
        if (blockIndex < 0 || blockIndex >= blockCount) {
            throw new IllegalArgumentException("Invalid block index: " + blockIndex + "; expected 0.." + blockCount);
        }
    }
}
