package threads.magnet.torrent;

import java.util.Objects;

public record Key(int pieceIndex, int offset, int length) {
    public static Key buildKey(int pieceIndex, int offset, int length) {
        return new Key(pieceIndex, offset, length);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Key key = (Key) o;
        return offset == key.offset && length == key.length && pieceIndex == key.pieceIndex;
    }

    @Override
    public int hashCode() {
        return Objects.hash(pieceIndex, offset, length);
    }
}
