package threads.magnet.torrent;

import java.util.Collection;
import java.util.Collections;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import threads.magnet.LogUtils;
import threads.magnet.data.DataDescriptor;
import threads.magnet.data.Storage;
import threads.magnet.event.EventBus;
import threads.magnet.metainfo.Torrent;
import threads.magnet.metainfo.TorrentId;

public final class TorrentRegistry {
    private static final String TAG = TorrentRegistry.class.getSimpleName();
    private final EventBus eventBus;

    private final Set<TorrentId> torrentIds = ConcurrentHashMap.newKeySet();
    private final ConcurrentMap<TorrentId, Torrent> torrents = new ConcurrentHashMap<>();
    private final ConcurrentMap<TorrentId, TorrentDescriptor> descriptors
            = new ConcurrentHashMap<>();

    public TorrentRegistry(EventBus eventBus) {
        this.eventBus = eventBus;
    }

    public Collection<TorrentId> getTorrentIds() {
        return Collections.unmodifiableCollection(torrentIds);
    }

    public Torrent getTorrent(TorrentId torrentId) {
        Objects.requireNonNull(torrentId, "Missing threads.torrent ID");
        return torrents.get(torrentId);
    }

    public TorrentDescriptor getDescriptor(TorrentId torrentId) {
        Objects.requireNonNull(torrentId, "Missing threads.torrent ID");
        return descriptors.get(torrentId);
    }

    public TorrentDescriptor register(Torrent torrent, Storage storage) {
        TorrentId torrentId = torrent.torrentId();

        TorrentDescriptor descriptor = descriptors.get(torrentId);
        if (descriptor != null) {
            if (descriptor.getDataDescriptor() != null) {
                throw new IllegalStateException(
                        "Torrent already registered and data descriptor created: " + torrent.torrentId());
            }
            descriptor.setDataDescriptor(new DataDescriptor(storage, torrent, eventBus));

        } else {
            descriptor = new TorrentDescriptor();
            descriptor.setDataDescriptor(new DataDescriptor(storage, torrent, eventBus));

            TorrentDescriptor existing = descriptors.putIfAbsent(torrentId, descriptor);
            if (existing != null) {
                descriptor = existing;
            } else {
                torrentIds.add(torrentId);
            }
        }

        torrents.putIfAbsent(torrentId, torrent);
        return descriptor;
    }

    public void shutdown() {
        for (TorrentDescriptor descriptor : descriptors.values()) {
            if (descriptor.getDataDescriptor() != null) {
                try {
                    descriptor.getDataDescriptor().close();
                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                }
            }
        }

    }

    public TorrentDescriptor register(TorrentId torrentId) {
        TorrentDescriptor torrentDescriptor = getDescriptor(torrentId);
        if (torrentDescriptor != null) {
            return torrentDescriptor;
        }
        TorrentDescriptor descriptor = new TorrentDescriptor();

        TorrentDescriptor existing = descriptors.putIfAbsent(torrentId, descriptor);
        if (existing != null) {
            descriptor = existing;
        } else {
            torrentIds.add(torrentId);
        }

        return descriptor;

    }

    public boolean isSupportedAndActive(TorrentId torrentId) {
        TorrentDescriptor descriptor = getDescriptor(torrentId);
        // it's OK if descriptor is not present -- torrent might be being fetched at the time
        return getTorrentIds().contains(torrentId)
                && (descriptor == null || descriptor.isActive());
    }
}
