package threads.magnet.kad;


public interface DBItem extends Comparable<DBItem> {

    byte[] item();

    // sort by raw data. only really useful for binary search
    default int compareTo(DBItem other) {
        return threads.magnet.utils.Arrays.compareUnsigned(item(), other.item());

    }
}
