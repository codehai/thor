package threads.magnet;

import java.io.File;

import threads.magnet.data.Storage;
import threads.magnet.data.StorageUnit;
import threads.magnet.event.EventBus;
import threads.magnet.metainfo.TorrentFile;


public record ContentStorage(File root, EventBus eventBus) implements Storage {

    public File getRootFile() {
        if (!root.exists()) {
            throw new IllegalArgumentException("data directory does not exists");
        }
        return root;
    }

    @Override
    public StorageUnit getUnit(TorrentFile torrentFile) {
        try {
            return new ContentStorageUnit(this, torrentFile);
        } catch (Throwable e) {
            throw new IllegalArgumentException(e);
        }
    }
}
