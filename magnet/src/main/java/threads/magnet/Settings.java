package threads.magnet;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.util.Arrays;
import java.util.Collection;
import java.util.concurrent.ThreadLocalRandom;

import threads.magnet.net.InetPeerAddress;
import threads.magnet.protocol.crypto.EncryptionPolicy;

public interface Settings {
    byte[] BYTES_EMPTY = new byte[0];
    // DHT
    int MAX_ENTRIES_PER_BUCKET = 8;
    int MAX_ACTIVE_TASKS = 7;
    int MAX_ACTIVE_CALLS = 256;
    int MAX_CONCURRENT_REQUESTS = 10;
    int RPC_CALL_TIMEOUT_MAX = 10 * 1000;
    int RPC_CALL_TIMEOUT_BASELINE_MIN = 100; // ms
    int BOOTSTRAP_IF_LESS_THAN_X_PEERS = 30;

    int MAX_SIMULTANEOUSLY_ASSIGNED_PIECES = 3;
    int DHT_UPDATE_INTERVAL = 1000;
    int BUCKET_REFRESH_INTERVAL = 15 * 60 * 1000;
    int RECEIVE_BUFFER_SIZE = 5 * 1024;
    int CHECK_FOR_EXPIRED_ENTRIES = 5 * 60 * 1000;
    int MAX_ITEM_AGE = 60 * 60 * 1000;
    int TOKEN_TIMEOUT = 5 * 60 * 1000;
    int MAX_DB_ENTRIES_PER_KEY = 6000;


    // enter survival mode if we don't see new packets after this time
    int REACHABILITY_TIMEOUT = 60 * 1000;
    int BOOTSTRAP_MIN_INTERVAL = 4 * 60 * 1000;
    int USE_BT_ROUTER_IF_LESS_THAN_X_PEERS = 10;
    int SELF_LOOKUP_INTERVAL = 30 * 60 * 1000;
    int RANDOM_LOOKUP_INTERVAL = 10 * 60 * 1000;

    int ANNOUNCE_CACHE_MAX_AGE = 30 * 60 * 1000;
    int ANNOUNCE_CACHE_FAST_LOOKUP_AGE = 8 * 60 * 1000;


    // MAGNET
    int maxPeerConnections = 500;
    int maxPeerConnectionsPerTorrent = 500;
    int transferBlockSize = 16 * 1024;
    int maxIOQueueSize = Integer.MAX_VALUE;
    int maxConcurrentlyActivePeerConnectionsPerTorrent = 10;
    int maxPendingConnectionRequests = 50;
    int metadataExchangeBlockSize = 16 * 1024; // 16 KB
    int metadataExchangeMaxSize = 2 * 1024 * 1024; // 2 MB
    int msePrivateKeySize = 20; // 20 bytes
    int maxOutstandingRequests = 250;
    int networkBufferSize = 1024 * 1024; // 1 MB


    Duration peerDiscoveryInterval = Duration.ofSeconds(5);

    Duration peerHandshakeTimeout = Duration.ofSeconds(30);
    Duration peerConnectionInactivityThreshold = Duration.ofMinutes(3);

    Duration maxPieceReceivingTime = Duration.ofSeconds(5);
    Duration maxMessageProcessingInterval = Duration.ofMillis(100);
    Duration unreachablePeerBanDuration = Duration.ofMinutes(30);
    Duration timeoutedAssignmentPeerBanDuration = Duration.ofMinutes(1);
    EncryptionPolicy encryptionPolicy = EncryptionPolicy.PREFER_PLAINTEXT;
    int numOfHashingThreads = java.lang.Runtime.getRuntime().availableProcessors() * 2;


    // DHT BOOTSTRAP
    Collection<InetPeerAddress> BOOTSTRAP_NODES = Arrays.asList(
            new InetPeerAddress("router.bittorrent.com", 6881),
            new InetPeerAddress("dht.transmissionbt.com", 6881),
            new InetPeerAddress("router.utorrent.com", 6881),
            new InetPeerAddress("dht.aelitis.com", 6881)
    );

    // DHT BOOTSTRAP
    InetSocketAddress[] UNRESOLVED_BOOTSTRAP_NODES = new InetSocketAddress[]{
            InetSocketAddress.createUnresolved("dht.transmissionbt.com", 6881),
            InetSocketAddress.createUnresolved("router.bittorrent.com", 6881),
            InetSocketAddress.createUnresolved("router.utorrent.com", 6881),
            InetSocketAddress.createUnresolved("dht.aelitis.com", 6881)
    };

    static String getDHTVersion() {
        return "ml" + new String(new byte[]{(byte) (11 >> 8 & 0xFF), (byte) (11 & 0xff)}, StandardCharsets.ISO_8859_1);
    }

    static int nextFreePort() {
        int port = ThreadLocalRandom.current().nextInt(4001, 65535);
        while (true) {
            if (isLocalPortFree(port)) {
                return port;
            } else {
                port = ThreadLocalRandom.current().nextInt(4001, 65535);
            }
        }
    }

    private static boolean isLocalPortFree(int port) {
        try {
            new ServerSocket(port).close();
            return true;
        } catch (IOException e) {
            return false;
        }
    }
}
