package threads.magnet.metainfo;

import java.util.Arrays;
import java.util.Objects;

import threads.magnet.protocol.Protocols;

public record TorrentId(byte[] bytes) {

    private static final int TORRENT_ID_LENGTH = 20;

    public static TorrentId createTorrentId(byte[] torrentId) {
        Objects.requireNonNull(torrentId);
        if (torrentId.length != TORRENT_ID_LENGTH) {
            throw new RuntimeException("Illegal threads.torrent ID length: " + torrentId.length);
        }
        return new TorrentId(torrentId);
    }

    public static int length() {
        return TORRENT_ID_LENGTH;
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(bytes);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || !TorrentId.class.isAssignableFrom(obj.getClass())) {
            return false;
        }
        return Arrays.equals(bytes, ((TorrentId) obj).bytes());
    }


    @Override
    public String toString() {
        return Protocols.toHex(bytes);
    }
}
