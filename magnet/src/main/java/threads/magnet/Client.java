package threads.magnet;

import java.io.File;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

import threads.magnet.magnet.MagnetUri;
import threads.magnet.metainfo.Torrent;
import threads.magnet.processor.ChainProcessor;
import threads.magnet.processor.Context;
import threads.magnet.processor.ListenerSource;
import threads.magnet.processor.ProcessingEvent;
import threads.magnet.processor.TorrentProcessorFactory;
import threads.magnet.torrent.TorrentSessionState;

public class Client {

    private final Runtime runtime;
    private final ChainProcessor processor;
    private final ListenerSource listenerSource;
    private final Context context;
    private final ScheduledExecutorService listenerExecutor =
            Executors.newSingleThreadScheduledExecutor();

    Client(Runtime runtime, ChainProcessor processor,
           Context context, ListenerSource listenerSource) {
        this.runtime = runtime;
        this.processor = processor;
        this.context = context;
        this.listenerSource = listenerSource;
    }

    public static Client create(File root, MagnetUri magnetUri) throws Exception {
        Runtime runtime = new Runtime(root);
        ListenerSource listenerSource = new ListenerSource();
        listenerSource.addListener(ProcessingEvent.DOWNLOAD_COMPLETE, (context, next) -> null);

        return new Client(runtime, TorrentProcessorFactory.createMagnetProcessor(runtime),
                new Context(magnetUri, runtime.storage()), listenerSource);
    }

    public static Client create(File root, Torrent torrent) throws Exception {
        Runtime runtime = new Runtime(root);
        ListenerSource listenerSource = new ListenerSource();
        listenerSource.addListener(ProcessingEvent.DOWNLOAD_COMPLETE, (context, next) -> null);

        return new Client(runtime, TorrentProcessorFactory.createTorrentProcessor(runtime),
                new Context(torrent, runtime.storage()), listenerSource);
    }


    public void start(Consumer<TorrentSessionState> listener, long period) {

        listenerExecutor.scheduleWithFixedDelay(() -> {
            TorrentSessionState state = context.getState();
            if (state != null) {
                listener.accept(state);
            }
        }, period, period, TimeUnit.MILLISECONDS);
        runtime.startup();
        processor.process(context, listenerSource);
        stop();
    }

    public synchronized void stop() {
        try {
            runtime.shutdown();
            listenerExecutor.shutdownNow();
        } catch (Throwable throwable) {
            LogUtils.error(Client.class.getSimpleName(), throwable);
        }
    }
}
