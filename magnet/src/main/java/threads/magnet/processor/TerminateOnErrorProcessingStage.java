package threads.magnet.processor;


import threads.magnet.LogUtils;

public abstract class TerminateOnErrorProcessingStage extends RoutingProcessingStage {

    TerminateOnErrorProcessingStage(ProcessingStage next) {
        super(next);
    }

    @Override
    protected final ProcessingStage doExecute(Context context, ProcessingStage next) {
        try {
            doExecute(context);
        } catch (Exception e) {
            LogUtils.error(LogUtils.TAG, e);
            next = null; // terminate processing chain
        }
        return next;
    }

    /**
     * Perform processing. Implementations are free to throw exceptions,
     * in which case the processing chain will be terminated.
     */
    protected abstract void doExecute(Context context);
}
