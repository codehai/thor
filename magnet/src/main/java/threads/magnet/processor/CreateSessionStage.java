package threads.magnet.processor;


import java.util.function.Supplier;

import threads.magnet.data.Bitfield;
import threads.magnet.event.EventSource;
import threads.magnet.metainfo.TorrentId;
import threads.magnet.net.ConnectionSource;
import threads.magnet.net.MessageDispatcher;
import threads.magnet.torrent.Assignments;
import threads.magnet.torrent.PieceStatistics;
import threads.magnet.torrent.TorrentDescriptor;
import threads.magnet.torrent.TorrentRegistry;
import threads.magnet.torrent.TorrentSessionState;
import threads.magnet.torrent.TorrentWorker;

public class CreateSessionStage extends TerminateOnErrorProcessingStage {

    private final TorrentRegistry torrentRegistry;
    private final EventSource eventSource;
    private final ConnectionSource connectionSource;
    private final MessageDispatcher messageDispatcher;

    public CreateSessionStage(ProcessingStage next,
                              TorrentRegistry torrentRegistry,
                              EventSource eventSource,
                              ConnectionSource connectionSource,
                              MessageDispatcher messageDispatcher) {
        super(next);
        this.torrentRegistry = torrentRegistry;
        this.eventSource = eventSource;
        this.connectionSource = connectionSource;
        this.messageDispatcher = messageDispatcher;

    }

    @Override
    protected void doExecute(Context context) {
        TorrentId torrentId = context.getTorrentId();
        TorrentDescriptor descriptor = torrentRegistry.register(torrentId);


        Supplier<Bitfield> bitfieldSupplier = context::getBitfield;
        Supplier<Assignments> assignmentsSupplier = context::getAssignments;
        Supplier<PieceStatistics> statisticsSupplier = context::getPieceStatistics;


        new TorrentWorker(torrentId, messageDispatcher,
                connectionSource, context.getRouter(),
                bitfieldSupplier, assignmentsSupplier, statisticsSupplier, eventSource);

        context.setState(new TorrentSessionState(descriptor));
    }

    @Override
    public ProcessingEvent after() {
        return null;
    }
}
