package threads.magnet.processor;

public abstract class RoutingProcessingStage implements ProcessingStage {

    private final ProcessingStage next;

    RoutingProcessingStage(ProcessingStage next) {
        this.next = next;
    }

    @Override
    public ProcessingStage execute(Context context) {
        return doExecute(context, next);
    }


    protected abstract ProcessingStage doExecute(Context context, ProcessingStage next);
}
