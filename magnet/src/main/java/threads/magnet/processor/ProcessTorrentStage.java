package threads.magnet.processor;

import java.util.Objects;

import threads.magnet.event.EventSink;
import threads.magnet.metainfo.TorrentId;
import threads.magnet.torrent.TorrentDescriptor;
import threads.magnet.torrent.TorrentRegistry;
import threads.magnet.torrent.TorrentSessionState;

public class ProcessTorrentStage extends TerminateOnErrorProcessingStage {

    private final TorrentRegistry torrentRegistry;

    private final EventSink eventSink;

    ProcessTorrentStage(TorrentRegistry torrentRegistry, EventSink eventSink) {
        super(null);
        this.torrentRegistry = torrentRegistry;
        this.eventSink = eventSink;
    }

    @Override
    protected void doExecute(Context context) {
        TorrentId torrentId = context.getTorrentId();
        TorrentDescriptor descriptor = getDescriptor(torrentId);

        descriptor.start();

        eventSink.fireTorrentStarted(torrentId);

        while (descriptor.isActive()) {
            TorrentSessionState state = context.getState();
            if (state != null) {
                if (state.getPiecesRemaining() == 0) {
                    break;
                }
            }
        }
    }


    private TorrentDescriptor getDescriptor(TorrentId torrentId) {
        TorrentDescriptor td = torrentRegistry.getDescriptor(torrentId);
        Objects.requireNonNull(td, "No descriptor present for threads.torrent ID: " + torrentId);
        return td;
    }

    @Override
    public ProcessingEvent after() {
        return ProcessingEvent.DOWNLOAD_COMPLETE;
    }
}
