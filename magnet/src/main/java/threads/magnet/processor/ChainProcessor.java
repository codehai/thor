package threads.magnet.processor;

import java.util.Collection;
import java.util.Collections;
import java.util.function.BiFunction;

import threads.magnet.LogUtils;

public record ChainProcessor(ProcessingStage chainHead, ContextFinalizer finalizer) {

    public void process(Context context, ListenerSource listenerSource) {
        executeStage(chainHead, context, listenerSource);
    }

    private void executeStage(ProcessingStage chainHead,
                              Context context,
                              ListenerSource listenerSource) {
        ProcessingEvent stageFinished = chainHead.after();
        Collection<BiFunction<Context, ProcessingStage, ProcessingStage>> listeners;
        if (stageFinished != null) {
            listeners = listenerSource.getListeners(stageFinished);
        } else {
            listeners = Collections.emptyList();
        }

        ProcessingStage next = doExecute(chainHead, context, listeners);
        if (next != null) {
            executeStage(next, context, listenerSource);
        }
    }

    private ProcessingStage doExecute(ProcessingStage stage,
                                      Context context,
                                      Collection<BiFunction<Context, ProcessingStage, ProcessingStage>> listeners) {


        ProcessingStage next;
        try {
            next = stage.execute(context);

        } catch (Exception e) {
            if (finalizer != null) {
                finalizer.finalizeContext(context);
            }
            throw e;
        }

        for (BiFunction<Context, ProcessingStage, ProcessingStage> listener : listeners) {
            try {
                // TODO: different listeners may return different next stages (including nulls)
                next = listener.apply(context, next);
            } catch (Exception e) {
                LogUtils.error(LogUtils.TAG, "Listener invocation failed", e);
            }
        }

        if (next == null) {
            if (finalizer != null) {
                finalizer.finalizeContext(context);
            }
        }
        return next;
    }
}
