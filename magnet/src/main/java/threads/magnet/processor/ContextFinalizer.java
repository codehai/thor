package threads.magnet.processor;

interface ContextFinalizer {

    void finalizeContext(Context context);
}
