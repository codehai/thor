package threads.magnet.bencoding;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * BEncoded list. May contain objects of different types.
 *
 * @since 1.0
 */
public record BEList(List<BEObject> value) implements BEObject {

    /**
     * @param value Parsed value
     * @since 1.0
     */
    public BEList(List<BEObject> value) {
        this.value = Collections.unmodifiableList(value);
    }

    @Override
    public void writeTo(OutputStream out) throws IOException {
        BEEncoder.encode(this, out);
    }

    @Override
    public boolean equals(Object obj) {

        if (!(obj instanceof BEList)) {
            return false;
        }

        if (obj == this) {
            return true;
        }

        return value.equals(((BEList) obj).value());
    }


    @Override
    public String toString() {
        return Arrays.toString(value.toArray());
    }
}
