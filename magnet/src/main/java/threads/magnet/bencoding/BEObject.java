package threads.magnet.bencoding;

import java.io.IOException;
import java.io.OutputStream;

public interface BEObject {
    void writeTo(OutputStream out) throws IOException;
}
