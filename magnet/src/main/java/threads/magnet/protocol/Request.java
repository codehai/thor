package threads.magnet.protocol;

public record Request(int pieceIndex, int offset, int length) implements Message {

    public Request {

        if (pieceIndex < 0 || offset < 0 || length <= 0) {
            throw new InvalidMessageException("Illegal arguments: piece index (" +
                    pieceIndex + "), offset (" + offset + "), length (" + length + ")");
        }

    }


    @Override
    public Integer getMessageId() {
        return StandardBittorrentProtocol.REQUEST_ID;
    }
}
