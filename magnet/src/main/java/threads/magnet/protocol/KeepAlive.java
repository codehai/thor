package threads.magnet.protocol;

public final class KeepAlive implements Message {

    private static final KeepAlive instance = new KeepAlive();

    private KeepAlive() {
    }

    public static KeepAlive instance() {
        return instance;
    }


    @Override
    public Integer getMessageId() {
        throw new UnsupportedOperationException();
    }
}
