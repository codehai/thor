package threads.magnet.protocol;

public final class NotInterested implements Message {

    private static final NotInterested instance = new NotInterested();

    private NotInterested() {
    }

    public static NotInterested instance() {
        return instance;
    }

    @Override
    public Integer getMessageId() {
        return StandardBittorrentProtocol.NOT_INTERESTED_ID;
    }
}
