package threads.magnet.protocol;

public final class Unchoke implements Message {

    private static final Unchoke instance = new Unchoke();

    private Unchoke() {
    }

    public static Unchoke instance() {
        return instance;
    }

    @Override
    public Integer getMessageId() {
        return StandardBittorrentProtocol.UNCHOKE_ID;
    }
}
