package threads.magnet.protocol;

public final class Choke implements Message {

    private static final Choke instance = new Choke();

    private Choke() {
    }

    public static Choke instance() {
        return instance;
    }

    @Override
    public Integer getMessageId() {
        return StandardBittorrentProtocol.CHOKE_ID;
    }
}
