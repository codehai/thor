package threads.magnet.protocol;

public final class Interested implements Message {

    private static final Interested instance = new Interested();

    private Interested() {
    }

    public static Interested instance() {
        return instance;
    }


    @Override
    public Integer getMessageId() {
        return StandardBittorrentProtocol.INTERESTED_ID;
    }
}
