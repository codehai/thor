package threads.magnet.protocol;

public record Bitfield(byte[] bitfield) implements Message {
    @Override
    public Integer getMessageId() {
        return StandardBittorrentProtocol.BITFIELD_ID;
    }
}
