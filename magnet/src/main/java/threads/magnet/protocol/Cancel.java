package threads.magnet.protocol;

public record Cancel(int pieceIndex, int offset, int length) implements Message {

    public Cancel {

        if (pieceIndex < 0 || offset < 0 || length <= 0) {
            throw new InvalidMessageException("Illegal arguments: piece index (" +
                    pieceIndex + "), offset (" + offset + "), length (" + length + ")");
        }
    }

    @Override
    public Integer getMessageId() {
        return StandardBittorrentProtocol.CANCEL_ID;
    }
}
