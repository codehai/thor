package threads.magnet.event;

import java.util.Objects;

import threads.magnet.metainfo.TorrentId;
import threads.magnet.net.ConnectionKey;

public class PeerDisconnectedEvent extends BaseEvent {

    private final ConnectionKey connectionKey;

    PeerDisconnectedEvent(long id, long timestamp, ConnectionKey connectionKey) {
        super(id, timestamp);
        this.connectionKey = Objects.requireNonNull(connectionKey);
    }


    public TorrentId getTorrentId() {
        return connectionKey.torrentId();
    }

    public ConnectionKey getConnectionKey() {
        return connectionKey;
    }


}
