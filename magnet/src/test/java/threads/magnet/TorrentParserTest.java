package threads.magnet;

import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;

import threads.magnet.metainfo.MetadataService;
import threads.magnet.metainfo.Torrent;

public class TorrentParserTest {
    final String path = "src/test/resources";

    @Test
    public void parseTorrent() throws IOException {
        File resources = new File(path);

        File file = new File(resources, "lubuntu-24.04-desktop-amd64.iso.torrent");

        try (InputStream inputStream = new FileInputStream(file)) {
            Torrent torrent = MetadataService.buildTorrent(inputStream.readAllBytes());
            Objects.requireNonNull(torrent);
        }
    }
}
