package tech.lp2p.ident;

import com.google.protobuf.ByteString;

import java.nio.ByteBuffer;
import java.util.Set;

import tech.lp2p.Lite;
import tech.lp2p.core.Identify;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Peeraddrs;
import tech.lp2p.core.Protocol;
import tech.lp2p.core.Utils;
import tech.lp2p.lite.LiteService;
import tech.lp2p.proto.IdentifyOuterClass;
import tech.lp2p.quic.Connection;
import tech.lp2p.quic.Requester;

public interface IdentifyService {
    String PROTOCOL_VERSION = "ipfs/0.1.0";


    static IdentifyOuterClass.Identify identify(PeerId self, String agent, Set<String> protocols) {


        IdentifyOuterClass.Identify.Builder builder = IdentifyOuterClass.Identify.newBuilder()
                .setAgentVersion(agent)
                .setPublicKey(ByteString.copyFrom(PeerId.identify(self)))
                .setProtocolVersion(PROTOCOL_VERSION);


        for (String protocol : protocols) {
            builder.addProtocols(protocol);
        }
        return builder.build();
    }


    static Identify identify(Connection connection) throws Exception {
        ByteBuffer data = Requester.createStream(connection)
                .request(Lite.TIMEOUT, Utils.encodeProtocols(Protocol.MULTISTREAM_PROTOCOL,
                        Protocol.IDENTITY_PROTOCOL));
        byte[] response = LiteService.receiveResponse(data);
        return identify(IdentifyOuterClass.Identify.parseFrom(response));
    }


    static Identify identify(IdentifyOuterClass.Identify identify) {

        String agent = identify.getAgentVersion();

        PeerId peerId = PeerId.identify(identify.getPublicKey().toByteArray());

        String[] protocols = new String[identify.getProtocolsCount()];
        identify.getProtocolsList().toArray(protocols);

        Peeraddrs peeraddrs = Peeraddrs.create(peerId, identify.getListenAddrsList());
        Peeraddr[] addresses = new Peeraddr[peeraddrs.size()];
        peeraddrs.toArray(addresses);

        return new Identify(peerId, agent, addresses, protocols);
    }

}
