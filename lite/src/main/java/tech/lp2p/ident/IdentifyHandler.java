package tech.lp2p.ident;

import tech.lp2p.Lite;
import tech.lp2p.core.Handler;
import tech.lp2p.core.Protocol;
import tech.lp2p.core.Utils;
import tech.lp2p.proto.IdentifyOuterClass;
import tech.lp2p.quic.Stream;

public final class IdentifyHandler implements Handler {

    private final IdentifyOuterClass.Identify identify;

    public IdentifyHandler(Lite host) {
        identify = IdentifyService.identify(
                host.peerId(), host.agent(), host.protocols().names());
    }

    @Override
    public void protocol(Stream stream) {
        Utils.error("IdentifyHandler: identify invoked");
        stream.writeOutput(true, Utils.encode(identify, Protocol.MULTISTREAM_PROTOCOL,
                Protocol.IDENTITY_PROTOCOL));
    }

    @Override
    public void data(Stream stream, byte[] data) throws Exception {
        if (data.length > 0) {
            throw new Exception("not expected data received for identify " + new String(data));
        }
    }

    @Override
    public void terminated(Stream stream) {
        // nothing to do here
    }

    @Override
    public void fin(Stream stream) {
        // nothing to do here
    }

}
