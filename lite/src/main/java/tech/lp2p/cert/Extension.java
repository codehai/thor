package tech.lp2p.cert;

import java.util.Objects;

/**
 * an object for the elements in the X.509 V3 extension block.
 */
public final class Extension extends ASN1Object {
    /**
     * Subject Directory Attributes
     */
    static final ASN1ObjectIdentifier subjectDirectoryAttributes = new ASN1ObjectIdentifier("2.5.29.9").intern();

    /**
     * Subject Alternative Name
     */
    static final ASN1ObjectIdentifier subjectAlternativeName = new ASN1ObjectIdentifier("2.5.29.17").intern();

    /**
     * Issuer Alternative Name
     */
    static final ASN1ObjectIdentifier issuerAlternativeName = new ASN1ObjectIdentifier("2.5.29.18").intern();

    /**
     * Certificate Issuer
     */
    static final ASN1ObjectIdentifier certificateIssuer = new ASN1ObjectIdentifier("2.5.29.29").intern();

    private final ASN1ObjectIdentifier extnId;
    private final boolean critical;
    private final ASN1OctetString value;

    /**
     * Constructor using a byte[] for the value.
     *
     * @param extnId   the OID associated with this extension.
     * @param critical true if the extension is critical, false otherwise.
     * @param value    the extension's value as a byte[] to be wrapped in an OCTET STRING.
     */
    Extension(ASN1ObjectIdentifier extnId, boolean critical, byte[] value) {
        this(extnId, critical, new DEROctetString(Arrays.clone(value)));
    }

    /**
     * Constructor using an OCTET STRING for the value.
     *
     * @param extnId   the OID associated with this extension.
     * @param critical true if the extension is critical, false otherwise.
     * @param value    the extension's value wrapped in an OCTET STRING.
     */
    Extension(ASN1ObjectIdentifier extnId, boolean critical, ASN1OctetString value) {
        this.extnId = extnId;
        this.critical = critical;
        this.value = value;
    }

    private Extension(ASN1Sequence seq) {
        if (seq.size() == 2) {
            this.extnId = ASN1ObjectIdentifier.getInstance(seq.getObjectAt(0));
            this.critical = false;
            this.value = ASN1OctetString.getInstance(seq.getObjectAt(1));
        } else if (seq.size() == 3) {
            this.extnId = ASN1ObjectIdentifier.getInstance(seq.getObjectAt(0));
            this.critical = ASN1Boolean.getInstance(seq.getObjectAt(1)).isTrue();
            this.value = ASN1OctetString.getInstance(seq.getObjectAt(2));
        } else {
            throw new IllegalArgumentException("Bad sequence size: " + seq.size());
        }
    }

    public static Extension getInstance(Object obj) {
        if (obj instanceof Extension) {
            return (Extension) obj;
        } else if (obj != null) {
            return new Extension(Objects.requireNonNull(ASN1Sequence.getInstance(obj)));
        }

        return null;
    }

    ASN1ObjectIdentifier getExtnId() {
        return extnId;
    }

    ASN1OctetString getExtnValue() {
        return value;
    }


    public boolean equals(
            Object o) {
        if (!(o instanceof Extension other)) {
            return false;
        }

        return other.extnId.equals(this.extnId)
                && other.value.equals(this.value)
                && (other.critical == this.critical);
    }

    public ASN1Primitive toASN1Primitive() {
        ASN1EncodableVector v = new ASN1EncodableVector(3);

        v.add(extnId);

        if (critical) {
            v.add(ASN1Boolean.getInstance(true));
        }

        v.add(value);

        return new DERSequence(v);
    }
}
