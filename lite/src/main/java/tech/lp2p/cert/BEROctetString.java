package tech.lp2p.cert;

import java.io.IOException;

/**
 * ASN.1 OctetStrings, with indefinite length rules, and <i>constructed form</i> support.
 * <p>
 * The Basic Encoding Rules (BER) format allows encoding using so called "<i>constructed form</i>",
 * which DER and CER formats forbid allowing only "primitive form".
 * </p><p>
 * This class <b>always</b> produces the constructed form with underlying segments
 * in an indefinite length array.  If the input wasn't the same, then this output
 * is not faithful reproduction.
 * </p>
 * <p>
 * See {@link ASN1OctetString} for X.690 encoding rules of OCTET-STRING objects.
 * </p>
 */
final class BEROctetString extends ASN1OctetString {
    private static final int DEFAULT_SEGMENT_LIMIT = 1000;

    private final int segmentLimit;
    private final ASN1OctetString[] elements;

    /**
     * Multiple {@link ASN1OctetString} data blocks are input,
     * the result is <i>constructed form</i>.
     *
     * @param elements an array of OCTET STRING to construct the BER OCTET STRING from.
     */
    BEROctetString(ASN1OctetString[] elements) {
        super(flattenOctetStrings(elements));
        this.elements = elements;
        this.segmentLimit = DEFAULT_SEGMENT_LIMIT;
    }

    /**
     * Convert a vector of octet strings into a single byte string
     */
    static byte[] flattenOctetStrings(ASN1OctetString[] octetStrings) {
        int count = octetStrings.length;
        switch (count) {
            case 0 -> {
                return EMPTY_OCTETS;
            }
            case 1 -> {
                return octetStrings[0].string;
            }
            default -> {
                int totalOctets = 0;
                for (ASN1OctetString octetString : octetStrings) {
                    totalOctets += octetString.string.length;
                }

                byte[] string = new byte[totalOctets];
                for (int i = 0, pos = 0; i < count; ++i) {
                    byte[] octets = octetStrings[i].string;
                    System.arraycopy(octets, 0, string, pos, octets.length);
                    pos += octets.length;
                }

//            assert pos == totalOctets;
                return string;
            }
        }
    }


    boolean encodeConstructed() {
        return null != elements || string.length > segmentLimit;
    }

    int encodedLength(boolean withTag)
            throws IOException {
        if (!encodeConstructed()) {
            return DEROctetString.encodedLength(withTag, string.length);
        }

        int totalLength = withTag ? 4 : 3;

        if (null != elements) {
            for (ASN1OctetString element : elements) {
                totalLength += element.encodedLength(true);
            }
        } else {
            int fullSegments = string.length / segmentLimit;
            totalLength += fullSegments * DEROctetString.encodedLength(true, segmentLimit);

            int lastSegmentLength = string.length - (fullSegments * segmentLimit);
            if (lastSegmentLength > 0) {
                totalLength += DEROctetString.encodedLength(true, lastSegmentLength);
            }
        }

        return totalLength;
    }

    void encode(ASN1OutputStream out, boolean withTag) throws IOException {
        if (!encodeConstructed()) {
            DEROctetString.encode(out, withTag, string, 0, string.length);
            return;
        }

        out.writeIdentifier(withTag, BERTags.CONSTRUCTED | BERTags.OCTET_STRING);
        out.write(0x80);

        if (null != elements) {
            out.writePrimitives(elements);
        } else {
            int pos = 0;
            while (pos < string.length) {
                int segmentLength = Math.min(string.length - pos, segmentLimit);
                DEROctetString.encode(out, true, string, pos, segmentLength);
                pos += segmentLength;
            }
        }

        out.write(0x00);
        out.write(0x00);
    }
}

