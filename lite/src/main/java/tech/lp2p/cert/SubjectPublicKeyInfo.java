package tech.lp2p.cert;

import java.util.Objects;

/**
 * The object that contains the public key stored in a certificate.
 * <p>
 * The getEncoded() method in the public keys in the JCE produces a DER
 * encoded one of these.
 */
public final class SubjectPublicKeyInfo extends ASN1Object {

    private final AlgorithmIdentifier algId;
    private final ASN1BitString keyData;

    private SubjectPublicKeyInfo(ASN1Sequence seq) {
        if (seq.size() != 2) {
            throw new IllegalArgumentException("Bad sequence size: "
                    + seq.size());
        }


        ASN1Encodable[] e = seq.toArrayInternal();

        this.algId = AlgorithmIdentifier.getInstance(e[0]);
        this.keyData = DERBitString.getInstance(e[1]);
    }

    public static SubjectPublicKeyInfo getInstance(Object obj) {
        if (obj instanceof SubjectPublicKeyInfo) {
            return (SubjectPublicKeyInfo) obj;
        } else if (obj != null) {
            return new SubjectPublicKeyInfo(Objects.requireNonNull(ASN1Sequence.getInstance(obj)));
        }

        return null;
    }


    /**
     * Produce an object suitable for an ASN1OutputStream.
     * <pre>
     * SubjectPublicKeyInfo ::= SEQUENCE {
     *                          algorithm AlgorithmIdentifier,
     *                          publicKey BIT STRING }
     * </pre>
     */
    public ASN1Primitive toASN1Primitive() {
        ASN1EncodableVector v = new ASN1EncodableVector(2);

        v.add(algId);
        v.add(keyData);

        return new DERSequence(v);
    }
}
