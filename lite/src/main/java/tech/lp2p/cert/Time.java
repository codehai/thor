package tech.lp2p.cert;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.SimpleTimeZone;

public final class Time extends ASN1Object implements ASN1Choice {
    private final ASN1Primitive time;

    private Time(ASN1Primitive time) {
        if (!(time instanceof ASN1UTCTime)
                && !(time instanceof ASN1GeneralizedTime)) {
            throw new IllegalArgumentException("unknown object passed to Time");
        }

        this.time = time;
    }

    /**
     * Creates a time object from a given date and locale - if the date is between 1950
     * and 2049 a UTCTime object is generated, otherwise a GeneralizedTime
     * is used. You may need to use this constructor if the default locale
     * doesn't use a Gregorian calender so that the GeneralizedTime produced is compatible with other ASN.1 implementations.
     *
     * @param time   a date object representing the time of interest.
     * @param locale an appropriate Locale for producing an ASN.1 GeneralizedTime value.
     */
    public Time(Date time, Locale locale) {
        SimpleTimeZone tz = new SimpleTimeZone(0, "Z");
        SimpleDateFormat dateF = new SimpleDateFormat("yyyyMMddHHmmss", locale);

        dateF.setTimeZone(tz);

        String d = dateF.format(time) + "Z";
        int year = Integer.parseInt(d.substring(0, 4));

        if (year < 1950 || year > 2049) {
            this.time = new DERGeneralizedTime(d);
        } else {
            this.time = new DERUTCTime(d.substring(2));
        }
    }

    public static Time getInstance(
            Object obj) {
        return switch (obj) {
            case null -> null;
            case Time ignored -> (Time) obj;
            case ASN1UTCTime asn1UTCTime -> new Time(asn1UTCTime);
            case ASN1GeneralizedTime asn1GeneralizedTime -> new Time(asn1GeneralizedTime);
            default ->
                    throw new IllegalArgumentException("unknown object in factory: " + obj.getClass().getName());
        };

    }


    /**
     * Produce an object suitable for an ASN1OutputStream.
     * <pre>
     * Time ::= CHOICE {
     *             utcTime        UTCTime,
     *             generalTime    GeneralizedTime }
     * </pre>
     */
    public ASN1Primitive toASN1Primitive() {
        return time;
    }

}
