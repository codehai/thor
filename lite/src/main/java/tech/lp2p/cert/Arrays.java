package tech.lp2p.cert;


/**
 * General array utilities.
 */
public final class Arrays {
    private Arrays() {
        // static class, hide constructor
    }

    public static boolean areEqual(byte[] a, byte[] b) {
        return java.util.Arrays.equals(a, b);
    }

    public static boolean areEqual(char[] a, char[] b) {
        return java.util.Arrays.equals(a, b);
    }

    public static int hashCode(byte[] data) {

        if (data == null) {
            return 0;
        }

        int i = data.length;
        int hc = i + 1;

        while (--i >= 0) {
            hc *= 257;
            hc ^= data[i];
        }

        return hc;
    }

    public static byte[] clone(byte[] data) {
        return null == data ? null : data.clone();
    }

    public static byte[] prepend(byte[] a, byte b) {
        if (a == null) {
            return new byte[]{b};
        }

        int length = a.length;
        byte[] result = new byte[length + 1];
        System.arraycopy(a, 0, result, 1, length);
        result[0] = b;
        return result;
    }

    public static boolean isNullOrContainsNull(Object[] array) {
        if (null == array) {
            return true;
        }
        for (Object o : array) {
            if (null == o) {
                return true;
            }
        }
        return false;
    }
}
