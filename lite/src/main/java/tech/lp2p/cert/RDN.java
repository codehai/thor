package tech.lp2p.cert;

/**
 * Holding class for a single Relative Distinguished Name (RDN).
 */
public final class RDN extends ASN1Object {
    private final ASN1Set values;

    /**
     * Create a single valued RDN.
     *
     * @param oid   RDN payloadType.
     * @param value RDN value.
     */
    RDN(ASN1ObjectIdentifier oid, ASN1Encodable value) {
        ASN1EncodableVector v = new ASN1EncodableVector(2);

        v.add(oid);
        v.add(value);

        this.values = new DERSet(new DERSequence(v));
    }


    /**
     * <pre>
     * RelativeDistinguishedName ::=
     *                     SET OF AttributeTypeAndValue
     *
     * AttributeTypeAndValue ::= SEQUENCE {
     *        payloadType     AttributeType,
     *        value    AttributeValue }
     * </pre>
     *
     * @return this object as its ASN1Primitive payloadType
     */
    public ASN1Primitive toASN1Primitive() {
        return values;
    }
}
