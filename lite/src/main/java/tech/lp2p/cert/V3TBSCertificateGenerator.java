package tech.lp2p.cert;

/**
 * Generator for Version 3 TBSCertificateStructures.
 * <pre>
 * TBSCertificate ::= SEQUENCE {
 *      version          [ 0 ]  Version DEFAULT v1(0),
 *      serialNumber            CertificateSerialNumber,
 *      signature               AlgorithmIdentifier,
 *      issuer                  Name,
 *      validity                Validity,
 *      subject                 Name,
 *      subjectPublicKeyInfo    SubjectPublicKeyInfo,
 *      issuerUniqueID    [ 1 ] IMPLICIT UniqueIdentifier OPTIONAL,
 *      subjectUniqueID   [ 2 ] IMPLICIT UniqueIdentifier OPTIONAL,
 *      extensions        [ 3 ] Extensions OPTIONAL
 *      }
 * </pre>
 */
public final class V3TBSCertificateGenerator {
    private final DERTaggedObject version = new DERTaggedObject(true, 0, new ASN1Integer(2));

    private ASN1Integer serialNumber;
    private AlgorithmIdentifier signature;
    private X500Name issuer;
    private Time startDate;
    private Time endDate;
    private X500Name subject;
    private SubjectPublicKeyInfo subjectPublicKeyInfo;
    private Extensions extensions;


    public V3TBSCertificateGenerator() {
    }

    public void setSerialNumber(ASN1Integer serialNumber) {
        this.serialNumber = serialNumber;
    }

    public void setSignature(AlgorithmIdentifier signature) {
        this.signature = signature;
    }

    public void setIssuer(X500Name issuer) {
        this.issuer = issuer;
    }

    public void setStartDate(Time startDate) {
        this.startDate = startDate;
    }

    public void setEndDate(Time endDate) {
        this.endDate = endDate;
    }

    public void setSubject(X500Name subject) {
        this.subject = subject;
    }

    public void setSubjectPublicKeyInfo(SubjectPublicKeyInfo pubKeyInfo) {
        this.subjectPublicKeyInfo = pubKeyInfo;
    }

    public void setExtensions(Extensions extensions) {
        this.extensions = extensions;
    }

    public TBSCertificate generateTBSCertificate() {
        if ((serialNumber == null) || (signature == null)
                || (issuer == null) || (startDate == null) || (endDate == null)
                || (subjectPublicKeyInfo == null)) {
            throw new IllegalStateException("not all mandatory fields set in V3 TBScertificate generator");
        }

        ASN1EncodableVector v = new ASN1EncodableVector(10);

        v.add(version);
        v.add(serialNumber);
        v.add(signature);
        v.add(issuer);

        //
        // before and after dates
        //
        {
            ASN1EncodableVector validity = new ASN1EncodableVector(2);
            validity.add(startDate);
            validity.add(endDate);

            v.add(new DERSequence(validity));
        }

        //noinspection ReplaceNullCheck
        if (subject != null) {
            v.add(subject);
        } else {
            v.add(new DERSequence());
        }

        v.add(subjectPublicKeyInfo);


        if (extensions != null) {
            v.add(new DERTaggedObject(true, 3, extensions));
        }

        return TBSCertificate.getInstance(new DERSequence(v));
    }
}
