/*
 * Copyright © 2019, 2020, 2021, 2022 Peter Doornbosch
 *
 * This file is part of Agent15, an implementation of TLS 1.3 in Java.
 *
 * Agent15 is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Agent15 is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package tech.lp2p.tls;

import java.nio.ByteBuffer;

// https://tools.ietf.org/html/rfc8446#section-4.3.1
public record EncryptedExtensions(Extension[] extensions, byte[] raw) implements HandshakeMessage {

    private static final int MINIMAL_MESSAGE_LENGTH = 1 + 3 + 2;


    public static EncryptedExtensions parse(ByteBuffer buffer,
                                            ExtensionParser customExtensionParser) throws ErrorAlert {
        if (buffer.remaining() < MINIMAL_MESSAGE_LENGTH) {
            throw new DecodeErrorException("Message too short");
        }

        int msgLength = buffer.getInt() & 0x00ffffff;
        if (buffer.remaining() < msgLength || msgLength < 2) {
            throw new DecodeErrorException("Incorrect message length");
        }

        Extension[] extensions = HandshakeMessage.parseExtensions(buffer,
                HandshakeType.server_hello, customExtensionParser);

        return new EncryptedExtensions(extensions, buffer.array());
    }

    @Override
    public HandshakeType getType() {
        return HandshakeType.encrypted_extensions;
    }


    @Override
    public byte[] getBytes() {
        return raw;
    }
}
