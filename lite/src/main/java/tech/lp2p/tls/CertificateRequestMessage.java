/*
 * Copyright © 2021, 2022 Peter Doornbosch
 *
 * This file is part of Agent15, an implementation of TLS 1.3 in Java.
 *
 * Agent15 is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Agent15 is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package tech.lp2p.tls;

import java.nio.ByteBuffer;

// https://tools.ietf.org/html/rfc8446#section-4.3.2
public record CertificateRequestMessage(byte[] certificateRequestContext,
                                        Extension[] extensions,
                                        byte[] raw) implements HandshakeMessage {

    private static final int MINIMUM_MESSAGE_SIZE = 1 + 3 + 1 + 2;


    public static CertificateRequestMessage parse(ByteBuffer buffer) throws ErrorAlert {
        int startPosition = buffer.position();
        int remainingLength = HandshakeMessage.parseHandshakeHeader(buffer, HandshakeType.certificate_request, MINIMUM_MESSAGE_SIZE);

        int contextLength = buffer.get();
        byte[] certificateRequestContext = new byte[contextLength];
        if (contextLength > 0) {
            buffer.get(certificateRequestContext);
        }

        Extension[] extensions = HandshakeMessage.parseExtensions(
                buffer, HandshakeType.certificate_request, null);

        if (buffer.position() - (startPosition + 4) != remainingLength) {
            throw new DecodeErrorException("inconsistent length");
        }


        return new CertificateRequestMessage(certificateRequestContext, extensions, buffer.array());
    }


    @Override
    public HandshakeType getType() {
        return HandshakeType.certificate_request;
    }

    @Override
    public byte[] getBytes() {
        return raw;
    }

}
