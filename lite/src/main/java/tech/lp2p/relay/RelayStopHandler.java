package tech.lp2p.relay;

import static tech.lp2p.proto.Circuit.Status.MALFORMED_MESSAGE;

import java.net.InetAddress;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;

import tech.lp2p.Lite;
import tech.lp2p.core.Handler;
import tech.lp2p.core.Network;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Peeraddrs;
import tech.lp2p.core.Protocol;
import tech.lp2p.core.Utils;
import tech.lp2p.pns.PnsServer;
import tech.lp2p.proto.Circuit;
import tech.lp2p.quic.Requester;
import tech.lp2p.quic.Stream;


public final class RelayStopHandler implements Handler {

    private final Lite lite;
    private final Function<PeerId, Boolean> isGated;
    private final PnsServer pnsServer;

    public RelayStopHandler(Lite lite, PnsServer pnsServer, Function<PeerId, Boolean> isGated) {
        this.lite = lite;
        this.pnsServer = pnsServer;
        this.isGated = isGated;
    }


    private static void createStatusMessage(Stream stream, Circuit.Status status) {
        Circuit.StopMessage.Builder builder =
                Circuit.StopMessage.newBuilder()
                        .setType(Circuit.StopMessage.Type.STATUS);
        builder.setStatus(status);
        stream.writeOutput(true, Utils.encode(builder.build()));
    }

    @Override
    public void protocol(Stream stream) {
        stream.writeOutput(false, Utils.encodeProtocols(Protocol.MULTISTREAM_PROTOCOL,
                Protocol.RELAY_PROTOCOL_STOP));
    }

    @Override
    public void data(Stream stream, byte[] data) throws Exception {

        if (stream.hasAttribute(Requester.PEER)) {
            PeerId target = (PeerId) stream.getAttribute(Requester.PEER);
            Objects.requireNonNull(target);

            RelayHolePunch holePunch = RelayHolePunch.decode(target, data);

            switch (holePunch.type()) {
                case CONNECT -> {
                    Peeraddrs peeraddrs = holePunch.peeraddrs();

                    if (peeraddrs.isEmpty()) {
                        throw new Exception("Received empty peeraddrs");
                    }
                    stream.setAttribute(Requester.ADDRS, peeraddrs);

                    stream.writeOutput(false,
                            RelayHolePunch.encode(lite.keys(), RelayHolePunch.Type.CONNECT,
                                    lite.peeraddrs()));
                }
                case SYNC -> {
                    Peeraddrs peeraddrs = (Peeraddrs) stream.getAttribute(Requester.ADDRS);
                    Objects.requireNonNull(peeraddrs, "No Peeraddrs"); // should not happen


                    for (Peeraddr peeraddr : peeraddrs) {
                        InetAddress inetAddress = InetAddress.getByAddress(peeraddr.address());
                        if (!Network.isLanAddress(inetAddress)) {
                            // hole punching not needed for LAN addressed
                            holePunching(pnsServer, peeraddr);
                        }
                    }

                    stream.fin(); // done here
                }
                default -> throw new Exception("invalid hole punch type");
            }
        } else {
            try {
                Circuit.StopMessage msg = Circuit.StopMessage.parseFrom(data);
                Objects.requireNonNull(msg);


                if (msg.getType() != Circuit.StopMessage.Type.CONNECT) {
                    createStatusMessage(stream, Circuit.Status.MALFORMED_MESSAGE);
                    return;
                }
                if (!msg.hasPeer()) {
                    createStatusMessage(stream, Circuit.Status.MALFORMED_MESSAGE);
                    return;
                }
                Circuit.Peer peer = msg.getPeer();

                Optional<PeerId> parsePeerId = PeerId.parse(peer.getId().toByteArray());

                //noinspection SimplifyOptionalCallChains
                if (!parsePeerId.isPresent()) {
                    createStatusMessage(stream, MALFORMED_MESSAGE);
                    return;
                }
                PeerId peerId = parsePeerId.get();

                if (Objects.equals(peerId, lite.peerId())) {
                    createStatusMessage(stream, Circuit.Status.PERMISSION_DENIED);
                    return;
                }

                if (isGated.apply(peerId)) {
                    createStatusMessage(stream, Circuit.Status.PERMISSION_DENIED);
                    return;
                }

                Circuit.StopMessage.Builder builder =
                        Circuit.StopMessage.newBuilder()
                                .setType(Circuit.StopMessage.Type.STATUS);
                builder.setStatus(Circuit.Status.OK);


                stream.writeOutput(false, Utils.encode(builder.build()));
                stream.setAttribute(Requester.PEER, peerId);


            } catch (Throwable throwable) {
                createStatusMessage(stream, Circuit.Status.UNEXPECTED_MESSAGE);
            }
        }
    }


    public void holePunching(PnsServer pnsServer, Peeraddr peeraddr) {
        long expireTimeStamp = System.currentTimeMillis() + (Lite.TIMEOUT * 1000); // ms
        new Thread(() -> pnsServer.punching(peeraddr, expireTimeStamp)).start();
    }

    @Override
    public void terminated(Stream stream) {
        stream.removeAttribute(Requester.PEER);
    }

    @Override
    public void fin(Stream stream) {
        stream.removeAttribute(Requester.PEER);
    }
}
