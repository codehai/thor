package tech.lp2p.relay;

import com.google.protobuf.ByteString;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import tech.lp2p.Lite;
import tech.lp2p.core.Keys;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Peeraddrs;
import tech.lp2p.core.Protocol;
import tech.lp2p.core.Utils;
import tech.lp2p.lite.LiteService;
import tech.lp2p.proto.Circuit;
import tech.lp2p.quic.Connection;
import tech.lp2p.quic.ConnectionBuilder;
import tech.lp2p.quic.Requester;
import tech.lp2p.quic.Stream;

public interface RelayService {


    static Peeraddr hopConnect(Lite host, Peeraddr relayAddress, PeerId target)
            throws Exception {
        Connection connection = null;
        try {
            connection = ConnectionBuilder.connect(host, relayAddress);
            RelayInfo relayInfo = relayPunchInfo(host, target);
            return hopConnect(connection, relayInfo);
        } finally {
            if (connection != null) {
                connection.close();
            }
        }
    }


    static Peeraddr hopConnect(Connection connection, RelayInfo relayInfo) throws Exception {

        CompletableFuture<SyncInfo> done = new CompletableFuture<>();

        Circuit.Peer.Builder builder = Circuit.Peer.newBuilder()
                .setId(ByteString.copyFrom(PeerId.multihash(relayInfo.target())));

        Circuit.HopMessage message = Circuit.HopMessage.newBuilder()
                .setType(Circuit.HopMessage.Type.CONNECT)
                .setPeer(builder.build())
                .build();

        Requester.createStream(connection, new ConnectRequest(done,
                relayInfo)).writeOutput(false,
                Utils.encode(message, Protocol.MULTISTREAM_PROTOCOL,
                        Protocol.RELAY_PROTOCOL_HOP));

        SyncInfo syncInfo = done.get(Lite.GRACE_PERIOD, TimeUnit.SECONDS);

        // Next, wait for rtt/2
        Thread.sleep(syncInfo.rtt() / 2);


        return syncInfo.peeraddr();
    }


    static void hopReserve(Connection connection, PeerId self) throws
            InterruptedException, TimeoutException, IOException {

        Circuit.Peer.Builder peerBuilder = Circuit.Peer.newBuilder()
                .setId(ByteString.copyFrom(PeerId.multihash(self)));

        Circuit.HopMessage message = Circuit.HopMessage.newBuilder()
                .setPeer(peerBuilder.build())
                .setType(Circuit.HopMessage.Type.RESERVE).build();


        ByteBuffer data = Requester.createStream(connection).request(
                Lite.TIMEOUT, Utils.encode(message, Protocol.MULTISTREAM_PROTOCOL,
                        Protocol.RELAY_PROTOCOL_HOP));


        byte[] response = LiteService.receiveResponse(data);
        if (response.length > 0) {
            Circuit.HopMessage msg = Circuit.HopMessage.parseFrom(response);

            if (msg.getType() != Circuit.HopMessage.Type.STATUS) {
                throw new IOException("NO RESERVATION STATUS");
            }
            if (msg.getStatus() != Circuit.Status.OK) {
                throw new IOException("RESERVATION STATUS = " + msg.getStatus());
            }
            if (!msg.hasReservation()) {
                throw new IOException("NO RESERVATION");
            }
            Circuit.Reservation reserve = msg.getReservation();
            Objects.requireNonNull(reserve);
            return;
        }
        throw new IOException("No Hop Service");
    }


    static RelayInfo relayPunchInfo(Lite host, PeerId target) throws Exception {

        Peeraddrs peeraddrs = host.peeraddrs();
        if (peeraddrs.isEmpty()) {
            throw new Exception("Hole punching not possible [abort]");
        }
        return new RelayInfo(target, host.keys(), peeraddrs);
    }


    record ConnectRequest(CompletableFuture<SyncInfo> done,
                          RelayInfo relayInfo) implements Requester {

        @Override
        public void throwable(Throwable throwable) {
            done.completeExceptionally(throwable);
        }

        @Override
        public void fin(Stream stream) {
            if (!done.isDone()) {
                done.completeExceptionally(new Throwable("stream finished before message"));
            }
            stream.removeAttribute(INITIALIZED);
        }

        @Override
        public void terminated(Stream stream) {
            if (!done.isDone()) {
                done.completeExceptionally(new Throwable("stream terminated"));
            }
            stream.removeAttribute(INITIALIZED);
        }

        @Override
        public void data(Stream stream, byte[] data) throws Exception {

            if (stream.hasAttribute(INITIALIZED)) {
                SyncInfo syncInfo = sendSync(stream, data);
                done.complete(syncInfo);
            } else {
                Circuit.HopMessage msg = Circuit.HopMessage.parseFrom(data);
                Objects.requireNonNull(msg);

                if (msg.getType() != Circuit.HopMessage.Type.STATUS) {
                    throwable(new Exception("Malformed message"));
                    stream.close();
                    return;
                }

                if (msg.getStatus() != Circuit.Status.OK) {
                    throwable(new Exception("No reservation reason " + msg.getStatus().name()));
                    stream.close();
                    return;
                }

                // Note the limit will not be checked even it is a non limited relay
                // always a hole punch will be done

                initializeConnect(stream);
                stream.setAttribute(INITIALIZED, true);

            }
        }


        private SyncInfo sendSync(Stream stream, byte[] data) throws Exception {

            Long timer = (Long) stream.getAttribute(Requester.TIMER); // timer set earlier
            Objects.requireNonNull(timer, "Timer not set on stream");
            long rtt = System.currentTimeMillis() - timer;

            // B receives the Connect message from A

            RelayHolePunch msg = RelayHolePunch.decode(relayInfo.target(), data);
            Objects.requireNonNull(msg, "Message is not defined");

            if (msg.type() != RelayHolePunch.Type.CONNECT) {
                throw new Exception("[A] send wrong message connect payloadType, abort");
            }

            Peeraddrs peeraddrs = msg.peeraddrs();

            if (peeraddrs.isEmpty()) {
                throw new Exception("[A] send no observed addresses, abort");
            }


            stream.writeOutput(true,
                    RelayHolePunch.encode(relayInfo.keys,
                            RelayHolePunch.Type.SYNC, Peeraddrs.empty()));

            Optional<Peeraddr> best = Peeraddrs.best(peeraddrs);
            if (best.isPresent()) {
                return new SyncInfo(best.get(), rtt);
            } else {
                throw new Exception("No peeraddr could be evaluated.");
            }
        }

        void initializeConnect(Stream stream) throws Exception {
            stream.setAttribute(Requester.TIMER, System.currentTimeMillis());
            stream.writeOutput(false,
                    RelayHolePunch.encode(relayInfo.keys, RelayHolePunch.Type.CONNECT,
                            relayInfo.peeraddrs));
        }
    }

    record SyncInfo(Peeraddr peeraddr, long rtt) {
    }

    record RelayInfo(PeerId target, Keys keys, Peeraddrs peeraddrs) {
    }
}