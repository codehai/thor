package tech.lp2p.relay;


import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import tech.lp2p.core.Keys;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Peeraddrs;
import tech.lp2p.core.Utils;

public record RelayHolePunch(Type type, Peeraddrs peeraddrs) {

    public RelayHolePunch {
        Utils.checkTrue(peeraddrs.size() <= Byte.MAX_VALUE, "to many peeraddrs");
    }

    public static ByteBuffer encode(Keys keys, Type type, Peeraddrs peeraddrs) throws Exception {

        Utils.checkTrue(peeraddrs.size() <= Byte.MAX_VALUE, "to many peeraddrs");

        int size = 1 + Byte.BYTES;

        Set<PeerId> checkIfValid = new HashSet<>();

        ArrayList<byte[]> encodedAddresses = new ArrayList<>(peeraddrs.size());

        byte[] toVerify = Utils.BYTES_EMPTY;
        for (Peeraddr peeraddr : peeraddrs) {
            checkIfValid.add(peeraddr.peerId());
            byte[] encoded = peeraddr.encoded();
            encodedAddresses.add(encoded);
            toVerify = Utils.concat(toVerify, encoded);

            size = size + Utils.unsignedVariantSize(encoded.length) + encoded.length;
        }

        // only addresses of the same peerId
        Utils.checkTrue(checkIfValid.size() <= 1, "Invalid usage");

        byte[] signature = keys.sign(toVerify);
        size = size + Utils.unsignedVariantSize(signature.length) + signature.length;

        int dataLength = Utils.unsignedVariantSize(size);

        ByteBuffer buffer = ByteBuffer.allocate(size + dataLength);
        Utils.writeUnsignedVariant(buffer, size);
        buffer.put(Type.encode(type));
        buffer.put((byte) peeraddrs.size());


        for (byte[] encoded : encodedAddresses) {
            Utils.writeUnsignedVariant(buffer, encoded.length);
            buffer.put(encoded);
        }

        Utils.writeUnsignedVariant(buffer, signature.length);
        buffer.put(signature);

        Utils.checkTrue(!buffer.hasRemaining(), "Still data to write");
        buffer.rewind();
        return buffer;

    }

    public static RelayHolePunch decode(PeerId peerId, byte[] data) throws Exception {

        ByteBuffer buffer = ByteBuffer.wrap(data);

        Type type = Type.decode(buffer.get());

        byte[] toVerify = Utils.BYTES_EMPTY;
        byte size = buffer.get();
        Peeraddrs peeraddrs = new Peeraddrs(size);
        for (int i = 0; i < size; i++) {
            int length = Utils.readUnsignedVariant(buffer);
            byte[] raw = new byte[length];
            buffer.get(raw);
            toVerify = Utils.concat(toVerify, raw);
            peeraddrs.add(Peeraddr.create(peerId, raw));
        }

        int sigSize = Utils.readUnsignedVariant(buffer);
        byte[] signature = new byte[sigSize];
        buffer.get(signature);

        peerId.verify(toVerify, signature);

        Utils.checkTrue(!buffer.hasRemaining(), "still data available");

        return new RelayHolePunch(type, peeraddrs);

    }

    public enum Type {
        CONNECT, SYNC;

        static byte encode(Type type) {
            return switch (type) {
                case CONNECT -> (byte) 0x00;
                case SYNC -> (byte) 0x01;
            };
        }

        static Type decode(byte value) {
            if (value == 0x00) {
                return CONNECT;
            }
            if (value == 0x01) {
                return SYNC;
            }
            throw new IllegalStateException();
        }
    }
}