package tech.lp2p.dag;


import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.WritableByteChannel;
import java.util.function.Consumer;

import tech.lp2p.core.Channel;
import tech.lp2p.core.Fid;
import tech.lp2p.core.Link;
import tech.lp2p.core.Utils;


public final class DagChannel implements Channel {

    private final DagFetch dagFetch;
    private final Fid root;
    private final long size;
    private int index = -1;
    private int left = -1;

    private DagChannel(DagFetch dagFetch, Fid root, long size) {
        this.size = size;
        this.root = root;
        this.dagFetch = dagFetch;
    }

    public static DagChannel create(DagFetch dagFetch, Fid fid) {
        long size = fid.size();
        return new DagChannel(dagFetch, fid, size);

    }

    public long size() {
        return size;
    }

    private ByteBuffer next() throws IOException {
        index++;

        if (index >= root.links().size()) {
            return ByteBuffer.allocate(0);
        }
        Link link = root.links().get(index);
        return dagFetch.getBlock(link.cid());
    }


    public void seek(long offset) {
        Utils.checkTrue(offset >= 0, "invalid offset");

        if (offset == 0) {
            this.left = 0;
            this.index = 0;
            return;
        }

        if (!root.links().isEmpty()) {

            // Internal nodes have no data, so just iterate through the
            // sizes of its children (advancing the child index of the
            // `dagWalker`) to find where we need to go down to next in
            // the search
            long div = Math.floorDiv(offset, Short.MAX_VALUE);

            Utils.checkTrue(div < Integer.MAX_VALUE, "Invalid number of links");

            this.index = (int) div;

            this.left = Math.floorMod(offset, Short.MAX_VALUE);
        } else {
            this.left = (int) offset;
        }
    }

    public ByteBuffer loadNextData() throws IOException {

        int offset = this.left;
        this.left = -1;

        if (offset >= 0) {
            Link link = root.links().get(index);
            ByteBuffer data = dagFetch.getBlock(link.cid());
            data.rewind();
            data.position(offset);
            return data;
        }

        ByteBuffer data = next();
        data.rewind();
        return data;
    }

    @Override
    public void transferTo(WritableByteChannel channel, Consumer<Integer> read) throws IOException {
        int n;
        while ((n = channel.write(loadNextData())) > 0) {
            read.accept(n);
        }
    }

    @Override
    public void transferTo(WritableByteChannel channel) throws IOException {
        //noinspection StatementWithEmptyBody
        while (channel.write(loadNextData()) > 0) ;
    }
}
