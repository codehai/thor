package tech.lp2p.dag;

import java.io.IOException;
import java.nio.ByteBuffer;

public interface DagFetch {

    ByteBuffer getBlock(long cid) throws IOException;

}
