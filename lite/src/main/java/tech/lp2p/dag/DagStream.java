package tech.lp2p.dag;


import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;

import tech.lp2p.core.Utils;


public class DagStream extends InputStream {

    private final DagChannel dagChannel;
    protected ByteBuffer buffer = null;

    public DagStream(DagChannel dagChannel) {
        this.dagChannel = dagChannel;
    }

    @Override
    public int available() {
        return (int) dagChannel.size();
    }

    public void loadNextData() throws IOException {
        buffer = dagChannel.loadNextData();
    }


    @Override
    public int read(byte[] b, int off, int len) throws IOException {
        if (buffer == null) { // initial the buffer is null
            loadNextData();
        }
        if (buffer.capacity() == 0) {
            return Utils.EOF;
        }

        int max = Math.min(len, buffer.remaining());
        buffer.get(b, off, max);

        if (!buffer.hasRemaining()) {
            loadNextData();
        }

        return max;
    }

    @Override
    public int read(byte[] b) throws IOException {
        if (buffer == null) { // initial the buffer is null
            loadNextData();
        }
        if (buffer.capacity() == 0) {
            return Utils.EOF;
        }

        int max = Math.min(b.length, buffer.remaining());
        buffer.get(b, 0, max);

        if (!buffer.hasRemaining()) {
            loadNextData();
        }
        return max;
    }

    @Override
    public int read() throws IOException {

        if (buffer == null) { // initial the buffer is null
            loadNextData();
        }
        if (buffer.capacity() == 0) {
            return Utils.EOF;
        }
        if (buffer.hasRemaining()) {
            return buffer.get() & 0xFF;
        } else {
            loadNextData();
            return read();
        }
    }


    @Override
    public byte[] readAllBytes() throws IOException {

        ByteBuffer all = ByteBuffer.allocate(available());

        if (buffer == null) { // initial the buffer is null
            loadNextData();
        }
        while (buffer.capacity() > 0) {
            all.put(buffer);
            loadNextData();
        }
        return all.array();

    }

    @Override
    public long transferTo(OutputStream out) throws IOException {
        int max = Math.min(available(), Short.MAX_VALUE);
        byte[] buf = new byte[max];
        int n;
        while ((n = read(buf)) > 0) {
            out.write(buf, 0, n);
        }
        return n;
    }


    @Override
    public long skip(long n) throws IOException {
        dagChannel.seek(n);
        loadNextData();
        return n;
    }
}
