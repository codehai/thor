package tech.lp2p.dag;

import tech.lp2p.core.Link;

public record DagLink(DagType dagType, long cid, long size, String name) implements Link {
    @Override
    public boolean isRaw() {
        return dagType == DagType.RAW;
    }

    @Override
    public boolean isDir() {
        return dagType == DagType.DIR;
    }

    @Override
    public boolean isFid() {
        return dagType == DagType.FID;
    }
}
