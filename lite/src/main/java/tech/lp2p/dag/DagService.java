package tech.lp2p.dag;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.ReadableByteChannel;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import tech.lp2p.core.BlockStore;
import tech.lp2p.core.Cid;
import tech.lp2p.core.Dir;
import tech.lp2p.core.Fid;
import tech.lp2p.core.Link;
import tech.lp2p.core.Node;
import tech.lp2p.core.Raw;
import tech.lp2p.core.Utils;

public interface DagService {


    static Fid storeChannel(ReadableByteChannel channel, BlockStore blockStore, String name)
            throws IOException {
        return fillTrickleRec(channel, blockStore, name);
    }


    private static Fid fillTrickleRec(ReadableByteChannel channel, BlockStore blockStore,
                                      String name) throws IOException {


        List<Link> links = new ArrayList<>();

        ByteBuffer chunk = ByteBuffer.allocateDirect(Short.MAX_VALUE);

        long size = 0L;
        boolean done = false;
        while (!done) {
            int read = channel.read(chunk);
            if (read < Short.MAX_VALUE) {
                done = true;
            }

            if (read > 0) {
                chunk.flip();
                long cid = Cid.createHash(chunk);
                chunk.rewind();
                blockStore.storeBlock(cid, chunk);
                links.add(new DagLink(DagType.RAW, cid,
                        Node.UNDEFINED_RAW_SIZE, Node.UNDEFINED_RAW_NAME));
                size = size + read;
            }
            chunk.clear();
        }

        ByteBuffer bytes = encode(DagType.FID, links, size, name, Node.UNDEFINED_DATA);
        long cid = Cid.createHash(bytes);
        blockStore.storeBlock(cid, bytes);

        return new Fid(cid, size, name, links, Node.UNDEFINED_DATA);
    }

    static Node info(BlockStore blockStore, long cid) throws IOException {
        Node node = DagService.dagNode(blockStore, cid);
        Objects.requireNonNull(node);
        return node;
    }


    static Dir addToDirectory(BlockStore blockStore, Dir dir, Link link) throws IOException {

        ByteBuffer block = blockStore.getBlock(dir.cid());
        Objects.requireNonNull(block, "Block not local available");
        Node dirNode = decode(dir.cid(), block);
        Objects.requireNonNull(dirNode);
        return DagService.addChild(blockStore, dirNode, link);

    }


    static Dir updateDirectory(BlockStore blockStore, Dir dir, Link link) throws IOException {
        ByteBuffer block = blockStore.getBlock(dir.cid());
        Objects.requireNonNull(block, "Block not local available");
        Node dirNode = decode(dir.cid(), block);
        Objects.requireNonNull(dirNode);
        return DagService.updateDirectory(blockStore, dirNode, link);
    }


    static Dir renameDirectory(BlockStore blockStore, Dir dir, String name) throws IOException {
        ByteBuffer block = blockStore.getBlock(dir.cid());
        Objects.requireNonNull(block, "Block not local available");
        Node dirNode = decode(dir.cid(), block);
        Objects.requireNonNull(dirNode);
        return DagService.renameDirectory(blockStore, dirNode, name);
    }


    static Dir removeFromDirectory(BlockStore blockStore, Dir dir, Link link) throws IOException {
        ByteBuffer block = blockStore.getBlock(dir.cid());
        Objects.requireNonNull(block, "Block not local available");
        Node dirNode = decode(dir.cid(), block);
        Objects.requireNonNull(dirNode);
        return DagService.removeChild(blockStore, dirNode, link);
    }


    static void removeNode(BlockStore blockStore, Node node) throws IOException {
        if (node.isDir()) {
            removeBlocks(blockStore, (Dir) node);
        } else if (node.isFid()) {
            removeBlocks(blockStore, (Fid) node);
        } else if (node.isRaw()) {
            removeBlocks(blockStore, (Raw) node);
        } else {
            throw new IllegalStateException("not supported node");
        }
    }

    private static void removeBlocks(BlockStore blockStore, Raw raw) {
        blockStore.deleteBlock(raw.cid());
    }

    private static void removeBlocks(BlockStore blockStore, Dir dir) throws IOException {
        for (Link link : dir.links()) {
            Utils.checkTrue(!link.isRaw(), "No raw links expected");
            Node node = info(blockStore, link.cid());
            removeNode(blockStore, node);
        }
        blockStore.deleteBlock(dir.cid());
    }

    private static void removeBlocks(BlockStore blockStore, Fid fid) {
        for (Link link : fid.links()) {
            Utils.checkTrue(link.isRaw(), "Only raw links expected");
            blockStore.deleteBlock(link.cid());
        }
        blockStore.deleteBlock(fid.cid());
    }

    static boolean hasChild(DagFetch dagFetch, Dir dir, String name) throws IOException {
        Node node = DagService.dagNode(dagFetch, dir.cid());
        Objects.requireNonNull(node);
        Utils.checkTrue(node.isDir(), "not a directory");
        return DagService.hasLink(node, name);
    }


    static Node dagNode(DagFetch dagFetch, long cid) throws IOException {
        ByteBuffer block = dagFetch.getBlock(cid);
        Objects.requireNonNull(block, "Cid is not available in block store");
        return decode(cid, block);
    }

    static Raw createRaw(BlockStore blockStore, byte[] data) throws IOException {

        ByteBuffer bytes = encode(DagType.RAW, List.of(), data.length, Utils.STRING_EMPTY, data);
        long cid = Cid.createHash(bytes);
        blockStore.storeBlock(cid, bytes);
        return new Raw(cid, data.length, Node.UNDEFINED_RAW_NAME, Node.UNDEFINED_LINKS, data);
    }

    static long createRaw(byte[] data) {
        ByteBuffer bytes = encode(DagType.RAW, List.of(), data.length, Utils.STRING_EMPTY, data);
        return Cid.createHash(bytes);
    }

    static Dir createDirectory(BlockStore blockStore, String name, List<Link> links) throws IOException {

        long fileSize = 0;

        for (Link info : links) {
            Utils.checkTrue(!info.isRaw(), "Raw type can not be added to directory");
            long size = info.size();
            fileSize = fileSize + size;
        }

        ByteBuffer bytes = encode(DagType.DIR, links, fileSize, name, Utils.BYTES_EMPTY);
        long cid = Cid.createHash(bytes);
        blockStore.storeBlock(cid, bytes);
        return new Dir(cid, fileSize, name, links, Node.UNDEFINED_DATA);
    }

    static Dir createEmptyDirectory(BlockStore blockStore, String name) throws IOException {
        ByteBuffer bytes = encode(DagType.DIR, List.of(), 0L, name, Utils.BYTES_EMPTY);
        long cid = Cid.createHash(bytes);
        blockStore.storeBlock(cid, bytes);
        return new Dir(cid, 0L, name, Node.UNDEFINED_LINKS, Node.UNDEFINED_DATA);
    }


    static Dir addChild(BlockStore blockStore, Node dirNode, Link link) throws IOException {

        long dirSize = dirNode.size();
        long newDirSize = dirSize + link.size();

        int size = dirNode.links().size();

        List<Link> links = new ArrayList<>(size + 1);
        links.addAll(dirNode.links());
        links.add(link);


        ByteBuffer bytes = encode(DagType.DIR, links, newDirSize, dirNode.name(), Utils.BYTES_EMPTY);
        long cid = Cid.createHash(bytes);
        blockStore.storeBlock(cid, bytes);

        return new Dir(cid, newDirSize, dirNode.name(), links, Node.UNDEFINED_DATA);
    }


    static Dir updateDirectory(BlockStore blockStore, Node node, Link child) throws IOException {

        Utils.checkTrue(node.isDir(), "Wrong node type");
        long filesize = 0L;

        List<Link> links = new ArrayList<>();

        for (Link link : node.links()) {
            if (!Objects.equals(link.name(), link.name())) {
                links.add(link);
                filesize = filesize + link.size();
            }
        }

        // added the updated link
        links.add(child);
        filesize = filesize + child.size();


        ByteBuffer bytes = encode(DagType.DIR, links, filesize, node.name(), node.data());
        long cid = Cid.createHash(bytes);
        blockStore.storeBlock(cid, bytes);

        return new Dir(cid, filesize, node.name(), links, Node.UNDEFINED_DATA);
    }

    static Dir removeChild(BlockStore blockStore, Node node, Link info) throws IOException {

        Utils.checkTrue(node.isDir(), "Wrong node type");
        long filesize = 0L;

        List<Link> links = new ArrayList<>();

        for (Link link : node.links()) {
            if (!Objects.equals(link.name(), info.name())) {
                links.add(link);
                filesize = filesize + link.size();
            }
        }

        ByteBuffer bytes = encode(DagType.DIR, links, filesize, node.name(), node.data());
        long cid = Cid.createHash(bytes);
        blockStore.storeBlock(cid, bytes);

        return new Dir(cid, filesize, node.name(), links, Node.UNDEFINED_DATA);

    }


    static Dir renameDirectory(BlockStore blockStore, Node node, String name)
            throws IOException {
        Utils.checkTrue(node.isDir(), "Wrong node type");
        ByteBuffer bytes = encode(DagType.DIR, node.links(), node.size(), name, node.data());
        long cid = Cid.createHash(bytes);
        blockStore.storeBlock(cid, bytes);

        return new Dir(cid, node.size(), name, node.links(), Node.UNDEFINED_DATA);
    }


    static boolean hasLink(Node node, String name) {

        for (Link link : node.links()) {
            if (Objects.equals(link.name(), name)) {
                return true;
            }
        }
        return false;

    }


    static ByteBuffer encode(DagType dagType, List<Link> links, long size,
                             String name, byte[] data) {
        Objects.requireNonNull(name);
        Utils.checkTrue(size >= 0, "Invalid size");
        Objects.requireNonNull(data);
        Objects.requireNonNull(links);
        Utils.checkTrue(name.length() <= Short.MAX_VALUE,
                "Max name length is " + Short.MAX_VALUE);
        Utils.checkTrue(data.length <= Short.MAX_VALUE,
                "Max data length is " + Short.MAX_VALUE);
        if (dagType == DagType.DIR) {
            Utils.checkTrue(data.length == 0, "Directory has no data");
        }


        int length = length(dagType, links, size, name, data);

        ByteBuffer buffer = ByteBuffer.allocate(length);
        buffer.put(DagType.encode(dagType));
        Utils.writeUnsignedVariant(buffer, links.size());

        for (Link info : links) {
            buffer.put(DagType.encode(info));
            buffer.putLong(info.cid());

            if (info.isDir() || info.isFid()) {
                Utils.writeUnsignedVariant(buffer, info.size());
                Utils.writeUnsignedVariant(buffer, info.name().length());
                buffer.put(info.name().getBytes());
            }
        }

        Utils.writeUnsignedVariant(buffer, size);

        if (dagType == DagType.DIR || dagType == DagType.FID) {
            Utils.writeUnsignedVariant(buffer, name.length());
            buffer.put(name.getBytes());
        }

        Utils.writeUnsignedVariant(buffer, data.length);
        buffer.put(data);
        buffer.flip();
        return buffer;
    }

    private static int length(DagType dagType, List<Link> links, long size,
                              String name, byte[] data) {
        int length = 1 +
                Utils.unsignedVariantSize(links.size()) + // links size
                Utils.unsignedVariantSize(size); // size

        if (dagType == DagType.DIR || dagType == DagType.FID) {
            length = length +
                    Utils.unsignedVariantSize(name.length()) // name length
                    + name.length();
        }

        length = length + Utils.unsignedVariantSize(data.length) // data length
                + data.length;

        for (Link link : links) {
            length = length + 1 + Long.BYTES;

            if (link.isDir() || link.isFid()) {
                length = length + Utils.unsignedVariantSize(link.size()) // size
                        + Utils.unsignedVariantSize(name.length()) // name length
                        + link.name().length();
            }
        }
        return length;
    }


    static Node decode(long cid, ByteBuffer buffer) {

        DagType dagType = DagType.decode(buffer.get());

        int linksSize = Utils.readUnsignedVariant(buffer);
        Link[] links = new Link[linksSize];
        for (int i = 0; i < linksSize; i++) {

            DagType dagLinkType = DagType.decode(buffer.get());
            long linkCid = buffer.getLong();
            long size = Node.UNDEFINED_RAW_SIZE;
            String name = Node.UNDEFINED_RAW_NAME;
            if (dagLinkType == DagType.DIR || dagLinkType == DagType.FID) {
                size = Utils.readLongUnsignedVariant(buffer);
                int nameLength = Utils.readUnsignedVariant(buffer);
                byte[] nameData = new byte[nameLength];
                buffer.get(nameData);
                name = new String(nameData);
            }
            links[i] = new DagLink(dagLinkType, linkCid, size, name);
        }


        long size = Utils.readLongUnsignedVariant(buffer);

        String name = Node.UNDEFINED_RAW_NAME;

        if (dagType == DagType.DIR || dagType == DagType.FID) {
            int nameLength = Utils.readUnsignedVariant(buffer);
            byte[] nameData = new byte[nameLength];
            buffer.get(nameData);
            name = new String(nameData);
        }

        int dataLength = Utils.readUnsignedVariant(buffer);
        byte[] content = new byte[dataLength];
        buffer.get(content);

        Utils.checkTrue(!buffer.hasRemaining(), "still data available");


        return switch (dagType) {
            case FID -> new Fid(cid, size, name, List.of(links), Node.UNDEFINED_DATA);
            case DIR -> new Dir(cid, size, name, List.of(links), Node.UNDEFINED_DATA);
            case RAW -> new Raw(cid, size, name, Node.UNDEFINED_LINKS, content);
        };

    }
}
