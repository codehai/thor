package tech.lp2p.dag;

import tech.lp2p.core.Link;

public enum DagType {
    FID, DIR, RAW;

    static byte encode(DagType dagType) {
        return switch (dagType) {
            case RAW -> (byte) 0x00;
            case FID -> (byte) 0x01;
            case DIR -> (byte) 0x02;
        };
    }

    static byte encode(Link link) {
        if (link.isRaw()) {
            return (byte) 0x00;
        }
        if (link.isFid()) {
            return (byte) 0x01;
        }
        if (link.isDir()) {
            return (byte) 0x02;
        }
        throw new IllegalStateException();
    }

    static DagType decode(byte value) {
        if (value == 0x00) {
            return RAW;
        }
        if (value == 0x01) {
            return FID;
        }
        if (value == 0x02) {
            return DIR;
        }
        throw new IllegalStateException();
    }
}
