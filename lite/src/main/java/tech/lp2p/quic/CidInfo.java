package tech.lp2p.quic;


final class CidInfo implements Comparable<CidInfo> {

    private final int sequenceNumber;
    private volatile Number cid;
    private volatile CidStatus cidStatus;


    CidInfo(int sequenceNumber, Number cid, CidStatus status) {
        this.sequenceNumber = sequenceNumber;
        this.cid = cid;
        this.cidStatus = status;

    }

    int sequenceNumber() {
        return sequenceNumber;
    }

    public Number cid() {
        return cid;
    }

    CidStatus cidStatus() {
        return cidStatus;
    }

    void cidStatus(CidStatus newStatus) {
        cidStatus = newStatus;
    }

    @Override
    public int compareTo(CidInfo o) {
        return Integer.compare(sequenceNumber, o.sequenceNumber);
    }

    public void cid(Number cid) {
        this.cid = cid;
    }
}

