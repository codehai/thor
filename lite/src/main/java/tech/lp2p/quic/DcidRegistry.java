package tech.lp2p.quic;


import java.util.ArrayList;
import java.util.List;


final class DcidRegistry extends CidRegistry {

    private volatile int notRetiredThreshold;  // all sequence numbers below are retired


    DcidRegistry(Number initialCid) {
        super(new CidInfo(0, initialCid, CidStatus.IN_USE));
    }

    /**
     * @return whether the connection id could be added as new; when its sequence number implies that it as retired already, false is returned.
     */
    boolean registerNewConnectionId(int sequenceNr, Number cid, byte[] ignored) {

        if (sequenceNr >= notRetiredThreshold) {
            cidInfos().add(new CidInfo(sequenceNr, cid, CidStatus.NEW));
            return true;
        } else {
            cidInfos().add(new CidInfo(sequenceNr, cid, CidStatus.RETIRED));
            return false;
        }
    }

    List<Integer> retireAllBefore(int retirePriorTo) {
        notRetiredThreshold = retirePriorTo;

        List<Integer> toRetire = new ArrayList<>();
        for (CidInfo cidInfo : cidInfos()) {
            int sequenceNumber = cidInfo.sequenceNumber();
            if (sequenceNumber < retirePriorTo) {
                if (cidInfo.cidStatus() != CidStatus.RETIRED) {
                    cidInfo.cidStatus(CidStatus.RETIRED);
                    toRetire.add(sequenceNumber);
                }
            }
        }


        // Find one that is not retired
        CidInfo nextCid = cidInfos().stream()
                .filter(cid -> cid.cidStatus() != CidStatus.RETIRED)
                .findFirst()
                .orElseThrow(() -> new IllegalStateException("Can't find connection id that is not retired"));
        nextCid.cidStatus(CidStatus.IN_USE);


        return toRetire;
    }

}

