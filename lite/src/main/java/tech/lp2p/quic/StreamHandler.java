package tech.lp2p.quic;

import java.nio.ByteBuffer;

public interface StreamHandler {

    void terminated();

    void fin();

    boolean readFully();

    void data(ByteBuffer data);

}
