package tech.lp2p.quic;

import java.nio.ByteBuffer;
import java.util.Objects;

import tech.lp2p.core.Utils;

public abstract class StreamState {


    byte[] frame = null;
    int frameIndex = 0;
    boolean reset = false;


    public static boolean isProtocol(ByteBuffer data) {
        int pos = data.position();
        boolean result = false;
        if (data.remaining() > 2) {
            boolean first = data.get() == '/';
            data.position(data.limit() - 1);
            boolean second = data.get() == '\n';
            result = first && second;
        }
        data.position(pos);
        return result;
    }

    public static byte[] unsignedVarintReader(ByteBuffer data) {
        Objects.requireNonNull(data);
        if (data.remaining() == 0) {
            return Utils.BYTES_EMPTY;
        }
        return new byte[Utils.readUnsignedVariant(data)];
    }


    static void iteration(StreamState streamState, Stream stream, ByteBuffer bytes) throws Exception {

        if (!streamState.reset) {
            if (streamState.length() == 0) {
                streamState.frame = unsignedVarintReader(bytes);
                streamState.frameIndex = 0;
                if (streamState.length() < 0) {
                    streamState.frame = null;
                    streamState.frameIndex = 0;
                    throw new Exception("invalid length of < 0 : " + streamState.length());
                } else {

                    int read = Math.min(streamState.length(), bytes.remaining());
                    for (int i = 0; i < read; i++) {
                        streamState.frame[streamState.frameIndex] = bytes.get();
                        streamState.frameIndex++;
                    }

                    if (read == streamState.length()) {
                        byte[] frame = Objects.requireNonNull(streamState.frame);
                        streamState.frame = null;
                        streamState.accept(stream, ByteBuffer.wrap(frame));
                    }

                    // check for a next iteration
                    if (bytes.remaining() > 0) {
                        iteration(streamState, stream, bytes);
                    }
                }
            } else {
                byte[] frame = Objects.requireNonNull(streamState.frame);
                int remaining = streamState.frame.length - streamState.frameIndex;
                int read = Math.min(remaining, bytes.remaining());
                for (int i = 0; i < read; i++) {
                    streamState.frame[streamState.frameIndex] = bytes.get();
                    streamState.frameIndex++;
                }
                remaining = streamState.frame.length - streamState.frameIndex;
                if (remaining == 0) { // frame is full
                    streamState.frame = null;
                    streamState.frameIndex++;
                    streamState.accept(stream, ByteBuffer.wrap(frame));
                }
                // check for a next iteration
                if (bytes.remaining() > 0) {
                    iteration(streamState, stream, bytes);
                }
            }
        }
    }

    void reset() {
        frame = null;
        reset = true;
    }

    protected abstract void accept(Stream stream, ByteBuffer frame) throws Exception;

    int length() {
        if (frame != null) {
            return frame.length;
        }
        return 0;
    }

}
