package tech.lp2p.quic;


import static tech.lp2p.quic.Level.App;
import static tech.lp2p.quic.Level.Handshake;
import static tech.lp2p.quic.Level.Initial;
import static tech.lp2p.quic.TransportError.Code.CONNECTION_ID_LIMIT_ERROR;
import static tech.lp2p.quic.TransportError.Code.FRAME_ENCODING_ERROR;
import static tech.lp2p.quic.TransportError.Code.PROTOCOL_VIOLATION;
import static tech.lp2p.quic.TransportError.Code.TRANSPORT_PARAMETER_ERROR;
import static tech.lp2p.quic.TransportError.Code.VERSION_NEGOTIATION_ERROR;

import java.io.IOException;
import java.net.ConnectException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;

import javax.net.ssl.X509TrustManager;

import tech.lp2p.core.PeerId;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Utils;
import tech.lp2p.tls.ApplicationLayerProtocolNegotiationExtension;
import tech.lp2p.tls.BadRecordMacAlert;
import tech.lp2p.tls.CertificateMessage;
import tech.lp2p.tls.CertificateVerifyMessage;
import tech.lp2p.tls.CertificateWithPrivateKey;
import tech.lp2p.tls.CipherSuite;
import tech.lp2p.tls.ClientHello;
import tech.lp2p.tls.ClientMessageSender;
import tech.lp2p.tls.Extension;
import tech.lp2p.tls.FinishedMessage;
import tech.lp2p.tls.TlsClientEngine;
import tech.lp2p.tls.TlsStatusEventHandler;

public final class ClientConnection extends Connection {

    private final Thread receiverThread;
    private final TlsClientEngine tlsEngine;
    private final CountDownLatch handshakeFinishedCondition = new CountDownLatch(1);
    private final TransportParameters transportParams;
    private final Function<Stream, StreamHandler> streamHandlerFunction;
    private final Connector connector;
    private final ScidRegistry scidRegistry;
    private final DcidRegistry dcidRegistry;
    private final Number originalDcid;

    /**
     * The maximum numbers of connection IDs this endpoint can use; determined by the TP
     * supplied by the peer
     */
    private final AtomicInteger remoteCidLimit = new AtomicInteger(Settings.ACTIVE_CONNECTION_ID_LIMIT);
    private final PeerId remotePeerId;
    private volatile boolean isClosing = false;

    private ClientConnection(int initialRtt, DatagramSocket socket, String serverName,
                             PeerId remotePeerId, InetSocketAddress remoteAddress, int version,
                             List<CipherSuite> cipherSuites, X509TrustManager trustManager,
                             X509Certificate clientCertificate, PrivateKey clientCertificateKey,
                             Function<Stream, StreamHandler> streamHandlerFunction,
                             Connector connector) {

        super(version, initialRtt, socket, remoteAddress);
        this.remotePeerId = remotePeerId;
        this.streamHandlerFunction = streamHandlerFunction;
        this.connector = connector;
        this.scidRegistry = new ScidRegistry();
        Integer initialScid = (Integer) scidRegistry.getInitial();
        this.receiverThread = new Thread(this::run, "receiver-loop");
        this.receiverThread.setDaemon(true);

        // https://www.rfc-editor.org/rfc/rfc9000.html#name-negotiating-connection-ids
        // "When an Initial packet is sent by a client (...), the client populates the Destination
        // Connection ID field with an unpredictable value.
        // This Destination Connection ID MUST be at least 8 bytes in length."
        this.originalDcid = VariableLengthInteger.generateNumber(Long.BYTES);

        this.dcidRegistry = new DcidRegistry(originalDcid);


        TransportParameters.VersionInformation versionInformation = null;
        if (Version.isV2(version)) {
            int[] otherVersions = {Version.V2, Version.V1};
            versionInformation = new TransportParameters.VersionInformation(
                    Version.V2, otherVersions);
        }

        this.transportParams = TransportParameters.createClient(initialScid,
                Settings.ACTIVE_CONNECTION_ID_LIMIT, versionInformation);

        Extension tpExtension = TransportParametersExtension.create(
                this.version, transportParams, true);
        Extension aplnExtension = ApplicationLayerProtocolNegotiationExtension.create(
                Settings.ALPN);
        this.tlsEngine = new TlsClientEngine(serverName, trustManager, cipherSuites,
                List.of(tpExtension, aplnExtension),
                new CryptoMessageSender(), new StatusEventHandler());
        initializeCryptoStreams(tlsEngine);

        tlsEngine.setClientCertificateCallback(authorities ->
                new CertificateWithPrivateKey(clientCertificate, clientCertificateKey));
    }

    public static ClientConnection create(DatagramSocket datagramSocket, int initialRtt,
                                          String serverName, PeerId remotePeerId,
                                          InetSocketAddress remoteAddress,
                                          int version, List<CipherSuite> cipherSuites,
                                          X509TrustManager trustManager,
                                          X509Certificate clientCertificate,
                                          PrivateKey clientCertificateKey,
                                          Function<Stream, StreamHandler> streamHandlerFunction,
                                          Connector connector) {
        return new ClientConnection(initialRtt, datagramSocket, serverName, remotePeerId,
                remoteAddress, version,
                cipherSuites, trustManager, clientCertificate,
                clientCertificateKey, streamHandlerFunction, connector);
    }

    public void start() {
        receiverThread.start();
    }

    public void connect(int timeout) throws InterruptedException, ConnectException, TimeoutException {

        try {
            startHandshake();
        } catch (Throwable throwable) {
            abortHandshake();
            throw new ConnectException("Error : " + throwable.getMessage());
        }

        try {

            boolean handshakeFinished = handshakeFinishedCondition.await(timeout, TimeUnit.SECONDS);
            if (!handshakeFinished) {
                abortHandshake();
                throw new TimeoutException("Connection timed out after " + timeout + " s");
            } else if (state() != State.Connected) {
                abortHandshake();
                throw new ConnectException("Handshake error state is " + state());
            }
            connector.registerConnection(this);
        } catch (InterruptedException interruptedException) {
            abortHandshake();
            throw new InterruptedException("Interrupt exception occur");  // Should not happen.
        }
    }

    private void startHandshake() throws BadRecordMacAlert {

        computeInitialKeys(dcidRegistry.getInitial());

        start();
        startRequester();

        tlsEngine.startHandshake();

    }


    public PeerId remotePeerId() {
        return remotePeerId;
    }

    private void abortHandshake() {
        state(State.Failed);
        clearRequests();
        terminate();
    }


    /**
     * Registers the initial connection ID issued by the peer (server). Used in client role only.
     */
    void registerInitialCid(Integer cid) {
        dcidRegistry.initialConnectionId(cid);
    }

    /**
     * Registers that the given connection is used by the peer (as destination connection ID)
     * to send messages to this endpoint.
     *
     * @param cid the connection ID used
     */
    void registerCidInUse(Number cid) {

        if (scidRegistry.registerUsedConnectionId(cid)) {
            // New connection id, not used before.
            // https://www.rfc-editor.org/rfc/rfc9000.html#name-issuing-connection-ids
            // "If an endpoint provided fewer connection IDs than the peer's active_connection_id_limit, it MAY supply
            //  a new connection ID when it receives a packet with a previously unused connection ID."
            if (scidRegistry.getActiveCids() < remoteCidLimit.get()) {
                sendNewCid();
            }
        }
    }

    @Override
    public boolean process(PacketHeader packetHeader, long timeReceived) throws IOException {
        switch (packetHeader.level()) {
            case Handshake -> {
                return processFrames(packetHeader, timeReceived);
            }
            case Initial -> {
                registerInitialCid(packetHeader.scid());
                return processFrames(packetHeader, timeReceived);
            }
            case App -> {
                registerCidInUse(packetHeader.dcid());
                return processFrames(packetHeader, timeReceived);
            }
        }
        return false;
    }

    @Override
    public void process(FrameReceived.HandshakeDoneFrame handshakeDoneFrame) {

        HandshakeState state = handshakeState.updateAndGet(handshakeState -> {
            if (handshakeState.transitionAllowed(HandshakeState.Confirmed)) {
                return HandshakeState.Confirmed;
            }
            return handshakeState;
        });

        Utils.checkTrue(state == HandshakeState.Confirmed,
                "Handshake state cannot be set to Confirmed");


        discard(Level.Handshake);

        // https://tools.ietf.org/html/draft-ietf-quic-tls-32#section-4.9.2
        // "An endpoint MUST discard its handshake keys when the TLS handshake is confirmed"
        // 4.9.2. Discarding Handshake Keys
        // An endpoint MUST discard its handshake keys when the TLS handshake is confirmed
        // (Section 4.1.2).
        discardHandshakeKeys();

    }

    /**
     * Send a retire connection ID frame, that informs the peer the given connection ID will not be used by this
     * endpoint anymore for addressing the peer.
     */
    private void sendRetireCid(int seqNr) {
        // https://www.rfc-editor.org/rfc/rfc9000.html#name-retransmission-of-informati
        // "Likewise, retired connection IDs are sent in RETIRE_CONNECTION_ID frames and retransmitted if the packet
        //  containing them is lost."
        sendRequestQueue(App).appendRequest(Frame.createRetireConnectionsIdFrame(seqNr));
    }

    @Override
    void process(FrameReceived.NewConnectionIdFrame newConnectionIdFrame) {

        // https://www.rfc-editor.org/rfc/rfc9000.html#name-new_connection_id-frames
        // "Receiving a value in the Retire Prior To field that is greater than that in the
        // Sequence Number field MUST
        //  be treated as a connection error of payloadType FRAME_ENCODING_ERROR."
        if (newConnectionIdFrame.retirePriorTo() > newConnectionIdFrame.sequenceNr()) {
            immediateCloseWithError(App, new TransportError(FRAME_ENCODING_ERROR));
            return;
        }
        CidInfo cidInfo = dcidRegistry.cidInfo(newConnectionIdFrame.sequenceNr());
        if (cidInfo == null) {

            boolean added = dcidRegistry.registerNewConnectionId(newConnectionIdFrame.sequenceNr(),
                    newConnectionIdFrame.connectionId(), newConnectionIdFrame.statelessResetToken());
            if (!added) {
                // https://www.rfc-editor.org/rfc/rfc9000.html#name-new_connection_id-frames
                // "An endpoint that receives a NEW_CONNECTION_ID frame with a sequence number
                // smaller than the Retire Prior To
                //  field of a previously received NEW_CONNECTION_ID frame MUST send a
                //  corresponding RETIRE_CONNECTION_ID
                //  frame that retires the newly received connection ID, "
                sendRetireCid(newConnectionIdFrame.sequenceNr());
            }
        } else if (!Objects.equals(cidInfo.cid(), newConnectionIdFrame.connectionId())) {
            // https://www.rfc-editor.org/rfc/rfc9000.html#name-new_connection_id-frames
            // "... or if a sequence number is used for different connection IDs, the endpoint
            // MAY treat that receipt as a
            //  connection error of payloadType PROTOCOL_VIOLATION."
            immediateCloseWithError(App, new TransportError(PROTOCOL_VIOLATION));
            return;
        }
        if (newConnectionIdFrame.retirePriorTo() > 0) {
            List<Integer> retired = dcidRegistry.retireAllBefore(newConnectionIdFrame.retirePriorTo());
            retired.forEach(this::sendRetireCid);
        }
        // https://www.rfc-editor.org/rfc/rfc9000.html#name-issuing-connection-ids
        // "After processing a NEW_CONNECTION_ID frame and adding and retiring active connection
        // IDs, if the number of
        // active connection IDs exceeds the value advertised in its active_connection_id_limit
        // transport parameter, an
        // endpoint MUST close the connection with an error of payloadType CONNECTION_ID_LIMIT_ERROR."
        if (dcidRegistry.getActiveCids() > Settings.ACTIVE_CONNECTION_ID_LIMIT) {
            immediateCloseWithError(App, new TransportError(CONNECTION_ID_LIMIT_ERROR));
        }

    }


    @Override
    void process(FrameReceived.RetireConnectionIdFrame retireConnectionIdFrame, Number dcid) {
        // https://www.rfc-editor.org/rfc/rfc9000.html#name-retire_connection_id-frames
        // "Receipt of a RETIRE_CONNECTION_ID frame containing a sequence number greater
        // than any previously sent to the
        // peer MUST be treated as a connection error of payloadType PROTOCOL_VIOLATION."
        if (retireConnectionIdFrame.sequenceNumber() > scidRegistry.maxSequenceNr()) {
            immediateCloseWithError(App, new TransportError(PROTOCOL_VIOLATION));
            return;
        }
        int sequenceNr = retireConnectionIdFrame.sequenceNumber();
        // https://www.rfc-editor.org/rfc/rfc9000.html#name-retire_connection_id-frames
        // "The sequence number specified in a RETIRE_CONNECTION_ID frame MUST NOT refer to the
        //  Destination Connection ID field of the packet in which the frame is contained.
        //  The peer MAY treat this as a connection error of payloadType PROTOCOL_VIOLATION."
        if (Objects.equals(Objects.requireNonNull(
                scidRegistry.cidInfo(sequenceNr)).cid(), dcid)) {
            immediateCloseWithError(App, new TransportError(PROTOCOL_VIOLATION));
            return;
        }

        Number retiredCid = scidRegistry.retireCid(sequenceNr);
        // If not retired already
        if (retiredCid != null) {
            // connectionRegistry.deregisterConnectionId(retiredCid);
            // https://www.rfc-editor.org/rfc/rfc9000.html#name-issuing-connection-ids
            // "An endpoint SHOULD supply a new connection ID when the peer retires a connection ID."
            if (scidRegistry.getActiveCids() < remoteCidLimit.get()) {
                sendNewCid();
            }
        }

    }

    /**
     * Generate, register and send a new connection ID (that identifies this endpoint).
     */
    private void sendNewCid() {
        CidInfo cidInfo = scidRegistry.generateNew();
        Integer cid = (Integer) cidInfo.cid();
        Objects.requireNonNull(cid);
        sendRequestQueue(App).appendRequest(Frame.createNewConnectionIdFrame(cidInfo.sequenceNumber(),
                0, cid));
    }


    @Override
    Function<Stream, StreamHandler> streamHandler() {
        return streamHandlerFunction;
    }

    @Override
    public void terminate() {
        super.terminate();
        handshakeFinishedCondition.countDown();
        connector.removeConnection(this);
        isClosing = true;
        receiverThread.interrupt();
        datagramSocket.close();

    }

    private void run() {
        try {

            byte[] receiveBuffer = new byte[Settings.MAX_DATAGRAM_PACKET_SIZE];
            while (!isClosing) {
                DatagramPacket receivedPacket = new DatagramPacket(receiveBuffer, receiveBuffer.length);
                datagramSocket.receive(receivedPacket);
                try {
                    process(System.currentTimeMillis(), receivedPacket);
                } catch (Throwable throwable) {
                    Utils.error(throwable);
                }
            }

        } catch (IOException ioException) {
            if (!isClosing) {
                // This is probably fatal
                Utils.error(ioException);
            }
        } catch (Throwable fatal) {
            Utils.error(fatal);
        }
    }

    Number initialDcid() {
        return dcidRegistry.getInitial();
    }

    private void process(long timeReceived, DatagramPacket datagramPacket) {
        ByteBuffer buffer = ByteBuffer.wrap(datagramPacket.getData(), 0, datagramPacket.getLength());
        nextPacket(buffer, timeReceived);
    }

    private void validateAndProcess(TransportParameters remoteTransportParameters) {

        if (remoteTransportParameters.maxUdpPayloadSize() < 1200) {
            immediateCloseWithError(Handshake, new TransportError(TRANSPORT_PARAMETER_ERROR));
            return;
        }
        if (remoteTransportParameters.ackDelayExponent() > 20) {
            immediateCloseWithError(Handshake, new TransportError(TRANSPORT_PARAMETER_ERROR));
            return;
        }
        if (remoteTransportParameters.maxAckDelay() > 16384) { // 16384 = 2^14 ()
            immediateCloseWithError(Handshake, new TransportError(TRANSPORT_PARAMETER_ERROR));
            return;
        }
        if (remoteTransportParameters.activeConnectionIdLimit() < 2) {
            immediateCloseWithError(Handshake, new TransportError(TRANSPORT_PARAMETER_ERROR));
            return;
        }


        // https://tools.ietf.org/html/draft-ietf-quic-transport-29#section-7.3
        // "An endpoint MUST treat absence of the initial_source_connection_id
        //   transport parameter from either endpoint or absence of the
        //   original_destination_connection_id transport parameter from the
        //   server as a connection error of payloadType TRANSPORT_PARAMETER_ERROR."
        if (remoteTransportParameters.initialScid() == null ||
                remoteTransportParameters.originalDcid() == null) {

            immediateCloseWithError(Handshake, new TransportError(TRANSPORT_PARAMETER_ERROR));
            return;
        }

        // https://tools.ietf.org/html/draft-ietf-quic-transport-29#section-7.3
        // "An endpoint MUST treat the following as a connection error of payloadType
        // TRANSPORT_PARAMETER_ERROR or PROTOCOL_VIOLATION:
        // a mismatch between values received from a peer in these transport parameters and the
        // value sent in the
        // corresponding Destination or Source Connection ID fields of Initial packets."

        if (!Objects.equals(initialDcid(),
                remoteTransportParameters.initialScid())) {
            immediateCloseWithError(Handshake, new TransportError(PROTOCOL_VIOLATION));
            return;
        }

        if (!Objects.equals(originalDcid, remoteTransportParameters.originalDcid())) {
            immediateCloseWithError(Handshake, new TransportError(PROTOCOL_VIOLATION));
            return;
        }


        TransportParameters.VersionInformation versionInformation = remoteTransportParameters.versionInformation();
        if (versionInformation != null) {
            if (versionInformation.chosenVersion() != version) {
                // https://www.ietf.org/archive/id/draft-ietf-quic-version-negotiation-08.html
                // "clients MUST validate that the server's Chosen Version is equal to the negotiated version; if they do not
                //  match, the client MUST close the connection with a version negotiation error. "

                immediateCloseWithError(Handshake,
                        new TransportError(VERSION_NEGOTIATION_ERROR));
                return;
            }
        }

        this.remoteDelayScale.set(remoteTransportParameters.ackDelayScale());


        init(remoteTransportParameters.initialMaxData(),
                remoteTransportParameters.initialMaxStreamDataBidiLocal(),
                remoteTransportParameters.initialMaxStreamDataBidiRemote(),
                remoteTransportParameters.initialMaxStreamDataUni()
        );


        initialMaxStreamsBidi(remoteTransportParameters.initialMaxStreamsBidi());
        initialMaxStreamsUni(remoteTransportParameters.initialMaxStreamsUni());

        remoteMaxAckDelay = remoteTransportParameters.maxAckDelay();
        remoteCidLimit(remoteTransportParameters.activeConnectionIdLimit());

        determineIdleTimeout(transportParams.maxIdleTimeout(), remoteTransportParameters.maxIdleTimeout());


        if (remoteTransportParameters.retrySourceConnectionId() != null) {
            immediateCloseWithError(Handshake, new TransportError(TRANSPORT_PARAMETER_ERROR));
        }
    }


    /**
     * Register the active connection ID limit of the peer (as received by this endpoint as TP active_connection_id_limit)
     * and determine the maximum number of peer connection ID's this endpoint is willing to maintain.
     * "This is an integer value specifying the maximum number of connection IDs from the peer that an endpoint is
     * willing to store.", so it puts an upper bound to the number of connection IDs this endpoint can generate.
     */
    void remoteCidLimit(int remoteCidLimit) {
        // https://www.rfc-editor.org/rfc/rfc9000.html#name-issuing-connection-ids
        // "An endpoint MUST NOT provide more connection IDs than the peer's limit."
        this.remoteCidLimit.set(remoteCidLimit);
    }

    /**
     * Abort connection due to a fatal error in this client. No message is sent to peer; just inform client it's all over.
     *
     * @param throwable the exception that caused the trouble
     */
    @Override
    void abortConnection(Throwable throwable) {
        Utils.error(throwable);
        state(State.Failed);
        clearRequests();
        terminate();
    }


    /**
     * Returns the connection ID that this endpoint considers as "current".
     * Note that in QUIC, there is no such thing as a "current" connection ID, there are only active and retired
     * connection ID's. The peer can use any time any active connection ID.
     */
    @Override
    Number activeScid() {
        return scidRegistry.getActive();
    }

    /**
     * Returns the (peer's) connection ID that is currently used by this endpoint to address the peer.
     */
    @Override
    Number activeDcid() {
        return dcidRegistry.getActive();
    }


    private void validateALPN(String[] protocols) {
        for (String protocol : protocols) {
            if (Objects.equals(protocol, Settings.ALPN)) {
                return; // done all good
            }
        }
        immediateCloseWithError(Handshake, new TransportError(TRANSPORT_PARAMETER_ERROR));
    }

    @SuppressWarnings("unused")
    X509Certificate remoteCertificate() {
        return tlsEngine.remoteCertificate();
    }

    @SuppressWarnings("unused")
    public Peeraddr remotePeeraddr() {
        return Peeraddr.create(remotePeerId, remoteAddress());
    }

    private class StatusEventHandler implements TlsStatusEventHandler {

        @Override
        public void handshakeSecretsKnown() {
            // Server Hello provides a new secret, so:
            computeHandshakeSecrets(tlsEngine, tlsEngine.getSelectedCipher());
            HandshakeState state = handshakeState.updateAndGet(handshakeState -> {
                if (handshakeState.transitionAllowed(HandshakeState.HasHandshakeKeys)) {
                    return HandshakeState.HasHandshakeKeys;
                }
                return handshakeState;
            });

            Utils.checkTrue(state == HandshakeState.HasHandshakeKeys,
                    "Handshake state cannot be set to HasHandshakeKeys");

        }

        @Override
        public void handshakeFinished() {
            // note this is not 100% correct, it discards only when handshake is finished,
            // not when the first handshake message is written [but fine for now !!!]

            // https://tools.ietf.org/html/draft-ietf-quic-tls-29#section-4.11.1
            // "Thus, a client MUST discard Initial keys when it first sends a Handshake packet (...).
            // This results in abandoning loss recovery state for the Initial encryption level and
            // ignoring any outstanding Initial packets."
            discard(Level.Initial);

            // https://tools.ietf.org/html/draft-ietf-quic-tls-32#section-4.9.1
            // -> (Thus, a client MUST discard Initial keys when it first sends a Handshake)
            // 4.9.1. Discarding Initial Keys
            // Packets protected with Initial secrets (Section 5.2) are not authenticated,
            // meaning that an attacker could spoof packets with the intent to disrupt a connection.
            // To limit these attacks, Initial packet protection keys are discarded more aggressively
            // than other keys.
            //
            // The successful use of Handshake packets indicates that no more Initial packets need to
            // be exchanged, as these keys can only be produced after receiving all CRYPTO frames from
            // Initial packets. Thus, a client MUST discard Initial keys when it first sends a
            // Handshake packet and a server MUST discard Initial keys when it first successfully
            // processes a Handshake packet. Endpoints MUST NOT send Initial packets after this point.
            //
            // This results in abandoning loss recovery state for the Initial encryption level and
            // ignoring any outstanding Initial packets.
            discardInitialKeys();


            computeApplicationSecrets(tlsEngine, tlsEngine.getSelectedCipher());

            HandshakeState state = handshakeState.updateAndGet(handshakeState -> {
                if (handshakeState.transitionAllowed(HandshakeState.HasAppKeys)) {
                    return HandshakeState.HasAppKeys;
                }
                return handshakeState;
            });

            Utils.checkTrue(state == HandshakeState.HasAppKeys,
                    "Handshake state cannot be set to HasAppKeys");

            state(State.Connected);

            handshakeFinishedCondition.countDown();
        }


        @Override
        public void extensionsReceived(Extension[] extensions) {
            for (Extension ex : extensions) {
                if (ex instanceof TransportParametersExtension transportParametersExtension) {
                    validateAndProcess(transportParametersExtension.getTransportParameters());
                } else if (ex instanceof ApplicationLayerProtocolNegotiationExtension alpnExtension) {
                    validateALPN(alpnExtension.protocols());
                } else {
                    Utils.debug("not handled extension received " + ex);
                    immediateCloseWithError(Handshake, new TransportError(TRANSPORT_PARAMETER_ERROR));
                }
            }
        }
    }

    private class CryptoMessageSender implements ClientMessageSender {
        @Override
        public void send(ClientHello clientHello) {
            CryptoStream cryptoStream = getCryptoStream(Initial);
            cryptoStream.write(clientHello);
            flush();
            state(State.Handshaking);
        }

        @Override
        public void send(FinishedMessage finished) {
            CryptoStream cryptoStream = getCryptoStream(Handshake);
            cryptoStream.write(finished);
            flush();
        }

        @Override
        public void send(CertificateMessage certificateMessage) {
            CryptoStream cryptoStream = getCryptoStream(Handshake);
            cryptoStream.write(certificateMessage);
            flush();
        }

        @Override
        public void send(CertificateVerifyMessage certificateVerifyMessage) {
            CryptoStream cryptoStream = getCryptoStream(Handshake);
            cryptoStream.write(certificateVerifyMessage);
            flush();
        }
    }

}
