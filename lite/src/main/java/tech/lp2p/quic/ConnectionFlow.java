package tech.lp2p.quic;


import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.IntUnaryOperator;
import java.util.function.LongUnaryOperator;

import tech.lp2p.core.Utils;
import tech.lp2p.tls.TlsEngine;


/**
 * This class implements the flow concepts of a QUIC connection
 * -> RttEstimator
 * <p>
 * -> CongestionController
 * <a href="https://datatracker.ietf.org/doc/html/rfc9002#name-congestion-control">...</a>
 * -> RecoveryManager
 * QUIC Loss Detection is specified in <a href="https://www.rfc-editor.org/rfc/rfc9002.html">...</a>.
 * <p>
 * "QUIC senders use acknowledgments to detect lost packets and a PTO to ensure acknowledgments are received"
 * It uses a single timer, because either there are lost packets to detect, or a probe must be scheduled, never both.
 * <p>
 * <b>Ack based loss detection</b>
 * When an Ack is received, packets that are sent "long enough" before the largest acked, are deemed lost; for the
 * packets not send "long enough", a timer is set to mark them as lost when "long enough" time has been passed.
 * <p>
 * An example:
 * -----------------------time------------------->>
 * sent:   1           2      3        4
 * acked:                                    4
 * \--- long enough before 4 --/                       => 1 is marked lost immediately
 * \--not long enough before 4 --/
 * |
 * Set timer at this point in time, as that will be "long enough".
 * At that time, a new timer will be set for 3, unless acked meanwhile.
 * <p>
 * <b>Detecting tail loss with probe timeout</b>
 * When no Acks arrive, no packets will be marked as lost. To trigger the peer to send an ack (so loss detection can do
 * its job again), a probe (ack-eliciting packet) will be sent after the probe timeout. If the situation does not change
 * (i.e. no Acks received), additional probes will be sent, but with an exponentially growing delay.
 * <p>
 * An example:
 * -----------------------time------------------->>
 * sent:   1           2      3        4
 * acked:                                    4
 * \-- timer set at loss time  --/
 * |
 * When the timer fires, there is no new ack received, so
 * nothing can be marked as lost. A probe is scheduled for
 * "probe timeout" time after the time 3 was sent:
 * \-- timer set at "probe timeout" time after 3 was sent --\
 * |
 * Send probe!
 * <p>
 * Note that packet 3 will not be marked as lost as long no ack is received!
 * <p>
 * <b>Exceptions</b>
 * Because a server might be blocked by the anti-amplification limit, a client must also send probes when it has no
 * ack eliciting packets in flight, but is not sure whether the peer has validated the client address.
 */
class ConnectionFlow extends ConnectionSecrets {


    private final SendRequestQueue[] sendRequestQueues =
            new SendRequestQueue[Level.LENGTH];
    private final PacketAssembler[] packetAssemblers =
            new PacketAssembler[Level.LENGTH];
    private final AckGenerator[] ackGenerators =
            new AckGenerator[Level.LENGTH];
    private final AtomicBoolean[] discardedLevels =
            new AtomicBoolean[Level.LENGTH];
    private final CryptoStream[] cryptoStreams =
            new CryptoStream[Level.LENGTH];
    private final AtomicLong maxDataAssigned = new AtomicLong(0);
    private final int initialRtt;
    private final AtomicInteger rttVar = new AtomicInteger(Settings.NOT_DEFINED);
    private final AtomicInteger smoothedRtt = new AtomicInteger(Settings.NOT_DEFINED);
    private final AtomicInteger minRtt = new AtomicInteger(Integer.MAX_VALUE);
    private final AtomicInteger latestRtt = new AtomicInteger(0);
    private final AtomicLong bytesInFlight = new AtomicLong(0);
    private final AtomicLong congestionWindow = new AtomicLong(Settings.INITIAL_CONGESTION_WINDOW);
    private final ReduceCongestionWindowUpdater reduceCongestionWindowUpdater = new ReduceCongestionWindowUpdater();
    private final LossDetector[] lossDetectors = new LossDetector[Level.LENGTH];

    // https://tools.ietf.org/html/draft-ietf-quic-transport-30#section-8.2
    // "If this value is absent, a default of 25 milliseconds is assumed."
    protected volatile int remoteMaxAckDelay = Settings.MAX_ACK_DELAY;
    // The maximum amount of data that can be sent (to the peer) on the connection as a whole
    private volatile long maxDataAllowed = 0L;
    // https://tools.ietf.org/html/draft-ietf-quic-transport-32#section-18.2
    // "initial_max_stream_data_bidi_local (0x0005):  This parameter is an integer value specifying the initial flow control limit for
    //  locally-initiated bidirectional streams.
    private volatile long initialMaxStreamDataBidiLocal = 0L;
    // "initial_max_stream_data_bidi_remote (0x0006):  This parameter is an integer value specifying the initial flow control limit for peer-
    //  initiated bidirectional streams. "
    private volatile long initialMaxStreamDataBidiRemote = 0L;
    // "initial_max_stream_data_uni (0x0007):  This parameter is an integer value specifying the initial flow control limit for unidirectional
    //  streams."
    private volatile long initialMaxStreamDataUni = 0L;
    private volatile long slowStartThreshold = Long.MAX_VALUE;
    private volatile long congestionRecoveryStartTime = 0L;


    protected ConnectionFlow(int version, int initialRtt) {
        super(version);
        this.initialRtt = initialRtt;


        for (Level level : Level.levels()) {
            sendRequestQueues[level.ordinal()] = new SendRequestQueue();
        }

        for (Level level : Level.levels()) {
            ackGenerators[level.ordinal()] = new AckGenerator();
        }

        discardedLevels[Level.Initial.ordinal()] = new AtomicBoolean(false);
        discardedLevels[Level.Handshake.ordinal()] = new AtomicBoolean(false);
        discardedLevels[Level.App.ordinal()] = new AtomicBoolean(false);


        for (Level level : Level.levels()) {
            int levelIndex = level.ordinal();
            packetAssemblers[levelIndex] = new PacketAssembler(version, level,
                    sendRequestQueues[levelIndex], ackGenerators[levelIndex]);
        }

        for (Level level : Level.levels()) {
            lossDetectors[level.ordinal()] = new LossDetector(this);
        }
    }

    protected void initializeCryptoStreams(TlsEngine tlsEngine) {
        for (Level level : Level.levels()) {
            cryptoStreams[level.ordinal()] = new CryptoStream(version, level,
                    tlsEngine, sendRequestQueues[level.ordinal()]);
        }
    }

    final AckGenerator ackGenerator(Level level) {
        return ackGenerators[level.ordinal()];
    }

    final SendRequestQueue sendRequestQueue(Level level) {
        return sendRequestQueues[level.ordinal()];
    }

    PacketAssembler packetAssembler(Level level) {
        return packetAssemblers[level.ordinal()];
    }

    final void packetSent(Packet packet, long timeSent, int size) {

        if (Packet.isInflightPacket(packet)) {
            PacketStatus packetStatus = new PacketStatus(packet, timeSent, size);
            registerInFlight(packetStatus);
            lossDetectors[packet.level().ordinal()].packetSent(packetStatus);
        }

    }

    final void insertRequest(Level level, Frame frame) {
        sendRequestQueue(level).insertRequest(frame);
    }

    final void addRequest(Level level, Frame frame) {
        sendRequestQueue(level).appendRequest(frame);
    }

    final void process(FrameReceived.AckFrame ackFrame, Level level, long timeReceived) {
        ackGenerator(level).ackFrameReceived(ackFrame);

        lossDetectors[level.ordinal()].processAckFrameReceived(ackFrame, timeReceived);
    }

    /**
     * Stop sending packets, but don't shutdown yet, so connection close can be sent.
     */
    final void clearRequests() {
        // Stop sending packets, so discard any packet waiting to be send.
        for (SendRequestQueue queue : sendRequestQueues) {
            queue.clear();
        }

        // No more retransmissions either.
        stopRecovery();
    }

    void cleanup() {
        for (Level level : Level.levels()) {
            discard(level);
        }
        discardKeys();

        for (Level level : Level.levels()) {
            cryptoStreams[level.ordinal()].cleanup();
        }
    }

    final CryptoStream getCryptoStream(Level level) {
        // https://tools.ietf.org/html/draft-ietf-quic-transport-24#section-19.6
        // "There is a separate flow of cryptographic handshake data in each encryption level"
        return cryptoStreams[level.ordinal()];
    }

    final boolean isDiscarded(Level level) {
        return discardedLevels[level.ordinal()].get();
    }

    final void discard(Level level) {

        discardedLevels[level.ordinal()].set(true);

        // clear all send requests and probes on that level
        sendRequestQueues[level.ordinal()].clear();

        // 5.5.  Discarding Keys and Packet State
        //
        //   When packet protection keys are discarded (see Section 4.9 of
        //   [QUIC-TLS]), all packets that were sent with those keys can no longer
        //   be acknowledged because their acknowledgements cannot be processed
        //   anymore.  The sender MUST discard all recovery state associated with
        //   those packets and MUST remove them from the count of bytes in flight.
        lossDetectors[level.ordinal()].stop();


        // deactivate ack generator for level
        ackGenerator(level).cleanup();
    }

    final int getPto() {
        return getSmoothedRtt() + 4 * getRttVar() + remoteMaxAckDelay;
    }


    final void init(long initialMaxData, long initialMaxStreamDataBidiLocal,
                    long initialMaxStreamDataBidiRemote, long initialMaxStreamDataUni) {
        this.initialMaxStreamDataBidiLocal = initialMaxStreamDataBidiLocal;
        this.initialMaxStreamDataBidiRemote = initialMaxStreamDataBidiRemote;
        this.initialMaxStreamDataUni = initialMaxStreamDataUni;
        this.maxDataAllowed = initialMaxData;
    }

    final void addMaxDataAssigned(long proposedStreamIncrement) {
        maxDataAssigned.getAndAdd(proposedStreamIncrement);
    }

    final long determineInitialMaxStreamData(Stream stream) {
        if (stream.isUnidirectional()) {
            return initialMaxStreamDataUni;
        } else if (stream.isClientInitiatedBidirectional()) {
            // For the receiver (imposing the limit) the stream is peer-initiated (remote).
            // "This limit applies to newly created bidirectional streams opened by the endpoint that receives
            // the transport parameter."
            return initialMaxStreamDataBidiRemote;
        } else if (stream.isServerInitiatedBidirectional()) {
            // For the receiver (imposing the limit), the stream is locally-initiated
            // "This limit applies to newly created bidirectional streams opened by the endpoint that sends the
            // transport parameter."
            return initialMaxStreamDataBidiLocal;
        } else {
            throw new IllegalStateException();
        }
    }

    final long getInitialMaxStreamDataBidiRemote() {
        return initialMaxStreamDataBidiRemote;
    }

    final void maxDataAllowed(long value) {
        maxDataAllowed = value;
    }

    /**
     * Returns the current connection flow control limit.
     *
     * @return current connection flow control limit
     */
    final long maxDataAllowed() {
        return maxDataAllowed;
    }

    final long maxDataAssigned() {
        return maxDataAssigned.get();
    }

    final void addSample(long timeReceived, long timeSent, int ackDelay) {
        if (timeReceived < timeSent) {
            // This sometimes happens in the Interop runner;
            // reconsider solution after new sender is implemented.
            Utils.debug("Receiving negative rtt estimate: sent=" +
                    timeSent + ", received=" + timeReceived);
            return;
        }

        if (ackDelay > remoteMaxAckDelay) {
            ackDelay = remoteMaxAckDelay;
        }


        int rttSample = (int) (timeReceived - timeSent);
        if (rttSample < minRtt.get())
            minRtt.set(rttSample);
        // Adjust for ack delay if it's plausible. Because times are truncated at millisecond precision,
        // consider rtt equal to min as plausible.
        if (rttSample >= minRtt.get() + ackDelay) {
            rttSample -= ackDelay;
        }
        latestRtt.set(rttSample);

        if (smoothedRtt.get() == Settings.NOT_DEFINED) {
            // First time
            smoothedRtt.set(rttSample);
            rttVar.set(rttSample / 2);
        } else {
            int currentRttVar = Math.abs(smoothedRtt.get() - rttSample);
            rttVar.updateAndGet(new RttVarUpdater(currentRttVar));
            smoothedRtt.updateAndGet(new SmoothedRttUpdater(rttSample));
        }
    }

    final int getSmoothedRtt() {
        int val = smoothedRtt.get();
        if (val == Settings.NOT_DEFINED) {
            return initialRtt;
        } else {
            return val;
        }
    }

    final int getRttVar() {
        // Rtt-var is only used for computing PTO.
        // https://tools.ietf.org/html/draft-ietf-quic-recovery-23#section-5.3
        // "The initial probe timeout for a new connection or new path SHOULD be set to twice the initial RTT"
        // https://tools.ietf.org/html/draft-ietf-quic-recovery-23#section-5.2.1
        // "PTO = smoothed_rtt + max(4*rttvar, kGranularity) + max_ack_delay"
        // Hence, using an initial rtt-var of initial-rtt / 4, will result in an initial PTO of twice the initial RTT.
        // After the first packet is received, the rttVar will be computed from the real RTT sample.
        int val = rttVar.get();
        if (val == Settings.NOT_DEFINED) {
            return initialRtt / 4;
        } else {
            return val;
        }
    }

    final int getLatestRtt() {
        return latestRtt.get();
    }

    // the packet status is a packet send earlier and has now been acknowlegded
    final void processAckedPacket(PacketStatus acknowlegdedPacket) {

        bytesInFlight.updateAndGet(new BytesInFlightDiscard(acknowlegdedPacket.size()));


        // https://datatracker.ietf.org/doc/html/rfc9002#name-underutilizing-the-congesti
        // 7.8. Underutilizing the Congestion Window
        // When bytes in flight is smaller than the congestion window and sending is not pacing
        // limited, the congestion window is underutilized. This can happen due to insufficient
        // application data or flow control limits. When this occurs, the congestion window
        // SHOULD NOT be increased in either slow start or congestion avoidance.
        //


        boolean underutilizingCongestionWindow = bytesInFlight.get() < congestionWindow.get();
        if (!underutilizingCongestionWindow) {
            // https://tools.ietf.org/html/draft-ietf-quic-recovery-23#section-6.4
            // "QUIC defines the end of recovery as a packet sent after the start of recovery being acknowledged"
            if (acknowlegdedPacket.timeSent() > congestionRecoveryStartTime) {
                congestionWindow.updateAndGet(
                        new IncreaseCongestionWindowUpdater(acknowlegdedPacket, slowStartThreshold));
            }
        }
    }

    final void discardBytesInFlight(PacketStatus packetStatus) {
        bytesInFlight.updateAndGet(new BytesInFlightDiscard(packetStatus.size()));
    }

    final void registerLost(PacketStatus packetStatus) {


        discardBytesInFlight(packetStatus);

        // 6.4.  Recovery Period
        //
        //   Recovery is a period of time beginning with detection of a lost
        //   packet or an increase in the ECN-CE counter.  Because QUIC does not
        //   retransmit packets, it defines the end of recovery as a packet sent
        //   after the start of recovery being acknowledged.  This is slightly
        //   different from TCP's definition of recovery, which ends when the lost
        //   packet that started recovery is acknowledged.
        //
        //   The recovery period limits congestion window reduction to once per
        //   round trip.  During recovery, the congestion window remains unchanged
        //   irrespective of new losses or increases in the ECN-CE counter.


        if (packetStatus.timeSent() > congestionRecoveryStartTime) {
            congestionRecoveryStartTime = System.currentTimeMillis();

            //   When a loss is detected,
            //   NewReno halves the congestion window and sets the slow start
            //   threshold  to the new congestion window.
            slowStartThreshold = congestionWindow.updateAndGet(reduceCongestionWindowUpdater);
        }
    }

    final long remainingCwnd() {
        return Math.max(congestionWindow.get() - bytesInFlight.get(), 0);
    }

    final void registerInFlight(PacketStatus packetStatus) {
        bytesInFlight.addAndGet(packetStatus.size());
    }

    void lossDetection() {
        for (Level level : Level.levels()) {
            lossDetectors[level.ordinal()].detectLostPackets();
        }
    }

    final void stopRecovery() {
        for (LossDetector lossDetector : lossDetectors) {
            lossDetector.stop();
        }
    }

    private record RttVarUpdater(int currentRttVar) implements IntUnaryOperator {

        @Override
        public int applyAsInt(int operand) {
            // Add 2 to round to nearest integer
            return (3 * operand + currentRttVar + 2) / 4;
        }
    }

    private record SmoothedRttUpdater(int rrtSample) implements IntUnaryOperator {

        @Override
        public int applyAsInt(int operand) {
            // Add 4 to round to nearest integer
            return (7 * operand + rrtSample + 4) / 8;
        }
    }

    private static class ReduceCongestionWindowUpdater implements LongUnaryOperator {

        // When a loss is detected,
        // NewReno halves the congestion window and sets the slow start
        // threshold to the new congestion window.
        @Override
        public long applyAsLong(long operand) {
            long window = operand / Settings.CONGESTION_LOSS_REDUCTION_FACTOR;
            if (window < Settings.MINIMUM_CONGESTION_WINDOW) {
                return Settings.MINIMUM_CONGESTION_WINDOW;
            }
            return window;
        }
    }

    private record IncreaseCongestionWindowUpdater(
            PacketStatus packetStatus, long slowStartThreshold) implements LongUnaryOperator {

        @Override
        public long applyAsLong(long operand) {
            if (operand < slowStartThreshold) {
                // i.e. mode is slow start
                return operand + packetStatus.size(); // ok
            } else {
                // i.e. mode is congestion avoidance
                // A sender in congestion avoidance uses an Additive Increase Multiplicative
                // Decrease (AIMD) approach that MUST limit the increase to the congestion window to
                // at most one maximum datagram size for each congestion window that is acknowledged.
                return operand + ((long) Settings.MAX_DATAGRAM_SIZE * packetStatus.size() / operand);
            }
        }
    }

    private record BytesInFlightDiscard(long discard) implements LongUnaryOperator {
        @Override
        public long applyAsLong(long operand) {
            return Math.max(operand - discard, 0); // just for safety
        }
    }

}
