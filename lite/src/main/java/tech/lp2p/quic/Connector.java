package tech.lp2p.quic;


import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Consumer;

import tech.lp2p.core.Event;
import tech.lp2p.core.Utils;


public final class Connector {
    private final Set<ClientConnection> connections = ConcurrentHashMap.newKeySet();
    private final Consumer<Event> eventConsumer;

    public Connector(Consumer<Event> eventConsumer) {
        this.eventConsumer = eventConsumer;
    }

    public Set<ClientConnection> connections() {
        Set<ClientConnection> clientConnections = new HashSet<>();
        for (ClientConnection connection : connections) {
            if (connection.isConnected()) {
                clientConnections.add(connection);
            } else {
                removeConnection(connection);
            }
        }
        return clientConnections;
    }

    public void shutdown() {
        try {
            connections.forEach(Connection::close);
            connections.clear();
        } catch (Throwable throwable) {
            Utils.error(throwable); // should not occur
        }
    }

    public void registerConnection(ClientConnection connection) {
        connections.add(connection);
    }

    public void removeConnection(ClientConnection connection) {
        connections.remove(connection);
        if (connection.isMarked()) {
            eventConsumer.accept(Event.OUTGOING_RESERVE_EVENT);
        }
    }


}
