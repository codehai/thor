package tech.lp2p.quic;


record PacketStatus(Packet packet, long timeSent, int size) {
}

