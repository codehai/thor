package tech.lp2p.quic;

import static tech.lp2p.quic.TransportError.Code.STREAM_LIMIT_ERROR;
import static tech.lp2p.quic.TransportError.Code.STREAM_STATE_ERROR;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Function;

import tech.lp2p.core.Utils;

abstract class ConnectionStreams extends ConnectionFlow {

    private final Map<Integer, Stream> streams = new ConcurrentHashMap<>();
    private final ReentrantLock streamLock = new ReentrantLock();
    private final Semaphore openBidirectionalStreams = new Semaphore(0);
    private final Semaphore openUnidirectionalStreams = new Semaphore(0);
    private final AtomicLong maxOpenStreamIdUni = new AtomicLong(Settings.MAX_STREAMS_UNI);
    private final AtomicLong maxOpenStreamIdBidi = new AtomicLong(Settings.MAX_STREAMS_BIDI);
    private final AtomicBoolean maxOpenStreamsUniUpdateQueued = new AtomicBoolean(false);
    private final AtomicBoolean maxOpenStreamsBidiUpdateQueued = new AtomicBoolean(false);
    private final AtomicInteger nextStreamId = new AtomicInteger(0);
    private final AtomicLong maxStreamsAcceptedByPeerBidi = new AtomicLong(0);
    private final AtomicLong maxStreamsAcceptedByPeerUni = new AtomicLong(0);
    private final AtomicLong absoluteUnidirectionalStreamIdLimit = new AtomicLong(Integer.MAX_VALUE);
    private final AtomicLong absoluteBidirectionalStreamIdLimit = new AtomicLong(Integer.MAX_VALUE);

    protected ConnectionStreams(int version, int initialRtt) {
        super(version, initialRtt);
    }

    private static boolean isUni(int streamId) {
        return streamId % 4 > 1;
    }

    private static boolean isBidi(int streamId) {
        return streamId % 4 < 2;
    }


    final Stream createStream(Connection connection, boolean bidirectional,
                              Function<Stream, StreamHandler> streamHandlerFunction)
            throws InterruptedException, TimeoutException {

        streamLock.lock();
        try {
            boolean acquired;

            if (bidirectional) {
                acquired = openBidirectionalStreams.tryAcquire(Settings.STREAM_TIMEOUT, TimeUnit.SECONDS);
            } else {
                acquired = openUnidirectionalStreams.tryAcquire(Settings.STREAM_TIMEOUT, TimeUnit.SECONDS);
            }
            if (!acquired) {
                throw new TimeoutException("Timeout for acquire a stream");
            }

            int streamId = generateStreamId(bidirectional);
            Stream stream = new Stream(connection, streamId, streamHandlerFunction);
            streams.put(streamId, stream);
            return stream;
        } finally {
            streamLock.unlock();
        }
    }

    private int generateStreamId(boolean bidirectional) {
        // https://tools.ietf.org/html/draft-ietf-quic-transport-17#section-2.1:
        // "0x0  | Client-Initiated, Bidirectional"
        // "0x1  | Server-Initiated, Bidirectional"
        int id = (nextStreamId.getAndIncrement() << 2);
        if (!bidirectional) {
            // "0x2  | Client-Initiated, Unidirectional |"
            // "0x3  | Server-Initiated, Unidirectional |"
            id += 0x02;
        }
        return id;
    }


    abstract Function<Stream, StreamHandler> streamHandler();


    void processStreamFrame(Connection connection, FrameReceived.StreamFrame frame) throws TransportError {
        int streamId = frame.streamId();
        Stream stream = streams.get(streamId);
        if (stream != null) {
            stream.add(frame);
            // This implementation maintains a fixed maximum number of open streams, so when the peer closes a stream
            // it is allowed to open another.
            if (frame.isFinal() && isPeerInitiated(streamId)) {
                increaseMaxOpenStreams(streamId);
            }
        } else {
            if (isPeerInitiated(streamId)) {
                if (isUni(streamId) && streamId < maxOpenStreamIdUni.get() ||
                        isBidi(streamId) && streamId < maxOpenStreamIdBidi.get()) {
                    stream = new Stream(connection, streamId, streamHandler());
                    streams.put(streamId, stream);
                    stream.add(frame);
                    if (frame.isFinal()) {
                        increaseMaxOpenStreams(streamId);
                    }
                } else {
                    // https://tools.ietf.org/html/draft-ietf-quic-transport-32#section-19.11
                    // "An endpoint MUST terminate a connection with a STREAM_LIMIT_ERROR error
                    // if a peer opens more streams than was permitted."
                    throw new TransportError(STREAM_LIMIT_ERROR);
                }
            } else {
                // happens because of timeout (local created stream -> not remote)
                Utils.debug("Receiving frame for non-existent stream " + streamId +
                        " FRAME " + frame);
            }
        }
    }


    void processMaxStreamDataFrame(FrameReceived.MaxStreamDataFrame frame) throws TransportError {

        int streamId = frame.streamId();


        long maxStreamData = frame.maxData();

        Stream stream = streams.get(streamId);
        if (stream != null) {
            stream.increaseMaxStreamDataAllowed(maxStreamData);
        } else {
            // https://tools.ietf.org/html/draft-ietf-quic-transport-33#section-19.10
            // "Receiving a MAX_STREAM_DATA frame for a locally-initiated stream that has not yet been created MUST
            //  be treated as a connection error of payloadType STREAM_STATE_ERROR."
            if (locallyInitiated(streamId)) {
                throw new TransportError(STREAM_STATE_ERROR);
            }
        }
    }

    private boolean locallyInitiated(int streamId) {
        return streamId % 2 == 0;
    }

    void process(FrameReceived.MaxDataFrame maxDataFrame) {

        // If frames are received out of order, the new max can be smaller than the current value.
        long currentMaxDataAllowed = maxDataAllowed();
        if (maxDataFrame.maxData() > currentMaxDataAllowed) {
            boolean maxDataWasReached = currentMaxDataAllowed == maxDataAssigned();
            maxDataAllowed(maxDataFrame.maxData());
            if (maxDataWasReached) {
                for (Stream stream : streams.values()) {
                    stream.unblock();
                }
            }
        }
    }

    void process(FrameReceived.StopSendingFrame stopSendingFrame) {

        // https://www.rfc-editor.org/rfc/rfc9000.html#name-solicited-state-transitions
        // "A STOP_SENDING frame requests that the receiving endpoint send a RESET_STREAM frame."
        Stream stream = streams.get(stopSendingFrame.streamId());
        if (stream != null) {
            // "An endpoint SHOULD copy the error code from the STOP_SENDING frame to the RESET_STREAM frame it sends, ..."
            stream.resetStream(stopSendingFrame.errorCode());
        }
    }

    void process(FrameReceived.ResetStreamFrame resetStreamFrame) {

        Stream stream = streams.get(resetStreamFrame.streamId());
        if (stream != null) {
            // "An endpoint SHOULD copy the error code from the STOP_SENDING frame to the RESET_STREAM frame it sends, ..."
            stream.terminate(resetStreamFrame.errorCode());
        }
    }

    private void increaseMaxOpenStreams(int streamId) {

        if (isUni(streamId) && maxOpenStreamIdUni.get() + 4 <
                absoluteUnidirectionalStreamIdLimit.get()) {
            maxOpenStreamIdUni.getAndAdd(4);
            if (!maxOpenStreamsUniUpdateQueued.getAndSet(true)) {
                sendRequestQueue(Level.App).appendRequest(createMaxStreamsUpdateUni());
            }
        } else if (isBidi(streamId) && maxOpenStreamIdBidi.get() +
                4 < absoluteBidirectionalStreamIdLimit.get()) {
            maxOpenStreamIdBidi.getAndAdd(4);
            if (!maxOpenStreamsBidiUpdateQueued.getAndSet(true)) {
                sendRequestQueue(Level.App).appendRequest(createMaxStreamsUpdateBidi());
            }
        }

    }

    private Frame createMaxStreamsUpdateUni() {
        maxOpenStreamsUniUpdateQueued.set(false);
        // largest streamId < maxStreamId; e.g. client initiated: max-id = 6, server initiated: max-id = 7 => max streams = 1,
        return Frame.createMaxStreamsFrame(maxOpenStreamIdUni.get() / 4, false);
    }

    private Frame createMaxStreamsUpdateBidi() {
        maxOpenStreamsBidiUpdateQueued.set(false);

        // largest streamId < maxStreamId; e.g. client initiated: max-id = 4, server initiated: max-id = 5 => max streams = 1,
        return Frame.createMaxStreamsFrame(maxOpenStreamIdBidi.get() / 4, true);
    }

    private boolean isPeerInitiated(int streamId) {
        return streamId % 2 == (1);
    }

    void process(FrameReceived.MaxStreamsFrame frame) {
        if (frame.appliesToBidirectional()) {
            long streamsAcceptedByPeerBidi = maxStreamsAcceptedByPeerBidi.get();
            if (frame.maxStreams() > streamsAcceptedByPeerBidi) {
                int increment = (int) (frame.maxStreams() - streamsAcceptedByPeerBidi);
                maxStreamsAcceptedByPeerBidi.set(frame.maxStreams());
                openBidirectionalStreams.release(increment);
            }
        } else {
            long streamsAcceptedByPeerUni = maxStreamsAcceptedByPeerUni.get();
            if (frame.maxStreams() > streamsAcceptedByPeerUni) {
                int increment = (int) (frame.maxStreams() - streamsAcceptedByPeerUni);
                maxStreamsAcceptedByPeerUni.set(frame.maxStreams());
                openUnidirectionalStreams.release(increment);
            }
        }
    }

    /**
     * Set initial max bidirectional streams that the peer will accept.
     */
    void initialMaxStreamsBidi(long initialMaxStreamsBidi) {
        if (initialMaxStreamsBidi >= maxStreamsAcceptedByPeerBidi.get()) {
            maxStreamsAcceptedByPeerBidi.set(initialMaxStreamsBidi);
            if (initialMaxStreamsBidi > Integer.MAX_VALUE) {
                initialMaxStreamsBidi = Integer.MAX_VALUE;
            }
            openBidirectionalStreams.release((int) initialMaxStreamsBidi);
        } else {
            Utils.debug("Attempt to reduce value of initial_max_streams_bidi from "
                    + maxStreamsAcceptedByPeerBidi + " to " + initialMaxStreamsBidi + "; ignoring.");
        }
    }

    /**
     * Set initial max unidirectional streams that the peer will accept.
     */
    void initialMaxStreamsUni(long initialMaxStreamsUni) {
        if (initialMaxStreamsUni >= maxStreamsAcceptedByPeerUni.get()) {

            maxStreamsAcceptedByPeerUni.set(initialMaxStreamsUni);
            if (initialMaxStreamsUni > Integer.MAX_VALUE) {
                initialMaxStreamsUni = Integer.MAX_VALUE;
            }
            openUnidirectionalStreams.release((int) initialMaxStreamsUni);
        } else {
            Utils.debug("Attempt to reduce value of initial_max_streams_uni from "
                    + maxStreamsAcceptedByPeerUni + " to " + initialMaxStreamsUni + "; ignoring.");
        }
    }

    final void cleanup() {
        super.cleanup();
        streams.values().forEach(Stream::terminate);
        streams.clear();
    }

    public void unregisterStream(int streamId) {
        streams.remove(streamId);
    }

}

