package tech.lp2p.quic;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Iterator;
import java.util.Queue;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.atomic.AtomicInteger;

import tech.lp2p.tls.DecodeErrorException;
import tech.lp2p.tls.ErrorAlert;
import tech.lp2p.tls.Extension;
import tech.lp2p.tls.HandshakeMessage;
import tech.lp2p.tls.HandshakeType;
import tech.lp2p.tls.ProtectionKeysType;
import tech.lp2p.tls.TlsEngine;
import tech.lp2p.tls.TlsMessageParser;


final class CryptoStream {

    private final int version;
    private final ProtectionKeysType tlsProtectionType;
    private final TlsEngine tlsEngine;
    private final SendRequestQueue sendRequestQueue;
    private final TlsMessageParser tlsMessageParser;
    private final Queue<ByteBuffer> sendQueue = new ConcurrentLinkedDeque<>();
    private final AtomicInteger dataToSendOffset = new AtomicInteger(0);
    private final AtomicInteger sendStreamSize = new AtomicInteger(0);
    private final SortedSet<FrameReceived.CryptoFrame> frames = new TreeSet<>(); // no concurrency
    private long processedToOffset = 0; // no concurrency

    CryptoStream(int version, Level level,
                 TlsEngine tlsEngine, SendRequestQueue sendRequestQueue) {
        this.version = version;
        this.tlsEngine = tlsEngine;
        this.sendRequestQueue = sendRequestQueue;

        this.tlsProtectionType =
                level == Level.Handshake ? ProtectionKeysType.Handshake :
                        level == Level.App ? ProtectionKeysType.Application :
                                ProtectionKeysType.None;

        this.tlsMessageParser = new TlsMessageParser(this::quicExtensionsParser);
    }


    public void add(FrameReceived.CryptoFrame cryptoFrame) throws ErrorAlert {
        if (addFrame(cryptoFrame)) {

            int msgSize = peekMsgSize();
            while (msgSize > 0) {
                long availableBytes = bytesAvailable();

                if (availableBytes >= (msgSize + 4)) {  // 4 is the msgSize + msgType encoded

                    ByteBuffer msgBuffer = ByteBuffer.allocate(4 + msgSize);
                    read(msgBuffer);
                    msgBuffer.flip();

                    tlsMessageParser.parseAndProcessHandshakeMessage(msgBuffer,
                            tlsEngine, tlsProtectionType);

                    msgSize = peekMsgSize(); // next iteration
                } else {
                    break;
                }
            }
        }
    }

    private Extension quicExtensionsParser(ByteBuffer buffer, HandshakeType ignoredContext) throws ErrorAlert {
        buffer.mark();
        int extensionType = buffer.getShort();
        buffer.reset();
        if (TransportParametersExtension.isCodepoint(extensionType & 0xffff)) {
            try {
                return TransportParametersExtension.parse(version, buffer, false);
            } catch (IOException exception) {
                throw new DecodeErrorException(exception.getClass().getSimpleName() +
                        " " + exception.getMessage());
            }
        } else {
            return null;
        }
    }

    public void write(HandshakeMessage message) {
        write(message.getBytes());
    }

    private void write(byte[] data) {
        sendQueue.add(ByteBuffer.wrap(data));
        sendStreamSize.getAndAdd(data.length);
        sendRequestQueue.appendRequest(this::sendFrame, 10);  // Caller should flush sender.
    }

    private Frame sendFrame(int maxSize) {
        int leftToSend = sendStreamSize.get() - dataToSendOffset.get();
        int bytesToSend = Integer.min(leftToSend, maxSize - 10);
        if (bytesToSend == 0) {
            return null;
        }
        if (bytesToSend < leftToSend) {
            // Need (at least) another frame to send all data. Because current method
            // is the sender callback, flushing sender is not necessary.
            sendRequestQueue.appendRequest(this::sendFrame, 10);
        }

        byte[] frameData = new byte[bytesToSend];
        int frameDataOffset = 0;
        while (frameDataOffset < bytesToSend && !sendQueue.isEmpty()) {
            ByteBuffer buffer = sendQueue.peek();
            if (buffer != null) {
                int bytesToCopy = Integer.min(bytesToSend - frameDataOffset, buffer.remaining());
                buffer.get(frameData, frameDataOffset, bytesToCopy);
                if (buffer.remaining() == 0) {
                    sendQueue.poll();
                }
                frameDataOffset += bytesToCopy;
            }
        }

        Frame frame = Frame.createCryptoFrame(dataToSendOffset.get(), frameData);
        dataToSendOffset.getAndAdd(bytesToSend);
        return frame;
    }

    /**
     * Add a stream frame to this stream. The frame can contain any number of bytes positioned anywhere in the stream;
     * the read method will take care of returning stream bytes in the right order, without gaps.
     *
     * @return true if the frame is adds bytes to this stream; false if the frame does not add bytes to the stream
     * (because the frame is a duplicate or its stream bytes where already received with previous frames).
     */
    private boolean addFrame(FrameReceived.CryptoFrame frame) {
        if (frame.offsetLength() >= processedToOffset) {
            return frames.add(frame);
        } else {
            return false;
        }
    }

    /**
     * Returns the number of bytes that can be read from this stream.
     */
    private int bytesAvailable() {
        if (frames.isEmpty()) {
            return 0;
        } else {
            int available = 0;
            long countedUpTo = processedToOffset;

            for (FrameReceived.CryptoFrame nextFrame : frames) {
                if (nextFrame.offset() <= countedUpTo) {
                    if (nextFrame.offsetLength() > countedUpTo) {
                        available += (int) (nextFrame.offsetLength() - countedUpTo);
                        countedUpTo = nextFrame.offsetLength();
                    }
                } else {
                    break;
                }
            }
            return available;
        }
    }


    private int peekMsgSize() {
        for (FrameReceived.CryptoFrame nextFrame : frames) {
            if (nextFrame.offset() <= processedToOffset) {
                if (nextFrame.offsetLength() >= processedToOffset + 4) {
                    // Note: + 1 (because the first byte is handshake type)
                    // Note: the length is 3, the content of the msg size
                    return VariableLengthInteger.encodeInteger(nextFrame.payload(),
                            (int) (processedToOffset - nextFrame.offset()) + 1, 3);
                }
            } else {
                break;
            }
        }
        return 0;
    }

    private void read(ByteBuffer buffer) {

        Iterator<FrameReceived.CryptoFrame> iterator = frames.iterator();

        while (iterator.hasNext() && buffer.remaining() > 0) {
            FrameReceived.CryptoFrame nextFrame = iterator.next();
            if (nextFrame.offset() <= processedToOffset) {
                if (nextFrame.offsetLength() > processedToOffset) {
                    long available = nextFrame.offset() - processedToOffset + nextFrame.length();
                    int bytesToRead = (int) Long.min(buffer.limit() - buffer.position(), available);
                    buffer.put(nextFrame.payload(), (int) (processedToOffset - nextFrame.offset()), bytesToRead);
                    processedToOffset += bytesToRead;
                    if (nextFrame.offsetLength() <= processedToOffset) {
                        iterator.remove();
                    }
                }
            } else {
                break;
            }
        }
    }


    public void cleanup() {
        frames.clear();
        sendQueue.clear();
    }
}
