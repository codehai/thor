package tech.lp2p.quic;


import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.LongUnaryOperator;

final class LossDetector {

    private static final float TimeThreshold = 9f / 8f;
    private final ConnectionFlow connectionFlow;
    private final Map<Long, PacketStatus> packetSentLog = new ConcurrentHashMap<>();
    private final AtomicLong largestAcked = new AtomicLong(-1);
    private volatile boolean isStopped = false;

    LossDetector(ConnectionFlow connectionFlow) {
        this.connectionFlow = connectionFlow;
    }

    void packetSent(PacketStatus packetStatus) {
        if (isStopped) {
            return;
        }

        // During a reset operation, no new packets must be logged as sent.
        packetSentLog.put(packetStatus.packet().packetNumber(), packetStatus);

    }

    void processAckFrameReceived(FrameReceived.AckFrame ackFrame, long timeReceived) {
        if (isStopped) {
            return;
        }

        largestAcked.updateAndGet(new LargestAckedUpdater(ackFrame));


        PacketStatus newlyAcked = null;

        // it is ordered, packet status with highest packet number is at the top
        long[] acknowledgedRanges = ackFrame.acknowledgedRanges();
        for (int i = 0; i < acknowledgedRanges.length; i++) {
            long to = acknowledgedRanges[i];
            i++;
            long from = acknowledgedRanges[i];
            for (long pn = to; pn >= from; pn--) {
                PacketStatus packetStatus = packetSentLog.get(pn);
                if (packetStatus != null) {

                    packetSentLog.remove(pn); // remove entry
                    if (Packet.isAckEliciting(packetStatus.packet())) {
                        connectionFlow.processAckedPacket(packetStatus);
                    }
                    if (newlyAcked == null) {
                        newlyAcked = packetStatus;
                    }
                }


            }
        }

        // https://tools.ietf.org/html/draft-ietf-quic-recovery-33#section-5.1
        // "An endpoint generates an RTT sample on receiving an ACK frame that meets the following
        // two conditions:
        // the largest acknowledged packet number is newly acknowledged, and
        // at least one of the newly acknowledged packets was ack-eliciting."
        if (newlyAcked != null) {
            if (newlyAcked.packet().packetNumber() == ackFrame.largestAcknowledged()) {
                if (Packet.isAckEliciting(newlyAcked.packet())) { // at least one of the newly acknowledged packets was ack-eliciting."
                    connectionFlow.addSample(timeReceived, newlyAcked.timeSent(), ackFrame.ackDelay());
                }
            }
        }
    }

    public void stop() {
        isStopped = true;

        for (PacketStatus packetStatus : packetSentLog.values()) {
            connectionFlow.discardBytesInFlight(packetStatus);
        }

        packetSentLog.clear();
    }

    void detectLostPackets() {
        if (isStopped) {
            return;
        }

        int lossDelay = (int) (TimeThreshold * Integer.max(
                connectionFlow.getSmoothedRtt(), connectionFlow.getLatestRtt()));

        long lostSendTime = System.currentTimeMillis() - lossDelay;


        // https://tools.ietf.org/html/draft-ietf-quic-recovery-20#section-6.1
        // "A packet is declared lost if it meets all the following conditions:
        //   o  The packet is unacknowledged, in-flight, and was sent prior to an
        //      acknowledged packet.
        //   o  Either its packet number is kPacketThreshold smaller than an
        //      acknowledged packet (Section 6.1.1), or it was sent long enough in
        //      the past (Section 6.1.2)."
        // https://tools.ietf.org/html/draft-ietf-quic-recovery-20#section-2
        // "In-flight:  Packets are considered in-flight when they have been sent
        //      and neither acknowledged nor declared lost, and they are not ACK-
        //      only."

        for (PacketStatus packetStatus : packetSentLog.values()) {
            if (pnTooOld(packetStatus) || sentTimeTooLongAgo(packetStatus, lostSendTime)) {
                if (!packetStatus.packet().isAckOnly()) {
                    declareLost(packetStatus);
                }
            }
        }
    }

    private boolean pnTooOld(PacketStatus p) {
        int kPacketThreshold = 3;
        return p.packet().packetNumber() <= largestAcked.get() - kPacketThreshold;
    }

    private boolean sentTimeTooLongAgo(PacketStatus p, long lostSendTime) {
        return p.packet().packetNumber() <= largestAcked.get() && p.timeSent() < lostSendTime;
    }

    private void declareLost(PacketStatus packetStatus) {

        if (Packet.isAckEliciting(packetStatus.packet())) {
            connectionFlow.registerLost(packetStatus);
        }
        // Retransmitting the frames in the lost packet is delegated to the lost frame callback,
        // because whether retransmitting the frame is necessary (and in which manner) depends
        // on frame payloadType,
        // see https://tools.ietf.org/html/draft-ietf-quic-transport-32#section-13.3

        List<Frame> frames = packetStatus.packet().frames();
        for (Frame frame : frames) {
            switch (frame.frameType()) {
                case MaxStreamsFrame, MaxStreamDataFrame, StopSendingFrame, ResetStreamFrame,
                     RetireConnectionIdFrame, StreamFrame, MaxDataFrame, HandshakeDoneFrame ->
                        connectionFlow.insertRequest(packetStatus.packet().level(), frame);
                case CryptoFrame, NewConnectionIdFrame, DataBlockedFrame,
                     StreamDataBlockedFrame ->
                        connectionFlow.addRequest(packetStatus.packet().level(), frame);
                default -> {
                }
            }

        }

        packetSentLog.remove(packetStatus.packet().packetNumber());

    }

    private record LargestAckedUpdater(
            FrameReceived.AckFrame ackFrame) implements LongUnaryOperator {
        @Override
        public long applyAsLong(long operand) {
            return Long.max(operand, ackFrame.largestAcknowledged());
        }
    }

}
