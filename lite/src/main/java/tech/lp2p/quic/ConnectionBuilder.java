package tech.lp2p.quic;

import java.net.ConnectException;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeoutException;

import javax.net.ssl.X509TrustManager;

import tech.lp2p.Lite;
import tech.lp2p.core.Certificate;
import tech.lp2p.core.Network;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Responder;
import tech.lp2p.core.Utils;
import tech.lp2p.lite.LiteTrustManager;
import tech.lp2p.tls.CipherSuite;

public final class ConnectionBuilder {

    private static final X509TrustManager libp2pTrust = new LiteTrustManager();


    public static Connection connect(Lite host, Peeraddr address)
            throws ConnectException, InterruptedException, TimeoutException {

        InetAddress inetAddress;
        try {
            inetAddress = InetAddress.getByAddress(address.address());
        } catch (Throwable throwable) {
            throw new ConnectException("invalid address " + address.peerId());
        }
        Objects.requireNonNull(inetAddress);

        return connect(host, address.peerId(), inetAddress, address.port());
    }

    private static Connection connect(Lite host, PeerId peerId,
                                      InetAddress inetAddress, int port)
            throws ConnectException, InterruptedException, TimeoutException {


        int initialRtt = Settings.INITIAL_RTT;
        if (Network.isLanAddress(inetAddress)) {
            initialRtt = 100;
        }

        Certificate certificate = host.certificate();

        Responder responder = new Responder(host.protocols());
        Objects.requireNonNull(responder, "Responder not defined for libp2p ALPN");

        DatagramSocket datagramSocket;
        try {
            datagramSocket = new DatagramSocket(Utils.nextFreePort());
        } catch (Throwable throwable) {
            throw new IllegalStateException("Socket could not be create");
        }

        ClientConnection clientConnection = ClientConnection.create(datagramSocket, initialRtt,
                inetAddress.getHostName(), peerId, new InetSocketAddress(inetAddress, port),
                Version.V1, List.of(CipherSuite.TLS_AES_128_GCM_SHA256),
                libp2pTrust, certificate.x509Certificate(), certificate.certificateKey(),
                stream -> AlpnResponder.create(stream, responder), host.connector());
        clientConnection.connect(Lite.TIMEOUT);


        return clientConnection;
    }

}
