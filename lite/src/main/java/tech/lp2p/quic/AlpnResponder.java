package tech.lp2p.quic;

import java.net.UnknownServiceException;
import java.nio.ByteBuffer;

import tech.lp2p.core.Protocol;
import tech.lp2p.core.Responder;
import tech.lp2p.core.Status;

public record AlpnResponder(Stream stream, Responder responder,
                            Libp2pState streamState) implements StreamHandler {


    public static AlpnResponder create(Stream stream, Responder responder) {
        return new AlpnResponder(stream, responder, new Libp2pState(responder));
    }

    @Override
    public void data(ByteBuffer data) {
        try {
            StreamState.iteration(streamState, stream, data);
        } catch (UnknownServiceException unknownServiceException) {
            stream.resetStream(Status.PROTOCOL_NEGOTIATION_FAILED);
        } catch (Throwable throwable) {
            stream.resetStream(Status.INTERNAL_ERROR);
        }
    }

    @Override
    public void terminated() {
        streamState.reset();
        responder.terminated(stream, streamState.protocol());
    }

    @Override
    public void fin() {
        streamState.reset();
        responder.fin(stream, streamState.protocol());
    }


    @Override
    public boolean readFully() {
        return false;
    }


    static class Libp2pState extends StreamState {
        private final Responder responder;
        Protocol protocol = null;

        Libp2pState(Responder responder) {
            this.responder = responder;
        }


        public Protocol protocol() {
            return protocol;
        }

        public void accept(Stream stream, ByteBuffer frame) throws Exception {
            if (frame.hasRemaining()) {
                if (isProtocol(frame)) {
                    String name = new String(frame.array(), 0, frame.limit() - 1);
                    protocol = Protocol.name(name);
                    responder.protocol(stream, protocol);
                } else {
                    responder.data(stream, protocol, frame.array());
                }
            }
        }
    }
}
