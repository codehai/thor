package tech.lp2p.quic;


import java.nio.ByteBuffer;
import java.util.concurrent.TimeoutException;

public interface Requester {
    String PEER = "PEER";
    String ADDRS = "ADDRS";
    String TIMER = "TIMER";
    String INITIALIZED = "INITIALIZED";


    static Stream createStream(Connection connection, Requester requester)
            throws InterruptedException, TimeoutException {
        return connection.createStream(stream -> AlpnRequester.create(stream,
                requester), true);
    }

    static Stream createStream(Connection connection)
            throws InterruptedException, TimeoutException {
        return connection.createStream(RequestResponse::new, true);
    }

    void throwable(Throwable throwable);

    void data(Stream stream, byte[] data) throws Exception;

    void terminated(Stream stream);

    void fin(Stream stream);

    record RequestResponse(Stream stream) implements StreamHandler {

        @Override
        public void terminated() {
        }

        @Override
        public void fin() {
        }

        @Override
        public boolean readFully() {
            return true;
        }

        @Override
        public void data(ByteBuffer data) {
            throw new IllegalStateException("should never be invoked");
        }
    }
}
