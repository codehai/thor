package tech.lp2p.quic;

import java.net.UnknownServiceException;
import java.nio.ByteBuffer;

import tech.lp2p.core.Status;

public record AlpnRequester(Stream stream, Requester requester, StreamState streamState)
        implements StreamHandler {

    public static AlpnRequester create(Stream stream, Requester requester) {
        return new AlpnRequester(stream, requester, new AlpnState(requester));
    }

    @Override
    public void data(ByteBuffer data) {
        try {
            StreamState.iteration(streamState, stream, data);
        } catch (UnknownServiceException unknownServiceException) {
            stream.resetStream(Status.PROTOCOL_NEGOTIATION_FAILED);
            throwable(unknownServiceException);
        } catch (Throwable throwable) {
            stream.resetStream(Status.INTERNAL_ERROR);
            throwable(throwable);
        }
    }

    @Override
    public void terminated() {
        streamState.reset();
        requester.terminated(stream);
    }

    @Override
    public void fin() {
        streamState.reset();
        requester.fin(stream);
    }

    public void throwable(Throwable throwable) {
        streamState.reset();
        requester.throwable(throwable);
    }

    @Override
    public boolean readFully() {
        return false;
    }


    static class AlpnState extends StreamState {

        private final Requester requester;


        AlpnState(Requester requester) {
            this.requester = requester;
        }

        public void accept(Stream stream, ByteBuffer frame) throws Exception {
            if (frame.hasRemaining()) {
                if (!StreamState.isProtocol(frame)) {
                    requester.data(stream, frame.array());
                }
            }
        }
    }
}
