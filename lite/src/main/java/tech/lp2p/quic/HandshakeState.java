package tech.lp2p.quic;

public enum HandshakeState {

    Initial,
    HasHandshakeKeys,
    HasAppKeys,
    Confirmed;

    public boolean transitionAllowed(HandshakeState proposedState) {
        return this.ordinal() < proposedState.ordinal();
    }
}

