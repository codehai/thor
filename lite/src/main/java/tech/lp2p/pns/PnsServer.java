package tech.lp2p.pns;


import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.SocketAddress;
import java.security.cert.X509Certificate;
import java.util.HashSet;
import java.util.Objects;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Consumer;
import java.util.function.Function;

import javax.net.ssl.SSLSocket;

import tech.lp2p.core.Event;
import tech.lp2p.core.Network;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.RequestHandler;
import tech.lp2p.core.Utils;

public class PnsServer {
    private final RequestHandler requestHandler;
    private final Thread serverThread;
    private final Consumer<Event> eventConsumer;
    private final ServerSocket serverSocket;
    private final Set<SocketAddress> expectation = ConcurrentHashMap.newKeySet();
    private final Function<PeerId, Boolean> isGated;
    private final Set<PnsConnection> connections = ConcurrentHashMap.newKeySet();


    public PnsServer(Consumer<Event> eventConsumer, Function<PeerId, Boolean> isGated,
                     RequestHandler requestHandler, ServerSocket serverSocket) {
        this.eventConsumer = eventConsumer;
        this.requestHandler = requestHandler;
        this.serverSocket = serverSocket;
        this.isGated = isGated;
        this.serverThread = new Thread(this::acceptConnections);
        this.serverThread.setDaemon(true);
    }


    public Set<PnsConnection> connections() {
        Set<PnsConnection> result = new HashSet<>();
        for (PnsConnection connection : connections) {
            if (connection.isConnected()) {
                result.add(connection);
            } else {
                Utils.error("Misses a connection for removing");
                connections.remove(connection);
            }
        }
        return result;
    }

    public Set<PnsConnection> connections(PeerId peerId) {
        Set<PnsConnection> result = new HashSet<>();
        for (PnsConnection connection : connections()) {
            if (Objects.equals(peerId, connection.remotePeerId())) {
                result.add(connection);
            }
        }
        return result;
    }

    public void registerConnection(PnsConnection connection) {
        connections.add(connection);

        try {
            eventConsumer.accept(Event.INCOMING_CONNECT_EVENT);
        } catch (Throwable throwable) {
            Utils.error(throwable); // should not occur
        }
    }

    public void removeConnection(PnsConnection connection) {
        connections.remove(connection);

        try {
            eventConsumer.accept(Event.INCOMING_CONNECT_EVENT);
        } catch (Throwable throwable) {
            Utils.error(throwable); // should not occur
        }
    }

    public void punching(Peeraddr peeraddr, long expireTimeStamp) {
        try {
            InetAddress address = InetAddress.getByAddress(peeraddr.address());

            int port = peeraddr.port();
            if (Network.publicAddresses().contains(address)) {
                // same machine not need for punching
                Utils.error("Ignore punching same machine " + address);
                return;
            }
            punching(address, port, expireTimeStamp);
        } catch (Throwable throwable) {
            Utils.error(throwable); // should not occur
        }
    }

    public int punching(InetAddress address, int port, long expireTimeStamp) {

        int punches = 0;

        InetSocketAddress inetSocketAddress = new InetSocketAddress(address, port);

        try {
            DatagramSocket socket = new DatagramSocket(serverSocket.getLocalPort());
            boolean added = expectation.add(inetSocketAddress);
            if (added) { // otherwise punching is is already currently working on the address
                try {
                    punches = punch(socket, inetSocketAddress, expireTimeStamp);
                } finally {
                    expectation.remove(inetSocketAddress);
                }
            }
        } catch (Throwable throwable) {
            Utils.error(throwable);
        }
        return punches;
    }

    public int punch(DatagramSocket socket, InetSocketAddress inetSocketAddress, long expireTimeStamp) {

        int punches = 0;
        // check expireTimeStamp
        if (System.currentTimeMillis() > expireTimeStamp) {
            return 0;
        }
        if (!expectation.contains(inetSocketAddress)) {
            return 0;
        }

        try {
            int length = 64;
            byte[] datagramData = new byte[length];
            Random rd = new Random();
            rd.nextBytes(datagramData);

            DatagramPacket datagram = new DatagramPacket(datagramData, length,
                    inetSocketAddress.getAddress(), inetSocketAddress.getPort());

            socket.send(datagram);
            punches++;


            Thread.sleep(new Random().nextInt(190) + 10); // sleep for random value between 10 and 200

            if (!Thread.currentThread().isInterrupted()) {
                punches += punch(socket, inetSocketAddress, expireTimeStamp);
            }
            // interrupt exception should occur, but it simply ends the loop
        } catch (Throwable throwable) {
            Utils.error(throwable);
        }
        return punches;
    }

    private void acceptConnections() {
        while (!serverSocket.isClosed()) {
            SSLSocket socket;

            // accept a connection
            try {
                socket = (SSLSocket) serverSocket.accept();
            } catch (Throwable throwable) {
                if (!serverSocket.isClosed()) {
                    Utils.error(throwable);
                }
                return;
            }

            try {
                java.security.cert.Certificate certificate =
                        socket.getSession().getPeerCertificates()[0];

                PeerId peerId = PeerId.extractPeerId((X509Certificate) certificate);

                if (isGated.apply(peerId)) {
                    socket.close();
                    return;
                }


                PnsConnection.start(this, socket, peerId, requestHandler);

                try { // This is for punching
                    expectation.remove(socket.getRemoteSocketAddress());
                } catch (Throwable throwable) {
                    Utils.error(throwable);
                }


            } catch (Throwable throwable) {
                Utils.error(throwable);

                // close the connection Invalid peerId
                try {
                    socket.close();
                } catch (Throwable ignore) {
                }
            }
        }
    }

    public void start() {
        serverThread.start();
    }


    public void shutdown() {

        connections().forEach(PnsConnection::close);
        connections.clear();

        try {
            serverSocket.close();
        } catch (Throwable throwable) {
            Utils.error(throwable);
        }
        serverThread.interrupt();
    }

    public InetAddress localAddress() {
        return serverSocket.getInetAddress();
    }
}
