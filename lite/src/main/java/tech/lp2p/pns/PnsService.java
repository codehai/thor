package tech.lp2p.pns;


import java.nio.ByteBuffer;
import java.security.KeyStore;
import java.util.function.Consumer;
import java.util.function.Function;

import javax.net.ServerSocketFactory;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.X509TrustManager;

import tech.lp2p.core.Certificate;
import tech.lp2p.core.Event;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.RequestHandler;
import tech.lp2p.core.TrustManager;
import tech.lp2p.core.Utils;

public interface PnsService {


    static ByteBuffer header(short protocol, int body) {
        int headerLength = Utils.unsignedVariantSize(protocol) + Utils.unsignedVariantSize(body);
        ByteBuffer buffer = ByteBuffer.allocate(headerLength);
        Utils.writeUnsignedVariant(buffer, protocol); // the protocol
        Utils.writeUnsignedVariant(buffer, body);
        buffer.rewind();
        return buffer;
    }


    static SSLContext sslContext(Certificate certificate) throws Exception {
        KeyStore ks = KeyStore.getInstance(KeyStore.getDefaultType());

        char[] passphrase = "passphrase".toCharArray();

        ks.load(null, null);
        ks.setKeyEntry("alias", certificate.certificateKey(), passphrase,
                new java.security.cert.Certificate[]{certificate.x509Certificate()});

        KeyManagerFactory kmf = KeyManagerFactory.getInstance(
                KeyManagerFactory.getDefaultAlgorithm());
        kmf.init(ks, passphrase);

        SSLContext context = SSLContext.getInstance("TLSv1.3");

        X509TrustManager[] trustManagers = new X509TrustManager[1];
        trustManagers[0] = new TrustManager();

        context.init(kmf.getKeyManagers(), trustManagers, null);

        return context;
    }

    static PnsServer createPnsServer(Consumer<Event> eventConsumer,
                                     Function<PeerId, Boolean> isGated,
                                     RequestHandler requestHandler,
                                     Certificate certificate, int port) throws Exception {


        SSLContext context = PnsService.sslContext(certificate);
        ServerSocketFactory ssf = context.getServerSocketFactory();

        SSLServerSocket serverSocket = (SSLServerSocket) ssf.createServerSocket(port);
        serverSocket.setReuseAddress(true); // for punching
        serverSocket.setNeedClientAuth(true);
        serverSocket.setWantClientAuth(true);
        return new PnsServer(eventConsumer, isGated, requestHandler, serverSocket);

    }
}
