package tech.lp2p.pns;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketException;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.WritableByteChannel;
import java.util.Objects;

import tech.lp2p.core.PeerId;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Request;
import tech.lp2p.core.RequestHandler;
import tech.lp2p.core.Response;
import tech.lp2p.core.Utils;

public final class PnsConnection {
    private final Socket socket;
    private final PeerId remotePeerId;
    private final RequestHandler requestHandler;
    private final PnsServer pnsServer;
    private final Thread thread;

    private PnsConnection(PnsServer pnsServer, Socket socket,
                          PeerId remotePeerId, RequestHandler requestHandler) {
        this.socket = socket;
        this.remotePeerId = remotePeerId;
        this.requestHandler = requestHandler;
        this.pnsServer = pnsServer;
        this.thread = new Thread(this::handleConnection);
        this.thread.setDaemon(true);
    }

    public static void start(PnsServer pnsServer, Socket socket, PeerId peerId,
                             RequestHandler requestHandler) {
        new PnsConnection(pnsServer, socket, peerId, requestHandler).start();
    }

    private void start() {
        thread.start();
    }

    public void close() {
        pnsServer.removeConnection(this);
        if (!socket.isClosed()) {
            try {
                socket.close();
            } catch (Throwable throwable) {
                Utils.error(throwable);
            }
        }
    }

    public boolean isConnected() {
        return socket.isConnected();
    }

    public InetSocketAddress remoteAddress() {
        return (InetSocketAddress) socket.getRemoteSocketAddress();
    }


    public void handleConnection() {
        pnsServer.registerConnection(this);
        try (InputStream inputStream = socket.getInputStream();
             OutputStream outputStream = socket.getOutputStream()) {

            while (socket.isConnected()) {
                short protocol = Utils.readShortUnsignedVariant(inputStream);
                if (protocol == Utils.EOF) {
                    break;
                }

                int length = Utils.readUnsignedVariant(inputStream);
                if (length == Utils.EOF) {
                    break;
                }

                ByteBuffer payload = ByteBuffer.allocate(length);

                while (payload.hasRemaining()) {
                    int read = Channels.newChannel(inputStream).read(payload);
                    if (read == Utils.EOF) {
                        break;
                    }
                }
                payload.rewind();


                Request request = new Request(remotePeerId, protocol, payload.array());
                Response response = requestHandler.handleRequest(request);

                ByteBuffer body = response.body();
                Objects.requireNonNull(body);

                WritableByteChannel wc = Channels.newChannel(outputStream);
                wc.write(PnsService.header(response.status(), body.capacity()));
                wc.write(body);
                outputStream.flush();
            }
        } catch (SocketException ignore) { // because of closing
        } catch (Throwable throwable) {
            Utils.error(throwable);
        } finally {
            close();
        }
    }

    public Peeraddr remotePeeraddr() {
        return Peeraddr.create(remotePeerId, remoteAddress());
    }

    public PeerId remotePeerId() {
        return remotePeerId;
    }
}
