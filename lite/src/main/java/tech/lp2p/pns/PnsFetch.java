package tech.lp2p.pns;

import java.io.IOException;
import java.net.URI;
import java.nio.ByteBuffer;

import tech.lp2p.Lite;
import tech.lp2p.core.BlockStore;
import tech.lp2p.core.Cid;
import tech.lp2p.core.Response;
import tech.lp2p.core.Status;
import tech.lp2p.dag.DagFetch;

public record PnsFetch(URI uri, Lite host, PnsConnector pnsConnector, BlockStore blockStore)
        implements DagFetch {


    private static ByteBuffer request(BlockStore blockStore, PnsChannel pnsChannel, long cid)
            throws IOException {
        Response response = pnsChannel.request(Lite.FETCH_PROTOCOL, Cid.hash(cid));
        if (response.status() == Status.OK) {
            ByteBuffer data = response.body();
            data.rewind();
            blockStore.storeBlock(cid, data);
            data.rewind();
            return data;
        }
        throw new IOException("Cid could not be fetched");
    }

    public static PnsFetch create(Lite host, BlockStore blockStore,
                                  PnsConnector pnsConnector, URI uri) {
        return new PnsFetch(uri, host, pnsConnector, blockStore);

    }

    @Override
    public ByteBuffer getBlock(long cid) throws IOException {
        ByteBuffer block = blockStore.getBlock(cid);
        if (block != null) {
            return block;
        }
        try {
            PnsChannel pnsChannel = pnsConnector.connect(host, uri);
            return request(blockStore, pnsChannel, cid);
        } catch (Throwable throwable) {
            throw new IOException(throwable);
        }
    }
}
