package tech.lp2p.pns;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.security.cert.X509Certificate;
import java.util.Objects;
import java.util.concurrent.locks.ReentrantLock;

import javax.net.SocketFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;

import tech.lp2p.core.PeerId;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Response;
import tech.lp2p.core.Status;
import tech.lp2p.core.Utils;


public final class PnsChannel {

    private final Socket socket;
    private final PnsConnector pnsConnector;
    private final PeerId remotePeerId;
    private final ReentrantLock lock = new ReentrantLock();


    private PnsChannel(PeerId remotePeerId, PnsConnector pnsConnector, Socket socket) {
        this.remotePeerId = remotePeerId;
        this.pnsConnector = pnsConnector;
        this.socket = socket;
    }


    public static PnsChannel open(PnsConnector pnsConnector, Peeraddr peeraddr) throws Exception {
        InetAddress inetAddress;
        try {
            inetAddress = InetAddress.getByAddress(peeraddr.address());
        } catch (Throwable throwable) {
            throw new Exception("invalid address " + peeraddr.peerId());
        }
        Objects.requireNonNull(inetAddress);


        final SSLContext context = PnsService.sslContext(pnsConnector.certificate());

        final SocketFactory sf = context.getSocketFactory();
        final SSLSocket socket = (SSLSocket) sf.createSocket(inetAddress, peeraddr.port());
        final PnsChannel channel = new PnsChannel(peeraddr.peerId(), pnsConnector, socket);

        try {
            java.security.cert.Certificate peerCertificate =
                    socket.getSession().getPeerCertificates()[0];

            PeerId peerId = PeerId.extractPeerId((X509Certificate) peerCertificate);

            Utils.checkTrue(Objects.equals(peerId, peeraddr.peerId()),
                    "Not connected to the expected peer");

            pnsConnector.registerChannel(channel);
            return channel;
        } catch (Throwable throwable) {
            channel.close();
            throw new Exception("Invalid peer certificate");
        }

    }


    public Peeraddr remotePeeraddr() {
        return Peeraddr.create(remotePeerId(), remoteAddress());
    }

    public Response request(short protocol, byte[] data) {
        lock.lock();
        try {
            Objects.requireNonNull(data);
            OutputStream outputStream = socket.getOutputStream();
            outputStream.write(PnsService.header(protocol, data.length).array());
            outputStream.write(data);
            outputStream.flush();
            return receive();
        } catch (Throwable throwable) {
            Utils.error(throwable);
            close();
            return Response.create(Status.SERVICE_UNAVAILABLE);
        } finally {
            lock.unlock();
        }
    }

    public Response receive() throws Exception {

        InputStream inputStream = socket.getInputStream();

        short status = Utils.readShortUnsignedVariant(inputStream);

        if (status == Utils.EOF) {
            throw new IllegalStateException("EOF");
        }

        int length = Utils.readUnsignedVariant(inputStream);
        if (length == Utils.EOF) {
            throw new IllegalStateException("EOF");
        }


        ByteBuffer payload = ByteBuffer.allocateDirect(length);
        ReadableByteChannel rc = Channels.newChannel(inputStream);
        while (payload.hasRemaining()) {
            int read = rc.read(payload);
            if (read == Utils.EOF) {
                throw new IllegalStateException("EOF");
            }
        }
        payload.rewind();
        return Response.create(status, payload);

    }


    public void close() {
        try {
            socket.close();
        } catch (Throwable throwable) {
            Utils.error(throwable);
        } finally {
            pnsConnector.removeChannel(this);
        }
    }


    public boolean isConnected() {
        return socket.isConnected();
    }

    public InetSocketAddress remoteAddress() {
        try {
            return (InetSocketAddress) socket.getRemoteSocketAddress();
        } catch (Throwable throwable) {
            Utils.error(throwable);
            throw new IllegalStateException(throwable);
        }
    }

    public PeerId remotePeerId() {
        return remotePeerId;
    }
}
