package tech.lp2p.pns;

import java.net.URI;
import java.util.HashSet;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import tech.lp2p.Lite;
import tech.lp2p.core.Certificate;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Utils;
import tech.lp2p.lite.LiteService;


public class PnsConnector {

    private final Set<PnsChannel> channels = ConcurrentHashMap.newKeySet();

    private final Certificate certificate;

    public PnsConnector(Certificate certificate) {
        this.certificate = certificate;
    }

    private PnsChannel resolve(PeerId target) {
        Set<PnsChannel> pnsChannels = channels(target);
        if (!pnsChannels.isEmpty()) {
            return pnsChannels.iterator().next();
        }
        return null;
    }

    private PnsChannel resolveAddress(Lite host, PeerId target) throws Exception {
        PnsChannel connection = resolve(target);
        if (connection == null) {
            Optional<Peeraddr> peeraddr = LiteService.hopConnect(host, target);
            if (peeraddr.isPresent()) {
                return PnsChannel.open(this, peeraddr.get());
            } else {
                throw new Exception("No hop connection established");
            }
        }
        return connection;

    }

    public PnsChannel connect(Lite host, URI uri) throws Exception {
        Optional<Peeraddr> peeraddr = Lite.extractPeeraddr(uri);
        if (peeraddr.isPresent()) {
            return connect(peeraddr.get());
        }

        Optional<PeerId> peerId = Lite.extractPeerId(uri);
        if (peerId.isPresent()) {
            return connect(host, peerId.get());
        }
        throw new Exception("Invalid URI " + uri);
    }

    public PnsChannel connect(Peeraddr peeraddr) throws Exception {
        Set<PnsChannel> pnsChannels = channels(peeraddr);
        if (!pnsChannels.isEmpty()) {
            return pnsChannels.iterator().next();
        }
        return PnsChannel.open(this, peeraddr);
    }

    public PnsChannel connect(Lite host, PeerId peerId) throws Exception {
        Set<PnsChannel> pnsChannels = channels(peerId);
        if (!pnsChannels.isEmpty()) {
            return pnsChannels.iterator().next();
        }
        return resolveAddress(host, peerId);
    }


    public Set<PnsChannel> channels(Peeraddr peeraddr) {
        Set<PnsChannel> pnsChannels = new HashSet<>();
        for (PnsChannel channel : channels()) {
            if (Objects.equals(channel.remotePeeraddr(), peeraddr)) {
                pnsChannels.add(channel);
            }

        }
        return pnsChannels;
    }

    public Set<PnsChannel> channels(PeerId peerId) {
        Set<PnsChannel> pnsChannels = new HashSet<>();
        for (PnsChannel channel : channels()) {
            if (Objects.equals(channel.remotePeerId(), peerId)) {
                pnsChannels.add(channel);
            }
        }
        return pnsChannels;
    }

    public Set<PnsChannel> channels() {
        Set<PnsChannel> pnsChannels = new HashSet<>();
        for (PnsChannel channel : channels) {
            if (channel.isConnected()) {
                pnsChannels.add(channel);
            } else {
                Utils.error("Misses a channel for removing");
                channels.remove(channel);
            }
        }
        return pnsChannels;
    }


    public void registerChannel(PnsChannel channel) {
        channels.add(channel);
    }

    public void removeChannel(PnsChannel channel) {
        channels.remove(channel);
    }


    public void shutdown() {
        channels().forEach(PnsChannel::close);
        channels.clear();
    }

    public Certificate certificate() {
        return certificate;
    }
}
