package tech.lp2p;

import java.io.IOException;
import java.io.InputStream;
import java.net.ConnectException;
import java.net.InetAddress;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeoutException;
import java.util.function.Consumer;
import java.util.function.Function;

import tech.lp2p.core.BlockStore;
import tech.lp2p.core.Certificate;
import tech.lp2p.core.Channel;
import tech.lp2p.core.Cid;
import tech.lp2p.core.Event;
import tech.lp2p.core.Fid;
import tech.lp2p.core.FileStore;
import tech.lp2p.core.Keys;
import tech.lp2p.core.MemoryPeers;
import tech.lp2p.core.Network;
import tech.lp2p.core.Node;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.PeerStore;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Peeraddrs;
import tech.lp2p.core.Protocol;
import tech.lp2p.core.Protocols;
import tech.lp2p.core.Request;
import tech.lp2p.core.RequestHandler;
import tech.lp2p.core.Response;
import tech.lp2p.core.Result;
import tech.lp2p.core.Root;
import tech.lp2p.core.Status;
import tech.lp2p.core.Utils;
import tech.lp2p.crypto.Ed25519Sign;
import tech.lp2p.dag.DagChannel;
import tech.lp2p.dag.DagFetch;
import tech.lp2p.dag.DagService;
import tech.lp2p.dag.DagStream;
import tech.lp2p.ident.IdentifyHandler;
import tech.lp2p.ident.IdentifyPushHandler;
import tech.lp2p.lite.LiteService;
import tech.lp2p.lite.LiteStreamHandler;
import tech.lp2p.pns.PnsChannel;
import tech.lp2p.pns.PnsConnection;
import tech.lp2p.pns.PnsConnector;
import tech.lp2p.pns.PnsFetch;
import tech.lp2p.pns.PnsServer;
import tech.lp2p.pns.PnsService;
import tech.lp2p.quic.Connector;
import tech.lp2p.relay.RelayStopHandler;


public final class Lite implements AutoCloseable {
    public static final String SEPARATOR = "/";
    public static final int RESOLVE_TIMEOUT = 60;
    public static final int GRACE_PERIOD = 15;
    public static final int DHT_ALPHA = 30;
    public static final int DHT_CONCURRENCY = 5;
    public static final int TIMEOUT = 5; // in seconds
    public static final short FETCH_PROTOCOL = 0;
    public static final String AGENT = "lite/1.0.0/";

    private final Certificate certificate;
    private final String agent;
    private final Peeraddrs bootstrap;
    private final PeerStore peerStore;
    private final BlockStore blockStore;

    private final Consumer<Event> eventConsumer;

    private final ConcurrentHashMap<Integer, RequestHandler> handlers = new ConcurrentHashMap<>();
    private final Protocols protocols = new Protocols();

    private final PnsConnector pnsConnector;
    private final PnsServer pnsServer;
    private final Connector connector;
    private final int port;
    private final Keys keys;
    private final boolean disableServer;

    private Lite(Keys keys, Certificate certificate, Peeraddrs bootstrap, PeerStore peerStore,
                 BlockStore blockStore, String agent, Function<PeerId, Boolean> isGated,
                 Consumer<Event> eventConsumer, int port, boolean disableServer) throws Exception {
        Objects.requireNonNull(keys);
        Objects.requireNonNull(certificate);
        Objects.requireNonNull(agent);
        Objects.requireNonNull(blockStore);
        Objects.requireNonNull(bootstrap);
        Objects.requireNonNull(peerStore);
        Objects.requireNonNull(isGated);
        Objects.requireNonNull(eventConsumer);

        this.keys = keys;
        this.blockStore = blockStore;
        this.bootstrap = bootstrap;
        this.agent = agent;
        this.certificate = certificate;
        this.peerStore = peerStore;
        this.eventConsumer = eventConsumer;
        this.port = port;
        this.disableServer = disableServer;


        protocols.put(Protocol.MULTISTREAM_PROTOCOL, new LiteStreamHandler());

        if (Protocol.IDENTITY_PROTOCOL.enabled()) {
            protocols.put(Protocol.IDENTITY_PROTOCOL, new IdentifyHandler(this));
        }
        if (Protocol.IDENTITY_PUSH_PROTOCOL.enabled()) {
            protocols.put(Protocol.IDENTITY_PUSH_PROTOCOL, new IdentifyPushHandler());
        }


        connector = new Connector(eventConsumer);
        pnsConnector = new PnsConnector(certificate);
        if (disableServer) {
            pnsServer = null;
        } else {
            pnsServer = PnsService.createPnsServer(eventConsumer, isGated,
                    createRequestHandler(), certificate, port);
            pnsServer.start();

            if (Protocol.RELAY_PROTOCOL_STOP.enabled()) {
                protocols.put(Protocol.RELAY_PROTOCOL_STOP, new RelayStopHandler(
                        this, pnsServer, isGated));
            }
        }

    }

    public static Keys generateKeys() {
        try {
            Ed25519Sign.KeyPair keyPair = Ed25519Sign.KeyPair.newKeyPair();
            return new Keys(new PeerId(keyPair.getPublicKey()),
                    keyPair.getPrivateKey());
        } catch (Throwable throwable) {
            throw new IllegalStateException(throwable);
        }
    }

    public static PeerId decodePeerId(String name) throws Exception {
        return PeerId.decodeName(name);
    }

    // Note : the cid is not stored in the block store
    public static long rawCid(String string) {
        return DagService.createRaw(string.getBytes(StandardCharsets.UTF_8));
    }

    public static Peeraddr createPeeraddr(PeerId peerId, InetAddress inetAddress, int port) {
        return new Peeraddr(peerId, inetAddress.getAddress(), port);
    }

    public static Peeraddr createPeeraddr(String peerId, String address, int port)
            throws Exception {
        return createPeeraddr(decodePeerId(peerId), InetAddress.getByName(address), port);
    }

    // Reservation feature is only possible when a public Inet6Address is available
    public static boolean reservationFeaturePossible() {
        return !Network.publicInet6Addresses().isEmpty();
    }

    public static Lite.Builder newBuilder() {
        return new Builder();
    }

    public static Lite newLite() throws Exception {
        return Lite.newBuilder().build();
    }

    public static Lite newClient() throws Exception {
        return Lite.newBuilder().disableServer().build();
    }

    public static URI createProtocolUri(PeerId peerId, short protocol) throws URISyntaxException {
        Objects.requireNonNull(peerId);
        URI uri = peerId.toUri();
        return createProtocolUri(uri, protocol);
    }

    public static URI createProtocolUri(Peeraddr peeraddr, short protocol)
            throws URISyntaxException {
        Objects.requireNonNull(peeraddr);
        URI uri = peeraddr.toUri();
        return createProtocolUri(uri, protocol);
    }

    public static URI createProtocolUri(URI uri, short protocol)
            throws URISyntaxException {
        Utils.checkTrue(protocol >= 0, "Protocol not well defined");
        return new URI(uri.getScheme(), uri.getUserInfo(), uri.getHost(), uri.getPort(),
                SEPARATOR + protocol, uri.getQuery(), uri.getFragment());
    }


    public static URI createFetchUri(PeerId peerId, long cid) throws URISyntaxException {
        Objects.requireNonNull(peerId);
        URI uri = peerId.toUri();
        return createFetchUri(uri, cid);
    }

    public static URI createFetchUri(Peeraddr peeraddr, long cid) throws URISyntaxException {
        Objects.requireNonNull(peeraddr);
        URI uri = peeraddr.toUri();
        return createFetchUri(uri, cid);
    }

    public static URI createFetchUri(Peeraddr peeraddr, Node node) throws URISyntaxException {
        Objects.requireNonNull(peeraddr);
        URI uri = peeraddr.toUri();
        return createFetchUri(uri, node.cid());
    }

    public static URI createFetchUri(URI uri, long cid) throws URISyntaxException {
        // Note: the protocol FETCH_REQUEST is the default one (no need to mention in URI)
        return new URI(uri.getScheme(), uri.getUserInfo(), uri.getHost(), uri.getPort(),
                Lite.SEPARATOR, uri.getQuery(), Cid.encode(cid));
    }

    public static URI createFetchUri(URI uri, Node node) throws URISyntaxException {
        Objects.requireNonNull(node);
        return createFetchUri(uri, node.cid());
    }


    public static short extractProtocol(URI uri) {
        short protocol = Lite.FETCH_PROTOCOL;
        String path = uri.getPath();
        if (!path.isEmpty()) {
            String proto = path.substring(1);
            if (!proto.isBlank()) {
                protocol = Short.parseShort(proto);
            }
        }
        return protocol;
    }

    public static Optional<PeerId> extractPeerId(URI uri) {
        return PeerId.extractPeerId(uri);
    }

    public static Optional<Peeraddr> extractPeeraddr(URI uri) {
        return Peeraddr.extractPeeraddr(uri);
    }

    public static Optional<Long> extractCid(URI uri) {
        return Cid.extractCid(uri);
    }


    private Response handleFetchRequest(Request request) {
        try {
            byte[] payload = request.body();

            if (payload.length == 0) { // root request

                Long root = blockStore.root();

                if (root == null) {
                    return Response.create(Status.NOT_FOUND);
                }

                return Response.create(Status.OK, ByteBuffer.wrap(Cid.hash(root)));
            }

            if (payload.length != Long.BYTES) { // body must be 32 bit (hash)
                return Response.create(Status.BAD_REQUEST);
            }

            long cid = Cid.createCid(payload);
            ByteBuffer block = blockStore.getBlock(cid);

            if (block == null) {
                return Response.create(Status.NOT_FOUND);
            }

            return Response.create(Status.OK, block);

        } catch (Throwable throwable) {
            return Response.create(Status.INTERNAL_ERROR);
        }
    }

    public Peeraddrs hopReserves() {
        return LiteService.hopReserves(this);
    }

    public boolean hasHopReserves() {
        return !hopReserves().isEmpty();
    }

    public void hopReserve(int maxReservation, int timeout) {
        Utils.checkTrue(!disableServer, "Server is disabled");
        LiteService.hopReserve(this, maxReservation, timeout);
    }

    public int numHopReserves() {
        return hopReserves().size();
    }

    public int numIncomingConnections() {
        if (disableServer) {
            return 0;
        }
        return pnsServer.connections().size();
    }

    public Peeraddrs incomingConnections() {
        Peeraddrs peeraddrs = new Peeraddrs();
        if (!disableServer) {
            Set<PnsConnection> connections = pnsServer.connections();
            for (PnsConnection connection : connections) {
                peeraddrs.add(connection.remotePeeraddr());
            }
        }
        return peeraddrs;
    }

    public void closeIncomingConnections(PeerId peerId) {
        if (!disableServer) {
            Set<PnsConnection> connections = pnsServer.connections(peerId);
            for (PnsConnection connection : connections) {
                connection.close();
            }
        }
    }

    public void closeOutgoingConnections(PeerId peerId) {
        Set<PnsChannel> pnsChannels = pnsConnector.channels(peerId);
        for (PnsChannel channel : pnsChannels) {
            channel.close();
        }
    }

    public boolean hasIncomingConnections(PeerId peerId) {
        if (disableServer) {
            return false;
        }
        return !pnsServer.connections(peerId).isEmpty();
    }

    public boolean hasOutgoingConnections(PeerId peerId) {
        return !pnsConnector.channels(peerId).isEmpty();
    }

    public int numOutgoingConnections() {
        return pnsConnector.channels().size();
    }

    public Peeraddr loopbackAddress() {
        return Peeraddr.loopbackPeeraddr(peerId(), port());
    }

    public int port() {
        return port;
    }

    public void addRequestHandler(int protocol, RequestHandler requestHandler) {
        Utils.checkTrue(protocol >= 10, "Invalid protocol (protocol >= 10)");
        handlers.put(protocol, requestHandler);
    }

    private RequestHandler createRequestHandler() {
        return request -> {
            try {
                int protocol = request.protocol();
                if (protocol == Lite.FETCH_PROTOCOL) {
                    return handleFetchRequest(request);
                }

                RequestHandler handler = handlers.get(protocol);
                if (handler != null) {
                    return handler.handleRequest(request);
                } else {
                    return Response.create(Status.NOT_IMPLEMENTED);
                }
            } catch (Throwable throwable) {
                return Response.create(Status.INTERNAL_ERROR);
            }
        };
    }

    public Optional<Root> fetchRoot(URI uri) {
        Result response = send(uri, Utils.BYTES_EMPTY);
        if (response.status() == Status.OK) {
            return Optional.of(
                    new Root(response.peeraddr(),
                            Cid.createCid(response.content())));
        }
        return Optional.empty();
    }

    public InputStream stream(URI uri) throws IOException {
        Node node = info(uri);
        if (!node.isDir() && !node.isRaw()) {
            DagFetch dagFetch = PnsFetch.create(this, blockStore, pnsConnector, uri);
            DagChannel dagChannel = DagChannel.create(dagFetch, (Fid) node);
            return new DagStream(dagChannel);
        }
        throw new IOException("Stream on directory or raw not possible");
    }

    public Channel channel(URI uri) throws IOException {
        Node node = info(uri);
        if (!node.isDir() && !node.isRaw()) {
            DagFetch dagFetch = PnsFetch.create(this, blockStore, pnsConnector, uri);
            return DagChannel.create(dagFetch, (Fid) node);
        }
        throw new IOException("Stream on directory or raw not possible");
    }


    public Node info(URI uri) throws IOException {
        Optional<Long> optional = Cid.extractCid(uri);
        if (optional.isPresent()) {
            long cid = optional.get();
            DagFetch dagFetch = PnsFetch.create(this, blockStore, pnsConnector, uri);
            Node top = DagService.dagNode(dagFetch, cid);
            Objects.requireNonNull(top);
            return top;
        } else {
            Optional<Root> root = fetchRoot(uri);
            if (root.isPresent()) {
                DagFetch dagFetch = PnsFetch.create(this, blockStore, pnsConnector, uri);
                Node top = DagService.dagNode(dagFetch, root.get().cid());
                Objects.requireNonNull(top);
                return top;
            }
            throw new IOException("Root could not be fetched");
        }
    }

    public String fetchText(URI uri) throws IOException {
        return new String(fetchData(uri));
    }

    public byte[] fetchData(URI uri) throws IOException {
        Node node = info(uri);
        if (node.isRaw()) {
            return node.data();
        }
        throw new IOException("Uri does not reference raw node");
    }

    public PeerStore peerStore() {
        return peerStore;
    }

    public Peeraddrs peeraddrs() {
        return Peeraddrs.peeraddrs(peerId(), port());
    }

    public Keys keys() {
        return keys;
    }

    public PeerId peerId() {
        return keys.peerId();
    }

    public Protocols protocols() {
        return protocols;
    }

    public Peeraddrs bootstrap() {
        return bootstrap;
    }

    public Certificate certificate() {
        return certificate;
    }

    public String agent() {
        return agent;
    }

    public BlockStore blockStore() {
        return blockStore;
    }

    public void ping(URI uri) {
        try {
            pnsConnector.connect(this, uri);
        } catch (Throwable throwable) {
            Utils.error(throwable);
        }
    }


    public Result send(URI uri, byte[] body) {
        Objects.requireNonNull(uri);
        Objects.requireNonNull(body);

        try {
            PnsChannel pnsChannel = pnsConnector.connect(this, uri);
            short protocol = extractProtocol(uri);
            Response response = pnsChannel.request(protocol, body);
            return new Result(pnsChannel.remotePeeraddr(),
                    response.status(), response.content());
        } catch (TimeoutException timeoutException) {
            Utils.error(timeoutException);
            return Result.create(Status.REQUEST_TIMEOUT);
        } catch (ConnectException connectException) {
            Utils.error(connectException);
            return Result.create(Status.SERVICE_UNAVAILABLE);
        } catch (InterruptedException interruptedException) {
            Utils.error(interruptedException);
            return Result.create(Status.CLIENT_CLOSED_REQUEST);
        } catch (IOException exception) {
            Utils.error(exception);
            return Result.create(Status.BAD_REQUEST);
        } catch (Throwable throwable) {
            Utils.error(throwable);
            return Result.create(Status.INTERNAL_ERROR);
        }
    }

    @Override
    public void close() {
        pnsConnector.shutdown();
        connector.shutdown();
        if (!disableServer) {
            pnsServer.shutdown();
        }
    }

    public Connector connector() {
        return connector;
    }

    public Consumer<Event> eventConsumer() {
        return eventConsumer;
    }

    public boolean isServerDisabled() {
        return disableServer;
    }

    public static class Builder {
        private String agent = Lite.AGENT;
        private Peeraddrs bootstrap = new Peeraddrs();
        private PeerStore peerStore = new MemoryPeers();
        private BlockStore blockStore = null;
        private Keys keys = null;
        private int port = 0;
        private boolean disableServer = false;
        private Function<PeerId, Boolean> isGated = peerId -> false;
        private Consumer<Event> eventConsumer = event -> {
        };

        public Builder disableServer() {
            this.disableServer = true;
            return this;
        }

        public Builder agent(String agent) {
            Objects.requireNonNull(agent);
            this.agent = agent;
            return this;
        }

        public Builder keys(Keys keys) {
            Objects.requireNonNull(keys);
            this.keys = keys;
            return this;
        }

        // only used when starting a server
        public Builder port(int port) {
            Utils.checkTrue(port > 100, "Port not well defined port > 100");
            Utils.checkTrue(port < 65535, "Port not well defined port < 65535");
            this.port = port;
            return this;
        }

        public Builder bootstrap(Peeraddrs bootstrap) {
            Objects.requireNonNull(bootstrap);
            this.bootstrap = bootstrap;
            return this;
        }

        public Builder peerStore(PeerStore peerStore) {
            Objects.requireNonNull(peerStore);
            this.peerStore = peerStore;
            return this;
        }

        public Builder blockStore(BlockStore blockStore) {
            Objects.requireNonNull(blockStore);
            this.blockStore = blockStore;
            return this;
        }

        public Builder isGated(Function<PeerId, Boolean> isGated) {
            Objects.requireNonNull(isGated);
            this.isGated = isGated;
            return this;
        }

        public Builder eventConsumer(Consumer<Event> eventConsumer) {
            Objects.requireNonNull(eventConsumer);
            this.eventConsumer = eventConsumer;
            return this;
        }


        public Lite build() throws Exception {

            if (blockStore == null) {
                blockStore = new FileStore();
            }
            if (port == 0) {
                port = Utils.nextFreePort();
            }
            if (keys == null) {
                keys = Lite.generateKeys();
            }

            Certificate certificate = Certificate.createCertificate(keys);

            return new Lite(keys, certificate, bootstrap, peerStore,
                    blockStore, agent, isGated, eventConsumer, port, disableServer);
        }
    }
}
