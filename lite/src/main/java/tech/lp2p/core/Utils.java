package tech.lp2p.core;


import com.google.protobuf.MessageLite;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.security.cert.X509Certificate;
import java.util.concurrent.ThreadLocalRandom;
import java.util.function.Consumer;


public interface Utils {
    boolean DEBUG = false;
    boolean ERROR = true;

    String STRING_EMPTY = "";
    byte[] BYTES_EMPTY = new byte[0];


    X509Certificate[] CERTIFICATES_EMPTY = new X509Certificate[0];
    int EOF = -1;

    static byte[] concat(byte[]... chunks) throws IllegalStateException {
        int length = 0;
        for (byte[] chunk : chunks) {
            if (length > Integer.MAX_VALUE - chunk.length) {
                throw new IllegalStateException("exceeded size limit");
            }
            length += chunk.length;
        }
        byte[] res = new byte[length];
        int pos = 0;
        for (byte[] chunk : chunks) {
            System.arraycopy(chunk, 0, res, pos, chunk.length);
            pos += chunk.length;
        }
        return res;
    }


    static int nextFreePort() {
        int port = ThreadLocalRandom.current().nextInt(4001, 65535);
        if (isLocalPortFree(port)) {
            return port;
        } else {
            return nextFreePort();
        }
    }

    private static boolean isLocalPortFree(int port) {
        try {
            new ServerSocket(port).close();
            return true;
        } catch (Throwable ignore) {
            return false;
        }
    }

    /**
     * Returns the values from each provided array combined into a single array. For example, {@code
     * concat(new int[] {a, b}, new int[] {}, new int[] {c}} returns the array {@code {a, b, c}}.
     *
     * @param arrays zero or more {@code int} arrays
     * @return a single array containing all the values from the source arrays, in order
     */
    static int[] concat(int[]... arrays) {
        int length = 0;
        for (int[] array : arrays) {
            length += array.length;
        }
        int[] result = new int[length];
        int pos = 0;
        for (int[] array : arrays) {
            System.arraycopy(array, 0, result, pos, array.length);
            pos += array.length;
        }
        return result;
    }

    static int readUnsignedVariant(ByteBuffer buffer) {
        int result = 0;
        int cur;
        int count = 0;
        do {
            cur = buffer.get() & 0xff;
            result |= (cur & 0x7f) << (count * 7);
            count++;
        } while (((cur & 0x80) == 0x80) && count < 5);
        if ((cur & 0x80) == 0x80) {
            throw new IllegalStateException("invalid unsigned variant sequence");
        }
        return result;
    }

    static int readUnsignedVariant(InputStream inputStream) throws IOException {
        int result = 0;
        int cur;
        int count = 0;
        do {
            cur = inputStream.read() & 0xff;
            result |= (cur & 0x7f) << (count * 7);
            count++;
        } while (((cur & 0x80) == 0x80) && count < 5);
        if ((cur & 0x80) == 0x80) {
            return Utils.EOF;
        }
        return result;
    }

    static short readShortUnsignedVariant(ByteBuffer buffer) {
        short result = 0;
        int cur;
        int count = 0;
        do {
            cur = buffer.get() & 0xff;
            result |= (short) ((cur & 0x7f) << (count * 7));
            count++;
        } while (((cur & 0x80) == 0x80) && count < 5);
        if ((cur & 0x80) == 0x80) {
            throw new IllegalStateException("invalid unsigned variant sequence");
        }
        return result;
    }

    static short readShortUnsignedVariant(InputStream inputStream) throws IOException {
        short result = 0;
        int cur;
        int count = 0;
        do {
            cur = inputStream.read() & 0xff;
            result |= (short) ((cur & 0x7f) << (count * 7));
            count++;
        } while (((cur & 0x80) == 0x80) && count < 5);
        if ((cur & 0x80) == 0x80) {
            return Utils.EOF;
        }
        return result;
    }

    static long readLongUnsignedVariant(ByteBuffer buffer) {
        long result = 0;
        long cur;
        int count = 0;
        do {
            cur = buffer.get() & 0xff;
            result |= (cur & 0x7f) << (count * 7);
            count++;
        } while (((cur & 0x80) == 0x80) && count < 5);
        if ((cur & 0x80) == 0x80) {
            throw new IllegalStateException("invalid unsigned variant sequence");
        }
        return result;
    }

    static int unsignedVariantSize(long value) {
        long remaining = value >> 7;
        int count = 0;
        while (remaining != 0) {
            remaining >>= 7;
            count++;
        }
        return count + 1;
    }

    static void writeUnsignedVariant(ByteBuffer buffer, long value) {
        long remaining = value >>> 7;
        while (remaining != 0) {
            buffer.put((byte) ((value & 0x7f) | 0x80));
            value = remaining;
            remaining >>>= 7;
        }
        buffer.put((byte) (value & 0x7f));
    }


    static ByteBuffer encode(MessageLite message) {
        return encode(message.toByteArray());
    }

    static ByteBuffer encode(byte[] data) {
        int dataLength = unsignedVariantSize(data.length);
        ByteBuffer buffer = ByteBuffer.allocate(dataLength + data.length);
        writeUnsignedVariant(buffer, data.length);
        buffer.put(data);
        buffer.rewind();
        return buffer;
    }


    private static ByteBuffer encodeProtocol(Protocol protocol) {
        byte[] data = protocol.name().getBytes(StandardCharsets.UTF_8);
        int length = data.length + 1; // 1 is "\n"
        int dataLength = unsignedVariantSize(length);
        ByteBuffer buffer = ByteBuffer.allocate(dataLength + length);
        writeUnsignedVariant(buffer, length);
        buffer.put(data);
        buffer.put((byte) '\n');
        buffer.rewind();
        return buffer;
    }

    static ByteBuffer[] encodeProtocols(Protocol... protocols) {
        ByteBuffer[] msg = new ByteBuffer[protocols.length];
        for (int i = 0; i < protocols.length; i++) {
            Protocol protocol = protocols[i];
            msg[i] = encodeProtocol(protocol);
        }
        return msg;
    }

    static ByteBuffer[] encode(MessageLite message, Protocol... protocols) {
        ByteBuffer[] msg = new ByteBuffer[protocols.length + 1];
        for (int i = 0; i < protocols.length; i++) {
            Protocol protocol = protocols[i];
            msg[i] = encodeProtocol(protocol);
        }
        msg[protocols.length] = encode(message);
        return msg;
    }


    static long copy(InputStream inputStream, OutputStream outputStream,
                     long read, long size, Consumer<Integer> consumer) throws IOException {

        byte[] buf = new byte[Short.MAX_VALUE];
        int remember = 0;
        int n;
        while ((n = inputStream.read(buf)) > 0) {
            outputStream.write(buf, 0, n);
            read += n;

            if (size > 0) {
                int percent = (int) ((read * 100.0f) / size);
                if (percent > remember) {
                    remember = percent;
                    consumer.accept(percent);
                }
            }
        }
        return read;
    }


    @SuppressWarnings("unused")
    static void copy(InputStream source, OutputStream sink) throws IOException {
        byte[] buf = new byte[Short.MAX_VALUE];
        int n;
        while ((n = source.read(buf)) > 0) {
            sink.write(buf, 0, n);
        }
    }

    static void debug(String message) {
        if (DEBUG) {
            System.out.println(message);
        }
    }

    static void error(String message) {
        if (ERROR) {
            System.err.println(message);
        }
    }

    static void error(Throwable throwable) {
        if (ERROR) {
            throwable.printStackTrace(System.err);
        }
    }


    static void checkArgument(int val, int cmp, String message) {
        if (val != cmp) {
            throw new IllegalArgumentException(message);
        }
    }

    static void checkTrue(boolean condition, String message) {
        if (!condition) {
            throw new IllegalArgumentException(message);
        }
    }

}
