package tech.lp2p.core;


import java.util.List;
import java.util.Objects;

public record Dir(long cid, long size, String name, List<Link> links, byte[] data) implements Node {
    public Dir {
        Objects.requireNonNull(name);
        Objects.requireNonNull(data);
        Objects.requireNonNull(links);
        Utils.checkTrue(data.length == 0, "Dir has no data");
        Utils.checkTrue(size >= 0, "Invalid size");
        Utils.checkTrue(name.length() <= Short.MAX_VALUE, "Invalid name length");
    }

    @Override
    public boolean isRaw() {
        return false;
    }

    @Override
    public boolean isDir() {
        return true;
    }

    @Override
    public boolean isFid() {
        return false;
    }
}
