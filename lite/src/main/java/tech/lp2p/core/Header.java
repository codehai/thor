package tech.lp2p.core;

@SuppressWarnings("unused")
public interface Header {
    String USER_AGENT = "User-Agent";
    String CONTENT_TYPE = "Content-Type";
    String CONTENT_LENGTH = "Content-Length";
    String CONTENT_TITLE = "Content-Title";
    String CONTENT_DOWNLOAD = "Content-Download";

}
