package tech.lp2p.core;

import java.math.BigInteger;

public record Key(byte[] hash, byte[] target) {

    public Key {
        Utils.checkArgument(hash.length, Hash.LENGTH, "hash size must be 32");
    }

    public static Key convertKey(byte[] target) {
        return new Key(Hash.createHash(target), target);
    }

    public static BigInteger distance(Key a, Key b) {
        // SetBytes interprets buf as the bytes of a big-endian unsigned
        // integer, sets z to that value, and returns z.
        // big.NewInt(0).SetBytes(k3)

        return Hash.distance(a.hash(), b.hash());
    }
}
