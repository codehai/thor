package tech.lp2p.core;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.file.Files;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicReference;


public final class FileStore implements BlockStore, AutoCloseable {

    private final File directory;
    private final AtomicReference<Long> root = new AtomicReference<>(null);

    public FileStore(File directory) {
        this.directory = directory;
        Utils.checkTrue(directory.exists(), "FileStore directory does not exists.");
    }

    public FileStore() throws IOException {
        directory = Files.createTempDirectory("").toFile();
    }

    public static File getTempDirectory() throws IOException {
        String property = System.getProperty("java.io.tmpdir");
        Objects.requireNonNull(property);
        File temp = new File(property);
        if (!temp.exists()) {
            throw new IOException("Tmpdir does not exists");
        }
        return temp;
    }

    public static void deleteDirectory(File dir) {
        File[] files = dir.listFiles();
        if (files != null) {
            for (File file : files) {
                if (file.isDirectory()) {
                    deleteDirectory(file);
                } else {
                    boolean result = file.delete();
                    if (Utils.DEBUG) {
                        Utils.debug(file.getName() + " deleting " + result);
                    }
                }
            }
        }
    }

    public void root(long cid) {
        root.set(cid);
    }

    public Long root() {
        return root.get();
    }

    @Override
    public void close() {
        deleteDirectory(directory);
        boolean result = directory.delete();
        if (Utils.DEBUG) {
            Utils.debug(directory.getName() + " deleting " + result);
        }
    }

    @Override
    public boolean hasBlock(long cid) {
        return getFile(cid).exists();
    }


    @Override
    public ByteBuffer getBlock(long cid) throws IOException {
        File file = getFile(cid);
        if (file.exists()) {
            ByteBuffer buffer = ByteBuffer.allocateDirect((int) file.length());
            try (InputStream inputStream = new FileInputStream(file)) {
                Channels.newChannel(inputStream).read(buffer);
                buffer.rewind();
                return buffer;
            }
        }
        return null;
    }

    @Override
    public void storeBlock(long cid, ByteBuffer data) throws IOException {
        File file = getFile(cid);
        if (!file.exists()) {
            try (OutputStream outputStream = new FileOutputStream(file)) {
                Channels.newChannel(outputStream).write(data);
            }
        }
    }

    private File getFile(long cid) {
        return new File(directory, Long.toHexString(cid));
    }

    @Override
    public void deleteBlock(long cid) {
        File file = getFile(cid);
        if (file.exists()) {
            boolean result = file.delete();
            if (Utils.DEBUG) {
                Utils.debug(file.getName() + " deleting " + result);
            }
        }
    }

}
