package tech.lp2p.core;

public record Root(Peeraddr peeraddr, long cid) {
}
