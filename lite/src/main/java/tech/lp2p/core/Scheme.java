package tech.lp2p.core;


@SuppressWarnings("unused")
public enum Scheme {
    https, http, pns, ipfs, magnet, file, about
}
