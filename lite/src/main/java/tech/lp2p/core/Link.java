package tech.lp2p.core;

public interface Link {
    boolean isRaw();

    long size();

    String name();

    long cid();

    boolean isDir();

    boolean isFid();
}
