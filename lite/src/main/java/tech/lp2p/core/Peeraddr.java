package tech.lp2p.core;


import com.google.protobuf.CodedInputStream;

import java.io.ByteArrayOutputStream;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Base64;
import java.util.Objects;
import java.util.Optional;


public record Peeraddr(PeerId peerId, byte[] address, int port) {


    public static Peeraddr loopbackPeeraddr(PeerId peerId, int port) {
        InetAddress inetAddress = InetAddress.getLoopbackAddress();
        return new Peeraddr(peerId, inetAddress.getAddress(), port);
    }


    public static Peeraddr create(PeerId peerId, byte[] raw) {
        return Objects.requireNonNull(Multiaddr.parseAddress(
                CodedInputStream.newInstance(raw), peerId), "Not supported peeraddr");
    }

    public static Peeraddr create(PeerId peerId, InetSocketAddress inetSocketAddress) {
        InetAddress inetAddress = inetSocketAddress.getAddress();
        return new Peeraddr(peerId, inetAddress.getAddress(), inetSocketAddress.getPort());
    }

    public static Optional<Peeraddr> extractPeeraddr(URI uri) {
        Optional<PeerId> peerId = PeerId.extractPeerId(uri);
        if (peerId.isPresent()) {
            String userInfo = uri.getUserInfo();
            if (userInfo != null && !userInfo.isBlank()) {
                try {
                    byte[] raw = Base64.getDecoder().decode(userInfo);
                    return Optional.of(Peeraddr.create(peerId.get(), raw));
                } catch (Throwable throwable) {
                    Utils.error(throwable);
                }
            }
        }
        return Optional.empty();
    }

    public byte[] encoded() {

        try (ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream()) {
            InetAddress inetAddress = InetAddress.getByAddress(address);
            if (inetAddress instanceof Inet4Address) {
                Multiaddr.encodeProtocol(Multiaddr.IP4, byteArrayOutputStream);
            } else {
                Multiaddr.encodeProtocol(Multiaddr.IP6, byteArrayOutputStream);
            }
            Multiaddr.encodePart(address, byteArrayOutputStream);
            Multiaddr.encodeProtocol(Multiaddr.UDP, byteArrayOutputStream);
            Multiaddr.encodePart(port, byteArrayOutputStream);

            return byteArrayOutputStream.toByteArray();
        } catch (Throwable throwable) {
            throw new IllegalStateException(throwable);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Peeraddr peeraddr = (Peeraddr) o;
        return port == peeraddr.port && Objects.equals(peerId, peeraddr.peerId)
                && Objects.deepEquals(address, peeraddr.address);
    }

    public URI toUri() throws URISyntaxException {
        return new URI(Scheme.pns.name(),
                Base64.getEncoder().encodeToString(encoded()),
                peerId.encode36(), -1,
                null, null, null);
    }

}
