package tech.lp2p.core;


import java.math.BigInteger;
import java.net.URI;
import java.nio.ByteBuffer;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.Objects;
import java.util.Optional;

import tech.lp2p.cert.ASN1OctetString;
import tech.lp2p.cert.ASN1Primitive;
import tech.lp2p.cert.DEROctetString;
import tech.lp2p.cert.DERSequence;
import tech.lp2p.cert.DLSequence;
import tech.lp2p.crypto.Ed25519Verify;

// hash is always (32 bit) and it is a Ed25519 public key
public record PeerId(byte[] hash) implements Hash {
    private static final byte[] Ed25519_PREFIX = new byte[]{8, 1, 18, 32};
    private static final byte[] Ed25519_ID_PREFIX = new byte[]{0, 36, 8, 1, 18, 32};
    private static final int SHA2_256 = 0x12;

    public PeerId {
        Objects.requireNonNull(hash, "hash can not be null");
        Utils.checkArgument(hash.length, Hash.LENGTH, "hash size must be 32");
    }

    public static Optional<PeerId> parse(byte[] raw) {
        try {
            if (prefixArraysEquals(Ed25519_ID_PREFIX, raw)) {
                PeerId peerId = new PeerId(
                        Arrays.copyOfRange(raw, Ed25519_ID_PREFIX.length, raw.length));
                return Optional.of(peerId);
            }
        } catch (Throwable throwable) {
            Utils.error(throwable);
        }
        return Optional.empty();
    }

    public static PeerId extractPeerId(X509Certificate cert) {
        try {
            byte[] extension = cert.getExtensionValue(Certificate.getLiteExtension());
            Objects.requireNonNull(extension);

            ASN1OctetString octs = (ASN1OctetString) ASN1Primitive.fromByteArray(extension);
            ASN1Primitive primitive = ASN1Primitive.fromByteArray(octs.getOctets());
            DLSequence sequence = (DLSequence) DERSequence.getInstance(primitive);
            DEROctetString pubKeyRaw = (DEROctetString)
                    Objects.requireNonNull(sequence).getObjectAt(0);

            return PeerId.identify(pubKeyRaw.getOctets());
        } catch (Throwable throwable) {
            throw new IllegalStateException(throwable);
        }
    }

    public static PeerId decodeName(String name) throws Exception {

        byte[] raw = Base58.decode(name);
        if (name.startsWith("1")) {
            Optional<PeerId> optional = parse(raw);
            if (optional.isPresent()) {
                return optional.get();
            }
        }

        if (name.startsWith("Qm")) {  // TODO [medium] only support Multihash.ID)
            ByteBuffer buffer = ByteBuffer.wrap(raw);

            int type = Utils.readUnsignedVariant(buffer);
            int len = Utils.readUnsignedVariant(buffer);

            byte[] hash = new byte[len];
            buffer.get(hash);
            Utils.checkTrue(!buffer.hasRemaining(), "still data available");
            Utils.checkTrue(type == SHA2_256, "invalid type");

            return new PeerId(hash);
        }

        throw new IllegalStateException("not supported multihash");
    }

    public static byte[] multihash(PeerId peerId) {
        return Utils.concat(Ed25519_ID_PREFIX, peerId.hash);
    }

    public static byte[] identify(PeerId peerId) {
        return Utils.concat(Ed25519_PREFIX, peerId.hash);
    }

    public static PeerId identify(byte[] raw) {
        if (prefixArraysEquals(Ed25519_PREFIX, raw)) {
            return new PeerId(Arrays.copyOfRange(raw, Ed25519_PREFIX.length, raw.length));
        }
        throw new IllegalStateException("Only Ed25519 expected");
    }

    public static boolean prefixArraysEquals(byte[] prefix, byte[] raw) {
        Utils.checkTrue(prefix.length < raw.length, "Prefix not smaller");
        for (int i = 0; i < prefix.length; i++) {
            if (prefix[i] != raw[i]) {
                return false;
            }
        }
        return true;
    }

    public static Optional<PeerId> extractPeerId(URI uri) {
        Objects.requireNonNull(uri.getScheme(), "Scheme not defined");
        Utils.checkTrue(Objects.equals(uri.getScheme(), Scheme.pns.name()),
                "Scheme not pns");
        String host = uri.getHost();
        if (host != null && !host.isBlank()) {
            try {
                return Optional.of(decode36(host));
            } catch (Throwable throwable) {
                Utils.error(throwable);
            }
        }
        return Optional.empty();
    }


    private static PeerId decode36(String data) {
        ByteBuffer wrap = ByteBuffer.wrap(new BigInteger(data, 36).toByteArray());
        int version = wrap.get();
        Utils.checkTrue(version == VERSION, "Invalid version");
        byte[] hash = new byte[Hash.LENGTH];
        wrap.get(hash);
        Utils.checkTrue(!wrap.hasRemaining(), "Still data available");
        return new PeerId(hash);
    }

    // https://docs.ipfs.tech/concepts/content-addressing/#cid-inspector
    // The default for CIDv1 is the case-insensitive base32, but use of the shorter base36 is
    // encouraged for IPNS names to ensure same text representation on subdomains.
    //

    public Key createKey() {
        return Key.convertKey(hash());
    }

    public void verify(byte[] data, byte[] signature) throws Exception {
        Ed25519Verify verifier = new Ed25519Verify(hash());
        verifier.verify(signature, data);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PeerId peer = (PeerId) o;
        return Arrays.equals(hash, peer.hash);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(hash()); // ok, checked, maybe opt
    }

    public String toBase58() {
        return Base58.encode(multihash(this));
    }

    public URI toUri() {
        return URI.create(Scheme.pns + "://" + encode36());
    }

    String encode36() {
        ByteBuffer allocate = ByteBuffer.allocate(1 + Hash.LENGTH);
        allocate.put(VERSION);
        allocate.put(hash());
        return new BigInteger(1, allocate.array()).toString(36);
    }
}
