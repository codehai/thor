package tech.lp2p.core;


import java.util.List;

public sealed interface Node extends Link permits Raw, Fid, Dir {
    String UNDEFINED_RAW_NAME = "";
    long UNDEFINED_RAW_SIZE = -1;
    byte[] UNDEFINED_DATA = new byte[0];
    List<Link> UNDEFINED_LINKS = List.of();

    byte[] data();

    List<Link> links();
}
