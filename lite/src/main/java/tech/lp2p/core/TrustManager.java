package tech.lp2p.core;

import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Objects;
import java.util.Set;

import javax.net.ssl.X509TrustManager;

import tech.lp2p.cert.ASN1OctetString;
import tech.lp2p.cert.ASN1Primitive;
import tech.lp2p.cert.DEROctetString;
import tech.lp2p.cert.DERSequence;
import tech.lp2p.cert.DLSequence;

public final class TrustManager implements X509TrustManager {
    private static final String TLS_HANDSHAKE = "libp2p-tls-handshake:";

    private static void verifyCertificate(X509Certificate cert) throws Exception {

        byte[] extension = cert.getExtensionValue(Certificate.getLiteExtension());
        Objects.requireNonNull(extension);

        ASN1OctetString octs = (ASN1OctetString) ASN1Primitive.fromByteArray(extension);
        ASN1Primitive primitive = ASN1Primitive.fromByteArray(octs.getOctets());
        DLSequence sequence = (DLSequence) DERSequence.getInstance(primitive);
        DEROctetString pubKeyRaw = (DEROctetString)
                Objects.requireNonNull(sequence).getObjectAt(0);

        PeerId peerId = PeerId.identify(pubKeyRaw.getOctets());

        DEROctetString signature = (DEROctetString) sequence.getObjectAt(1);
        byte[] skSignature = signature.getOctets();

        byte[] certKeyPub = cert.getPublicKey().getEncoded();

        byte[] verify = Utils.concat(TLS_HANDSHAKE.getBytes(), certKeyPub);

        peerId.verify(verify, skSignature);
    }

    private static void validCertificate(X509Certificate cert) throws Exception {
        // Checks that the certificate is currently valid. It is if
        // the current date and time are within the validity period given in the
        // certificate.
        //
        // The validity period consists of two date/time values:
        // the first and last dates (and times) on which the certificate
        // is valid.
        cert.checkValidity();


        // check if the certificate’s self-signature is valid (verified)
        // Verifies that this certificate was signed using the
        // private key that corresponds to the specified public key.
        cert.verify(cert.getPublicKey());

        boolean found = false;
        Set<String> critical = cert.getCriticalExtensionOIDs();
        if (critical.contains(Certificate.getLiteExtension())) {
            found = true;
            Utils.checkTrue(critical.size() == 1, "unknown critical extensions");
        } else {
            Utils.checkTrue(critical.isEmpty(), "unknown critical extensions");
        }

        if (!found) {
            Set<String> nonCritical = cert.getNonCriticalExtensionOIDs();
            if (nonCritical.contains(Certificate.getLiteExtension())) {
                found = true;
            }
        }
        Utils.checkTrue(found, "libp2p Public Key Extension is missing");

        // Certificates MUST omit the deprecated subjectUniqueId and issuerUniqueId fields.
        // Endpoints MAY abort the connection attempt if either is present.

        // Could be done, but left out is not required
        // Utils.error(TAG, Arrays.toString(cert.getSubjectUniqueID()));

        // Could be done, but left out is not required
        // Utils.error(TAG, Arrays.toString( cert.getIssuerUniqueID()));

    }

    @Override
    public void checkClientTrusted(X509Certificate[] chain, String authType)
            throws CertificateException {
        if (chain.length != 1) {
            throw new CertificateException("only one certificate allowed");
        }
        try {
            // here the expected peerId is null (because it is simply not known)
            // just check if the extracted peerId is not null
            for (X509Certificate cert : chain) {
                validCertificate(cert);
                verifyCertificate(cert);
            }
        } catch (Throwable throwable) {
            Utils.error(throwable);
            throw new CertificateException(throwable);
        }
    }

    @Override
    public void checkServerTrusted(X509Certificate[] chain, String authType)
            throws CertificateException {
        if (chain.length != 1) {
            throw new CertificateException("only one certificate allowed");
        }
        try {
            // now check the extracted peer ID with the expected value
            for (X509Certificate cert : chain) {
                validCertificate(cert);
                verifyCertificate(cert);
            }
        } catch (Throwable throwable) {
            Utils.error(throwable);
            throw new CertificateException(throwable);
        }
    }

    @Override
    public X509Certificate[] getAcceptedIssuers() {
        return Utils.CERTIFICATES_EMPTY;
    }
}
