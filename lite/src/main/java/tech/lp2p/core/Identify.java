package tech.lp2p.core;

import java.util.Arrays;

public record Identify(PeerId peerId, String agent, Peeraddr[] peeraddrs, String[] protocols) {

    @Override
    public String toString() {
        return "Identify{" +
                "peerId=" + peerId +
                ", agent='" + agent + '\'' +
                ", peeraddrs=" + Arrays.toString(peeraddrs) +
                ", protocols=" + Arrays.toString(protocols) +
                '}';
    }
}
