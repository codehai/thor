package tech.lp2p.core;

import java.io.IOException;
import java.nio.channels.WritableByteChannel;
import java.util.function.Consumer;


public interface Channel {

    void transferTo(WritableByteChannel channel, Consumer<Integer> read) throws IOException;

    void transferTo(WritableByteChannel channel) throws IOException;
}
