package tech.lp2p.core;

import java.net.URI;
import java.nio.ByteBuffer;
import java.util.Base64;
import java.util.Optional;
import java.util.zip.CRC32;

import tech.lp2p.quic.VariableLengthInteger;


public interface Cid {

    static long createCid(byte[] data) {
        Utils.checkTrue(data.length == Long.BYTES, "Invalid data");
        return VariableLengthInteger.bytesToLong(data);
    }

    static Optional<Long> extractCid(URI uri) {
        String fragment = uri.getFragment();
        if (fragment != null && !fragment.isBlank()) {
            long cid = Cid.decode(fragment);
            return Optional.of(cid);
        }
        return Optional.empty();
    }

    // BASE 64
    static String encode(long cid) {
        return Base64.getEncoder().encodeToString(Cid.hash(cid));
    }

    // BASE 64
    static long decode(String data) {
        return createCid(Base64.getDecoder().decode(data));
    }

    static long createHash(ByteBuffer bytes) {
        CRC32 crc32 = new CRC32();
        crc32.update(bytes);
        bytes.flip();
        return crc32.getValue();
    }

    static byte[] hash(long cid) {
        return VariableLengthInteger.numToBytes(cid, Long.BYTES);
    }

}