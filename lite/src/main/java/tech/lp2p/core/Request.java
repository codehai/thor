package tech.lp2p.core;


import java.util.Objects;

public record Request(PeerId peerId, short protocol, byte[] body) {
    public Request {
        Objects.requireNonNull(peerId);
        Objects.requireNonNull(body);
        Utils.checkTrue(protocol >= 0, "Invalid protocol");
    }
}
