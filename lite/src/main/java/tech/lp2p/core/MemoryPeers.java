package tech.lp2p.core;

import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public final class MemoryPeers implements PeerStore {
    private final Set<Peeraddr> peers = ConcurrentHashMap.newKeySet();

    @Override
    public List<Peeraddr> peeraddrs(int limit) {
        return peers.stream().limit(limit).collect(Collectors.toList());
    }

    @Override
    public void storePeeraddr(Peeraddr peeraddr) {
        peers.add(peeraddr);
    }

    @Override
    public void removePeeraddr(Peeraddr peeraddr) {
        peers.remove(peeraddr);
    }
}
