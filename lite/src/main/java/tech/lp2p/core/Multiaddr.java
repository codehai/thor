package tech.lp2p.core;

import com.google.protobuf.CodedInputStream;

import java.io.IOException;
import java.io.OutputStream;

// https://github.com/multiformats/multicodec/blob/master/table.csv (see multiaddr)
interface Multiaddr {
    int IP4 = 4;
    int IP6 = 41;
    int UDP = 273;
    int QUICV1 = 461;
    int HTTP = 480;

    static int size(int code) {
        return switch (code) {
            case IP4 -> 32;
            case IP6 -> 128;
            case UDP -> 16;
            case QUICV1, HTTP -> 0;
            default -> -1;
        };
    }

    private static void putUvarint(byte[] buf, long x) {
        int i = 0;
        while (x >= 0x80) {
            buf[i] = (byte) (x | 0x80);
            x >>= 7;
            i++;
        }
        buf[i] = (byte) x;
    }


    static void encodeProtocol(int code, OutputStream out) throws IOException {
        byte[] varint = new byte[(32 - Integer.numberOfLeadingZeros(code) + 6) / 7];
        putUvarint(varint, code);
        out.write(varint);
    }

    static void encodePart(int port, OutputStream outputStream) throws IOException {
        outputStream.write(new byte[]{(byte) (port >> 8), (byte) port});
    }

    static void encodePart(byte[] address, OutputStream outputStream) throws IOException {
        outputStream.write(address);
    }

    static Peeraddr parseAddress(CodedInputStream in, PeerId peerId) {

        byte[] address = null;
        int port = 0;
        try {
            label:
            while (!in.isAtEnd()) {
                int code = in.readRawVarint32();

                if (Utils.DEBUG) {
                    // support for HTTP, till now just check if there any out there [not yet]
                    if (code == Multiaddr.HTTP) {
                        Utils.error("Multiaddr HTTP detected " + peerId.toBase58());
                        return null;
                    }
                }
                if (Multiaddr.size(code) == 0) {
                    continue;
                }

                Object part = readPart(code, in);
                switch (part) {
                    case null:
                        return null;
                    case byte[] inetAddress:
                        address = inetAddress;
                        break;
                    case Integer i:
                        port = i;
                        break;
                    default:
                        break label;
                }
            }
        } catch (Throwable throwable) {
            Utils.error(throwable); // should not occur
            return null;
        }

        // check if address has a port, when it is not a dnsAddr
        if (port > 0 && address != null) {
            return new Peeraddr(peerId, address, port);
        }
        return null;
    }

    private static Object readPart(int code, CodedInputStream in) {
        try {
            int sizeForAddress = sizeForAddress(code, in);
            switch (code) {
                case IP4, IP6 -> {
                    return in.readRawBytes(sizeForAddress);
                }
                case UDP -> {
                    int a = in.readRawByte() & 0xFF;
                    int b = in.readRawByte() & 0xFF;
                    return (a << 8) | b;
                }
            }
        } catch (Throwable ignore) {
        }
        return null;
    }

    private static int sizeForAddress(int code, CodedInputStream in) throws IOException {
        int size = size(code);
        if (size > 0)
            return size / 8;
        if (size == 0)
            return 0;
        return in.readRawVarint32();
    }
}
