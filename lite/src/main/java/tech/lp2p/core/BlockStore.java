package tech.lp2p.core;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import tech.lp2p.dag.DagChannel;
import tech.lp2p.dag.DagFetch;
import tech.lp2p.dag.DagService;
import tech.lp2p.dag.DagStream;

public interface BlockStore extends DagFetch {

    boolean hasBlock(long cid);

    ByteBuffer getBlock(long cid) throws IOException;

    void deleteBlock(long cid);

    void storeBlock(long cid, ByteBuffer data) throws IOException;

    void root(long cid);

    Long root();

    default Node info(long cid) throws IOException {
        return DagService.info(this, cid);
    }

    default boolean hasChild(Dir dir, String name) throws IOException {
        return DagService.hasChild(this, dir, name);
    }

    // Note: remove the cid block (add all links blocks recursively) from the blockStore
    default void delete(Node node) throws IOException {
        DagService.removeNode(this, node);
    }

    default Dir removeFromDirectory(Dir dir, Dir child) throws IOException {
        return DagService.removeFromDirectory(this, dir, child);
    }

    default Dir removeFromDirectory(Dir dir, Fid child) throws IOException {
        return DagService.removeFromDirectory(this, dir, child);
    }

    default Dir addToDirectory(Dir dir, Dir child) throws IOException {
        return DagService.addToDirectory(this, dir, child);
    }

    default Dir addToDirectory(Dir dir, Fid child) throws IOException {
        return DagService.addToDirectory(this, dir, child);
    }

    default Dir updateDirectory(Dir dir, Fid child) throws IOException {
        return DagService.updateDirectory(this, dir, child);
    }

    default Dir updateDirectory(Dir dir, Dir child) throws IOException {
        return DagService.updateDirectory(this, dir, child);
    }

    default Dir createDirectory(String name, List<Link> links) throws IOException {
        return DagService.createDirectory(this, name, links);
    }

    default Dir createEmptyDirectory(String name) throws IOException {
        return DagService.createEmptyDirectory(this, name);
    }

    default Dir renameDirectory(Dir dir, String name) throws IOException {
        return DagService.renameDirectory(this, dir, name);
    }

    // Note: data is limited to Lite.CHUNK_SIZE
    default Raw storeData(byte[] data) throws IOException {
        return DagService.createRaw(this, data);
    }

    // Note: data is limited to Lite.CHUNK_SIZE
    default Raw storeText(String data) throws IOException {
        return storeData(data.getBytes());
    }


    default Fid storeFile(File file) throws IOException {
        try (FileInputStream inputStream = new FileInputStream(file)) {
            return storeInputStream(file.getName(), inputStream);
        }
    }

    default void fetchToFile(File file, Fid fid) throws IOException {
        try (FileOutputStream fileOutputStream = new FileOutputStream(file)) {
            FileChannel fosChannel = fileOutputStream.getChannel();
            channel(fid).transferTo(fosChannel);
        }
    }

    default InputStream stream(Fid fid) {
        Objects.requireNonNull(fid);
        DagChannel dagChannel = DagChannel.create(this, fid);
        return new DagStream(dagChannel);
    }

    default Channel channel(Fid fid) {
        Objects.requireNonNull(fid);
        return DagChannel.create(this, fid);
    }

    default Fid storeInputStream(String name, InputStream inputStream) throws IOException {
        ReadableByteChannel channel = Channels.newChannel(inputStream);
        return DagService.storeChannel(channel, this, name);
    }


    default byte[] fetchData(long cid) throws IOException {
        Node node = DagService.dagNode(this, cid);
        Objects.requireNonNull(node);
        Utils.checkTrue(node.isRaw(), "Wrong node type");
        return node.data();
    }

    default String fetchText(long cid) throws IOException {
        return new String(fetchData(cid));
    }

    default Dir storeDirectory(File directory) throws IOException {
        List<Link> children = new ArrayList<>();
        File[] files = directory.listFiles();
        if (files != null) {
            for (File file : files) {
                if (file.isDirectory()) {
                    Dir child = storeDirectory(file);
                    children.add(child);
                } else {
                    Fid fid = storeFile(file);
                    children.add(fid);
                }
            }
        }
        return createDirectory(directory.getName(), children);
    }
}


