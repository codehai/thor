package tech.lp2p.core;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class Protocols extends HashMap<Protocol, Handler> {

    public Set<String> names() {
        Set<String> names = new HashSet<>();
        for (Protocol protocol : keySet()) {
            names.add(protocol.name());
        }
        return names;
    }
}
