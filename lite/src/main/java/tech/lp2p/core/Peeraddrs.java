package tech.lp2p.core;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

public final class Peeraddrs extends ArrayList<Peeraddr> {
    private static final Peeraddrs EMPTY = new Peeraddrs(0);
    private static final int REACHABLE_TIMEOUT = 250; // in ms

    public Peeraddrs() {
        super();
    }

    public Peeraddrs(int size) {
        super(size);
    }

    public static Peeraddrs create(PeerId peerId, List<ByteString> byteStrings) {
        Peeraddrs peeraddrs = new Peeraddrs();
        for (ByteString entry : byteStrings) {
            Peeraddr peeraddr = Multiaddr.parseAddress(
                    CodedInputStream.newInstance(entry.toByteArray()), peerId);
            if (peeraddr != null) {
                peeraddrs.add(peeraddr);
            }
        }
        return peeraddrs;
    }


    public static Optional<Peeraddr> best(Peeraddrs peeraddrs) {

        if (peeraddrs.size() > 1) {
            removeLoopbacks(peeraddrs);
        }
        if (peeraddrs.size() > 1) {
            removeLinkLocals(peeraddrs);
        }
        if (peeraddrs.size() > 1) {
            removeSiteLocals(peeraddrs);
        }

        if (peeraddrs.size() == 1) {
            return Optional.of(peeraddrs.get(0));
        }

        for (Peeraddr resolvedAddr : peeraddrs) {
            if (isReachable(resolvedAddr)) { // only reachable
                return Optional.of(resolvedAddr);
            }
        }
        return Optional.empty();
    }


    public static Peeraddrs peeraddrs(PeerId peerId, int port) {
        Collection<InetAddress> inetSocketAddresses = Network.addresses();
        Peeraddrs result = new Peeraddrs();
        for (InetAddress inetAddress : inetSocketAddresses) {
            result.add(new Peeraddr(peerId, inetAddress.getAddress(), port));
        }
        return result;
    }


    public static Optional<Peeraddr> reduce(byte[] peerIdRaw, List<ByteString> byteStrings) {
        Optional<PeerId> peerId = PeerId.parse(peerIdRaw);
        if (peerId.isPresent()) {
            return reduce(peerId.get(), byteStrings);
        }
        return Optional.empty();
    }

    public static Optional<Peeraddr> reduce(PeerId peerId, List<ByteString> byteStrings) {

        Peeraddrs peeraddrs = Peeraddrs.create(peerId, byteStrings);

        if (peeraddrs.isEmpty()) {
            return Optional.empty();
        }

        if (peeraddrs.size() > 1) {
            removeLoopbacks(peeraddrs);
        }
        if (peeraddrs.size() > 1) {
            removeLinkLocals(peeraddrs);
        }
        if (peeraddrs.size() > 1) {
            removeSiteLocals(peeraddrs);
        }

        if (peeraddrs.size() == 1) {
            return Optional.of(peeraddrs.get(0));
        }

        for (Peeraddr resolvedAddr : peeraddrs) {
            if (isReachable(resolvedAddr)) { // only reachable
                return Optional.of(resolvedAddr);
            }
        }

        return Optional.empty();
    }


    private static void removeLoopbacks(Peeraddrs peeraddrs) {

        Iterator<Peeraddr> iterator = peeraddrs.iterator();

        while (iterator.hasNext()) {
            Peeraddr peeraddr = iterator.next();

            if (peeraddrs.size() == 1) {
                return;
            }
            try {
                InetAddress inetAddress = InetAddress.getByAddress(peeraddr.address());
                if (inetAddress.isLoopbackAddress()) {
                    iterator.remove();
                }
            } catch (Throwable throwable) {
                iterator.remove();
            }
        }
    }

    private static void removeLinkLocals(Peeraddrs peeraddrs) {

        Iterator<Peeraddr> iterator = peeraddrs.iterator();

        while (iterator.hasNext()) {
            Peeraddr peeraddr = iterator.next();

            if (peeraddrs.size() == 1) {
                return;
            }
            try {
                InetAddress inetAddress = InetAddress.getByAddress(peeraddr.address());
                if (inetAddress.isLinkLocalAddress()) {
                    iterator.remove();
                }
            } catch (Throwable throwable) {
                iterator.remove();
            }
        }

    }

    private static void removeSiteLocals(Peeraddrs peeraddrs) {

        Iterator<Peeraddr> iterator = peeraddrs.iterator();

        while (iterator.hasNext()) {
            Peeraddr peeraddr = iterator.next();

            if (peeraddrs.size() == 1) {
                return;
            }
            try {
                InetAddress inetAddress = InetAddress.getByAddress(peeraddr.address());
                if (inetAddress.isSiteLocalAddress()) {
                    iterator.remove();
                }
            } catch (Throwable throwable) {
                iterator.remove();
            }
        }

    }

    private static boolean isReachable(Peeraddr peeraddr) {
        try {
            InetAddress inetAddress = InetAddress.getByAddress(peeraddr.address());
            return inetAddress.isReachable(REACHABLE_TIMEOUT);
        } catch (Throwable throwable) {
            Utils.error(throwable); // should not occur
        }
        return false;
    }

    public static Peeraddrs empty() {
        return EMPTY;
    }
}
