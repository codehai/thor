package tech.lp2p.core;


public record Result(Peeraddr peeraddr, short status, byte[] content) {
    public static Result create(short status) {
        return new Result(null, status, Utils.BYTES_EMPTY);
    }
}
