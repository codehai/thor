package tech.lp2p.core;

public interface Status {

    short PROTOCOL_NEGOTIATION_FAILED = 100;
    short TIMEOUT = 130;
    short INTERRUPT = 140;

    // HTML codes
    short OK = 200;
    short BAD_REQUEST = 400;
    short NOT_FOUND = 404;
    short REQUEST_TIMEOUT = 408;
    short CLIENT_CLOSED_REQUEST = 499;
    short INTERNAL_ERROR = 500;
    short SERVICE_UNAVAILABLE = 503;
    short NOT_IMPLEMENTED = 501;

}
