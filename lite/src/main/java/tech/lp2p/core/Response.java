package tech.lp2p.core;


import java.nio.ByteBuffer;
import java.util.Objects;

public record Response(short status, ByteBuffer body) {

    public static Response create(short status, ByteBuffer body) {
        Objects.requireNonNull(body);
        return new Response(status, body);
    }

    public static Response create(short status) {
        return new Response(status, ByteBuffer.allocate(0));
    }

    public byte[] content() {
        ByteBuffer data = body();
        byte[] content = new byte[data.limit()];
        data.get(content);
        body.rewind(); // can read again
        return content;
    }

}
