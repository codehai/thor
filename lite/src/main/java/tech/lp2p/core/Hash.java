package tech.lp2p.core;

import java.math.BigInteger;
import java.security.MessageDigest;


// note Hash is always 32 bit width
public sealed interface Hash permits PeerId {
    int LENGTH = 32;
    byte VERSION = 2;

    static byte[] createHash(byte[] bytes) {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            return digest.digest(bytes);
        } catch (Throwable throwable) {
            throw new IllegalStateException(throwable);
        }
    }


    static BigInteger distance(byte[] a, byte[] b) {
        byte[] k3 = xor(a, b);

        // SetBytes interprets buf as the bytes of a big-endian unsigned
        // integer, sets z to that value, and returns z.
        // big.NewInt(0).SetBytes(k3)

        return new BigInteger(k3);
    }

    private static byte[] xor(byte[] x1, byte[] x2) {
        byte[] out = new byte[x1.length];

        for (int i = 0; i < x1.length; i++) {
            out[i] = (byte) (0xff & ((int) x1[i]) ^ ((int) x2[i]));
        }
        return out;
    }

    byte[] hash();
}
