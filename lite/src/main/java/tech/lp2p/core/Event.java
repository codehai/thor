package tech.lp2p.core;

public enum Event {
    OUTGOING_RESERVE_EVENT, INCOMING_CONNECT_EVENT
}
