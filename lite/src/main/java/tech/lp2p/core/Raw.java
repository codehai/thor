package tech.lp2p.core;

import java.util.List;
import java.util.Objects;

public record Raw(long cid, long size, String name, List<Link> links, byte[] data) implements Node {

    public Raw {
        Objects.requireNonNull(name);
        Objects.requireNonNull(data);
        Objects.requireNonNull(links);
        Utils.checkTrue(data.length <= Short.MAX_VALUE, "Invalid data size");
        Utils.checkTrue(size >= UNDEFINED_RAW_SIZE, "Invalid size");
        Utils.checkTrue(links.isEmpty(), "Raw have no links");
        Utils.checkTrue(name.isEmpty(), "Invalid name, must be empty");
    }

    @Override
    public boolean isRaw() {
        return true;
    }

    @Override
    public boolean isDir() {
        return false;
    }

    @Override
    public boolean isFid() {
        return false;
    }
}
