package tech.lp2p.dht;

import tech.lp2p.quic.Connection;

public interface DhtAcceptor {
    void consume(Connection connection);
}
