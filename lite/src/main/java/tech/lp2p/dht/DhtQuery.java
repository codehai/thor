package tech.lp2p.dht;

import java.net.ConnectException;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeoutException;

import tech.lp2p.Lite;
import tech.lp2p.core.Utils;


interface DhtQuery {

    static void runQuery(ExecutorService service, Lite host,
                         DhtPeers dhtPeers, DhtService.QueryFunc queryFn) {
        iteration(service, host, dhtPeers, queryFn);
    }


    static void iteration(ExecutorService service, Lite host,
                          DhtPeers dhtPeers, DhtService.QueryFunc queryFn) {
        if (service.isShutdown()) {
            return;
        }

        List<DhtPeer> nextPeersToQuery = dhtPeers.nextPeers();

        for (DhtPeer dhtPeer : nextPeersToQuery) {

            dhtPeer.start();

            service.execute(() -> {

                try {
                    queryFn.query(dhtPeers, dhtPeer);
                    dhtPeer.done();
                    iteration(service, host, dhtPeers, queryFn);
                } catch (TimeoutException | ConnectException exception) {
                    dhtPeer.done();
                    DhtService.removeFromPeerStore(host, dhtPeer);
                } catch (InterruptedException ignore) {
                    dhtPeer.done();
                } catch (Throwable throwable) {
                    Utils.error(throwable); // not expected (probably an internal issue)
                    dhtPeer.done();
                }
            });
        }
    }


}
