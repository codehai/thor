package tech.lp2p.dht;


import com.google.protobuf.ByteString;

import java.net.ConnectException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeoutException;

import tech.lp2p.Lite;
import tech.lp2p.core.Key;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Peeraddrs;
import tech.lp2p.core.Protocol;
import tech.lp2p.core.Utils;
import tech.lp2p.lite.LiteService;
import tech.lp2p.proto.Dht;
import tech.lp2p.quic.Connection;
import tech.lp2p.quic.ConnectionBuilder;
import tech.lp2p.quic.Requester;

public interface DhtService {

    static void addToPeerStore(Lite host, DhtPeer dhtPeer) {
        if (dhtPeer.replaceable()) {
            host.peerStore().storePeeraddr(dhtPeer.peeraddr());
        }
    }

    static void removeFromPeerStore(Lite host, DhtPeer peer) {
        host.peerStore().removePeeraddr(peer.peeraddr());
    }

    private static List<DhtPeer> evalClosestPeers(Dht.Message pms, DhtPeers peers, Key key) {

        List<DhtPeer> dhtPeers = new ArrayList<>();
        for (Dht.Message.Peer entry : pms.getCloserPeersList()) {
            if (entry.getAddrsCount() > 0) {
                Optional<Peeraddr> reduced = Peeraddrs.reduce(
                        entry.getId().toByteArray(), entry.getAddrsList());

                if (reduced.isPresent()) {
                    DhtPeer dhtPeer = DhtPeer.create(reduced.get(), true, key);
                    boolean result = peers.enhance(dhtPeer);
                    if (result) {
                        dhtPeers.add(dhtPeer);
                    }
                }
            }
        }
        return dhtPeers;
    }


    private static List<DhtPeer> bootstrap(Lite host, Key key) {
        List<DhtPeer> peers = new ArrayList<>();

        Peeraddrs peeraddrs = host.bootstrap();
        for (Peeraddr peeraddr : peeraddrs) {
            peers.add(DhtPeer.create(peeraddr, false, key));
        }

        List<Peeraddr> stored = host.peerStore().peeraddrs(Lite.DHT_ALPHA);

        for (Peeraddr peeraddr : stored) {
            peers.add(DhtPeer.create(peeraddr, true, key));
        }

        return peers;
    }


    static void findClosestPeers(ExecutorService service, Lite host, Key key, DhtAcceptor dhtAcceptor) {

        Dht.Message message = DhtService.createFindNodeMessage(key);
        runQuery(service, host, key, (peers, peer) -> {

            if (service.isShutdown()) {
                return;
            }
            Dht.Message pms = request(service, host, peer, message, dhtAcceptor);

            List<DhtPeer> res = evalClosestPeers(pms, peers, key);
            if (!res.isEmpty()) {
                DhtService.addToPeerStore(host, peer);
            }
        });
    }


    private static Dht.Message request(ExecutorService service, Lite host, DhtPeer dhtPeer,
                                       Dht.Message message, DhtAcceptor dhtAcceptor)
            throws InterruptedException, ConnectException, TimeoutException {
        Connection connection = ConnectionBuilder.connect(host, dhtPeer.peeraddr());
        Dht.Message msg = DhtService.request(connection, message);

        if (dhtPeer.replaceable()) {
            if (!service.isShutdown()) {
                service.execute(() -> {
                    try {
                        dhtAcceptor.consume(connection);
                    } catch (Throwable throwable) {
                        Utils.error(throwable);
                    }
                });
            }
        }
        return msg;

    }


    private static DhtPeers initialPeers(Lite host, Key key) {

        // now get the best result to limit 'Lite.DHT_ALPHA'
        DhtPeers dhtPeers = new DhtPeers(Lite.DHT_ALPHA);
        List<DhtPeer> pds = bootstrap(host, key);

        for (DhtPeer dhtPeer : pds) {
            dhtPeers.fill(dhtPeer);
        }
        return dhtPeers;
    }

    private static void runQuery(ExecutorService service, Lite host, Key key, QueryFunc queryFn) {
        DhtQuery.runQuery(service, host, initialPeers(host, key), queryFn);
    }

    static Dht.Message createFindNodeMessage(Key key) {
        return Dht.Message.newBuilder()
                .setType(Dht.Message.MessageType.FIND_NODE)
                .setKey(ByteString.copyFrom(key.target()))
                .setClusterLevelRaw(0).build();
    }

    static Dht.Message request(Connection connection, Dht.Message message)
            throws InterruptedException, TimeoutException, ConnectException {

        ByteBuffer data = Requester.createStream(connection)
                .request(Lite.TIMEOUT, Utils.encode(message, Protocol.MULTISTREAM_PROTOCOL,
                        Protocol.DHT_PROTOCOL));

        try {
            byte[] response = LiteService.receiveResponse(data);
            return Dht.Message.parseFrom(response);
        } catch (Throwable throwable) {
            throw new ConnectException(throwable.getMessage()); // response is shit
        }

    }

    interface QueryFunc {
        void query(DhtPeers peers, DhtPeer peer)
                throws InterruptedException, ConnectException, TimeoutException;
    }

}
