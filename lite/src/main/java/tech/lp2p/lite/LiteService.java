package tech.lp2p.lite;


import java.nio.ByteBuffer;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

import tech.lp2p.Lite;
import tech.lp2p.core.Event;
import tech.lp2p.core.Key;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Peeraddrs;
import tech.lp2p.core.Utils;
import tech.lp2p.dht.DhtService;
import tech.lp2p.quic.Connection;
import tech.lp2p.quic.StreamState;
import tech.lp2p.relay.RelayService;

public interface LiteService {


    static Optional<Peeraddr> hopConnect(Lite host, PeerId target) {

        AtomicReference<Peeraddr> done = new AtomicReference<>();

        Key key = target.createKey();

        Set<PeerId> handled = ConcurrentHashMap.newKeySet();
        long time = System.currentTimeMillis();
        ExecutorService service = Executors.newFixedThreadPool(Lite.DHT_ALPHA);
        try {
            DhtService.findClosestPeers(service, host, key, connection -> {
                if (service.isShutdown()) {
                    return;
                }
                try {
                    if (handled.add(connection.remotePeerId())) {

                        RelayService.RelayInfo relayInfo =
                                RelayService.relayPunchInfo(host, target);
                        Peeraddr resolved = RelayService.hopConnect(connection, relayInfo);

                        done.set(resolved);
                        service.shutdown();
                        service.shutdownNow();
                    }
                } catch (Throwable throwable) {
                    Utils.debug("RelayDialer hopConnect failure " + throwable.getMessage());
                } finally {
                    connection.close();
                }
            });

            if (!service.awaitTermination(Lite.RESOLVE_TIMEOUT, TimeUnit.SECONDS)) {
                service.shutdown();
                service.shutdownNow();
            }
            Utils.error("hopConnect done " + (System.currentTimeMillis() - time));
        } catch (Throwable throwable) {
            Utils.error(throwable);
            service.shutdown();
            service.shutdownNow();
        }
        return Optional.ofNullable(done.get());
    }

    static void hopReserve(Lite host, int maxReservation, int timeout) {

        Set<PeerId> handledRelays = ConcurrentHashMap.newKeySet();

        // check if reservations are still valid and not expired
        for (Connection connection : host.connector().connections()) {
            if (connection.isMarked()) {
                handledRelays.add(connection.remotePeerId());  // still valid
            }
        }

        AtomicInteger valid = new AtomicInteger(handledRelays.size());

        long time = System.currentTimeMillis();
        ExecutorService service = Executors.newFixedThreadPool(Lite.DHT_ALPHA);
        try {
            Key key = host.peerId().createKey();
            // fill up reservations [not yet enough]
            DhtService.findClosestPeers(service, host, key, connection -> {

                // handled relays with given peerId
                if (!handledRelays.add(connection.remotePeerId())) {
                    return;
                }

                if (valid.get() > maxReservation) {
                    // no more reservations
                    return; // just return, let the refresh mechanism finished
                }

                if (service.isShutdown()) {
                    return;
                }

                try {
                    hopReserve(host, connection);

                    if (valid.incrementAndGet() > maxReservation) {
                        // done
                        service.shutdown();
                        service.shutdownNow();
                    }
                } catch (Exception e) {
                    Utils.debug("Exception hopReserve " + e.getMessage());
                }
            });


            if (!service.awaitTermination(timeout, TimeUnit.SECONDS)) {
                service.shutdown();
                service.shutdownNow();
            }
            Utils.error("hopReserve done " + (System.currentTimeMillis() - time));
        } catch (Throwable throwable) {
            Utils.error(throwable);
            service.shutdown();
            service.shutdownNow();
        }
    }


    static void hopReserve(Lite host, Connection connection) throws Exception {
        connection.enableKeepAlive();
        try {
            RelayService.hopReserve(connection, host.peerId());
            connection.mark();
            host.eventConsumer().accept(Event.OUTGOING_RESERVE_EVENT);
        } catch (Throwable throwable) {
            connection.close();
            throw throwable;
        }
    }

    static Peeraddrs hopReserves(Lite host) {
        Peeraddrs peeraddrs = new Peeraddrs();
        for (Connection connection : host.connector().connections()) {
            if (connection.isMarked()) {
                peeraddrs.add(Peeraddr.create(connection.remotePeerId(), connection.remoteAddress()));
            }
        }
        return peeraddrs;
    }

    static byte[] receiveResponse(ByteBuffer data) {
        return transform(data);
    }

    private static byte[] transform(ByteBuffer bytes) {
        if (bytes.remaining() == 0) {
            return Utils.BYTES_EMPTY;
        }
        byte[] frame = StreamState.unsignedVarintReader(bytes);

        if (frame.length == 0) {
            return Utils.BYTES_EMPTY;
        } else {
            bytes.get(frame);

            if (!StreamState.isProtocol(ByteBuffer.wrap(frame))) {
                return frame;
            }

            return transform(bytes);
        }
    }
}
