package tech.lp2p;


import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;

import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.util.List;

import tech.lp2p.core.Dir;
import tech.lp2p.core.Fid;
import tech.lp2p.core.FileStore;
import tech.lp2p.core.Link;
import tech.lp2p.core.Node;
import tech.lp2p.core.Raw;
import tech.lp2p.dag.DagChannel;

public class AddTest {


    @Test(expected = Exception.class)
    public void add_and_remove() throws Exception {

        try (FileStore blockStore = new FileStore()) {

            String content = "Hallo dfsadf";
            Raw raw = blockStore.storeText(content);
            long text = raw.cid();

            assertTrue(blockStore.hasBlock(text));
            blockStore.delete(raw);
            assertFalse(blockStore.hasBlock(text));

            blockStore.fetchText(text); // closed exception expected
        }
    }

    @Test
    public void add_dir() throws Exception {
        try (FileStore blockStore = new FileStore()) {

            Dir dir = blockStore.createEmptyDirectory("Moin");
            assertNotNull(dir);
            Node node = blockStore.info(dir.cid());
            assertTrue(node instanceof Dir);
            Dir infoDir = (Dir) node;
            assertEquals(infoDir.name(), "Moin");
            assertEquals(node.name(), "Moin");

            dir = blockStore.renameDirectory(dir, "Hallo");
            assertNotNull(dir);
            node = blockStore.info(dir.cid());
            assertTrue(node instanceof Dir);
            infoDir = (Dir) node;
            assertEquals(infoDir.name(), "Hallo");
            assertEquals(node.name(), "Hallo");


            String content = "Hallo";
            Fid text = TestEnv.createContent(blockStore, "text.txt", content.getBytes());
            assertNotNull(text);


            try (InputStream stream = blockStore.stream(text)) {
                assertArrayEquals(content.getBytes(), stream.readAllBytes());
            }

            dir = blockStore.addToDirectory(dir, text);
            assertNotNull(dir);
            assertTrue(dir.size() > 0);

            boolean exists = blockStore.hasChild(dir, "text.txt");
            assertTrue(exists);


            exists = blockStore.hasChild(dir, "text2.txt");
            assertFalse(exists);

            List<Link> childs = dir.links();
            assertNotNull(childs);
            assertEquals(childs.size(), 1);

            dir = blockStore.removeFromDirectory(dir, text);
            assertNotNull(dir);

            childs = dir.links();
            assertNotNull(childs);
            assertEquals(childs.size(), 0);
        }

    }

    @Test
    public void update_dir() throws Exception {
        try (FileStore blockStore = new FileStore()) {

            Dir dir = blockStore.createEmptyDirectory("a");
            assertNotNull(dir);
            assertEquals(dir.size(), 0);

            String fileName = "test.txt";

            // TEST 1
            String content = "Hallo";
            Fid text = TestEnv.createContent(blockStore, fileName, content.getBytes());
            assertNotNull(text);

            dir = blockStore.addToDirectory(dir, text);
            assertNotNull(dir);
            assertEquals(dir.size(), content.length());

            List<Link> childs = dir.links();
            assertNotNull(childs);
            assertEquals(childs.size(), 1);
            assertEquals(childs.get(0).cid(), text.cid());

            // TEST 2
            String contentNew = "Hallo Moin";
            Fid cmp = TestEnv.createContent(blockStore, fileName, contentNew.getBytes());
            long textNew = cmp.cid();


            dir = blockStore.updateDirectory(dir, cmp);
            assertNotNull(dir);
            assertEquals(dir.size(), contentNew.length());

            childs = dir.links();
            assertNotNull(childs);
            assertEquals(childs.size(), 1);
            assertEquals(childs.get(0).cid(), textNew);
        }
    }

    @Test
    public void create_dir() throws Exception {

        try (FileStore blockStore = new FileStore()) {


            String content1 = "Hallo 1";
            Fid text1 = TestEnv.createContent(blockStore, "b.txt", content1.getBytes());
            assertNotNull(text1);

            String content2 = "Hallo 12";
            Fid text2 = TestEnv.createContent(blockStore, "a.txt", content2.getBytes());
            assertNotNull(text2);


            Dir dir = blockStore.createDirectory("zeit", List.of(text1, text2));
            assertNotNull(dir);

            List<Link> childs = dir.links();
            assertNotNull(childs);
            assertEquals(childs.size(), 2);

            assertEquals(childs.get(1).cid(), text2.cid());
        }
    }

    @Test
    public void add_wrap_test() throws Exception {
        try (FileStore blockStore = new FileStore()) {

            int packetSize = 1000;
            long maxData = 1000;
            File inputFile = TestEnv.createCacheFile();
            try (OutputStream outputStream = new FileOutputStream(inputFile)) {
                for (int i = 0; i < maxData; i++) {
                    byte[] randomBytes = TestEnv.getRandomBytes(packetSize);
                    outputStream.write(randomBytes);
                }
            }
            long size = inputFile.length();

            TestEnv.error("Bytes : " + inputFile.length() / 1000 + "[kb]");

            Fid fid = blockStore.storeFile(inputFile);
            assertNotNull(fid);

            List<Link> links = fid.links();
            assertNotNull(links);
            assertEquals(links.size(), 31);


            try (InputStream stream = blockStore.stream(fid)) {
                byte[] bytes = stream.readAllBytes();
                assertEquals(bytes.length, size);
            }

        }
    }

    @Test
    public void add_dir_test() throws Exception {
        try (FileStore blockStore = new FileStore()) {

            File inputFile = TestEnv.createCacheFile();
            assertTrue(inputFile.exists());
            try (OutputStream outputStream = new FileOutputStream(inputFile)) {
                for (int i = 0; i < 10; i++) {
                    byte[] randomBytes = TestEnv.getRandomBytes(1000);
                    outputStream.write(randomBytes);
                }
            }

            Fid fid = blockStore.storeFile(inputFile);
            assertNotNull(fid);

            List<Link> links = fid.links();
            assertNotNull(links);

            assertEquals(links.size(), 1);
        }
    }


    @Test
    public void add_test() throws Exception {

        int packetSize = 1000;
        long maxData = 1000;

        try (FileStore blockStore = new FileStore()) {

            File inputFile = TestEnv.createCacheFile();
            try (OutputStream outputStream = new FileOutputStream(inputFile)) {
                for (int i = 0; i < maxData; i++) {
                    byte[] randomBytes = TestEnv.getRandomBytes(packetSize);
                    outputStream.write(randomBytes);
                }
            }
            long size = inputFile.length();

            TestEnv.error("Bytes : " + inputFile.length() / 1000 + "[kb]");

            Fid fid = blockStore.storeFile(inputFile);
            assertNotNull(fid);

            List<Link> links = fid.links();
            assertNotNull(links);
            assertEquals(links.size(), 31);
            Link link = links.get(0);
            assertNotEquals(link.cid(), fid.cid());


            try (InputStream stream = blockStore.stream(fid)) {
                assertEquals(size, stream.readAllBytes().length);
            }
        }

    }


    @Test
    public void add_wrap_small_test() throws Exception {

        int packetSize = 200;
        long maxData = 1000;

        try (FileStore blockStore = new FileStore()) {

            File inputFile = TestEnv.createCacheFile();
            try (OutputStream outputStream = new FileOutputStream(inputFile)) {
                for (int i = 0; i < maxData; i++) {
                    byte[] randomBytes = TestEnv.getRandomBytes(packetSize);
                    outputStream.write(randomBytes);
                }
            }
            long size = inputFile.length();


            TestEnv.error("Bytes : " + inputFile.length() / 1000 + "[kb]");

            Fid fid = blockStore.storeFile(inputFile);
            assertNotNull(fid);

            List<Link> links = fid.links();
            assertNotNull(links);
            assertEquals(links.size(), 7);


            try (InputStream stream = blockStore.stream(fid)) {
                assertEquals(size, stream.readAllBytes().length);
            }
        }
    }

    @Test
    public void add_small_test() throws Exception {

        int packetSize = 200;
        long maxData = 1000;

        try (FileStore blockStore = new FileStore()) {

            File inputFile = TestEnv.createCacheFile();
            try (OutputStream outputStream = new FileOutputStream(inputFile)) {
                for (int i = 0; i < maxData; i++) {
                    byte[] randomBytes = TestEnv.getRandomBytes(packetSize);
                    outputStream.write(randomBytes);
                }
            }
            long size = inputFile.length();

            TestEnv.error("Bytes : " + inputFile.length() / 1000 + "[kb]");

            Fid fid = blockStore.storeFile(inputFile);
            assertNotNull(fid);

            List<Link> links = fid.links();
            assertNotNull(links);
            assertEquals(links.size(), 7);

            try (InputStream stream = blockStore.stream(fid)) {
                assertEquals(size, stream.readAllBytes().length);
            }
        }

    }


    @Test
    public void test_inputStream() throws Exception {

        try (FileStore blockStore = new FileStore()) {

            String text = "moin zehn";
            Raw cid = blockStore.storeText(text);
            assertTrue(blockStore.hasBlock(cid.cid()));

            byte[] bytes = blockStore.fetchData(cid.cid());
            assertNotNull(bytes);
            assertEquals(bytes.length, text.length());

            assertEquals(text, new String(cid.data()));

        }
    }


    @Test
    public void test_inputStreamBig() throws Exception {

        try (FileStore blockStore = new FileStore()) {
            byte[] text = TestEnv.getRandomBytes((Short.MAX_VALUE * 2) - 50);
            Fid fid = TestEnv.createContent(blockStore, "random.bin", text);

            try (InputStream stream = blockStore.stream(fid)) {
                assertArrayEquals(text, stream.readAllBytes());
            }
        }
    }

    @Test
    public void test_reader() throws Exception {

        try (FileStore blockStore = new FileStore()) {

            String text = "0123456789 jjjjjjjj";
            Fid fid = blockStore.storeInputStream("text.bin",
                    new ByteArrayInputStream(text.getBytes()));
            assertTrue(blockStore.hasBlock(fid.cid()));

            DagChannel dagChannel = DagChannel.create(blockStore, fid);


            dagChannel.seek(0);
            ByteBuffer buffer = dagChannel.loadNextData();
            assertNotNull(buffer);
            byte[] testData = new byte[buffer.limit()];
            buffer.get(testData);
            assertEquals(text, new String(testData));

            int pos = 11;
            dagChannel.seek(pos);
            buffer = dagChannel.loadNextData();
            assertNotNull(buffer);
            testData = new byte[buffer.limit() - buffer.position()];
            buffer.get(testData);
            assertEquals(text.substring(pos), new String(testData));

            pos = 5;
            dagChannel.seek(pos);
            buffer = dagChannel.loadNextData();
            assertNotNull(buffer);
            testData = new byte[buffer.limit() - buffer.position()];
            buffer.get(testData);
            assertEquals(text.substring(pos), new String(testData));
        }

    }

    @Test
    public void test_readerBig() throws Exception {

        int chunkData = Short.MAX_VALUE;
        try (FileStore blockStore = new FileStore()) {

            byte[] text = TestEnv.getRandomBytes((chunkData * 2) - 50);
            Fid fid = TestEnv.createContent(blockStore, "random.bin", text);

            assertTrue(blockStore.hasBlock(fid.cid()));

            DagChannel dagChannel = DagChannel.create(blockStore, fid);


            dagChannel.seek(0);
            ByteBuffer buffer = dagChannel.loadNextData();
            assertNotNull(buffer);
            assertEquals(chunkData, buffer.limit());
            buffer = dagChannel.loadNextData();
            assertNotNull(buffer);
            assertEquals(chunkData - 50, buffer.limit());

            int pos = chunkData + 50;
            dagChannel.seek(pos);
            buffer = dagChannel.loadNextData();
            assertNotNull(buffer);
            assertEquals(buffer.position(), 50);

            byte[] data = new byte[buffer.remaining()];
            buffer.get(data);

            int length = data.length;

            assertEquals(chunkData - 100, length);

            pos = chunkData - 50;
            dagChannel.seek(pos);
            buffer = dagChannel.loadNextData();
            assertNotNull(buffer);
            assertEquals(buffer.position(), chunkData - 50);

            data = new byte[buffer.remaining()];
            buffer.get(data);

            assertEquals(50, data.length);
        }
    }

    @Test
    public void storeDirectoryTest() throws Exception {
        try (FileStore blockStore = new FileStore()) {

            File directory = Files.createTempDirectory("").toFile();

            File child = new File(directory, "test.bin");
            boolean created = child.createNewFile();
            assertTrue(created);

            Dir dir = blockStore.storeDirectory(directory);
            assertNotNull(dir);


            List<Link> childs = dir.links();
            assertNotNull(childs);
            assertEquals(childs.size(), 1);
            assertEquals(childs.get(0).name(), "test.bin");


            FileStore.deleteDirectory(directory);
        }
    }

    @Test
    public void directoryTest() throws Throwable {

        try (FileStore blockStore = new FileStore()) {

            Fid fid = TestEnv.createContent(blockStore, "index.txt", "Moin Moin".getBytes());
            assertNotNull(fid);


            Dir a = blockStore.createEmptyDirectory("a");
            assertNotNull(a);
            Dir b = blockStore.createEmptyDirectory("b");
            assertNotNull(b);
            b = blockStore.addToDirectory(b, fid);
            assertNotNull(b);
            a = blockStore.addToDirectory(a, b);
            assertNotNull(a);

            assertEquals(a.size(), fid.size()); // test if same size

            // now change the "index.txt"
            fid = TestEnv.createContent(blockStore, "index.txt", "Moin Moin Moin".getBytes());
            assertNotNull(fid);

            b = blockStore.updateDirectory(b, fid);
            assertNotNull(b);
            a = blockStore.updateDirectory(a, b);
            assertNotNull(a);

            assertEquals(a.size(), fid.size()); // test if same size


            a = blockStore.removeFromDirectory(a, b);
            assertNotNull(a);

            assertEquals(a.size(), 0); // test if same size
        }
    }
}
