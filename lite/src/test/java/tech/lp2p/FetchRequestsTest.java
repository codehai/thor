package tech.lp2p;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertArrayEquals;

import org.junit.Test;

import java.io.InputStream;
import java.net.URI;

import tech.lp2p.core.BlockStore;
import tech.lp2p.core.Dir;
import tech.lp2p.core.Fid;


public class FetchRequestsTest {


    @Test
    public void fetchRequests() throws Throwable {

        try (Lite server = Lite.newLite()) {

            URI uri = server.loopbackAddress().toUri();

            BlockStore blockStore = server.blockStore();


            String content = "Moin Moin";
            byte[] bytes = TestEnv.getRandomBytes(5000);

            Fid fid = TestEnv.createContent(blockStore, "index.txt", content.getBytes());
            assertNotNull(fid);

            Fid bin = TestEnv.createContent(blockStore, "payload.bin", bytes);
            assertNotNull(bin);


            Dir a = blockStore.createEmptyDirectory("a");
            assertNotNull(a);
            Dir b = blockStore.createEmptyDirectory("b");
            assertNotNull(b);
            b = blockStore.addToDirectory(b, fid);
            assertNotNull(b);
            a = blockStore.addToDirectory(a, b);
            assertNotNull(a);
            a = blockStore.addToDirectory(a, bin);
            assertNotNull(a);


            blockStore.root(a.cid());

            try (Lite client = Lite.newClient()) {
                URI requestUri = Lite.createFetchUri(uri, fid);

                byte[] data;
                try (InputStream inputStream = client.stream(requestUri)) {
                    data = inputStream.readAllBytes();
                }

                assertNotNull(data);
                assertArrayEquals(data, content.getBytes());

                requestUri = Lite.createFetchUri(uri, bin);

                try (InputStream inputStream = client.stream(requestUri)) {
                    data = inputStream.readAllBytes();
                }
                assertNotNull(data);
                assertArrayEquals(data, bytes);
            }

        }
    }

}
