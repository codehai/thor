package tech.lp2p;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import java.net.URI;
import java.util.Optional;

import tech.lp2p.core.Node;
import tech.lp2p.core.Root;

public class FetchCidStressTest {


    @Test
    public void fetchCidIterations() throws Exception {
        try (Lite server = Lite.newLite()) {

            Node node = server.blockStore().createEmptyDirectory("Home");
            server.blockStore().root(node.cid());

            URI uri = server.loopbackAddress().toUri();

            try (Lite client = Lite.newClient()) {
                for (int i = 0; i < TestEnv.ITERATIONS; i++) {
                    Optional<Root> cid = client.fetchRoot(uri);
                    assertTrue(cid.isPresent());
                    long value = cid.get().cid();
                    assertEquals(value, node.cid());
                }
            }

        }

    }
}
