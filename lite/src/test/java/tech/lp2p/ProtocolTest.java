package tech.lp2p;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import java.util.Map;
import java.util.Objects;

import tech.lp2p.core.Handler;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Protocol;


public class ProtocolTest {

    @Test
    public void protocol_test() throws Exception {
        try (Lite server = Lite.newLite()) {

            int libp2pServerProtocols = 3;

            Map<Protocol, Handler> serverProtocols = server.protocols();
            assertNotNull(serverProtocols);

            assertTrue(serverProtocols.containsKey(Protocol.MULTISTREAM_PROTOCOL));
            assertTrue(serverProtocols.containsKey(Protocol.IDENTITY_PROTOCOL));
            assertTrue(serverProtocols.containsKey(Protocol.RELAY_PROTOCOL_STOP));
            assertEquals(serverProtocols.size(), libp2pServerProtocols);

            // for testing we are connecting to our own server
            Peeraddr peeraddr = server.loopbackAddress();
            Objects.requireNonNull(peeraddr);

        }
    }
}
