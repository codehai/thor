package tech.lp2p;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.fail;

import org.junit.Test;

import tech.lp2p.core.FileStore;
import tech.lp2p.core.Raw;


public class CatTest {


    @Test
    public void catNotExist() {

        long cid = Lite.rawCid("Hallo Welt");
        try (FileStore blockStore = new FileStore()) {
            blockStore.fetchData(cid);
            fail();
        } catch (Exception ignore) {
            //
        }

    }


    @Test
    public void catLocalTest() throws Exception {

        try (FileStore blockStore = new FileStore()) {
            Raw local = blockStore.storeText("Moin Moin Moin");
            assertNotNull(local);
            byte[] content = blockStore.fetchData(local.cid());
            assertNotNull(content);
        }
    }

}