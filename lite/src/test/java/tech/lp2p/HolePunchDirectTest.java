package tech.lp2p;


import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertArrayEquals;

import org.junit.Test;

import java.net.URI;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

import tech.lp2p.core.BlockStore;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Peeraddrs;
import tech.lp2p.core.Raw;
import tech.lp2p.relay.RelayService;


public class HolePunchDirectTest {


    @Test
    public void testHolePunchDirect() throws Throwable {

        Lite server = TestEnv.getTestInstance();
        assertNotNull(server);
        BlockStore blockStore = server.blockStore();
        byte[] original = TestEnv.getRandomBytes(100);
        Raw raw = blockStore.storeData(original);


        if (!server.hasHopReserves()) {
            TestEnv.error("nothing to test no dialable addresses");
            return;
        }

        Peeraddrs reservations = server.hopReserves();

        AtomicInteger success = new AtomicInteger(0);

        for (Peeraddr relay : reservations) {

            try (Lite client = Lite.newLite()) {
                URI uri = RelayService.hopConnect(client, relay, server.peerId()).toUri();
                Objects.requireNonNull(uri);

                byte[] data = client.fetchData(Lite.createFetchUri(uri, raw.cid()));
                assertArrayEquals(data, original);

                success.incrementAndGet();

            } catch (Throwable throwable) {
                TestEnv.error("Failed " + throwable.getMessage());
            }
        }
        assertTrue(server.hasHopReserves());
        TestEnv.error("Success " + success.get() + " Total " + reservations.size());

        assertTrue(success.get() > 0);
    }
}
