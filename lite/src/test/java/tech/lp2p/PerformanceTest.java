package tech.lp2p;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertTrue;

import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.concurrent.atomic.AtomicInteger;

import tech.lp2p.core.Fid;
import tech.lp2p.core.FileStore;
import tech.lp2p.core.Utils;

public class PerformanceTest {


    @Test
    public void smallContentReadWrite() throws Exception {

        int packetSize = 1000;
        long maxData = 100;

        try (FileStore blockStore = new FileStore()) {

            File inputFile = TestEnv.createCacheFile();
            try (OutputStream outputStream = new FileOutputStream(inputFile)) {
                for (int i = 0; i < maxData; i++) {
                    byte[] randomBytes = TestEnv.getRandomBytes(packetSize);
                    outputStream.write(randomBytes);
                }
            }
            AtomicInteger counter = new AtomicInteger(0);
            try (OutputStream outputStream = new ByteArrayOutputStream()) {
                try (InputStream inputStream = new FileInputStream(inputFile)) {
                    Utils.copy(inputStream, outputStream, 0L,
                            inputFile.length(), counter::set);
                    assertEquals(100, counter.get());
                }
            }
            long size = inputFile.length();


            TestEnv.error("Bytes : " + inputFile.length() / 1000 + "[kb]");
            long now = System.currentTimeMillis();
            Fid fid = blockStore.storeFile(inputFile);
            assertNotNull(fid);
            TestEnv.error("Add : " + fid +
                    " Time : " + ((System.currentTimeMillis() - now) / 1000) + "[s]");

            File file = TestEnv.createCacheFile();
            file.deleteOnExit();
            assertTrue(file.exists());
            assertTrue(file.delete());


            try (InputStream stream = blockStore.stream(fid)) {
                assertEquals(size, stream.readAllBytes().length);
            }

            File temp = TestEnv.createCacheFile();
            blockStore.fetchToFile(temp, fid);

            assertEquals(temp.length(), size);

            assertTrue(temp.delete());
            assertTrue(inputFile.delete());


            blockStore.delete(fid);
        }

    }


    @Test
    public void compareFileSizes() throws Exception {

        try (FileStore blockStore = new FileStore()) {

            Fid fid = TestEnv.createContent(blockStore, 100);
            assertNotNull(fid);
            File file = TestEnv.createCacheFile();
            blockStore.fetchToFile(file, fid);

            assertEquals(file.length(), fid.size());
            assertTrue(file.delete());

            blockStore.delete(fid);

        }
    }

    @Test
    public void performanceContentReadWrite() throws Exception {

        try (FileStore blockStore = new FileStore()) {
            int packetSize = 10000;
            long maxData = 25000;


            File inputFile = TestEnv.createCacheFile();

            try (OutputStream outputStream = new FileOutputStream(inputFile)) {
                for (int i = 0; i < maxData; i++) {
                    byte[] randomBytes = TestEnv.getRandomBytes(packetSize);
                    outputStream.write(randomBytes);
                }
            }

            long size = inputFile.length();
            assertEquals(packetSize * maxData, size);


            Fid fid = blockStore.storeFile(inputFile);
            assertNotNull(fid);


            File temp = TestEnv.createCacheFile();

            blockStore.fetchToFile(temp, fid);
            assertEquals(temp.length(), size);


            Fid fid2 = blockStore.storeFile(inputFile);
            assertNotNull(fid2);

            assertEquals(fid, fid2);


            File outputFile1 = TestEnv.createCacheFile();
            blockStore.fetchToFile(outputFile1, fid);


            File outputFile2 = TestEnv.createCacheFile();
            blockStore.fetchToFile(outputFile2, fid);


            assertEquals(outputFile1.length(), size);
            assertEquals(outputFile2.length(), size);
            assertTrue(outputFile2.delete());
            assertTrue(outputFile1.delete());
            assertTrue(inputFile.delete());

            blockStore.delete(fid);
        }
    }

    @Test
    public void contentWrite() throws Exception {

        try (FileStore blockStore = new FileStore()) {

            int packetSize = 1000;
            long maxData = 1000;


            File inputFile = TestEnv.createCacheFile();
            try (OutputStream outputStream = new FileOutputStream(inputFile)) {
                for (int i = 0; i < maxData; i++) {
                    byte[] randomBytes = TestEnv.getRandomBytes(packetSize);
                    outputStream.write(randomBytes);
                }
            }

            long size = inputFile.length();
            assertEquals(packetSize * maxData, size);

            Fid fid = blockStore.storeFile(inputFile);
            assertNotNull(fid);
        }

    }


    @Test
    public void storeInputStream() throws Exception {

        int packetSize = 1000;
        long maxData = 100;


        try (FileStore blockStore = new FileStore()) {

            File inputFile = TestEnv.createCacheFile();
            try (OutputStream outputStream = new FileOutputStream(inputFile)) {
                for (int i = 0; i < maxData; i++) {
                    byte[] randomBytes = TestEnv.getRandomBytes(packetSize);
                    outputStream.write(randomBytes);
                }
            }
            long size = inputFile.length();


            Fid fid = blockStore.storeFile(inputFile);
            assertNotNull(fid);

            assertEquals(fid.size(), size);

            File temp = TestEnv.createCacheFile();
            blockStore.fetchToFile(temp, fid);

            assertEquals(temp.length(), inputFile.length());

            assertTrue(temp.delete());

            assertTrue(inputFile.delete());
        }
    }

}
