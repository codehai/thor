package tech.lp2p;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNotNull;

import org.junit.Test;

import java.security.cert.X509Certificate;

import tech.lp2p.core.Certificate;
import tech.lp2p.core.PeerId;


public class SelfSignedTest {

    @Test
    public void certificateTest() throws Exception {

        assertNotNull(Certificate.getLiteExtension());

        try (Lite lite = Lite.newLite()) {
            X509Certificate cert = lite.certificate().x509Certificate();
            assertNotNull(cert);

            PeerId peerId = PeerId.extractPeerId(cert);
            assertNotNull(peerId);
            assertEquals(peerId, lite.peerId());
        }

    }

}
