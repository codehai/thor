package tech.lp2p;

import org.junit.Assert;
import org.junit.Test;

import java.net.URI;

import tech.lp2p.core.BlockStore;
import tech.lp2p.core.Raw;

public class FetchStressDataTest {


    @Test
    public void stressFetchData() throws Exception {

        int iterations = 100;
        try (Lite server = Lite.newLite()) {

            BlockStore blockStore = server.blockStore();

            try (Lite client = Lite.newClient()) {

                for (int i = 0; i < iterations; i++) {

                    byte[] data = TestEnv.getRandomBytes(Short.MAX_VALUE);
                    Raw raw = blockStore.storeData(data);

                    URI request = Lite.createFetchUri(server.loopbackAddress(), raw.cid());

                    byte[] cmp = client.fetchData(request);
                    Assert.assertArrayEquals(data, cmp);
                }

            }

        }
    }
}
