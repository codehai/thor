package tech.lp2p;


import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Assert;
import org.junit.Test;

import java.net.InetAddress;
import java.net.URI;
import java.util.Optional;

import tech.lp2p.core.Identify;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Peeraddrs;
import tech.lp2p.core.Root;
import tech.lp2p.core.Utils;
import tech.lp2p.ident.IdentifyService;
import tech.lp2p.quic.Connection;
import tech.lp2p.quic.ConnectionBuilder;


public class ConnectTest {


    @Test(expected = Exception.class) // can be Timeout or Connect Exception dependent of Network
    public void swarmPeer() throws Exception {

        PeerId peerId = Lite.decodePeerId("12D3KooWSzPeHsfxULJwFiLeq6Qsx6TruezAwjZ619qsLhqC7cUR");

        String host = "139.178.68.146";
        Peeraddr peeraddr = Lite.createPeeraddr(peerId, InetAddress.getByName(host), 4001);
        assertEquals(4001, peeraddr.port());

        assertEquals(peeraddr.peerId(), peerId);

        // peeraddr is just a fiction (will fail)
        Lite client = Lite.newLite();
        ConnectionBuilder.connect(client, peeraddr);
        fail(); // should not reached this point

    }

    @Test
    public void testServerIdle() throws Exception {

        if (!TestEnv.supportLongRunningTests()) {
            return;
        }


        try (Lite server = Lite.newLite()) {

            URI uri = server.loopbackAddress().toUri();

            try (Lite client = Lite.newLite()) {

                Optional<Root> optional = client.fetchRoot(uri); // Intern it sets keep alive to true
                assertFalse(optional.isPresent());

                Thread.sleep(10000); // timeout is 10 sec (should be reached)
                assertTrue(server.hasIncomingConnections(client.peerId())); // but connection is still valid (keep alive is true)
            }
        }
    }

    @Test
    public void testIdentify() throws Exception {

        Lite server = TestEnv.getTestInstance();
        assertNotNull(server);


        if (!server.hasHopReserves()) {
            TestEnv.error("nothing to test no dialable addresses");
            return;
        }

        for (Peeraddr peeraddr : server.hopReserves()) {
            TestEnv.error("Routing Peer " + peeraddr.toString());

            Connection connection;
            try {
                connection = ConnectionBuilder.connect(server, peeraddr);
            } catch (Throwable throwable) {
                TestEnv.error("Connection failed " +
                        throwable.getClass().getSimpleName());
                continue;
            }

            try {
                assertNotNull(connection);
                Identify info = IdentifyService.identify(connection);
                assertNotNull(info);
                TestEnv.always(info.toString());
            } catch (Throwable throwable) {
                Utils.error(throwable);
                fail();
            }
            assertNotNull(connection);
            connection.close();
        }

    }

    @Test
    public void testClientClose() throws Exception {
        try (Lite server = Lite.newLite()) {

            URI uri = server.loopbackAddress().toUri();

            try (Lite client = Lite.newLite()) {

                Optional<Root> optional = client.fetchRoot(uri); // Intern it sets keep alive to true
                assertFalse(optional.isPresent());

                Assert.assertTrue(server.hasIncomingConnections(client.peerId()));
                Assert.assertTrue(client.hasOutgoingConnections(server.peerId()));

                assertEquals(server.numOutgoingConnections(), 0);
                assertEquals(server.numIncomingConnections(), 1);

                assertEquals(client.numOutgoingConnections(), 1);
                assertEquals(client.numIncomingConnections(), 0);


                client.closeOutgoingConnections(server.peerId());
                Thread.sleep(100);

                assertEquals(server.numOutgoingConnections(), 0);
                assertEquals(server.numIncomingConnections(), 0);

                assertEquals(client.numOutgoingConnections(), 0);
                assertEquals(client.numIncomingConnections(), 0);

            }
        }
    }

    @Test
    public void testServerClose() throws Exception {
        try (Lite server = Lite.newLite()) {

            URI uri = server.loopbackAddress().toUri();

            try (Lite client = Lite.newLite()) {

                Optional<Root> optional = client.fetchRoot(uri); // Intern it sets keep alive to true
                assertFalse(optional.isPresent());

                Assert.assertTrue(server.hasIncomingConnections(client.peerId()));
                Assert.assertTrue(client.hasOutgoingConnections(server.peerId()));

                assertEquals(server.numOutgoingConnections(), 0);
                assertEquals(server.numIncomingConnections(), 1);

                assertEquals(client.numOutgoingConnections(), 1);
                assertEquals(client.numIncomingConnections(), 0);


                assertEquals(server.numHopReserves(), 0);
                Peeraddrs peeraddrs = server.incomingConnections();
                assertFalse(peeraddrs.isEmpty());
                assertEquals(peeraddrs.get(0).peerId(), client.peerId());

                server.closeIncomingConnections(client.peerId());
                Thread.sleep(100);

                assertEquals(server.numOutgoingConnections(), 0);
                assertEquals(server.numIncomingConnections(), 0);

                // still not working assertEquals(client.numOutgoingConnections(), 0);
                assertEquals(client.numIncomingConnections(), 0);

            }
        }
    }
}
