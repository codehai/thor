package tech.lp2p;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

import tech.lp2p.core.Utils;
import tech.lp2p.pns.PnsService;

public class HeaderTest {

    @Test
    public void testHeader() throws IOException {

        int maxProtocolSize = Utils.unsignedVariantSize(Short.MAX_VALUE);

        assertEquals(maxProtocolSize, Short.BYTES + 1);

        int maxMsgLength = Utils.unsignedVariantSize(Integer.MAX_VALUE);
        assertEquals(maxMsgLength, Integer.BYTES + 1);


        int protocolSize = Utils.unsignedVariantSize(0);
        assertEquals(protocolSize, 1);


        // test 1
        {
            ByteBuffer buffer = PnsService.header((short) 99, 712);
            short protocol = Utils.readShortUnsignedVariant(buffer);
            assertEquals(protocol, 99);
            int size = Utils.readUnsignedVariant(buffer);
            assertEquals(size, 712);

            try (InputStream inputStream = new ByteArrayInputStream(buffer.array())) {
                protocol = Utils.readShortUnsignedVariant(inputStream);
                assertEquals(protocol, 99);
                size = Utils.readUnsignedVariant(inputStream);
                assertEquals(size, 712);
            }
        }

        // test 2
        {
            ByteBuffer buffer = PnsService.header(Short.MAX_VALUE, Integer.MAX_VALUE);
            short protocol = Utils.readShortUnsignedVariant(buffer);
            assertEquals(protocol, Short.MAX_VALUE);
            int size = Utils.readUnsignedVariant(buffer);
            assertEquals(size, Integer.MAX_VALUE);

            try (InputStream inputStream = new ByteArrayInputStream(buffer.array())) {
                protocol = Utils.readShortUnsignedVariant(inputStream);
                assertEquals(protocol, Short.MAX_VALUE);
                size = Utils.readUnsignedVariant(inputStream);
                assertEquals(size, Integer.MAX_VALUE);
            }
        }

        // test 3
        {
            ByteBuffer buffer = PnsService.header((short) 0, 0);
            short protocol = Utils.readShortUnsignedVariant(buffer);
            assertEquals(protocol, 0);
            int size = Utils.readUnsignedVariant(buffer);
            assertEquals(size, 0);

            try (InputStream inputStream = new ByteArrayInputStream(buffer.array())) {
                protocol = Utils.readShortUnsignedVariant(inputStream);
                assertEquals(protocol, 0);
                size = Utils.readUnsignedVariant(inputStream);
                assertEquals(size, 0);
            }

        }
    }
}
