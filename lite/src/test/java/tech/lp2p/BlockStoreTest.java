package tech.lp2p;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Assert;
import org.junit.Test;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import tech.lp2p.core.FileStore;
import tech.lp2p.core.Raw;

public class BlockStoreTest {

    @Test
    public void testString() throws Exception {

        try (FileStore blockStore = new FileStore()) {
            String text = "Hello Moin und Zehn Elf";
            Raw raw = blockStore.storeText(text);
            assertNotNull(raw);

            byte[] result = blockStore.fetchData(raw.cid());
            assertNotNull(result);
            assertEquals(text, new String(result));
        }
    }

    @Test
    public void testParallel() throws Exception {

        try (FileStore blockStore = new FileStore()) {

            byte[] data = TestEnv.getRandomBytes(Short.MAX_VALUE);
            Raw raw = blockStore.storeData(data);

            assertNotNull(raw);

            ExecutorService executors = Executors.newFixedThreadPool(
                    Runtime.getRuntime().availableProcessors());

            AtomicInteger finished = new AtomicInteger();
            int instances = 100;

            for (int i = 0; i < instances; i++) {

                executors.execute(() -> {
                    try {
                        byte[] output = blockStore.fetchData(raw.cid());
                        assertArrayEquals(data, output);
                        finished.incrementAndGet();
                    } catch (Throwable throwable) {
                        TestEnv.error(throwable);
                        fail();
                    }
                });
            }
            executors.shutdown();

            assertTrue(executors.awaitTermination(Integer.MAX_VALUE, TimeUnit.SECONDS));
            Assert.assertEquals(finished.get(), instances);

        }
    }
}
