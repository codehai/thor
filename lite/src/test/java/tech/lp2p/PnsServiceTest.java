package tech.lp2p;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;


public class PnsServiceTest {

    @Test
    public void testReadoutStillData() throws IOException {
        byte[] data = new byte[]{1, 2, 3, 4};
        ByteBuffer buffer = ByteBuffer.allocate(5);


        Channels.newChannel(new ByteArrayInputStream(data)).read(buffer);
        buffer.flip();
        assertTrue(buffer.hasRemaining());
    }


    @Test
    public void testPositive() throws IOException {
        byte[] data = TestEnv.getRandomBytes(Short.MAX_VALUE);
        ByteBuffer buffer = ByteBuffer.allocate(Short.MAX_VALUE);

        Channels.newChannel(new ByteArrayInputStream(data)).read(buffer);
        buffer.flip();

        assertEquals(buffer.position(), 0);
        assertEquals(buffer.remaining(), Short.MAX_VALUE);
        assertArrayEquals(data, buffer.array());
    }
}
