package tech.lp2p;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import tech.lp2p.core.Fid;
import tech.lp2p.core.FileStore;

public class SkipTest {

    @Test
    public void skip() throws Exception {

        try (FileStore blockStore = new FileStore()) {

            byte[] data = TestEnv.getRandomBytes(10 * Short.MAX_VALUE);
            long skip = (long) (data.length * 0.9f);


            try (InputStream inputStream = new ByteArrayInputStream(data)) {
                Fid fid = blockStore.storeInputStream("test.bin", inputStream);


                try (InputStream is = blockStore.stream(fid)) {
                    long available = is.available();
                    assertEquals(available, data.length);
                    long skipped = is.skip(skip);
                    assertEquals(skipped, skip);
                    byte[] rest = new byte[(int) (data.length * 0.1)];
                    int read = is.read(rest);
                    assertNotNull(rest);
                    assertEquals(read, rest.length);
                    int val = is.read();
                    assertEquals(val, -1); // end of file
                }
            }
        }
    }
}
