package tech.lp2p;


import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.channels.WritableByteChannel;
import java.nio.file.Files;
import java.util.Random;
import java.util.concurrent.locks.ReentrantLock;

import tech.lp2p.core.BlockStore;
import tech.lp2p.core.Fid;
import tech.lp2p.core.FileStore;
import tech.lp2p.core.Keys;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Peeraddrs;
import tech.lp2p.core.Utils;


final class TestEnv {

    public static final int ITERATIONS = 4096;
    public static final Peeraddrs BOOTSTRAP = new Peeraddrs();
    private static final boolean DEBUG = false;

    private static final ReentrantLock reserve = new ReentrantLock();
    private static final Random random = new Random();
    private static volatile Lite INSTANCE = null;

    static {
        try {
            // "/ip4/104.131.131.82/udp/4001/quic/p2p/QmaCpDMGvV2BGHeYERUEnRQAwe3N8SzbUtfsmvsqQLuvuJ"
            BOOTSTRAP.add(Lite.createPeeraddr(
                    "QmaCpDMGvV2BGHeYERUEnRQAwe3N8SzbUtfsmvsqQLuvuJ",
                    "104.131.131.82", 4001));
        } catch (Throwable throwable) {
            TestEnv.error(throwable);
        }
    }


    public static Lite getInstance() throws Exception {
        if (INSTANCE == null) {
            synchronized (Lite.class) {
                if (INSTANCE == null) {
                    Keys keys = Lite.generateKeys();
                    INSTANCE = Lite.newBuilder().
                            bootstrap(BOOTSTRAP).
                            isGated(peerId -> false).
                            keys(keys).
                            port(Utils.nextFreePort()).
                            blockStore(new FileStore()).
                            eventConsumer(event -> TestEnv.always("Event : " + event.name())).
                            build();

                    Runtime.getRuntime().addShutdownHook(new Thread(INSTANCE::close));
                    Runtime.getRuntime().addShutdownHook(new Thread(TestEnv::cleanupTempDir));
                    if (Lite.reservationFeaturePossible()) {
                        INSTANCE.hopReserve(25, 120);

                        Peeraddrs peeraddrs = INSTANCE.hopReserves();
                        for (Peeraddr addr : peeraddrs) {
                            TestEnv.error("Reservation Address " + addr.toString());
                        }
                    }
                }
            }
        }
        return INSTANCE;
    }

    public static PeerId randomPeerId() {
        return new PeerId(getRandomBytes(32));
    }

    static File createCacheFile() throws IOException {
        return Files.createTempFile("temp", ".cid").toFile();
    }

    public static byte[] getRandomBytes(int number) {
        byte[] bytes = new byte[number];
        random.nextBytes(bytes);
        return bytes;
    }

    public static long randomLong() {
        return random.nextLong();
    }

    public static Fid createContent(BlockStore blockStore, String name, byte[] data) throws Exception {
        try (InputStream inputStream = new ByteArrayInputStream(data)) {
            return blockStore.storeInputStream(name, inputStream);
        }
    }


    public static Fid createContent(BlockStore blockStore, int iteration) throws Exception {
        File cacheFile = TestEnv.createCacheFile();


        try (FileOutputStream fos = new FileOutputStream(cacheFile)) {
            WritableByteChannel channel = fos.getChannel();
            for (int i = 0; i < iteration; i++) {
                channel.write(ByteBuffer.wrap(TestEnv.getRandomBytes(Short.MAX_VALUE)));
            }
            Fid fid = blockStore.storeFile(cacheFile);
            boolean check = cacheFile.delete();
            if (!check) {
                TestEnv.debug("file not deleted");
            }
            return fid;
        }
    }


    public static Lite getTestInstance() throws Exception {
        reserve.lock();
        try {
            return TestEnv.getInstance();
        } finally {
            reserve.unlock();
        }
    }

    public static void cleanupTempDir() {
        try {
            FileStore.deleteDirectory(FileStore.getTempDirectory());
        } catch (Throwable throwable) {
            TestEnv.error(throwable);
        }
    }

    static void error(String message) {
        if (DEBUG) {
            System.err.println(message);
        }
    }

    static void error(Throwable throwable) {
        throwable.printStackTrace(System.err);
    }

    static void always(String message) {
        System.err.println(message);
    }

    static void debug(String message) {
        if (DEBUG) {
            System.out.println(message);
        }
    }

    public static boolean supportLongRunningTests() {
        return DEBUG;
    }

}
