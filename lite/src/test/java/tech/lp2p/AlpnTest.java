package tech.lp2p;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import java.net.URI;
import java.util.Optional;

import tech.lp2p.core.BlockStore;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Peeraddrs;
import tech.lp2p.core.Raw;
import tech.lp2p.core.Root;


public class AlpnTest {


    @Test
    public void alpnTest() throws Exception {

        try (Lite server = Lite.newLite()) {
            BlockStore blockStore = server.blockStore();

            byte[] input = TestEnv.getRandomBytes(100); //

            Raw raw = blockStore.storeData(input);
            assertNotNull(raw);

            blockStore.root(raw.cid());

            byte[] cmp = blockStore.fetchData(raw.cid());
            assertArrayEquals(input, cmp);


            Peeraddrs peeraddrs = server.peeraddrs();
            for (Peeraddr peeraddr : peeraddrs) {
                try (Lite client = Lite.newClient()) {
                    Optional<Root> optional = client.fetchRoot(peeraddr.toUri());
                    assertTrue(optional.isPresent());
                    Root root = optional.get();
                    assertNotNull(root);
                    Peeraddr serverAddress = root.peeraddr();
                    assertEquals(serverAddress, peeraddr);

                    URI request = Lite.createFetchUri(peeraddr.toUri(), raw);

                    byte[] data = client.fetchData(request);

                    assertArrayEquals(input, data);

                }
            }
        }
    }
}
