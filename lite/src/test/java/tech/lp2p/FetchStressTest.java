package tech.lp2p;

import static junit.framework.TestCase.assertNotNull;

import junit.framework.TestCase;

import org.junit.Test;

import java.io.InputStream;
import java.net.URI;
import java.util.concurrent.atomic.AtomicInteger;

import tech.lp2p.core.BlockStore;
import tech.lp2p.core.Fid;

public class FetchStressTest {


    @Test
    public void stressFetchCalls() throws Exception {

        int iterations = 10;

        try (Lite server = Lite.newLite()) {

            BlockStore blockStore = server.blockStore();
            long now = System.currentTimeMillis();

            int dataSize = Short.MAX_VALUE;
            Fid fid = TestEnv.createContent(blockStore, "text.bin",
                    TestEnv.getRandomBytes(dataSize));
            assertNotNull(fid);

            blockStore.root(fid.cid());

            TestEnv.error("Store Input Stream : " + fid +
                    " Time : " + ((System.currentTimeMillis() - now) / 1000) + "[s]");

            URI uri = server.loopbackAddress().toUri();

            for (int i = 0; i < iterations; i++) {
                now = System.currentTimeMillis();
                try (Lite client = Lite.newLite()) {

                    int finalRead = 0;

                    client.ping(uri);

                    URI request = Lite.createFetchUri(uri, fid);
                    AtomicInteger read = new AtomicInteger(0);
                    try (InputStream inputStream = client.stream(request)) {
                        while (inputStream.read() >= 0) {
                            read.incrementAndGet();
                        }
                    }

                    TestCase.assertEquals(read.get(), dataSize);
                    finalRead += read.get();


                    TestEnv.error("Read Data : " + finalRead +
                            "[bytes] Time : " + (System.currentTimeMillis() - now) + "[ms]");
                }
            }
        }
    }
}
