package tech.lp2p;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import java.net.InetAddress;

import tech.lp2p.core.Certificate;
import tech.lp2p.core.Keys;
import tech.lp2p.core.Response;
import tech.lp2p.core.Status;
import tech.lp2p.core.Utils;
import tech.lp2p.pns.PnsServer;
import tech.lp2p.pns.PnsService;

public class PnsPunchTest {


    @Test
    public void testPunchServers() throws Exception {

        int port1 = Utils.nextFreePort();
        Keys serverKeys1 = Lite.generateKeys();
        Certificate certificateServer1 = Certificate.createCertificate(serverKeys1);

        PnsServer pnsServer1 = PnsService.createPnsServer(
                event -> {
                }, peerId -> false, request ->
                        Response.create(Status.NOT_IMPLEMENTED), certificateServer1, port1);
        pnsServer1.start();


        InetAddress inetAddress1 = pnsServer1.localAddress();


        int port2 = Utils.nextFreePort();
        Keys serverKeys2 = Lite.generateKeys();
        Certificate certificateServer2 = Certificate.createCertificate(serverKeys2);

        PnsServer pnsServer2 = PnsService.createPnsServer(event -> {
        }, peerId -> false, request ->
                Response.create(Status.NOT_IMPLEMENTED), certificateServer2, port2);
        pnsServer2.start();


        Thread.sleep(500); // let the server start

        int punches = pnsServer2.punching(inetAddress1, port1,
                System.currentTimeMillis() + 1000);

        assertTrue(punches > 5);
        pnsServer2.shutdown();
        pnsServer1.shutdown();

    }
}
