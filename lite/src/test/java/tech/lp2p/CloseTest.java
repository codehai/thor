package tech.lp2p;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import java.net.URI;
import java.util.Optional;

import tech.lp2p.core.BlockStore;
import tech.lp2p.core.Node;
import tech.lp2p.core.Root;


public class CloseTest {


    @Test
    public void closeConnect() throws Exception {

        try (Lite server = Lite.newLite()) {

            BlockStore blockStore = server.blockStore();

            Node node = blockStore.createEmptyDirectory("Home");
            blockStore.root(node.cid());

            URI uri = server.loopbackAddress().toUri();

            try (Lite client = Lite.newLite()) {
                Optional<Root> optional = client.fetchRoot(uri);
                assertTrue(optional.isPresent());
                long cid = optional.get().cid();
                assertEquals(cid, node.cid());
            }

            assertFalse(server.hasIncomingConnections(server.peerId()));
        }

    }
}
