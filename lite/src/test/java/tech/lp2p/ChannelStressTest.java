package tech.lp2p;

import static org.junit.Assert.assertArrayEquals;

import org.junit.Test;

import java.net.URI;
import java.util.Objects;

import tech.lp2p.core.BlockStore;
import tech.lp2p.core.Raw;

public class ChannelStressTest {


    public static final int TEST_DATA = Short.MAX_VALUE;
    public static final int ITERATIONS = 100;

    @Test
    public void channelTestRun() throws Exception {

        // create server instance with default values

        try (Lite server = Lite.newLite()) {

            BlockStore blockStore = server.blockStore();

            try (Lite client = Lite.newClient()) { // client instance default values

                for (int i = 0; i < ITERATIONS; i++) {
                    Raw raw = blockStore.storeData(TestEnv.getRandomBytes(TEST_DATA)); // store some text
                    URI request = Lite.createFetchUri(server.loopbackAddress(), raw);
                    long timestamp = System.currentTimeMillis();
                    byte[] data = client.fetchData(request); // fetch request
                    TestEnv.error("Fetch Data took " + (System.currentTimeMillis() - timestamp));
                    Objects.requireNonNull(data);
                    assertArrayEquals(raw.data(), data);
                }
            }
        } // this closes all connections and the running server
    }

}