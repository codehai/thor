package tech.lp2p;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URI;

import tech.lp2p.core.BlockStore;
import tech.lp2p.core.Fid;

public class FetchServerTest {

    @Test
    public void fetchDataTest() throws Exception {

        try (Lite server = Lite.newLite()) {
            BlockStore blockStore = server.blockStore();

            URI uri = server.loopbackAddress().toUri();

            long timestamp = System.currentTimeMillis();

            Fid fid = TestEnv.createContent(blockStore, 100); // 100 * Short.MAX_VALUE
            assertNotNull(fid);

            System.err.println("Time Create Content " + (System.currentTimeMillis() - timestamp)
                    + " [ms] Size " + (fid.size() / 1000000) + " MB");

            blockStore.root(fid.cid());

            timestamp = System.currentTimeMillis();


            try (Lite client = Lite.newLite()) {
                URI request = Lite.createFetchUri(uri, fid);

                File out = TestEnv.createCacheFile();
                try (FileOutputStream outputStream = new FileOutputStream(out)) {
                    try (InputStream inputStream = client.stream(request)) {
                        inputStream.transferTo(outputStream);
                    }
                }

                System.err.println("Time Client Read Content " +
                        (System.currentTimeMillis() - timestamp) + " [ms] ");

                assertEquals(out.length(), fid.size());
                assertTrue(out.delete());
            }
        }
    }
}
