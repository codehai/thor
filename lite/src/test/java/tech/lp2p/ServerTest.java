package tech.lp2p;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Test;

import java.io.InputStream;
import java.net.InetAddress;
import java.net.URI;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import tech.lp2p.core.BlockStore;
import tech.lp2p.core.Fid;
import tech.lp2p.core.Network;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Peeraddrs;
import tech.lp2p.core.Raw;


public class ServerTest {

    @Test
    public void fetchDataTest() throws Exception {

        try (Lite server = Lite.newLite()) {
            BlockStore blockStore = server.blockStore();

            URI uri = server.loopbackAddress().toUri();

            byte[] input = TestEnv.getRandomBytes(10000000); // 10 MB

            Fid fid = TestEnv.createContent(blockStore, "random.bin", input);
            assertNotNull(fid);

            blockStore.root(fid.cid());

            try (Lite client = Lite.newLite()) {
                URI request = Lite.createFetchUri(uri, fid);
                try (InputStream inputStream = client.stream(request)) {
                    assertArrayEquals(input, inputStream.readAllBytes());
                }
            }
        }
    }


    @Test
    public void reachableTest() throws Exception {

        try (Lite server = Lite.newLite()) {

            Peeraddrs peeraddrs = server.peeraddrs();
            assertNotNull(peeraddrs);
            boolean reachable = false;
            for (Peeraddr peeraddr : peeraddrs) {
                InetAddress inetAddress = InetAddress.getByAddress(peeraddr.address());

                TestEnv.error("Well known ipv4 translatable " +
                        Network.isWellKnownIPv4Translatable(inetAddress));

                boolean reach = inetAddress.isReachable(50);
                if (reach) {
                    TestEnv.error("Reachable " + inetAddress.getHostAddress());
                    reachable = true;
                } else {
                    TestEnv.error("Not reachable " + inetAddress.getHostAddress());
                    TestEnv.error("Site Local " + inetAddress.isSiteLocalAddress());
                    TestEnv.error("Link Local " + inetAddress.isLinkLocalAddress());
                    TestEnv.error("Any Local " + inetAddress.isAnyLocalAddress());
                }
            }
            assertTrue(reachable);
        }

    }


    @Test
    public void serverTest() throws Exception {

        try (Lite server = Lite.newLite()) {

            URI uri = server.loopbackAddress().toUri();
            assertNotNull(uri);
            BlockStore blockStore = server.blockStore();

            String text = "Hallo das ist ein Test";
            Raw raw = blockStore.storeText(text);
            assertNotNull(raw);

            PeerId host = server.peerId();
            assertNotNull(host);


            try (Lite client = Lite.newLite()) {
                String cmpText = client.fetchText(Lite.createFetchUri(uri, raw));
                assertEquals(text, cmpText);
            }
        }

    }


    @Test
    public void multipleClients() throws Exception {

        try (Lite server = Lite.newLite()) {
            URI uri = server.loopbackAddress().toUri();
            BlockStore blockStore = server.blockStore();

            byte[] input = TestEnv.getRandomBytes(Short.MAX_VALUE);

            Raw raw = blockStore.storeData(input);
            assertNotNull(raw);

            ExecutorService executors = Executors.newFixedThreadPool(
                    Runtime.getRuntime().availableProcessors());

            AtomicInteger finished = new AtomicInteger();
            int instances = 100;

            for (int i = 0; i < instances; i++) {

                executors.execute(() -> {
                    try (Lite client = Lite.newLite()) {

                        URI request = Lite.createFetchUri(uri, raw);

                        byte[] output = client.fetchData(request);
                        assertArrayEquals(input, output);
                        finished.incrementAndGet();
                    } catch (Throwable throwable) {
                        TestEnv.error(throwable);
                        fail();
                    }
                });
            }
            executors.shutdown();

            assertTrue(executors.awaitTermination(Integer.MAX_VALUE, TimeUnit.SECONDS));
            assertEquals(finished.get(), instances);

        }
    }


    @Test
    public void multiplePings() throws Exception {

        try (Lite server = Lite.newLite()) {
            Peeraddr peeraddr = server.loopbackAddress();

            ExecutorService executors = Executors.newFixedThreadPool(
                    Runtime.getRuntime().availableProcessors());

            AtomicInteger finished = new AtomicInteger();
            int instances = 100;


            for (int i = 0; i < instances; i++) {

                executors.execute(() -> {
                    try (Lite client = Lite.newLite()) {
                        client.ping(peeraddr.toUri());
                        finished.incrementAndGet();
                    } catch (Throwable throwable) {
                        TestEnv.error(throwable);
                        fail();
                    }
                });
            }
            executors.shutdown();

            assertTrue(executors.awaitTermination(Integer.MAX_VALUE, TimeUnit.SECONDS));
            assertEquals(finished.get(), instances);

        }
    }
}