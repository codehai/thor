package tech.lp2p;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import java.io.File;
import java.io.FileOutputStream;
import java.net.URI;
import java.nio.channels.FileChannel;

import tech.lp2p.core.BlockStore;
import tech.lp2p.core.Fid;

public class FetchTest {


    @Test
    public void testClientServerDownload() throws Exception {

        try (Lite server = Lite.newLite()) {

            BlockStore blockStore = server.blockStore();

            long start = System.currentTimeMillis();

            Fid fid = TestEnv.createContent(blockStore, 100);
            blockStore.root(fid.cid());


            long end = System.currentTimeMillis();
            TestEnv.always("Time for creating " + ((end - start)) +
                    " [ms]" + " " + fid.size() / 1000000 + " [MB]");


            assertNotNull(fid);
            start = System.currentTimeMillis();

            URI uri = server.loopbackAddress().toUri();

            File file = TestEnv.createCacheFile();

            try (Lite client = Lite.newClient()) {

                URI request = Lite.createFetchUri(uri, fid);

                try (FileOutputStream fileOutputStream = new FileOutputStream(file)) {
                    FileChannel fosChannel = fileOutputStream.getChannel();
                    client.channel(request).transferTo(fosChannel, integer -> {
                    });
                }
            }

            assertEquals(file.length(), fid.size());

            end = System.currentTimeMillis();
            TestEnv.always("Time for downloading " + ((end - start) / 1000) +
                    " [s]" + " " + file.length() / 1000000 + " [MB]");
            file.deleteOnExit();
        }
    }
}
