package tech.lp2p;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import java.io.File;
import java.nio.file.Files;

import tech.lp2p.core.FileStore;
import tech.lp2p.core.Raw;

public class FileStoreTest {


    @Test
    public void reopenFileStore() throws Exception {
        File directory = Files.createTempDirectory("").toFile();

        FileStore fileStore = new FileStore(directory);
        assertNotNull(fileStore);

        String textTest = "Hello World now";
        Raw raw = fileStore.storeText(textTest);
        assertNotNull(raw);

        byte[] dataTest = TestEnv.getRandomBytes(1000);

        Raw data = fileStore.storeData(dataTest);
        assertNotNull(data);

        FileStore fileStoreCmp = new FileStore(directory);
        assertNotNull(fileStoreCmp);

        String testCheck = fileStoreCmp.fetchText(raw.cid());
        assertEquals(testCheck, textTest);

        byte[] dataCheck = fileStoreCmp.fetchData(data.cid());
        assertArrayEquals(dataCheck, dataTest);

        fileStore.close();
        fileStoreCmp.close();

    }


    @Test
    public void lotsOfData() throws Exception {
        File directory = Files.createTempDirectory("").toFile();

        FileStore fileStore = new FileStore(directory);
        assertNotNull(fileStore);

        int instances = 10000;
        Raw[] raws = new Raw[instances];
        long timestamp = System.currentTimeMillis();
        for (int i = 0; i < instances; i++) {
            Raw data = fileStore.storeText("test" + i);
            assertNotNull(data);
            raws[i] = data;
        }
        TestEnv.debug("Time Writing lotsOfData " +
                (System.currentTimeMillis() - timestamp) + " [ms]");


        timestamp = System.currentTimeMillis();
        for (int i = 0; i < instances; i++) {
            String text = fileStore.fetchText(raws[i].cid());
            assertEquals(text, "test" + i);
        }
        TestEnv.debug("Time Reading lotsOfData " +
                (System.currentTimeMillis() - timestamp) + " [ms]");

        fileStore.close();
    }
}
