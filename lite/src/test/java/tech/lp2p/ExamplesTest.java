package tech.lp2p;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import java.net.URI;
import java.nio.ByteBuffer;

import tech.lp2p.core.BlockStore;
import tech.lp2p.core.Raw;
import tech.lp2p.core.Response;
import tech.lp2p.core.Result;
import tech.lp2p.core.Status;
import tech.lp2p.core.Utils;

public class ExamplesTest {


    @Test
    public void simpleRequestResponse() throws Exception {

        // create server and client instance
        try (Lite server = Lite.newLite();
             Lite client = Lite.newClient()) { // Note: the client has no server functionality

            assertTrue(client.isServerDisabled());
            assertFalse(server.isServerDisabled());

            BlockStore blockStore = server.blockStore();

            Raw raw = blockStore.storeText("Moin"); // store some text

            URI request = Lite.createFetchUri(server.loopbackAddress(), raw);

            String text = client.fetchText(request); // fetch request
            assertEquals(text, "Moin");

        } // this closes all connections and the running server
    }


    @Test
    public void enhanceServer() throws Exception {

        short protocol = 12;
        // create a new lite instance with default settings
        // start the server on random port
        try (Lite server = Lite.newLite()) {


            server.addRequestHandler(protocol, request -> {
                int proc = request.protocol();
                assertEquals(proc, protocol);

                // check peerId is not null
                assertNotNull(request.peerId());

                // Now prepare the result
                byte[] data = "Version 1.0.0".getBytes(); // result body


                return Response.create(Status.OK, ByteBuffer.wrap(data));
            });


            // create a client
            try (Lite client = Lite.newLite()) {

                // prepare the request
                URI request = Lite.createProtocolUri(server.loopbackAddress(), protocol);

                Result response = client.send(request, Utils.BYTES_EMPTY);

                // check return values
                byte[] content = response.content();
                assertEquals(new String(content), "Version 1.0.0");

                assertEquals(response.status(), Status.OK);
            }
        }
    }
}
