package tech.lp2p;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import java.net.URI;
import java.util.Optional;

import tech.lp2p.core.BlockStore;
import tech.lp2p.core.Node;
import tech.lp2p.core.PeerStore;
import tech.lp2p.core.Root;

public class HolePunchDialTest {

    @Test
    public void testHolePunch() throws Throwable {

        Lite server = TestEnv.getTestInstance();
        assertNotNull(server);

        PeerStore peerStore = server.peerStore();
        BlockStore blockStore = server.blockStore();

        Node node = blockStore.createEmptyDirectory("Home");
        server.blockStore().root(node.cid());

        if (!server.hasHopReserves()) {
            TestEnv.error("nothing to test no dialable addresses");
            return;
        }

        try (Lite client = Lite.newBuilder().peerStore(peerStore)
                .bootstrap(TestEnv.BOOTSTRAP).build()) {

            URI uri = server.peerId().toUri();
            assertNotNull(uri);

            Optional<Root> optional = client.fetchRoot(uri);
            assertTrue(optional.isPresent());
            long cid = optional.get().cid();
            assertEquals(cid, node.cid());

        }
    }
}