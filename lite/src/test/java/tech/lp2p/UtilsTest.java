package tech.lp2p;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import junit.framework.TestCase;

import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.math.BigInteger;
import java.net.InetAddress;
import java.net.URI;
import java.nio.ByteBuffer;
import java.util.Base64;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import tech.lp2p.core.Cid;
import tech.lp2p.core.FileStore;
import tech.lp2p.core.Key;
import tech.lp2p.core.Keys;
import tech.lp2p.core.MemoryPeers;
import tech.lp2p.core.Network;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Peeraddrs;
import tech.lp2p.core.Protocols;
import tech.lp2p.core.Raw;
import tech.lp2p.core.Scheme;


public class UtilsTest {


    @Test
    public void testBuilder() throws Exception {
        Keys keys = Lite.generateKeys();

        Lite lite = Lite.newBuilder().agent("a").
                keys(keys).
                bootstrap(new Peeraddrs()).
                peerStore(new MemoryPeers()).build();
        assertNotNull(lite);
        assertEquals("a", lite.agent());
        assertNotNull(lite.certificate());
        assertNotNull(lite.bootstrap());
        assertNotNull(lite.peerStore());

    }

    @Test
    public void stressPeeraddrUris() throws Exception {

        for (int i = 0; i < TestEnv.ITERATIONS; i++) {
            PeerId peerId = TestEnv.randomPeerId();

            Peeraddr peeraddr = Lite.createPeeraddr(peerId,
                    InetAddress.getLoopbackAddress(), 4001);
            Assert.assertNotNull(peeraddr);

            long cid = TestEnv.randomLong();

            URI test = Lite.createFetchUri(peeraddr, cid);
            Objects.requireNonNull(test);

            Optional<Peeraddr> cmp = Lite.extractPeeraddr(test);
            assertTrue(cmp.isPresent());
            Assert.assertEquals(cmp.get(), peeraddr);

            Optional<Long> optional = Cid.extractCid(test);
            assertTrue(optional.isPresent());
            assertEquals(optional.get().longValue(), cid);
        }
    }

    @Test
    public void stressToBase58() throws Exception {
        for (int i = 0; i < TestEnv.ITERATIONS; i++) {
            PeerId peerId = TestEnv.randomPeerId();
            String toBase58 = peerId.toBase58();
            assertNotNull(toBase58);
            PeerId cmp = Lite.decodePeerId(toBase58);
            assertEquals(peerId, cmp);
        }
    }


    @Test
    public void stressPeerIdUris() throws Exception {

        for (int i = 0; i < TestEnv.ITERATIONS; i++) {
            PeerId peerId = TestEnv.randomPeerId();

            long cid = TestEnv.randomLong();

            URI test = Lite.createFetchUri(peerId, cid);
            Objects.requireNonNull(test);

            Optional<PeerId> cmp = Lite.extractPeerId(test);
            assertTrue(cmp.isPresent());
            Assert.assertEquals(cmp.get(), peerId);

            Optional<Long> optional = Lite.extractCid(test);
            assertTrue(optional.isPresent());
            assertEquals(optional.get().longValue(), cid);
        }
    }


    @Test
    public void testAddress() throws Exception {
        PeerId peerId = TestEnv.randomPeerId();
        Peeraddr peeraddr = Lite.createPeeraddr(peerId.toBase58(),
                "2804:d41:432f:3f00:ccbd:8e0d:a023:376b", 4001);
        Assert.assertNotNull(peeraddr);


        Peeraddr cmp = Peeraddr.create(peerId, peeraddr.encoded());
        Assert.assertNotNull(cmp);
        TestCase.assertEquals(peeraddr.peerId(), cmp.peerId());
        TestCase.assertEquals(peeraddr, cmp);

        URI uri = peeraddr.toUri();
        Objects.requireNonNull(uri);

        Optional<Peeraddr> extract = Lite.extractPeeraddr(uri);
        assertTrue(extract.isPresent());
        assertEquals(extract.get(), peeraddr);


        URI request = Lite.createProtocolUri(peerId, (short) 99);
        assertNotNull(request);
        assertEquals(request.getPath(), Lite.SEPARATOR + 99);
        assertEquals(request.getScheme(), Scheme.pns.name());

    }


    @Test
    public void rawTest() {
        long rawCid = Lite.rawCid("moin elf");
        assertTrue(rawCid != 0);
    }

    @Test
    public void peerIdRandom() {
        PeerId peerId = TestEnv.randomPeerId();
        byte[] bytes = peerId.hash();
        TestCase.assertEquals(new PeerId(bytes), peerId);
    }


    @Test
    public void network() {
        int port = 4001;
        List<InetAddress> siteLocalAddresses = Network.siteLocalAddresses();
        Assert.assertNotNull(siteLocalAddresses);
        TestEnv.error(siteLocalAddresses.toString());

        Peeraddr peeraddr = Peeraddr.loopbackPeeraddr(TestEnv.randomPeerId(), port);
        Assert.assertNotNull(peeraddr);
        TestEnv.error(peeraddr.toString());
    }

    @Test
    public void distance() {

        PeerId peerId = TestEnv.randomPeerId();


        Key a = peerId.createKey();
        Key b = peerId.createKey();


        BigInteger dist = Key.distance(a, b);
        TestCase.assertEquals(dist.longValue(), 0L);


        PeerId randrom = TestEnv.randomPeerId();

        Key r1 = randrom.createKey();

        BigInteger distCmp = Key.distance(a, r1);
        assertNotEquals(distCmp.longValue(), 0L);

        long cid = Lite.rawCid("time");
        Assert.assertTrue(cid != 0);

    }


    @Test
    public void tinkEd25519() throws Exception {
        Keys keys = Lite.generateKeys();

        PeerId peerId = keys.peerId();

        byte[] msg = "moin moin".getBytes();
        byte[] signature = keys.sign(msg);

        Base64.Encoder encoder = Base64.getEncoder();
        String privateKeyAsString = encoder.encodeToString(keys.privateKey());
        Assert.assertNotNull(privateKeyAsString);
        String publicKeyAsString = encoder.encodeToString(keys.peerId().hash());
        Assert.assertNotNull(publicKeyAsString);

        Base64.Decoder decoder = Base64.getDecoder();

        byte[] privateKey = decoder.decode(privateKeyAsString);
        Assert.assertNotNull(privateKey);
        byte[] publicKey = decoder.decode(publicKeyAsString);

        PeerId peerIdCmp = new PeerId(publicKey);

        TestCase.assertEquals(peerId, peerIdCmp);

        peerIdCmp.verify(msg, signature);

    }

    @Test
    public void cidConversion() {

        long cid = Lite.rawCid("moin welt");
        Assert.assertNotNull(Cid.hash(cid));

        long cmp = Lite.rawCid("moin welt");
        Assert.assertNotNull(Cid.hash(cid));

        TestCase.assertEquals(cmp, cid);
        assertArrayEquals(Cid.hash(cmp), Cid.hash(cid));

    }


    @Test
    public void peerIdsDecoding() throws Exception {
        PeerId random = TestEnv.randomPeerId();

        // -- Peer ID (sha256) encoded as a raw base58btc multihash
        PeerId peerId = Lite.decodePeerId("QmYyQSo1c1Ym7orWxLYvCrM2EmxFTANf8wXmmE7DWjhx5N");
        assertNotNull(peerId);

        //  -- Peer ID (ed25519, using the "identity" multihash) encoded as a raw base58btc multihash
        peerId = Lite.decodePeerId("12D3KooWD3eckifWpRn9wQpMG9R9hX3sD158z7EqHWmweQAJU5SA");
        assertNotNull(peerId);

        peerId = Lite.decodePeerId(random.toBase58());
        assertNotNull(peerId);
        assertEquals(peerId, random);
    }


    @Test
    public void ipv6Test() throws Exception {
        assertNotNull(Lite.createPeeraddr("12D3KooWQ6SJ5A3uX5WjxCNbEbdAu8ufKJ3TmcjReTLSGaFk4HDU",
                "2804:d41:432f:3f00:ccbd:8e0d:a023:376b", 4001));
    }

    @Test
    public void listenAddresses() throws Exception {
        try (Lite server = Lite.newLite()) {

            Peeraddrs result = server.peeraddrs();
            assertNotNull(result);
            for (Peeraddr ma : result) {
                TestEnv.error("Listen Address : " + ma.toString());
            }

            assertNotNull(server.peerId());

            assertEquals(server.agent(), Lite.AGENT);

            Protocols protocols = server.protocols();
            for (String protocol : protocols.names()) {
                assertNotNull(protocol);
            }


        }
    }


    @Test
    public void fetchDataTest() throws Exception {

        try (FileStore blockStore = new FileStore()) {
            String test = "Moin";
            Raw cid = blockStore.storeText(test);
            assertNotNull(cid);
            byte[] bytes = blockStore.fetchData(cid.cid());
            assertNotNull(bytes);
            assertEquals(test, new String(bytes));

            try {
                long fault = TestEnv.randomLong();
                blockStore.fetchData(fault);
                fail();
            } catch (Exception ignore) {
                // ok
            }
        }
    }

    @Test
    public void textProgressTest() throws Exception {

        try (FileStore blockStore = new FileStore()) {

            String test = "Moin bla bla";
            Raw cid = blockStore.storeText(test);
            assertNotNull(cid);

            byte[] bytes = blockStore.fetchData(cid.cid());
            assertNotNull(bytes);
            assertEquals(test, new String(bytes));

            String text = blockStore.fetchText(cid.cid());
            assertNotNull(text);
            assertEquals(test, text);
        }

    }


    @Test
    public void rawCid() throws IOException {

        try (FileStore blockStore = new FileStore()) {
            Raw cid = blockStore.storeText("hallo");
            assertNotNull(cid);
        }
    }

    @Test
    public void peerStoreTest() {

        int port = 4001;
        PeerId random = TestEnv.randomPeerId();

        Peeraddr peeraddr = Peeraddr.loopbackPeeraddr(random, port);
        Assert.assertNotNull(peeraddr);
        byte[] data = peeraddr.encoded();
        Assert.assertNotNull(data);
        Peeraddr cmp = Peeraddr.create(random, data);
        Assert.assertNotNull(cmp);
        assertEquals(peeraddr, cmp);

    }

    @Test
    public void testHashCodeOnes() {
        byte[] dataOnes1 = new byte[500];
        byte[] dataComp = new byte[600];
        long one = Cid.createHash(ByteBuffer.wrap(dataOnes1));
        long two = Cid.createHash(ByteBuffer.wrap(dataComp));
        assertNotEquals(one, two);
    }
}
